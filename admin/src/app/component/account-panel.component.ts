import {Component, OnInit} from "@angular/core";
import {AccountService} from "../service/account.service";
import {LoginService} from "../service/login.service";
import {Router} from "@angular/router";
import {AdminAccountInfo} from "../model/admin-account-info.model";
@Component({
  selector: 'account-panel',
  templateUrl: '../view/account-panel.view.html'
})
export class AccountPanelComponent implements OnInit{

  private adminAccountInfo:AdminAccountInfo;

  constructor(private loginService:LoginService, private accountService:AccountService, private router:Router){}

  ngOnInit():void {
    this.accountService.getAdminAccountInfo().subscribe(
      (res:AdminAccountInfo)=>{
        this.adminAccountInfo=res;
        console.info(res);
      },
      (err)=>{
        if(err.status == 403){
          this.logout();
        }else{
          console.info(err);
        }
      }
    )
  }

  private navigateOfferApproval(){
    this.router.navigate(['home', { outlets: {'admin-router':['offerApproval'] }}]);
    return false;
  }

  private navigateUsers(){
    this.router.navigate(['home', { outlets: {'admin-router':['users'] }}]);
    return false;
  }

  private navigateAccountSettings(){
    this.router.navigate(['home', { outlets: {'admin-router':['accountSettings'] }}]);
    return false;
  }
  
  private navigateReports(){
    this.router.navigate(['home', { outlets: {'admin-router':['reports'] }}]);
    return false;
  }

  private navigatePriceList(){
    this.router.navigate(['home', { outlets: {'admin-router':['price-list'] }}]);
    return false;
  }

  private navigatePacketsList(){
    this.router.navigate(['home', { outlets: {'admin-router':['packets-list'] }}]);
    return false;
  }

  private logout(){
    this.loginService.logout().subscribe(
      (res)=>{
        console.info(res);
        LoginService.onLoggedOut();
        this.router.navigate(['']);
      },
      (err)=>{
        console.info(err);
        LoginService.onLoggedOut();
        this.router.navigate(['']);
      }
    );
    return false;
  }
}
