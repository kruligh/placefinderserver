import {Component, OnInit} from "@angular/core";
import {AdminService} from "../service/admin.service";
import {LoginService} from "../service/login.service";
import {PriceListItem} from "../model/price-list-item.model";


@Component({
  selector: 'price-list',
  templateUrl: '../view/price-list.view.html'
})
export class PriceListComponent implements OnInit{

  private countriesCode: string[];
  private country:string;
  private priceList:PriceListItem[];
  private error:string;
  private clickedItem:PriceListItem;
  private oldTempValue:number;

  constructor(private adminService:AdminService, private loginService:LoginService){
  }

  ngOnInit():void {
    this.adminService.getPriceListCountriesCode().subscribe(
      (res:string[])=>{
        this.countriesCode = res;
        console.info(res);
      },
      (err)=>{
        if(err.status == 403){
          this.loginService.on403();
        }else{
          this.error = "wystąpił błąd";
          console.info(err);
        }
      }
    )
  }

  getPriceList(countryCode:string){

      this.country = countryCode;


    this.adminService.getPriceList(countryCode).subscribe(
      (res:PriceListItem[])=>{
        this.priceList = res;
        console.info(res);
      },
      (err)=>{
        if(err.status == 403){
          this.loginService.on403();
        }else{
          this.error = "wystąpił błąd";
          console.info(err);
        }
      }
    );

    return false;
  }


  onAmountFocus(item:PriceListItem){
    this.clickedItem = item;
    this.oldTempValue = item.amount;
    return;
  }

  cancelEditPriceList(item:PriceListItem){
    this.clickedItem = null;
    item.amount = this.oldTempValue;
    return;
  }

  editPriceList(item:PriceListItem){
    this.adminService.editPriceList(item.id, item.amount).subscribe(
          (res)=>{
            this.clickedItem = null;
            this.oldTempValue = null;
            this.getPriceList(item.countryCode);
          },
          (err)=>{
            if(err.status == 403){
              this.loginService.on403();
            }else{
              this.error = "wystąpił błąd";
              console.info(err);
            }
          }
    );
  }
}
