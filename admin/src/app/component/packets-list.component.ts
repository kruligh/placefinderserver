import {Component, OnInit} from "@angular/core";
import {AdminService} from "../service/admin.service";
import {LoginService} from "../service/login.service";
import {PriceListItem} from "../model/price-list-item.model";
import {PacketItem} from "../model/packet-item.model";


@Component({
  selector: 'packets-list',
  templateUrl: '../view/packets-list.view.html'
})
export class PacketsListComponent implements OnInit {
  private packetsList:PacketItem[];
  private clickedItem:PacketItem;
  private error:string;

  private oldPointsCountValue:number;
  private oldPriceValue:number;
  private oldNameValue:string;


  constructor(private adminService:AdminService, private loginService:LoginService) {
  }

  ngOnInit():void {
    this.getPackets();
  }


  getPackets() {
    this.adminService.getPacketsList().subscribe(
      (res:PacketItem[])=> {
        this.packetsList = res;
        console.info(res);
      },
      (err)=> {
        if (err.status == 403) {
          this.loginService.on403();
        } else {
          this.error = "wystąpił błąd";
          console.info(err);
        }
      }
    );
  }

  onAmountFocus(item:PacketItem){
    this.clickedItem = item;
    this.oldNameValue = item.name;
    this.oldPointsCountValue = item.pointsCount;
    this.oldPriceValue = item.pricePln;
    return;
  }

  cancelEditPriceList(item:PacketItem){
    this.clickedItem = null;
    item.pointsCount = this.oldPointsCountValue;
    item.pricePln = this.oldPriceValue;
    item.name = this.oldNameValue;
    return;
  }

  editPacket(item:PacketItem){
    this.adminService.editPacket(item.id,item.name, item.pointsCount, item.pricePln).subscribe(
      (res)=>{
        this.clickedItem = null;
        this.oldPointsCountValue = null;
        this.oldPriceValue = null;
        this.oldNameValue = null;
        this.getPackets();
      },
      (err)=>{
        if(err.status == 403){
          this.loginService.on403();
        }else{
          this.error = "wystąpił błąd";
          console.info(err);
        }
      }
    );
  }
}
