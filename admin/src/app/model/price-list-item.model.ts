export class PriceListItem {
  private _id:number;
  private _countryCode:string;
  private _productType:string;
  private _amount:number;


    get id():number {
      return this._id;
    }

  set id(value:number){
      this._id=value;
      }


    get countryCode():string {
      return this._countryCode;
    }

  set countryCode(value:string){
      this._countryCode=value;
      }

  get productType():string {
      return this._productType;
    }

  set productType(value:string){
      this._productType=value;
      }

  get amount():number{
      return this._amount;
      }

  set amount(value:number){
      this._amount=value;
      }
}
