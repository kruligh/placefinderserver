
export class PacketItem{

  
  private _id:number;
  private _name:string;

  private _pointsCount:number;
  private _pricePln :number;


    get id() {
      return this._id;
    }

  set id(value){
      this._id=value;
      }

  get name() {
      return this._name;
    }

  set name(value){
      this._name=value;
      }


  get pointsCount():number{
      return this._pointsCount;
      }

  set pointsCount(value:number){
      this._pointsCount=value;
      }

  get pricePln():number{
      return this._pricePln;
      }

  set pricePln(value:number){
      this._pricePln=value;
      }
}
