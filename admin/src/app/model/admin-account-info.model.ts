export class AdminAccountInfo {
  private _name:string;
  private _email:string;
  private _usersCount:number;
  private _activeOffersCount:number;
  private _allOffersCount:number;

  get name():string {
    return this._name;
  }

  set name(value:string){
      this._name=value;
      }

  get email():string{
      return this._email;
      }

  set email(value:string){
      this._email=value;
      }


  get usersCount():number{
      return this._usersCount;
      }

  set usersCount(value:number){
      this._usersCount=value;
      }

  get activeOffersCount():number{
      return this._activeOffersCount;
      }

  set activeOffersCount(value:number){
      this._activeOffersCount=value;
      }

  get allOffersCount():number{
      return this._allOffersCount;
      }

  set allOffersCount(value:number){
      this._allOffersCount=value;
      }
}
