import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs/Rx";
import 'rxjs/add/operator/map';
import {ConfigService} from "./config.service";
import {LoginService} from "./login.service";
import {AdminAccountInfo} from "../model/admin-account-info.model";

@Injectable()
export class AccountService{
  constructor(private http:Http){}

  public getAdminAccountInfo():Observable<AdminAccountInfo> {
    let url = ConfigService.buildUrl('account/get');
    return this.http.get(url,LoginService.generateAuthGetRequestOptions())
      .map(ConfigService.extractData);
  }

 
}
