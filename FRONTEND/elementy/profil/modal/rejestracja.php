<div class="ui tiny united modal long" id="modal_zarejestrujSie" style="min-height: 550px;">
    <i class="close icon"></i>
    <div class="ui center aligned teal header">
        <?php echo $TLUMACZENIA['profil20'];?>
    </div>
    <form class="ui large form" id="rejestrecjaform" >
        <div class="ui basic segment">
            <span style="font-size: 85%;position: absolute;"><b><?php echo $TLUMACZENIA['profil21'];?></b></span>
            <div class="ui divider"></div>
            <div class="field">
                <div class="ui left icon input">
                    <i class="mail icon"></i>
                    <input type="text" name="email" placeholder="<?php echo $TLUMACZENIA['profil9'];?>" autocomplete="off">
                </div>
            </div>
            <div class="field">
                <div class="ui left icon input">
                    <i class="lock icon"></i>
                    <input type="password" name="password" placeholder="<?php echo $TLUMACZENIA['profil10'];?>" autocomplete="new-password">
                </div>
            </div>
            <div class="field" style="margin-bottom: 20px;">
                <div class="ui left icon input">
                    <i class="lock icon"></i>
                    <input type="password" name="password2"  placeholder="<?php echo $TLUMACZENIA['profil22'];?>" autocomplete="new-password">
                </div>
            </div>

            <span style="font-size: 85%;"><b><?php echo $TLUMACZENIA['profil23'];?></b></span>
            <div class="ui divider" style="margin-top: 1px;"></div>
            <div class="field">
                <div class="ui left icon input">
                    <i class="user icon"></i>
                    <input type="text" name="name" placeholder="<?php echo $TLUMACZENIA['profil24'];?>" autocomplete="off">
                </div>
            </div>
            <div class="field">
                <div class="ui left icon input">
                    <i class="call icon"></i>
                    <input type="text" name="phone" placeholder="<?php echo $TLUMACZENIA['profil25'];?>" autocomplete="off">
                </div>
            </div>

            <div class="two fields">
                <div class="field">
                    <div class="ui radio checkbox">
                        <input type="radio" name="isAgency" checked="checked" value="false">
                        <label><?php echo $TLUMACZENIA['profil26'];?></label>
                    </div>
                </div>
                <div class="field">
                    <div class="ui radio checkbox">
                        <input type="radio" name="isAgency" value="true">
                        <label><?php echo $TLUMACZENIA['profil27'];?></label>
                    </div>
                </div>
            </div>
            <div class="ui divider"></div>
            <div class="field" >
                <div class="ui checkbox">
                    <input type="checkbox" name="terms" class="hidden">
                    <label><?php echo $TLUMACZENIA['profil28'];?><a href="regulamin.php"><?php echo $TLUMACZENIA['profil29'];?></a> <?php echo $TLUMACZENIA['profil30'];?></label>
                </div>
            </div>

            <div class="ui fluid large teal submit button" ><?php echo $TLUMACZENIA['profil31'];?></div>
        </div>

        <div style="width: 90%;margin: auto auto 15px auto;" class="ui error message"></div>

        <div style="width: 90%;margin: auto auto 15px auto;" class="ui negative message" id="jestjuzmailtaki">
            <div class="header">
                <?php echo $TLUMACZENIA['profil32'];?>
            </div>
            <ul class="list">
                <li><?php echo $TLUMACZENIA['profil33'];?> <a style="cursor: pointer;" id="idzdologowania2"><?php echo $TLUMACZENIA['profil34'];?></a> </li>
            </ul>
        </div>

    </form>

    <div class="ui icon message" style="bottom: 0;">
        <i class="users icon" style="font-size: 150%;"></i>
        <div class="content">
            <div class="header" >
                <?php echo $TLUMACZENIA['profil35'];?> <a style="cursor: pointer;" id="idzdologowania"><?php echo $TLUMACZENIA['profil34'];?></a>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {


        $.fn.serializeObject = function()
        {
            var o = {};
            var a = this.serializeArray();
            $.each(a, function() {
                if (o[this.name] !== undefined) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });

            return o;
        };

        $('#jestjuzmailtaki').hide();
//        $('#jestjuzmailtaki').show();
        $('form#rejestrecjaform').form({
            fields: {
                email: {
                    identifier: 'email',
                    rules: [
                        {
                            type   : 'email',
                            prompt : TLUMACZENIA['profil18']
                        }
                    ]
                },
                name: {
                    identifier: 'name',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : TLUMACZENIA['profil36']
                        }
                    ]
                },
                phone: {
                    identifier: 'phone',
                    rules: [
                        {
                            type   : 'regExp[/^[+_0-9 ]{9,13}$/]',
                            prompt : TLUMACZENIA['profil37']
                        }
                    ]
                },
                password: {
                    identifier: 'password',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : TLUMACZENIA['profil19']
                        },
                        {
                            type   : 'minLength[6]',
                            prompt : TLUMACZENIA['profil39']
                        }
                    ]
                },
                password2: {
                    identifier: 'password2',
                    rules: [
                        {
                            type   : 'match[password]',
                            prompt : TLUMACZENIA['profil38']
                        }
                    ]
                },
                terms: {
                    identifier: 'terms',
                    rules: [
                        {
                            type   : 'checked',
                            prompt : TLUMACZENIA['profil40']
                        }
                    ]
                }
            },
            onFailure : function(formErrors, fields)
            {
                $('#modal_zarejestrujSie').modal("refresh")
                return false;
            },
            onSuccess : function(event, fields)
            {
                $('#LADOWANIE_CZEKENIE').addClass('active');
                fields['password'] = CryptoJS.SHA3(fields['password']).toString();
                fields['password2'] = CryptoJS.SHA3(fields['password2']).toString();


                $.ajax({
                    url: SERWER+'register',
                    type: "POST",
                    contentType: "application/json",
                    data : JSON.stringify(fields) ,
                    success : function(data){
                        location.reload();
                    },
                    error: function(data)
                    {
                        $('#LADOWANIE_CZEKENIE').removeClass('active');
                        if(data.responseText == "DUPLICATE_EMAIL")
                            $('#jestjuzmailtaki').show();

                    }
                });

                return false;
            }
        }) ;


    });
</script>
