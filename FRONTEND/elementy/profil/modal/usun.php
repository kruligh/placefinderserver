<div class="ui basic modal" id="UsunModal">
    <div class="ui icon header">
        <i class="trash icon"></i>
        <?php echo $TLUMACZENIA['profil41'];?>
    </div>
    <div class="content" style="text-align: center;">
        <p><?php echo $TLUMACZENIA['profil42'];?></p>
        <span style="display: none" id="idtegodousuniecia"></span>
    </div>
    <div class="actions" style="text-align: center;">
        <div class="ui red basic cancel inverted button">
            <i class="remove icon"></i>
            <?php echo $TLUMACZENIA['profil43'];?>
        </div>
        <div class="ui green ok inverted button" id="potwierdzUsuniecie">
            <i class="checkmark icon"></i>
            <?php echo $TLUMACZENIA['profil44'];?>
        </div>
    </div>
</div>