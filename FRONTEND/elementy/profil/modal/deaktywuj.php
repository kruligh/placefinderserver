<div class="ui basic modal" id="DeaktywujModal">
    <div class="ui icon header">
        <i class="hourglass half icon"></i>
        <?php echo $TLUMACZENIA['profil45'];?>
    </div>
    <div class="content" style="text-align: center;">
        <p><?php echo $TLUMACZENIA['profil46'];?> </p>
        <span style="display: none" id="idtegododeaktywacji"></span>
    </div>
    <div class="actions" style="text-align: center;">
        <div class="ui red basic cancel inverted button">
            <i class="remove icon"></i>
            <?php echo $TLUMACZENIA['profil43'];?>
        </div>
        <div class="ui green ok inverted button" id="potwierdzDeaktywacje">
            <i class="checkmark icon"></i>
            <?php echo $TLUMACZENIA['profil44'];?>
        </div>
    </div>
</div>