<div class="ui tiny united modal" id="modal_zapomnialemHasla" style="min-height: 230px;" >
    <div class="ui center aligned teal header">
        <?php echo $TLUMACZENIA['profil49'];?>
    </div>
    <form class="ui large form" id="zapomnialemform" >
        <div style="margin: 15px 15px 15px 15px;">
            <div class="field">
                <div class="ui left icon input">
                    <i class="user icon"></i>
                    <input type="text" name="email" placeholder="<?php echo $TLUMACZENIA['profil9'];?> ">
                </div>
            </div>

            <div class="ui fluid large teal submit button" ><?php echo $TLUMACZENIA['profil50'];?> </div>
        </div>

        <div style="width: 90%;margin: auto auto 75px auto;" class="ui error message"></div>

        <div style="width: 90%;margin: auto;margin-bottom: 70px;" class="ui error message" id="niematakiegomeila">
            <div class="header">
                <?php echo $TLUMACZENIA['profil51'];?>
            </div>
        </div>

        <div style="width: 90%;margin: auto;margin-bottom: 70px;" class="ui message" id="wejdzsenameila">
            <div class="header">
                <?php echo $TLUMACZENIA['profil52'];?>
            </div>
        </div>

    </form>

    <div class="ui icon message" style=" position: absolute;bottom: 0;">
        <i class="users icon" style="font-size: 150%;"></i>
        <div class="content">
            <div class="header" >
                <?php echo $TLUMACZENIA['profil35'];?> <a style="cursor: pointer;" id="idzdologowania3"><?php echo $TLUMACZENIA['profil34'];?></a>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {


        $('#niematakiegomeila').hide();
        $('#wejdzsenameila').hide();

        $('form#zapomnialemform').form({
            fields: {
                email: {
                    identifier: 'email',
                    rules: [
                        {
                            type   : 'email',
                            prompt : TLUMACZENIA['profil18']
                        }
                    ]
                }
            },
            onFailure : function(formErrors, fields)
            {
                return false;
            },
            onSuccess : function(event, fields)
            {

                $.ajax({
                    url: SERWER+'forgottenPassword',
                    type: "POST",
                    contentType: "application/json",
                    data : JSON.stringify($('form#zapomnialemform').serializeObject()) ,
                    success : function(data){
                        $('#wejdzsenameila').show();
                        //location.reload();
                    },
                    error: function(data)
                    {
                        if(data.responseText == "USER_DOESNT_EXISTS")
                            $('#niematakiegomeila').show();

                    }
                });

                return false;
            }
        }) ;

    });
</script>
