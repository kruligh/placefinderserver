<div class="ui tiny united modal" id="modal_zalogujSie" style="min-height: 280px;">
    <i class="close icon"></i>
    <div class="ui center aligned teal header">
        <?php echo $TLUMACZENIA['profil8'];?>
    </div>
    <form class="ui large form" >
        <div class="ui basic segment">
            <div class="field">
                <div class="ui left icon input focus">
                    <i class="user icon"></i>
                    <input type="text" name="email" placeholder="<?php echo $TLUMACZENIA['profil9'];?>" id="MOJLOGIN" autocomplete="off" >
                </div>
            </div>
            <div class="field">
                <div class="ui left icon input">
                    <i class="lock icon"></i>
                    <input type="password" name="password" placeholder="<?php echo $TLUMACZENIA['profil10'];?>" id="MOJHASLO" autocomplete="new-password">
                </div>
            </div>
            <div class="ui fluid large teal submit button"><?php echo $TLUMACZENIA['profil11'];?></div>
        </div>
        <div style="width: 90%;margin: auto auto 70px auto;" class="ui error message"></div>
    </form>
    <div style="width: 90%;margin: auto auto 60px auto;position: relative;" class="ui error message" id="zlyloginalbohaslo">
        <div class="header">
            <?php echo $TLUMACZENIA['profil12'];?>
        </div>
        <ul class="list">
            <li><?php echo $TLUMACZENIA['profil13'];?></li>
        </ul>
    </div>
    <div class="ui icon message" style=" position: absolute;bottom: 0;">
        <i class="users icon" style="font-size: 150%;"></i>
        <div class="content">
            <div class="header" >
                <?php echo $TLUMACZENIA['profil14'];?> <a style="cursor: pointer;" id="idzdorejestruj"><?php echo $TLUMACZENIA['profil15'];?></a> <?php echo $TLUMACZENIA['profil16'];?> <a style="cursor: pointer;" id="idzdozapomanialemhasla"><?php echo $TLUMACZENIA['profil17'];?></a>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        $('#zlyloginalbohaslo').hide();
        $('.ui.form').form({
                fields: {
                    email: {
                        identifier: 'email',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : TLUMACZENIA['profil18']
                            }
                        ]
                    },
                    password: {
                        identifier: 'password',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : TLUMACZENIA['profil19']
                            }
                        ]
                    }
                },
                onSuccess : function(event, fields)
                {

                    $('#LADOWANIE_CZEKENIE').addClass('active');
                    $.ajax({
                        url: SERWER+"person/session/login",
                        type: "POST",
                        contentType: "application/json",
                        data: JSON.stringify({login: $('#MOJLOGIN').val(),
                            password: CryptoJS.SHA3($('#MOJHASLO').val()).toString()}),
                        success : function(data){
                            localStorage.setItem("X-Auth-Token",data);
                            location.reload();
                        },
                        error: function(data)
                        {
                            $('#LADOWANIE_CZEKENIE').removeClass('active');
                            //console.log("error")
                            if(data.status == 403)
                            $('#zlyloginalbohaslo').show();
                        }
                    });

                    return false;
                }
            }) ;


    });
</script>
