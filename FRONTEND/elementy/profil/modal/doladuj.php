<div class="ui modal" id="modal_dodaluj"  >
    <i class="icon close"></i>
    <div class="ui center aligned grey header">
        <?php echo $TLUMACZENIA['profil47'];?>
    </div>


    <div id="checkboxiki" class="ui form" style="width: 80%;padding-bottom: 80px;margin: auto;margin-top: 10px;">
        <div class="ui divided selection list">
            <a class="ui radio checkbox item" style="padding-top: 20px;padding-bottom: 20px;">
                <input type="radio" name="plan" checked="checked" >
                <div style="font-size: 150%;">
                    <div class="right floated content kosztOpcji"></div>
                    <div class="ui green horizontal label ilepktOpcji" style="margin-left: 30px;margin-top: -10px;font-size: 80%;"></div>
                    <span class="nameOpcji"></span>
                </div>
            </a>
            <a class="ui radio checkbox item" style="padding-top: 20px;padding-bottom: 20px;">
                <input type="radio" name="plan" >
                <div style="font-size: 150%;">
                    <div class="right floated content kosztOpcji" ></div>
                    <div class="ui green horizontal label ilepktOpcji" style="margin-left: 30px;margin-top: -10px;font-size: 80%;"></div>
                    <span class="nameOpcji"></span>
                </div>
            </a>
            <a class="ui radio checkbox item" style="padding-top: 20px;padding-bottom: 20px;">
                <input type="radio" name="plan" >
                <div style="font-size: 150%;">
                    <div class="right floated content kosztOpcji" ></div>
                    <div class="ui green horizontal label ilepktOpcji" style="margin-left: 30px;margin-top: -10px;font-size: 80%;"></div>
                    <span class="nameOpcji"></span>
                </div>
            </a>
        </div>
    </div>

        <div style="position: absolute;right: 10%;margin-top: -70px;">
             <button class="ui right positive floated button" id="zamow"><?php echo $TLUMACZENIA['profil48'];?></button>
        </div>

</div>


<script>
    $(document).ready(function() {
        $('.checkbox').checkbox();

            $.ajax({
                url: SERWER+"person/points/packets",
                headers : {
                    'X-Auth-Token' : localStorage.getItem('X-Auth-Token')
                },
                success : function(data){
                    for(var i=0;i<data.length;i++)
                    {
                        $('#checkboxiki .list .ilepktOpcji').eq(i).html(data[i].pointsCount)
                        $('#checkboxiki .list .kosztOpcji').eq(i).html( (data[i].pricePln )/100 + ' zł' );
                        $('#checkboxiki .list .nameOpcji').eq(i).html( data[i].name );
                    }
                },
                error: function(data)
                {
                }
            });


        $('#zamow').click(function () {

            var radioButtons = $("#checkboxiki input:radio[name='plan']");
            var selectedIndex = radioButtons.index(radioButtons.filter(':checked'));
            $.ajax({
                url: SERWER+"person/points/payu/buy",
                headers : {
                    'X-Auth-Token' : localStorage.getItem('X-Auth-Token')
                },
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify({packetId: selectedIndex+1}),
                success : function(data){
                    location.href = data.url;
                },
                error: function(data)
                {
                }
            });


        });

    });
</script>