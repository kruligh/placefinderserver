<div class="ui message" id="brakmieszkan" style="margin-top: 20px;">
    <div class="header">
        <?php echo $TLUMACZENIA['profil6'];?>
    </div>
</div>

<div class="ui warning message" id="emailwiadomosc" style="margin-top: 20px;">
    <div class="header">
        <?php echo $TLUMACZENIA['profil57'];?>
    </div>
    <?php echo $TLUMACZENIA['profil58'];?>
</div>

<div style="margin: 20px 0 20px 0;padding-bottom: 5px;border-bottom: 1px dashed #ddd8de;font-size: 110%;" >
    <b><?php echo $TLUMACZENIA['profil5'];?></b>
    <div class="ui text menu pierwszutkienie" style="margin-top: -5px;margin-bottom: -10px;">
        <div class="header item" id="ILOSCzaznazcznocyh_my">0</div>
        <div class="header item"><?php echo $TLUMACZENIA['lista6'];?> </div>
        <a class="item">
            <i class="icon long arrow up strzalkagora"></i><i class="icon long arrow down strzalkadol"></i><?php echo $TLUMACZENIA['lista7'];?>
        </a>
        <a class="item">
            <i class="icon long arrow up strzalkagora"></i><i class="icon long arrow down strzalkadol"></i><?php echo $TLUMACZENIA['lista8'];?>
        </a>
        <a class="item">
            <i class="icon long arrow up strzalkagora"></i><i class="icon long arrow down strzalkadol"></i>Aktywności
        </a>
    </div>
</div>


<div style="height: 80%;padding-bottom: 50px;display:block;overflow:auto;position: relative;" id="dodaneogloszenia2" >
<!--    <div style="outline: none;padding-right: 15px;"  class="ui items ogloszeniaitd" id="dodaneogloszenia"  ></div>-->

    <div id="promotedHeader_my" style="display: inline-block;width: 95%;margin-bottom: -10px;margin-top: 10px;" class="ui dividing header">Promowane oferty:</div>
    <div style="padding-right: 10px;"  class="ui divided items" id="mojezaznaczone_promowane_my" ></div>

    <div id="normalHeader_my" style="display: inline-block;width: 95%;position: relative;top: -40px;" class="ui dividing header">Lista ofert:</div>
    <div style="outline: none;padding-right: 10px;position: relative;top: -55px;"  class="ui divided items" id="mojezaznaczone_my" ></div>


<!--    <div id="stronicowanie_my" style="text-align: center;margin-right: 10%;margin-top: 30px;position: absolute;bottom:50px;">-->
<!--        <div id="stronicowanie1_my" class="ui large green label" style="position:relative;top: 2px;cursor: pointer;">-->
<!--            Poprzednie-->
<!--        </div>-->
<!--        <div class="ui mini input" style="width: 44px;">-->
<!--            <input type="text" value="1" maxlength="3" id="ktoraStrona_my" onkeypress='validate(event)'>-->
<!--        </div>-->
<!--        <label style="position:relative;top: 2px;left: 3px;margin-right: 5px;">z <span id="zilustronicowanie_my">100</span></label>-->
<!---->
<!--        <div id="stronicowanie2_my" class="ui large green label" style="position:relative;top: 2px;cursor: pointer;">-->
<!--            Następna-->
<!--        </div>-->
<!--    </div>-->

</div>


<div id="LADOWANIE_MOICH_DODANYCH" >
    <img src="img/loading4.gif" style="width: 30%;" />
</div>


<script>
    var linkacz_my='',linkacz2_my = '';
    var stronaa_my = 1,sortowaniko_my = '';
    var limit_my = '&limit=10';

    $(document).on('click','#stronicowanie1_my.green',function(){
        stronaa_my--;
        $('#ktoraStrona_my').val(stronaa_my);
        if( $('#ktoraStrona_my').val() == 1 )
            $('#stronicowanie1_my').removeClass('green');

        if(!$('#stronicowanie2_my').hasClass('green'))
            $('#stronicowanie2_my').addClass('green');

        linkacz_my = linkacz2_my+limit_my+"&page="+(stronaa_my-1)+sortowaniko_my;
        console.log(linkacz_my);
        dodajMarkerki_my(linkacz_my,3)
    })

    $(document).on('click','#stronicowanie2_my.green',function(){
        stronaa_my++;
        $('#ktoraStrona_my').val(stronaa_my);
        if( $('#ktoraStrona_my').val() == parseInt($('#zilustronicowanie_my').text()) )
            $('#stronicowanie2_my').removeClass('green');

        if(!$('#stronicowanie1_my').hasClass('green'))
            $('#stronicowanie1_my').addClass('green');

        linkacz_my = linkacz2_my+limit_my+"&page="+(stronaa_my-1)+sortowaniko_my;
        console.log(linkacz_my);
        dodajMarkerki_my(linkacz_my,3);
    })

    function sprawdzwarunki()
    {
        if(parseInt($('#zilustronicowanie_my').text()) != 1)
        {
            if($('#ktoraStrona_my').val() > parseInt($('#zilustronicowanie_my').text()))
                $('#ktoraStrona_my').val($('#zilustronicowanie_my').text())
            else if($('#ktoraStrona').val() == 0)
                $('#ktoraStrona').val(1);

            stronaa_my= $('#ktoraStrona_my').val();
            if( $('#ktoraStrona_my').val() == $('#zilustronicowanie_my').text() )
            {
                $('#stronicowanie2_my').removeClass('green');

                if(!$('#stronicowanie1_my').hasClass('green'))
                    $('#stronicowanie1_my').addClass('green');
            }
            else if( $('#ktoraStrona_my').val() == 1 )
            {
                $('#stronicowanie1_my').removeClass('green');

                if(!$('#stronicowanie2_my').hasClass('green'))
                    $('#stronicowanie2_my').addClass('green');
            }
        }
        else
            $('#ktoraStrona_my').val(1);

    }


    $(document).on('focusout','#ktoraStrona_my',function(){
        sprawdzwarunki();

        linkacz_my = linkacz2_my+limit_my+"&page="+(stronaa_my-1)+sortowaniko_my;
        dodajMarkerki_my(linkacz_my,3)

    })


    $(document).on('keypress','#ktoraStrona_my',function(e){
        if(e.which == 13) {
            $('#ktoraStrona_my').blur();
        }
    })
</script>