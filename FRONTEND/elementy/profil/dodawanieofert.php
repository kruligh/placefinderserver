
<div class="ui united modal" id="formularz_podstawowe">
    <i class="close icon"></i>
    <div class="header">
        <div class="ui header">
            <?php echo $TLUMACZENIA['profil71']; ?>
        </div>
        <div class="ui huge breadcrumb">
            <div class="section"><?php echo $TLUMACZENIA['profil72']; ?></div>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_adres" ><?php echo $TLUMACZENIA['profil73']; ?></a>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_zdjecia" ><?php echo $TLUMACZENIA['profil74']; ?></a>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_szczegoly" ><?php echo $TLUMACZENIA['profil75']; ?></a>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_podsumowanie" ><?php echo $TLUMACZENIA['profil76']; ?></a>
            <span class="poczekaj_zdjecia" style="font-size: 90%;" ><i class="notched circle loading icon"></i><?php echo $TLUMACZENIA['profil77']; ?></span>
        </div>
    </div>
    <div class="content" >
        <div class="ui form">
            <div class="field">
                <label><?php echo $TLUMACZENIA['profil78']; ?></label>
                <input id="formularz_TYTUL" type="text" maxlength="35">
            </div>
            <div class="fields" >
                <div class="five wide field">
                    <label><?php echo $TLUMACZENIA['profil79']; ?></label>
                    <div class="ui selection dropdown" >
                        <input type="hidden" id="formularz_rodzajM">
                        <div class="default text"><?php echo $TLUMACZENIA['profil80']; ?></div>
                        <i class="dropdown icon"></i>
                        <div class="menu" style="overflow:auto;max-height:300px;">
                            <div class="item" data-value="0">
                                <i class="home icon"></i>
                                <?php echo $TLUMACZENIA['wyszukiwarka6']; ?>
                            </div>
                            <div class="item" data-value="50">
                                <i class="building icon"></i>
                                <?php echo $TLUMACZENIA['wyszukiwarka7']; ?>
                            </div>
                            <div class="item" data-value="100">
                                <i class="bed icon"></i>
                                <?php echo $TLUMACZENIA['wyszukiwarka8']; ?>
                            </div>
                            <div class="item" data-value="150">
                                <i class="tree icon"></i>
                                <?php echo $TLUMACZENIA['wyszukiwarka9']; ?>
                            </div>
                            <div class="item" data-value="200">
                                <i class="car icon"></i>
                                <?php echo $TLUMACZENIA['wyszukiwarka10']; ?>
                            </div>
                            <div class="item" data-value="250">
                                <i class="travel icon"></i>
                                <?php echo $TLUMACZENIA['wyszukiwarka11']; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="six wide field">
                    <label><?php echo $TLUMACZENIA['profil81']; ?></label>
                    <div class="ui selection dropdown" >
                        <input type="hidden" id="formularz_rodzajT" >
                        <div class="default text"><?php echo $TLUMACZENIA['profil80']; ?></div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item" data-value="0">
                                <?php echo $TLUMACZENIA['wyszukiwarka13']; ?>
                            </div>
                            <div class="item" data-value="1">
                                <?php echo $TLUMACZENIA['wyszukiwarka14']; ?>
                            </div>
                            <div class="item" data-value="2">
                                <?php echo $TLUMACZENIA['wyszukiwarka15']; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="three wide field">
                    <label><?php echo $TLUMACZENIA['profil82']; ?></label>
                    <input type="text" id="formularz_cena" maxlength="13" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                </div>
                <div class="two wide field">
                    <label><?php echo $TLUMACZENIA['profil83']; ?></label>
                    <div class="ui selection dropdown fluid" >
                        <input type="hidden" id="formularz_waluta">
                        <div class="default text"><?php echo $TLUMACZENIA['profil80']; ?></div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item" data-value="0">
                                <?php echo $TLUMACZENIA['wyszukiwarka20']; ?>
                            </div>
                            <div class="item" data-value="1">
                                <?php echo $TLUMACZENIA['wyszukiwarka21']; ?>
                            </div>
                            <div class="item" data-value="2">
                                <?php echo $TLUMACZENIA['wyszukiwarka22']; ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="fields" style="margin-top: 15px;">
                <div class="nine wide field" >
                    <label><?php echo $TLUMACZENIA['profil84']; ?></label>
                    <textarea rows="7" style="resize: none;" id="formularz_OPIS" maxlength="350"></textarea>
                </div>
                <div class="seven wide field" style="margin-top: 20px;">
                    <div class="ui toggle checkbox" id="uzyjGlownykontakt">
                        <input type="checkbox" checked value="true" id="formularz_glownyKontakt"  >
                        <label><?php echo $TLUMACZENIA['profil85']; ?></label>
                    </div>
                    <div class="disabled field" style="margin-top: 15px;">
                        <div class="inline field" >
                            <label><?php echo $TLUMACZENIA['przegladaj10']; ?>&nbsp;</label>
                            <input type="text" style="width: 70%;" id="formularz_imieKontakt" >
                        </div>
                        <div class="inline field" >
                            <label><?php echo $TLUMACZENIA['przegladaj20']; ?>&nbsp;&nbsp;&nbsp;&nbsp;</label>
                            <input type="text" style="width: 70%;" id="formularz_emailKontakt"  >
                        </div>
                        <div class="inline field" >
                            <label><?php echo $TLUMACZENIA['przegladaj12']; ?></label>
                            <input type="text" style="width: 70%;" id="formularz_telKontakt" >
                        </div>
                    </div>
                </div>
            </div>




        </div>

    </div>
    <div class="actions">
        <div class="ui disabled deny button">
            <?php echo $TLUMACZENIA['profil86']; ?>
        </div>
        <div class="ui positive right labeled icon button idzDo_formularz_adres">
            <?php echo $TLUMACZENIA['profil87']; ?>
            <i class="right arrow icon"></i>
        </div>
    </div>
</div>

<div class="ui united modal" id="formularz_adres" >
    <i class="close icon"></i>
    <div class="header">
        <div class="ui header">
            <?php echo $TLUMACZENIA['profil71']; ?>
        </div>
        <div class="ui huge breadcrumb">
            <a class="section idzDo_formularz_podstawowe" ><?php echo $TLUMACZENIA['profil72']; ?></a>
            <i class="right chevron icon divider"></i>
            <div class="section" ><?php echo $TLUMACZENIA['profil73']; ?></div>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_zdjecia" ><?php echo $TLUMACZENIA['profil74']; ?></a>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_szczegoly" ><?php echo $TLUMACZENIA['profil75']; ?></a>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_podsumowanie" ><?php echo $TLUMACZENIA['profil76']; ?></a>
            <span class="poczekaj_zdjecia" style="font-size: 90%;" ><i class="notched circle loading icon"></i><?php echo $TLUMACZENIA['profil77']; ?></span>
        </div>
    </div>
    <div class="content">

        <div class="ui negative message" id="PUSTA_ULICA" style="display: none;">
            <div class="header">
                <?php echo $TLUMACZENIA['profil88']; ?>
            </div>
        </div>

        <div class="ui form">
            <div class="field" >
                <label><?php echo $TLUMACZENIA['profil89']; ?></label>
                <div id="MAPA2" style="width: 100%;height: 350px;z-index: 10"></div>
                <div class="ui fluid input" style="width: 100%;">
                    <input type="text" placeholder="<?php echo $TLUMACZENIA['wyszukiwarka3']; ?>" id="wyszukiwarka_mapa2" style="width: 100%;">
                </div>
            </div>



            <div id="ADRESY" style="margin-bottom: 30px;">
                <input type="text" name="xx" id="formularz_XX" style="display: none;">
                <input type="text" name="yy" id="formularz_YY" style="display: none;">

                <div class="fields">
                    <div class="six wide field">
                        <label><?php echo $TLUMACZENIA['profil90']; ?>&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        <input type="text" id="formularz_ulica">
                    </div>
                    <div class="four wide field">
                        <label><?php echo $TLUMACZENIA['profil91']; ?></label>
                        <input type="text" id="formularz_miasto">
                    </div>
                    <div class="six wide field">
                        <label><?php echo $TLUMACZENIA['profil92']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        <input type="text" id="formularz_kraj">
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="actions">
        <div class="ui deny button idzDo_formularz_podstawowe">
            <?php echo $TLUMACZENIA['profil86']; ?>
        </div>
        <div class="ui positive right labeled icon button idzDo_formularz_zdjecia">
            <?php echo $TLUMACZENIA['profil87']; ?>
            <i class="right arrow icon"></i>
        </div>
    </div>
</div>

<div class="ui united modal" id="formularz_zdjecia">
    <i class="close icon"></i>
    <div class="header">
        <div class="ui header">
            <?php echo $TLUMACZENIA['profil71']; ?>
        </div>
        <div class="ui huge breadcrumb">
            <a class="section idzDo_formularz_podstawowe" ><?php echo $TLUMACZENIA['profil72']; ?></a>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_adres" ><?php echo $TLUMACZENIA['profil73']; ?></a>
            <i class="right chevron icon divider"></i>
            <div class="section" ><?php echo $TLUMACZENIA['profil74']; ?></div>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_szczegoly" ><?php echo $TLUMACZENIA['profil75']; ?></a>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_podsumowanie" ><?php echo $TLUMACZENIA['profil76']; ?></a>
            <span class="poczekaj_zdjecia" style="font-size: 90%;" ><i class="notched circle loading icon"></i><?php echo $TLUMACZENIA['profil77']; ?></span>
        </div>
    </div>
    <div class="content">

        <div class="ui form">
            <div class="field">
                <label><?php echo $TLUMACZENIA['profil74']; ?>: </label>
                <input type="file" name="files[]" id="filer_input" multiple="multiple">
            </div>
        </div>

    </div>
    <div class="actions">
        <div class="ui deny button idzDo_formularz_adres">
            <?php echo $TLUMACZENIA['profil86']; ?>
        </div>
        <div class="ui positive right labeled icon button idzDo_formularz_szczegoly">
            <?php echo $TLUMACZENIA['profil87']; ?>
            <i class="right arrow icon"></i>
        </div>
    </div>
</div>

<div class="ui united modal" id="formularz_szczegoly">
    <i class="close icon"></i>
    <div class="header">
        <div class="ui header">
            <?php echo $TLUMACZENIA['profil71']; ?>
        </div>
        <div class="ui huge breadcrumb">
            <a class="section active idzDo_formularz_podstawowe" ><?php echo $TLUMACZENIA['profil72']; ?></a>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_adres" ><?php echo $TLUMACZENIA['profil73']; ?></a>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_zdjecia" ><?php echo $TLUMACZENIA['profil74']; ?></a>
            <i class="right chevron icon divider"></i>
            <div class="section" ><?php echo $TLUMACZENIA['profil75']; ?></div>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_podsumowanie" ><?php echo $TLUMACZENIA['profil76']; ?></a>
            <span class="poczekaj_zdjecia" style="font-size: 90%;" ><i class="notched circle loading icon"></i><?php echo $TLUMACZENIA['profil77']; ?></span>
        </div>
    </div>
    <div class="content">

        <div class="ui form">

            <div class="two fields">
                <div class="eight wide field disabled" id="rodzajZabudowyonczyoff">
                    <label><?php echo $TLUMACZENIA['wyszukiwarka24']; ?></label>
                    <div class="ui selection dropdown fluid">
                        <input type="hidden" id="formularz_szczegoly_propertyBuildingType" >
                        <div class="default text"><?php echo $TLUMACZENIA['profil80']; ?></div>
                        <i class="dropdown icon"></i>
                        <div class="menu" >
                            <div class="item rodzajZabudowyDom" data-value="0" ><?php echo $TLUMACZENIA['wyszukiwarka57'];?></div>
                            <div class="item rodzajZabudowyDom" data-value="1" ><?php echo $TLUMACZENIA['wyszukiwarka58'];?></div>
                            <div class="item rodzajZabudowyDom" data-value="2" ><?php echo $TLUMACZENIA['wyszukiwarka59'];?></div>
                            <div class="item rodzajZabudowyDom" data-value="3" ><?php echo $TLUMACZENIA['wyszukiwarka60'];?></div>
                            <div class="item rodzajZabudowyBlok" data-value="50" ><?php echo $TLUMACZENIA['wyszukiwarka61'];?></div>
                            <div class="item rodzajZabudowyBlok" data-value="51" ><?php echo $TLUMACZENIA['wyszukiwarka62'];?></div>
                            <div class="item rodzajZabudowyPokoj" data-value="100" ><?php echo $TLUMACZENIA['wyszukiwarka63'];?></div>
                            <div class="item rodzajZabudowyPokoj" data-value="101" ><?php echo $TLUMACZENIA['wyszukiwarka64'];?></div>
                            <div class="item rodzajZabudowyDzialka" data-value="150" ><?php echo $TLUMACZENIA['wyszukiwarka65'];?></div>
                            <div class="item rodzajZabudowyDzialka" data-value="151" ><?php echo $TLUMACZENIA['wyszukiwarka66'];?></div>
                            <div class="item rodzajZabudowyDzialka" data-value="152" ><?php echo $TLUMACZENIA['wyszukiwarka67'];?></div>
                            <div class="item rodzajZabudowyGaraz" data-value="200" ><?php echo $TLUMACZENIA['wyszukiwarka68'];?></div>
                            <div class="item rodzajZabudowyGaraz" data-value="201" ><?php echo $TLUMACZENIA['wyszukiwarka69'];?></div>
                            <div class="item rodzajZabudowyGaraz" data-value="202" ><?php echo $TLUMACZENIA['wyszukiwarka70'];?></div>
                            <div class="item rodzajZabudowyLokale" data-value="250" ><?php echo $TLUMACZENIA['wyszukiwarka71'];?></div>
                            <div class="item rodzajZabudowyLokale" data-value="251" ><?php echo $TLUMACZENIA['wyszukiwarka72'];?></div>
                            <div class="item rodzajZabudowyLokale" data-value="252" ><?php echo $TLUMACZENIA['wyszukiwarka73'];?></div>
                            <div class="item rodzajZabudowyLokale" data-value="253" ><?php echo $TLUMACZENIA['wyszukiwarka74'];?></div>
                            <div class="item rodzajZabudowyLokale" data-value="254" ><?php echo $TLUMACZENIA['wyszukiwarka75'];?></div>
                            <div class="item rodzajZabudowyLokale" data-value="255" ><?php echo $TLUMACZENIA['wyszukiwarka76'];?></div>
                        </div>
                    </div>
                </div>
                <div class="one wide field"></div>
                <div class="nine wide field">
                    <label><?php echo $TLUMACZENIA['wyszukiwarka30'];?></label>
                    <div class="ui selection dropdown fluid">
                        <input type="hidden" id="formularz_szczegoly_propertyHeatingType">
                        <div class="default text"></div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item" data-value="0"><?php echo $TLUMACZENIA['wyszukiwarka32'];?></div>
                            <div class="item" data-value="1"><?php echo $TLUMACZENIA['wyszukiwarka31'];?></div>
                            <div class="item" data-value="2"><?php echo $TLUMACZENIA['wyszukiwarka33'];?></div>
                            <div class="item" data-value="3"><?php echo $TLUMACZENIA['wyszukiwarka78'];?></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="two fields">
                <div class="eight wide field">
                    <label><?php echo $TLUMACZENIA['przegladaj39'];?></label>
                    <div class="ui right labeled input">
                        <input type="text" maxlength="12" id="formularz_szczegoly_propertyArea" onkeypress='return validate(event)'>
                        <div class="ui basic label">
                            <?php echo $TLUMACZENIA['wyszukiwarka35'];?>
                        </div>
                    </div>
                </div>
                <div class="one wide field"></div>
                <div class="nine wide field">
                    <label><?php echo $TLUMACZENIA['przegladaj43'];?></label>
                    <input name="rocznik" type="text" maxlength="4" id="formularz_szczegoly_propertyBuildYear" onkeypress='return validate(event)' >
                </div>
            </div>

            <div class="four fields">
                <div class="four wide field">
                    <label><?php echo $TLUMACZENIA['przegladaj51'];?></label>
                    <input name="rocznik" maxlength="4" type="text" id="formularz_szczegoly_propertyMaxPerson" onkeypress='return validate(event)' >
                </div>
                <div class="three wide field">
                    <label><?php echo $TLUMACZENIA['przegladaj50'];?></label>
                    <input name="rocznik" maxlength="4" type="text" id="formularz_szczegoly_propertyRoomCount" onkeypress='return validate(event)' >
                </div>
                <div class="one wide field"></div>
                <div class="three wide field">
                    <label><?php echo $TLUMACZENIA['wyszukiwarka38'];?></label>
                    <input name="rocznik" maxlength="3" type="text" id="formularz_szczegoly_propertyFloor" onkeypress='return validate(event)' >
                </div>

            </div>

            <h3 class="ui dividing header">
                <?php echo $TLUMACZENIA['profil93'];?>
            </h3>
<!--            <div class="ui divider" style="margin-top: 30px;"></div>-->

            <div class="fields">
                <div class="three wide field">
                    <label><?php echo $TLUMACZENIA['wyszukiwarka40'];?></label>
                    <div class="ui selection dropdown fluid">
                        <input type="hidden" id="formularz_szczegoly_propertyIsFurnished">
                        <div class="default text"><?php echo $TLUMACZENIA['profil80'];?></div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item" data-value="true">
                                <?php echo $TLUMACZENIA['wyszukiwarka41']; ?>
                            </div>
                            <div class="item" data-value="false">
                                <?php echo $TLUMACZENIA['wyszukiwarka42']; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="three wide field">
                    <label><?php echo $TLUMACZENIA['wyszukiwarka49'];?></label>
                    <div class="ui selection dropdown fluid">
                        <input type="hidden" id="formularz_szczegoly_propertyHasBalcony">
                        <div class="default text"><?php echo $TLUMACZENIA['wyszukiwarka42']; ?></div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item" data-value="true">
                                <?php echo $TLUMACZENIA['wyszukiwarka41']; ?>
                            </div>
                            <div class="item" data-value="false">
                                <?php echo $TLUMACZENIA['wyszukiwarka42']; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="three wide field">
                    <label><?php echo $TLUMACZENIA['wyszukiwarka44'];?></label>
                    <div class="ui selection dropdown fluid">
                        <input type="hidden" id="formularz_szczegoly_propertyHasLift">
                        <div class="default text"><?php echo $TLUMACZENIA['wyszukiwarka42']; ?></div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item" data-value="true">
                                <?php echo $TLUMACZENIA['wyszukiwarka41']; ?>
                            </div>
                            <div class="item" data-value="false">
                                <?php echo $TLUMACZENIA['wyszukiwarka42']; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="three wide field">
                    <label><?php echo $TLUMACZENIA['wyszukiwarka45'];?></label>
                    <div class="ui selection dropdown fluid">
                        <input type="hidden" id="formularz_szczegoly_propertyHasBasement">
                        <div class="default text"><?php echo $TLUMACZENIA['wyszukiwarka42']; ?></div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item" data-value="true">
                                <?php echo $TLUMACZENIA['wyszukiwarka41']; ?>
                            </div>
                            <div class="item" data-value="false">
                                <?php echo $TLUMACZENIA['wyszukiwarka42']; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="three wide field">
                    <label><?php echo $TLUMACZENIA['wyszukiwarka48'];?></label>
                    <div class="ui selection dropdown fluid">
                        <input type="hidden" id="formularz_szczegoly_propertyHasParking">
                        <div class="default text"><?php echo $TLUMACZENIA['wyszukiwarka42']; ?></div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item" data-value="true">
                                <?php echo $TLUMACZENIA['wyszukiwarka41']; ?>
                            </div>
                            <div class="item" data-value="false">
                                <?php echo $TLUMACZENIA['wyszukiwarka42']; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="fields">
                <div class="three wide field">
                    <label><?php echo $TLUMACZENIA['wyszukiwarka51'];?></label>
                    <div class="ui selection dropdown fluid">
                        <input type="hidden" id="formularz_szczegoly_propertyHasGarden">
                        <div class="default text"><?php echo $TLUMACZENIA['wyszukiwarka42']; ?></div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item" data-value="true">
                                <?php echo $TLUMACZENIA['wyszukiwarka41']; ?>
                            </div>
                            <div class="item" data-value="false">
                                <?php echo $TLUMACZENIA['wyszukiwarka42']; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="three wide field">
                    <label><?php echo $TLUMACZENIA['wyszukiwarka50'];?></label>
                    <div class="ui selection dropdown fluid">
                        <input type="hidden" id="formularz_szczegoly_propertyHasClima">
                        <div class="default text"><?php echo $TLUMACZENIA['wyszukiwarka42']; ?></div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item" data-value="true">
                                <?php echo $TLUMACZENIA['wyszukiwarka41']; ?>
                            </div>
                            <div class="item" data-value="false">
                                <?php echo $TLUMACZENIA['wyszukiwarka42']; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="three wide field">
                    <label><?php echo $TLUMACZENIA['wyszukiwarka46'];?></label>
                    <div class="ui selection dropdown fluid">
                        <input type="hidden" id="formularz_szczegoly_propertyAllowSmoking">
                        <div class="default text"><?php echo $TLUMACZENIA['wyszukiwarka42']; ?></div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item" data-value="true">
                                <?php echo $TLUMACZENIA['wyszukiwarka41']; ?>
                            </div>
                            <div class="item" data-value="false">
                                <?php echo $TLUMACZENIA['wyszukiwarka42']; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="three wide field">
                    <label><?php echo $TLUMACZENIA['wyszukiwarka47'];?></label>
                    <div class="ui selection dropdown fluid">
                        <input type="hidden" id="formularz_szczegoly_propertyAllowPets">
                        <div class="default text"><?php echo $TLUMACZENIA['wyszukiwarka42']; ?></div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item" data-value="true">
                                <?php echo $TLUMACZENIA['wyszukiwarka41']; ?>
                            </div>
                            <div class="item" data-value="false">
                                <?php echo $TLUMACZENIA['wyszukiwarka42']; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>

    </div>
    <div class="actions">
        <div class="ui deny button idzDo_formularz_zdjecia">
            <?php echo $TLUMACZENIA['profil86']; ?>
        </div>
        <div class="ui positive right labeled icon button idzDo_formularz_podsumowanie">
            <?php echo $TLUMACZENIA['profil87']; ?>
            <i class="right arrow icon"></i>
        </div>
    </div>
</div>

<div class="ui united modal" id="formularz_podsumowanie">
    <i class="close icon"></i>
    <div class="header">
        <div class="ui header">
            <?php echo $TLUMACZENIA['profil71']; ?>
        </div>
        <div class="ui huge breadcrumb">
            <a class="section active idzDo_formularz_podstawowe" ><?php echo $TLUMACZENIA['profil72']; ?></a>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_adres" ><?php echo $TLUMACZENIA['profil73']; ?></a>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_zdjecia" ><?php echo $TLUMACZENIA['profil74']; ?></a>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_szczegoly"><?php echo $TLUMACZENIA['profil75']; ?></a>
            <i class="right chevron icon divider"></i>
            <div class="section" ><?php echo $TLUMACZENIA['profil76']; ?></div>
        </div>
    </div>
    <div class="content">

        <form class="ui form" id="FORMULARZ_DODAWANIA">
            <div class="field" style="display: none;">
                <label><?php echo $TLUMACZENIA['profil78'];?></label>
                <input name="tytul" id="set_formularz_TYTUL" type="text">
                <input name="opis" id="set_formularz_OPIS" type="text">
                <input name="rodzajT" id="set_formularz_RodzajT" type="text">
                <input name="rodzajM" id="set_formularz_RodzajM" type="text">
                <input name="cena" id="set_formularz_Cena" type="text">
                <input name="waluta" id="set_formularz_Waluta" type="text">
                <input name="ulica" id="set_formularz_Ulica" type="text">
                <input name="miasto" id="set_formularz_Miasto" type="text">
                <input name="kraj" id="set_formularz_Kraj" type="text">
                <input name="xx" id="set_formularz_XX" type="text">
                <input name="yy" id="set_formularz_YY" type="text">

                <input name="glownyKontakt" id="set_formularz_glownyKontakt" type="text">
                <input name="imieKontakt" id="set_formularz_imieKontakt" type="text">
                <input name="emailKontakt" id="set_formularz_emailKontakt" type="text">
                <input name="telKontakt" id="set_formularz_telKontakt" type="text">


                <!-- SZCEGOLYYYYYY-->
                <input name="propertyBuildingType" id="set_formularz_szczegoly_propertyBuildingType" type="text">
                <input name="propertyMinPerson" id="set_formularz_szczegoly_propertyMinPerson" type="text">
                <input name="propertyMaxPerson" id="set_formularz_szczegoly_propertyMaxPerson" type="text">
                <input name="propertyHeatingType" id="set_formularz_szczegoly_propertyHeatingType" type="text">
                <input name="propertyArea" id="set_formularz_szczegoly_propertyArea" type="text">
                <input name="propertyRoomCount" id="set_formularz_szczegoly_propertyRoomCount" type="text">
                <input name="propertyFloor" id="set_formularz_szczegoly_propertyFloor" type="text">
                <input name="propertyBuildYear" id="set_formularz_szczegoly_propertyBuildYear" type="text">
                <input name="propertyIsFurnished" id="set_formularz_szczegoly_propertyIsFurnished" type="text">
                <input name="propertyHasBalcony" id="set_formularz_szczegoly_propertyHasBalcony" type="text">
                <input name="propertyHasLift" id="set_formularz_szczegoly_propertyHasLift" type="text">
                <input name="propertyHasBasement" id="set_formularz_szczegoly_propertyHasBasement" type="text">
                <input name="propertyHasParking" id="set_formularz_szczegoly_propertyHasParking" type="text">
                <input name="propertyAllowPets" id="set_formularz_szczegoly_propertyAllowPets" type="text">
                <input name="propertyHasGarden" id="set_formularz_szczegoly_propertyHasGarden" type="text">
                <input name="propertyHasClima" id="set_formularz_szczegoly_propertyHasClima" type="text">
                <input name="propertyAllowSmoking" id="set_formularz_szczegoly_propertyAllowSmoking" type="text">




            </div>

            <div id="PODGLAD" style="height: auto;">
                <div class="two fields" >
                    <div class="field" style="margin-right: 15px;">
                        <div id="niemaZdjec"  style="width: 100%;height:300px;background: url('img/noimage.png') center;background-size: cover;" >
                        </div>
                        <div class="popup-gallery"  style="width: 87%;">
                            <div id="podglad_obrazy_glowne" ></div>
                            <div id="podglad_obrazy_mini" class="MINIATURKI" ></div>
                        </div>
                    </div>
                    <div class="field">
                        <div style="left: 48%;top: 20px;">
                            <h3 class="ui header">
                                <div id="podglad_tytul"><?php echo $TLUMACZENIA['profil94'];?></div>
                            </h3>

                            <h1 class="ui header" style="margin-top: -10px;">
                                <span id="podglad_ulica"><?php echo $TLUMACZENIA['profil90'];?></span>
                                <div class="sub header"><span id="podglad_kraj"><?php echo $TLUMACZENIA['profil92'];?></span> - <span id="podglad_miasto"><?php echo $TLUMACZENIA['profil91'];?></span> </div>
                            </h1>

                            <div class="ui clearing" style="margin-bottom: 50px;">
                                <h3 class="ui left floated header" id="podglad_offertyp"><?php echo $TLUMACZENIA['profil95'];?></h3>
                                <h5 class="ui left floated header" style="margin-top: 5px;"><?php echo $TLUMACZENIA['przegladaj4'];?></h5>
                                <h3 class="ui left floated header" id="podglad_typ"><?php echo $TLUMACZENIA['profil96'];?></h3>

                                <h2 class="ui right floated header" style="margin-left: 35px;margin-top: -5px;">
                                    <span id="podglad_cena"></span> <span id="podglad_cena_waluta"></span>
                                </h2>
                            </div>

                            <div class="ui accordion" style="height: 100%;">
                                <div class="title">
                                    <i class="dropdown icon"></i>
                                    <?php echo $TLUMACZENIA['przegladaj7']; ?>
                                </div>
                                <div class="content">
                                    <div class="ui divided middle aligned selection list" id="podglad_szczegoly" ></div>
                                </div>
                                <div class="title">
                                    <i class="dropdown icon"></i>
                                    <?php echo $TLUMACZENIA['przegladaj8']; ?>
                                </div>
                                <div class="content" id="podglad_opis"></div>
                                <div class="title">
                                    <i class="dropdown icon"></i>
                                    <?php echo $TLUMACZENIA['przegladaj9']; ?>
                                </div>
                                <div class="content" id="podglad_kontakt"></div>
                            </div>

                        </div>
                    </div>
                </div>


            </div>


            <div id="ERORKIFORM" style="margin-top: 30px;" class="ui error message"></div>

            <div id="ERORKI_dodawnaie" style="margin-top: 30px;display: none;" class="ui error message">
                <?php echo $TLUMACZENIA['profil97']; ?>
            </div>



            <div class="ui middle aligned divided list" id="WYLICZ_KOSZTA" style="width: 97%;margin-left: 1%;margin-top: 20px;margin-bottom: 20px;">



                <div class="two ui buttons" id="pasekDodawnieitd_przydodwaniau">
                    <div class="ui labeled button" id="MOZNA_KUPOWAC">
                        <div class="ui green button" style="width: 70%;">
                            <i class="checkmark icon"></i><?php echo $TLUMACZENIA['profil98']; ?>
                        </div>
                        <a class="ui basic green left pointing label" style="width: 30%;">
                            <span style="width: 100%;" class="ile_mam_hajsu"></span>
                        </a>
                    </div>
                    <div class="ui labeled button" id="NIE_MOZNA_KUPOWAC">
                        <div class="ui red button" style="width: 70%;">
                            <i class="remove icon"></i><?php echo $TLUMACZENIA['profil99']; ?>
                        </div>
                        <a class="ui basic red left pointing label" style="width: 30%;">
                            <span style="width: 100%;" class="ile_mam_hajsu"></span>
                        </a>
                    </div>
                    <div class="ui labeled button" id="KOSZT_OPERACJI" tabindex="0">
                        <div class="ui basic button" style="width: 70%;">
                            <i class="money icon"></i><?php echo $TLUMACZENIA['profil100']; ?>
                        </div>
                        <a class="ui basic left pointing label" style="width: 30%;">
                            <span style="width: 100%;" id="koszt_suma"></span>
                        </a>
                    </div>

                </div>
                <div class="ui pointing label">
                    <?php echo $TLUMACZENIA['profil101']; ?>
                </div>
            </div>






        </form>






    </div>
    <div class="actions">
        <div class="ui deny button idzDo_formularz_szczegoly">
            <?php echo $TLUMACZENIA['profil86']; ?>
        </div>
        <div class="ui positive right labeled icon button" id="WYSLIJ_FORMULARZ">
            <?php echo $TLUMACZENIA['profil114']; ?>
            <i class="checkmark icon"></i>
        </div>
    </div>

</div>



<script>
    var tablicaLadujacychSie = [];
    var niemazdjec = true;
    $(document).ready(function() {


        var options = {
            url: function(phrase) {
                var link = WYSZUKIWARKA_LINK + phrase;
                return link;
            },
            getValue: function(element) {
                var miasto = "";
                var ulica = "";
                var ulicaNR = "";

                if(element.address.city != undefined)
                    miasto = element.address.city ;
                else if(element.address.town != undefined)
                    miasto = element.address.town ;

                if(element.address.road != undefined)
                    ulica = element.address.road ;

                if(element.address.house_number != undefined)
                    ulicaNR = element.address.house_number ;


                return ""+miasto+" "+ulica+" "+ulicaNR;

            },
            list: {
                onClickEvent: function() {
                    var value = $("#wyszukiwarka_mapa2").getSelectedItemData();

                    wyszukiwanie_google_formularz(value, MAPA2,true);

                }
            },
            adjustWidth: false,
            minCharNumber: 2

        };
        $("#wyszukiwarka_mapa2").easyAutocomplete(options);

        $('#wyszukiwarka_mapa2').keypress(function(event){
            if(event.keyCode == 13){
                $(this).trigger('blur')
                $("#wyszukiwarka_mapa2").val($('#eac-container-wyszukiwarka_mapa2 ul li').eq(0).text()).trigger("change");

                $.get( WYSZUKIWARKA_LINK+$('#eac-container-wyszukiwarka_mapa2 ul li').eq(0).text(), function(data) {
                    wyszukiwanie_google_formularz(data[0], MAPA2,true);
                })
            }
        });





        $('#podglad_obrazy_mini').slick({
            infinite: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            variableWidth: true
        });



        $('.united.modal').modal({closable: false,observeChanges: true,allowMultiple: false,duration : 150}); //400 domyslne

        $('#formularz_podstawowe').modal('attach events', '.idzDo_formularz_podstawowe');
        $('#formularz_adres').modal('attach events', '.idzDo_formularz_adres');
        $('#formularz_zdjecia').modal('attach events', '.idzDo_formularz_zdjecia');
        $('#formularz_szczegoly').modal('attach events', '.idzDo_formularz_szczegoly');
        $('#formularz_podsumowanie').modal('attach events', '.idzDo_formularz_podsumowanie');


        $('#dodajOgloszenie').on('click', function(){
            $('#formularz_podstawowe').modal('show');
        });



        $('.idzDo_formularz_szczegoly').on('click', function() {
            $('.rodzajZabudowyDom,.rodzajZabudowyBlok,.rodzajZabudowyLokale, .rodzajZabudowyPokoj, .rodzajZabudowyGaraz, .rodzajZabudowyDzialka').hide();
            if($('#formularz_rodzajM').val() != '')
            {
                $('#rodzajZabudowyonczyoff').removeClass('disabled');

                if($('#formularz_rodzajM').val() == 0)
                    $('.rodzajZabudowyDom').show();
                else if($('#formularz_rodzajM').val() == 50)
                    $('.rodzajZabudowyBlok').show();
                else if($('#formularz_rodzajM').val() == 100)
                    $('.rodzajZabudowyPokoj').show();
                else if($('#formularz_rodzajM').val() == 150)
                    $('.rodzajZabudowyDzialka').show();
                else if($('#formularz_rodzajM').val() == 200)
                    $('.rodzajZabudowyGaraz').show();
                else if($('#formularz_rodzajM').val() == 250)
                    $('.rodzajZabudowyLokale').show();
            }

        });

        var OBIEKT,OBIEKT2;
        $('.idzDo_formularz_podsumowanie').on('click', function(){

            $('#WYLICZ_KOSZTA').hide();

            var getTytul = $('#formularz_TYTUL').val();
            var getOpis = $('#formularz_OPIS').val();
            var getRodzajT = $('#formularz_rodzajT').val();
            var getRodzajM = $('#formularz_rodzajM').val();

            var getCena = $('#formularz_cena').val();
            var getWaluta = $('#formularz_waluta').val();

            var getUlica = $('#formularz_ulica').val();
            var getMiasto = $('#formularz_miasto').val();
            var getKraj = $('#formularz_kraj').val();

            var getXX = $('#formularz_XX').val();
            var getYY = $('#formularz_YY').val();

            var glownyKontakt = $('#formularz_glownyKontakt').val();
            var imieKontakt = $('#formularz_imieKontakt').val();
            var telKontakt = $('#formularz_emailKontakt').val();
            var emailKontakt = $('#formularz_telKontakt').val();



            photos = $("input[name='photos']").map(function(){return $(this).val();}).get();

            $('#set_formularz_TYTUL').val( getTytul );
            $('#set_formularz_OPIS').val( getOpis );
            $('#set_formularz_RodzajT').val( getRodzajT );
            $('#set_formularz_RodzajM').val( getRodzajM );

            $('#set_formularz_Cena').val( getCena );
            $('#set_formularz_Waluta').val( getWaluta );

            $('#set_formularz_Ulica').val( getUlica );
            $('#set_formularz_Miasto').val( getMiasto );
            $('#set_formularz_Kraj').val( getKraj );

            $('#set_formularz_XX').val( getXX );
            $('#set_formularz_YY').val( getYY );

            $('#set_formularz_glownyKontakt').val( glownyKontakt );
            $('#set_formularz_imieKontakt').val( imieKontakt );
            $('#set_formularz_emailKontakt').val( emailKontakt );
            $('#set_formularz_telKontakt').val( telKontakt );

            //SZCZEGOLY
            if($('#formularz_szczegoly_propertyBuildingType').val()!="")
                $('#set_formularz_szczegoly_propertyBuildingType').val( $('#formularz_szczegoly_propertyBuildingType').val() )

            if($('#formularz_szczegoly_propertyBuildingType').val()!="")
                $('#set_formularz_szczegoly_propertyMinPerson').val( $('#formularz_szczegoly_propertyBuildingType').val() )

            if($('#formularz_szczegoly_propertyMinPerson').val()!="")
                $('#set_formularz_szczegoly_propertyMinPerson').val( $('#formularz_szczegoly_propertyMinPerson').val() )

            if($('#formularz_szczegoly_propertyMaxPerson').val()!="")
                $('#set_formularz_szczegoly_propertyMaxPerson').val( $('#formularz_szczegoly_propertyMaxPerson').val() )

            if($('#formularz_szczegoly_propertyHeatingType').val()!="")
                $('#set_formularz_szczegoly_propertyHeatingType').val( $('#formularz_szczegoly_propertyHeatingType').val() )

            if($('#formularz_szczegoly_propertyArea').val()!="")
                $('#set_formularz_szczegoly_propertyArea').val( $('#formularz_szczegoly_propertyArea').val() )

            if($('#formularz_szczegoly_propertyRoomCount').val()!="")
                $('#set_formularz_szczegoly_propertyRoomCount').val( $('#formularz_szczegoly_propertyRoomCount').val() )

            if($('#formularz_szczegoly_propertyFloor').val()!="")
                $('#set_formularz_szczegoly_propertyFloor').val( $('#formularz_szczegoly_propertyFloor').val() )

            if($('#formularz_szczegoly_propertyBuildYear').val()!="")
                $('#set_formularz_szczegoly_propertyBuildYear').val( $('#formularz_szczegoly_propertyBuildYear').val() )

            if($('#formularz_szczegoly_propertyIsFurnished').val()!="")
                $('#set_formularz_szczegoly_propertyIsFurnished').val( $('#formularz_szczegoly_propertyIsFurnished').val() )

            if($('#formularz_szczegoly_propertyHasBalcony').val()!="")
                $('#set_formularz_szczegoly_propertyHasBalcony').val( $('#formularz_szczegoly_propertyHasBalcony').val() )

            if($('#formularz_szczegoly_propertyHasLift').val()!="")
                $('#set_formularz_szczegoly_propertyHasLift').val( $('#formularz_szczegoly_propertyHasLift').val() )

            if($('#formularz_szczegoly_propertyHasBasement').val()!="")
                $('#set_formularz_szczegoly_propertyHasBasement').val( $('#formularz_szczegoly_propertyHasBasement').val() )

            if($('#formularz_szczegoly_propertyHasParking').val()!="")
                $('#set_formularz_szczegoly_propertyHasParking').val( $('#formularz_szczegoly_propertyHasParking').val() )

            if($('#formularz_szczegoly_propertyAllowPets').val()!="")
                $('#set_formularz_szczegoly_propertyAllowPets').val( $('#formularz_szczegoly_propertyAllowPets').val() )

            if($('#formularz_szczegoly_propertyHasGarden').val()!="")
                $('#set_formularz_szczegoly_propertyHasGarden').val( $('#formularz_szczegoly_propertyHasGarden').val() )

            if($('#formularz_szczegoly_propertyHasClima').val()!="")
                $('#set_formularz_szczegoly_propertyHasClima').val( $('#formularz_szczegoly_propertyHasClima').val() )

            if($('#formularz_szczegoly_propertyAllowSmoking').val()!="")
                $('#set_formularz_szczegoly_propertyAllowSmoking').val( $('#formularz_szczegoly_propertyAllowSmoking').val() )


            //console.log( $('#formularz_szczegoly_propertyHasParking').val() + " " + $('#set_formularz_szczegoly_propertyHasParking').val() )

            $('#podglad_szczegoly').empty();
            if($('#formularz_szczegoly_propertyBuildingType').val() != "") $('#podglad_szczegoly').append(divekSzczegolik(TLUMACZENIA['przegladaj38'],intToRodzajBT($('#formularz_szczegoly_propertyBuildingType').val()),''));
            $('#podglad_szczegoly').append(divekSzczegolik(TLUMACZENIA['przegladaj39'],$('#formularz_szczegoly_propertyArea').val(),TLUMACZENIA['wyszukiwarka35']));
            if($('#formularz_szczegoly_propertyHeatingType').val() != "") $('#podglad_szczegoly').append(divekSzczegolik(TLUMACZENIA['przegladaj40'],intToHeating($('#formularz_szczegoly_propertyHeatingType').val()),''));
            $('#podglad_szczegoly').append(divekSzczegolik(TLUMACZENIA['przegladaj41'],$('#formularz_szczegoly_propertyHasBasement').val(),''));
            $('#podglad_szczegoly').append(divekSzczegolik(TLUMACZENIA['przegladaj42'],$('#formularz_szczegoly_propertyHasBalcony').val(),''));
            $('#podglad_szczegoly').append(divekSzczegolik(TLUMACZENIA['przegladaj43'],$('#formularz_szczegoly_propertyBuildYear').val(),''));
            $('#podglad_szczegoly').append(divekSzczegolik(TLUMACZENIA['przegladaj44'],$('#formularz_szczegoly_propertyHasClima').val(),''));
            $('#podglad_szczegoly').append(divekSzczegolik(TLUMACZENIA['przegladaj45'],$('#formularz_szczegoly_propertyFloor').val(),''));
            $('#podglad_szczegoly').append(divekSzczegolik(TLUMACZENIA['przegladaj46'],$('#formularz_szczegoly_propertyIsFurnished').val(),''));
            $('#podglad_szczegoly').append(divekSzczegolik(TLUMACZENIA['przegladaj47'],$('#formularz_szczegoly_propertyHasGarden').val(),''));
            $('#podglad_szczegoly').append(divekSzczegolik(TLUMACZENIA['przegladaj48'],$('#formularz_szczegoly_propertyHasLift').val(),''));
            $('#podglad_szczegoly').append(divekSzczegolik(TLUMACZENIA['przegladaj49'],$('#formularz_szczegoly_propertyAllowPets').val(),''));
            $('#podglad_szczegoly').append(divekSzczegolik(TLUMACZENIA['przegladaj50'],$('#formularz_szczegoly_propertyRoomCount').val(),''));
            $('#podglad_szczegoly').append(divekSzczegolik(TLUMACZENIA['przegladaj51'],$('#formularz_szczegoly_propertyMaxPerson').val(),''));
            $('#podglad_szczegoly').append(divekSzczegolik(TLUMACZENIA['przegladaj52'],$('#formularz_szczegoly_propertyHasParking').val(),''));


            $('#podglad_kontakt').html('<table class="ui very basic table" >'+
                '<tbody>'+
                '<tr>'+
                '<td><i class="user icon"></i>'+TLUMACZENIA['przegladaj10']+'</td>'+
                '<td><span>'+$('#formularz_imieKontakt').val()+'</span></td>'+
                '</tr>'+
                '<tr>'+
                '<td><i class="mail outline icon"></i>'+TLUMACZENIA['przegladaj20']+'</td>'+
                '<td><span>'+$('#formularz_emailKontakt').val()+'</span></td>'+
                '</tr>'+
                '<tr>'+
                '<td><i class="call icon"></i>'+TLUMACZENIA['przegladaj12']+'</td>'+
                '<td><span>'+$('#formularz_telKontakt').val()+'</span></td>'+
                '</tr>'+
                '</tbody>'+
                '</table>')

            if(getTytul!="") $('#podglad_tytul').html(getTytul);
            if(getOpis!="") $('#podglad_opis').html(getOpis);
            if(getRodzajT!="") $('#podglad_typ').html(intToRodzajT(getRodzajT));
            if(getRodzajM!="") $('#podglad_offertyp').html(intToRodzajM(getRodzajM));

            if(getCena!="") $('#podglad_cena').html(getCena);

            if(getWaluta!="")
            {
                if(getRodzajT == 0)
                    $('#podglad_cena_waluta').html('<sup style="font-size: 70%;">'+intToWaluta(getWaluta)+'</sup>&frasl;<sub style="font-size: 60%;">'+TLUMACZENIA['przegladaj5']+'</sub>');
                else if(getRodzajT == 1)
                    $('#podglad_cena_waluta').html('<span style="font-size: 70%;">'+intToWaluta(getWaluta)+'</span>');
                else if(getRodzajT == 2)
                    $('#podglad_cena_waluta').html('<sup style="font-size: 70%;">'+intToWaluta(getWaluta)+'</sup>&frasl;<sub style="font-size: 60%;">'+TLUMACZENIA['przegladaj6']+'</sub>');

            }

            if(getUlica!="") $('#podglad_ulica').html(getUlica);
            if(getMiasto!="") $('#podglad_miasto').html(getMiasto);
            if(getKraj!="") $('#podglad_kraj').html(getKraj);


            $('#FORMULARZ_DODAWANIA').submit();



        });


        $('#WYSLIJ_FORMULARZ').on('click', function(){
            OBIEKT["photos"] = photos;
            OBIEKT = JSON.stringify(OBIEKT).replace(new RegExp("\\\\", "g"), "");

            $('#LADOWANIE_CZEKENIE').addClass('active');

            $.ajax({
                url: SERWER+'person/offer/add',
                type: "POST",
                contentType: "application/json",
                data : OBIEKT,
                headers : {
                    'X-Auth-Token' : localStorage.getItem('X-Auth-Token')
                },
                beforeSend : function(data){
                },
                success : function(data){
                    location.reload();
                },
                error: function(data)
                {
                    $('#LADOWANIE_CZEKENIE').removeClass('active');
                    $('#ERORKI_dodawnaie').show();
                }
            });

        });

        $('#FORMULARZ_DODAWANIA').form({
            fields: {
                tytul: { identifier: 'tytul',
                    rules: [{ type   : 'empty',
                        prompt : TLUMACZENIA['profil60'] }]},
                opis: { identifier: 'opis',
                    rules: [{ type   : 'empty',
                        prompt : TLUMACZENIA['profil61'] }]},
                rodzajT: { identifier: 'rodzajT',
                    rules: [{ type   : 'empty',
                        prompt : TLUMACZENIA['profil62'] }]},
                rodzajM: { identifier: 'rodzajT',
                    rules: [{ type   : 'empty',
                        prompt : TLUMACZENIA['profil63'] }]},
                cena: { identifier: 'cena',
                    rules: [{ type   : 'empty',
                        prompt : TLUMACZENIA['profil64'] }]},
                waluta: { identifier: 'waluta',
                    rules: [{ type   : 'empty',
                        prompt : TLUMACZENIA['profil65'] }]},
                ulica: { identifier: 'ulica',
                    rules: [{ type   : 'empty',
                        prompt : TLUMACZENIA['profil66'] }]},
                miasto: { identifier: 'miasto',
                    rules: [{ type   : 'empty',
                        prompt : TLUMACZENIA['profil67'] }]},
                kraj: { identifier: 'kraj',
                    rules: [{ type   : 'empty',
                        prompt : TLUMACZENIA['profil68'] }]}
            },
            onFailure : function(formErrors, fields)
            {

                $('#WYSLIJ_FORMULARZ').addClass('disabled');
                $('#WYLICZ_KOSZTA').hide();
                OBIEKT2 = fields;
                console.log( OBIEKT2 )
                return false;
            },
            onSuccess : function(event, fields)
            {
                OBIEKT = fields;
                OBIEKT2 = fields;
                OBIEKT2["photos"] = photos;
                OBIEKT2 = JSON.stringify(OBIEKT2).replace(new RegExp("\\\\", "g"), "");

                if(DEBUGOWANIE)
                {
                    console.log("wyceniam:");
                    console.log(OBIEKT2);
                }


                $.ajax({
                    url: SERWER+'person/offer/add-valuation',
                    type: "POST",
                    contentType: "application/json",
                    data : OBIEKT2,
                    headers : {
                        'X-Auth-Token' : localStorage.getItem('X-Auth-Token')
                    },
                    success : function(data){
//                    console.log(data)


                        $('.kosztdousuniecia').remove();

                        for(var i = 0; i < data.rows.length; ++i) {
                            $('#WYLICZ_KOSZTA').prepend('<div class="item kosztdousuniecia" style="width: 96%;padding-bottom: 20px;padding-top: 20px;">'+
                                '<div class="right floated content">'+
                                data.rows[i].points+' '+
                                '</div>'+
                                '<div class="content">'+
                                intToProdukt(data.rows[i].productType)+':'+
                                '</div>'+
                                '</div>');
                        }
                        if(data.rows.length != 0)
                            $('#WYLICZ_KOSZTA').prepend('<h3 class="ui header kosztdousuniecia"  style="margin-bottom: 0px;" > '+TLUMACZENIA['profil70']+' </h3>');

                        $('#koszt_suma').text(data.sum);
                        var koszt_suma = $('#koszt_suma').text();
                        $('.ile_mam_hajsu').text($('#IloscPunktow').text());
//                        console.log( $('#IloscPunktow').text() + " " + koszt_suma  )
                        if( parseInt($('#IloscPunktow').text()) >= parseInt(koszt_suma) )
                        {
                            $('#MOZNA_KUPOWAC').show();
                            $('#NIE_MOZNA_KUPOWAC').hide();
                            $('#NIE_MOZNA_KUPOWAC').parent().next('.label').hide();
                            $('#WYSLIJ_FORMULARZ').removeClass('disabled');
                        }
                        else
                        {
                            moge_kupowac = false;
                            $('#MOZNA_KUPOWAC').hide();
                            $('#NIE_MOZNA_KUPOWAC').show();

                        }
                        $('#WYLICZ_KOSZTA').show();

                    },
                    error: function(data)
                    {
                    }
                });


                return false;
            }}) ;



        $('#uzyjGlownykontakt').checkbox({
            onChecked: function () {
                $(this).parent().parent().find('.field').addClass('disabled');
                $('#formularz_imieKontakt').val( $('#imieInazwiskoMoj_input').val() );
                $('#formularz_emailKontakt').val( $('#telefonMoj_input').val() );
                $('#formularz_telKontakt').val( $('#emailMoj_input').val() );
            },
            onUnchecked: function () {
                $(this).parent().parent().find('.field').removeClass('disabled');
            }
        });

        $('#ADRESY').hide();
        var bylo = false;
        $(document).on('click', '.idzDo_formularz_adres',function(){
            if(!bylo)
            {
                bylo = true;

                MAPA2 = L.map('MAPA2', { center: L.latLng(50.088488, 18.237890),zoom: 10, attributionControl: false });
                window.dispatchEvent(new Event('resize'));
                L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(MAPA2);



                setTimeout(function(){
                    MAPA2.invalidateSize();
                    MAPA2.setView([50.07, 19.937890],12);
                },300);
            }
        });


        $('.dropdown').dropdown();



        $("#filer_input").change( handleFileSelect );

        $('.poczekaj_zdjecia').hide();



        $('.popup-gallery').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: TLUMACZENIA['przegladaj59']+' #%curr%...',
            tClose: TLUMACZENIA['przegladaj65'],
            mainClass: 'mfp-img-mobile',
            gallery: {
                tPrev: TLUMACZENIA['przegladaj62'], // Alt text on left arrow
                tNext: TLUMACZENIA['przegladaj63'], // Alt text on right arrow
                tCounter: '%curr% '+TLUMACZENIA['przegladaj64']+' %total%', // Markup for "1 of 7" counter
                enabled: true,
                navigateByImgClick: true,
                preload: [0,1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">'+TLUMACZENIA['przegladaj60']+' #%curr%</a> '+TLUMACZENIA['przegladaj61'],
                titleSrc: function(item) {
                    return item.el.attr('title');
                }
            }
        });

    });


    $(document).on('click', '.WLEWO', function(){
        $('#podglad_obrazy_mini').slick('unslick');
        if($(this).index(".WLEWO") != 0)
        {
            var ten = $(this).parentsUntil('.jFiler-item').parent();
            var lewy = $(this).parentsUntil('.jFiler-item').parent().prev('.jFiler-item');

            var strDiv1Cont = ten.html();
            var strDiv2Cont = lewy.html();

            ten.html(strDiv2Cont);
            lewy.html(strDiv1Cont);

            if($(ten).index('.jFiler-item') == 1)
            {
                ten.find('.glownezdjecie').text($(ten).index('.jFiler-item')+1);
                lewy.find('.glownezdjecie').text(DomyslneZdj_TXT);

                var div1 = $('#podglad_obrazy_glowne > a img').eq(0);
                var div2 = $('#podglad_obrazy_mini > a img').eq(0);

                var tdiv1 = div1.clone();
                var tdiv2 = div2.clone();

                div1.replaceWith(tdiv2);
                div2.replaceWith(tdiv1);

            }
            else
            {
                ten.find('.glownezdjecie').text( $(ten).index('.jFiler-item')+1 );
                lewy.find('.glownezdjecie').text( $(lewy).index('.jFiler-item')+1 );

                var div1 = $('#podglad_obrazy_mini > a img').eq($(ten).index('.jFiler-item')-1);
                var div2 = $('#podglad_obrazy_mini > a img').eq($(lewy).index('.jFiler-item')-1);

                var tdiv1 = div1.clone();
                var tdiv2 = div2.clone();

                div1.replaceWith(tdiv2);
                div2.replaceWith(tdiv1);

            }

        }

        $('#podglad_obrazy_mini').slick({
            infinite: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            variableWidth: true
        });
    });

    $(document).on('click', '.WPRAWO', function(){
        $('#podglad_obrazy_mini').slick('unslick');
        if( $(this).index(".WPRAWO")+1 != $(".WPRAWO").size() )
        {
            var ten = $(this).parentsUntil('.jFiler-item').parent();
            var prawy = $(this).parentsUntil('.jFiler-item').parent().next('.jFiler-item');

            var strDiv1Cont = ten.html();
            var strDiv2Cont = prawy.html();


            ten.html(strDiv2Cont);
            prawy.html(strDiv1Cont);

            if($(ten).index('.jFiler-item') == 0)
            {
                ten.find('.glownezdjecie').text(DomyslneZdj_TXT);
                prawy.find('.glownezdjecie').text($(prawy).index('.jFiler-item')+1);

                var div1 = $('#podglad_obrazy_glowne > a img').eq(0);
                var div2 = $('#podglad_obrazy_mini > a img').eq(0);

                var tdiv1 = div1.clone();
                var tdiv2 = div2.clone();

                div1.replaceWith(tdiv2);
                div2.replaceWith(tdiv1);
            }
            else
            {
                ten.find('.glownezdjecie').text( $(ten).index('.jFiler-item')+1 );
                prawy.find('.glownezdjecie').text( $(prawy).index('.jFiler-item')+1 );

                var div1 = $('#podglad_obrazy_mini > a img').eq($(ten).index('.jFiler-item')-1);
                var div2 = $('#podglad_obrazy_mini > a img').eq($(prawy).index('.jFiler-item')-1);

                var tdiv1 = div1.clone();
                var tdiv2 = div2.clone();

                div1.replaceWith(tdiv2);
                div2.replaceWith(tdiv1);
            }

        }

        $('#podglad_obrazy_mini').slick({
            infinite: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            variableWidth: true
        });
    });


    $(document).on('click', '.akcja1', function(){

        var index = $(this).index('.jFiler-item-trash-action');
        var indexx = tablicaLadujacychSie.indexOf( index );
        tablicaLadujacychSie.splice(indexx, 1);


        if(tablicaLadujacychSie.length == 0)
        {
            $('.idzDo_formularz_podsumowanie').show();
            $('.poczekaj_zdjecia').hide();
        }


        if(index ==0 && $('.glownezdjecie').length == 1)
        {
            $('#niemaZdjec').show();
            niemazdjec = true;
        }

        if( index == 0)
        {
            $('#podglad_obrazy_mini').slick('unslick');

            var div1 = $('#podglad_obrazy_glowne > a img').eq(0);
            var div2 = $('#podglad_obrazy_mini > a img').eq(0);

            var tdiv1 = div1.clone();
            var tdiv2 = div2.clone();

            div1.replaceWith(tdiv2);
            div2.replaceWith(tdiv1);

            $('#podglad_obrazy_mini').slick({
                infinite: false,
                slidesToShow: 4,
                slidesToScroll: 1,
                variableWidth: true
            });

            $('#podglad_obrazy_mini').slick('slickRemove',index);

            if($('#podglad_obrazy_mini a img').size() == 0 && $('#podglad_obrazy_glowne a img').size() == 0 )
                $('#podglad_obrazy_glowne').empty();

            //itemEl.next('.jFiler-item').find('.glownezdjecie').text('Głowne');
            for (var i = index+2; i < $('.glownezdjecie').length; i++) {
                $('.glownezdjecie').eq(i).text(i);
            }
        }
        else
        {
            for (var i = index; i < $('.glownezdjecie').length; i++) {
                $('.glownezdjecie').eq(i).text(i);
            }
            $('#podglad_obrazy_mini').slick('slickRemove',index-1);
        }


    });


</script>
