
<div class="ui united modal" id="formularz_podstawowe_edit">
    <i class="icon close"></i>
    <div class="header">
        <div class="ui header">
            <?php echo $TLUMACZENIA['profil102']; ?>
        </div>
        <div class="ui huge breadcrumb">
            <div class="section"><?php echo $TLUMACZENIA['profil72']; ?></div>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_adres2" ><?php echo $TLUMACZENIA['profil73']; ?></a>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_zdjecia2" ><?php echo $TLUMACZENIA['profil74']; ?></a>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_szczegoly2" ><?php echo $TLUMACZENIA['profil75']; ?></a>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_podsumowanie2" ><?php echo $TLUMACZENIA['profil76']; ?></a>
            <span class="poczekaj_zdjecia_edit" style="font-size: 90%;" ><i class="notched circle loading icon"></i>Uploading...</span>
        </div>
    </div>
    <div class="content" >

        <div class="ui labeled input" style="width: 90%;max-width: 300px;">
            <div class="ui label" style="width: 50%;font-size: 12px!important;line-height: 25px;">
                &nbsp;&nbsp;<?php echo $TLUMACZENIA['profil103']; ?>
            </div>
            <div class="ui fluid selection dropdown" style="width: 50%!important;">
                <input type="hidden" name="przedluzenie" id="przedluzenie">
                <i class="dropdown icon"></i>
                <div class="default text"><?php echo $TLUMACZENIA['profil104']; ?></div>
                <div class="menu">
                    <div class="item" data-value="1"><?php echo $TLUMACZENIA['profil105']; ?></div>
                    <div class="item" data-value="0"><?php echo $TLUMACZENIA['profil116']; ?></div>
                </div>
            </div>
        </div>

        <div class="ui divider"></div>

        <div class="ui form">
            <div class="field">
                <label><?php echo $TLUMACZENIA['profil78']; ?></label>
                <input maxlength="35" id="formularz_TYTUL_edit" type="text">
            </div>
            <div class="fields" >
                <div class="five wide field">
                    <label><?php echo $TLUMACZENIA['profil79']; ?></label>
                    <div class="ui selection dropdown" id="formularz_rodzajM_edit">
                        <input type="hidden" >
                        <div class="default text">Typ</div>
                        <i class="dropdown icon"></i>
                        <div class="menu" style="overflow:auto;max-height:300px;">
                            <div class="item" data-value="0">
                                <i class="home icon"></i>
                                <?php echo $TLUMACZENIA['wyszukiwarka6']; ?>
                            </div>
                            <div class="item" data-value="50">
                                <i class="building icon"></i>
                                <?php echo $TLUMACZENIA['wyszukiwarka7']; ?>
                            </div>
                            <div class="item" data-value="100">
                                <i class="bed icon"></i>
                                <?php echo $TLUMACZENIA['wyszukiwarka8']; ?>
                            </div>
                            <div class="item" data-value="150">
                                <i class="tree icon"></i>
                                <?php echo $TLUMACZENIA['wyszukiwarka9']; ?>
                            </div>
                            <div class="item" data-value="200">
                                <i class="car icon"></i>
                                <?php echo $TLUMACZENIA['wyszukiwarka10']; ?>
                            </div>
                            <div class="item" data-value="250">
                                <i class="travel icon"></i>
                                <?php echo $TLUMACZENIA['wyszukiwarka11']; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="six wide field">
                    <label><?php echo $TLUMACZENIA['profil81']; ?></label>
                    <div class="ui selection dropdown" id="formularz_rodzajT_edit">
                        <input type="hidden" >
                        <div class="default text">Typ</div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item" data-value="0">
                                <?php echo $TLUMACZENIA['wyszukiwarka13']; ?>
                            </div>
                            <div class="item" data-value="1">
                                <?php echo $TLUMACZENIA['wyszukiwarka14']; ?>
                            </div>
                            <div class="item" data-value="2">
                                <?php echo $TLUMACZENIA['wyszukiwarka15']; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="three wide field">
                    <label><?php echo $TLUMACZENIA['profil82']; ?></label>
                    <input type="text" maxlength="13" id="formularz_cena_edit" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                </div>
                <div class="two wide field">
                    <label><?php echo $TLUMACZENIA['profil83']; ?></label>
                    <div class="ui selection dropdown fluid" >
                        <input type="hidden" id="formularz_waluta_edit">
                        <div class="default text"><?php echo $TLUMACZENIA['profil80']; ?></div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item" data-value="0">
                                <?php echo $TLUMACZENIA['wyszukiwarka20']; ?>
                            </div>
                            <div class="item" data-value="1">
                                <?php echo $TLUMACZENIA['wyszukiwarka21']; ?>
                            </div>
                            <div class="item" data-value="2">
                                <?php echo $TLUMACZENIA['wyszukiwarka22']; ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="fields" style="margin-top: 15px;">
                <div class="nine wide field" >
                    <label><?php echo $TLUMACZENIA['profil84']; ?></label>
                    <textarea maxlength="350" rows="7" style="resize: none;" id="formularz_OPIS_edit"></textarea>
                </div>
                <div class="seven wide field" style="margin-top: 20px;">
                    <div class="ui toggle checkbox" id="uzyjGlownykontakt_edit">
                        <input type="checkbox" checked value="true" id="formularz_glownyKontakt_edit"  >
                        <label><?php echo $TLUMACZENIA['profil85']; ?></label>
                    </div>
                    <div class="disabled field" style="margin-top: 15px;">
                        <div class="inline field" >
                            <label><?php echo $TLUMACZENIA['przegladaj10']; ?>&nbsp;</label>
                            <input type="text" style="width: 70%;" id="formularz_imieKontakt_edit" >
                        </div>
                        <div class="inline field" >
                            <label><?php echo $TLUMACZENIA['przegladaj20']; ?>&nbsp;&nbsp;&nbsp;&nbsp;</label>
                            <input type="text" style="width: 70%;" id="formularz_emailKontakt_edit"  >
                        </div>
                        <div class="inline field" >
                            <label><?php echo $TLUMACZENIA['przegladaj12']; ?></label>
                            <input type="text" style="width: 70%;" id="formularz_telKontakt_edit" >
                        </div>
                    </div>
                </div>
            </div>




        </div>

    </div>
    <div class="actions">
        <div class="ui disabled deny button">
            <?php echo $TLUMACZENIA['profil86']; ?>
        </div>
        <div class="ui positive right labeled icon button idzDo_formularz_adres2">
            <?php echo $TLUMACZENIA['profil87']; ?>
            <i class="right arrow icon"></i>
        </div>
    </div>
</div>

<div class="ui united modal" id="formularz_adres_edit" >
    <i class="icon close"></i>
    <div class="header">
        <div class="ui header">
            <?php echo $TLUMACZENIA['profil102']; ?>
        </div>
        <div class="ui huge breadcrumb">
            <a class="section idzDo_formularz_podstawowe2" ><?php echo $TLUMACZENIA['profil72']; ?></a>
            <i class="right chevron icon divider"></i>
            <div class="section" ><?php echo $TLUMACZENIA['profil73']; ?></div>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_zdjecia2" ><?php echo $TLUMACZENIA['profil74']; ?></a>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_szczegoly2" ><?php echo $TLUMACZENIA['profil75']; ?></a>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_podsumowanie2" ><?php echo $TLUMACZENIA['profil76']; ?></a>
            <span class="poczekaj_zdjecia_edit" style="font-size: 90%;" ><i class="notched circle loading icon"></i>Uploading...</span>
        </div>
    </div>
    <div class="content">

        <div class="ui form">
            <div class="field">
                <label><?php echo $TLUMACZENIA['profil89']; ?></label>
                <div id="MAPA2_edit" style="width: 100%;height: 350px;z-index: 10"></div>
                <div class="ui fluid input" style="width: 100%;">
                    <input type="text" placeholder="<?php echo $TLUMACZENIA['wyszukiwarka3']; ?>" id="wyszukiwarka_mapa2_edit" style="width: 100%;">

                </div>
            </div>

            <div class="ui negative message" id="PUSTA_ULICA_edit" style="display: none;">
                <div class="header">
                    <?php echo $TLUMACZENIA['profil88']; ?>
                </div>
            </div>

            <div id="ADRESY_edit" style="margin-bottom: 30px;display: none;">
                <input type="text" name="xx" id="formularz_XX_edit" style="display: none;">
                <input type="text" name="yy" id="formularz_YY_edit" style="display: none;">

                <div class="fields">
                    <div class="six wide field">
                        <label><?php echo $TLUMACZENIA['profil90']; ?>&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        <input type="text" id="formularz_ulica_edit">
                    </div>
                    <div class="four wide field">
                        <label><?php echo $TLUMACZENIA['profil91']; ?></label>
                        <input type="text" id="formularz_miasto_edit">
                    </div>
                    <div class="six wide field">
                        <label><?php echo $TLUMACZENIA['profil92']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        <input type="text" id="formularz_kraj_edit">
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="actions">
        <div class="ui deny button idzDo_formularz_podstawowe2">
            <?php echo $TLUMACZENIA['profil86']; ?>
        </div>
        <div class="ui positive right labeled icon button idzDo_formularz_zdjecia2">
            <?php echo $TLUMACZENIA['profil87']; ?>
            <i class="right arrow icon"></i>
        </div>
    </div>
</div>

<div class="ui united modal" id="formularz_zdjecia_edit">
    <i class="icon close"></i>
    <div class="header">
        <div class="ui header">
            <?php echo $TLUMACZENIA['profil102']; ?>
        </div>
        <div class="ui huge breadcrumb">
            <a class="section idzDo_formularz_podstawowe2" ><?php echo $TLUMACZENIA['profil72']; ?></a>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_adres2" ><?php echo $TLUMACZENIA['profil73']; ?></a>
            <i class="right chevron icon divider"></i>
            <div class="section" ><?php echo $TLUMACZENIA['profil74']; ?></div>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_szczegoly2" ><?php echo $TLUMACZENIA['profil75']; ?></a>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_podsumowanie2" ><?php echo $TLUMACZENIA['profil76']; ?></a>
            <span class="poczekaj_zdjecia_edit" style="font-size: 90%;" ><i class="notched circle loading icon"></i>Uploading...</span>
        </div>
    </div>
    <div class="content">

        <div class="ui form">
            <div class="field">
                <label><?php echo $TLUMACZENIA['profil74']; ?>: </label>
                <input type="file" name="files[]" id="filer_input_edit" multiple="multiple">

                <ul class="jFiler-items-list jFiler-items-grid" id="obecnieDodaneimages">

                </ul>



            </div>
        </div>

    </div>
    <div class="actions">
        <div class="ui deny button idzDo_formularz_adres2">
            <?php echo $TLUMACZENIA['profil86']; ?>
        </div>
        <div class="ui positive right labeled icon button idzDo_formularz_szczegoly2">
            <?php echo $TLUMACZENIA['profil87']; ?>
            <i class="right arrow icon"></i>
        </div>
    </div>
</div>

<div class="ui united modal" id="formularz_szczegoly_edit">
    <i class="icon close"></i>
    <div class="header">
        <div class="ui header">
            <?php echo $TLUMACZENIA['profil102']; ?>
        </div>
        <div class="ui huge breadcrumb">
            <a class="section active idzDo_formularz_podstawowe2" ><?php echo $TLUMACZENIA['profil72']; ?></a>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_adres2" ><?php echo $TLUMACZENIA['profil73']; ?></a>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_zdjecia2" ><?php echo $TLUMACZENIA['profil74']; ?></a>
            <i class="right chevron icon divider"></i>
            <div class="section" ><?php echo $TLUMACZENIA['profil75']; ?></div>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_podsumowanie2" ><?php echo $TLUMACZENIA['profil76']; ?></a>
            <span class="poczekaj_zdjecia_edit" style="font-size: 90%;" ><i class="notched circle loading icon"></i>Uploading...</span>
        </div>
    </div>
    <div class="content">

        <div class="ui form">

            <div class="two fields">
                <div class="eight wide field disabled" id="rodzajZabudowyonczyoff_edit">
                    <label><?php echo $TLUMACZENIA['wyszukiwarka24']; ?></label>
                    <div class="ui selection dropdown fluid">
                        <input type="hidden" id="formularz_szczegoly_propertyBuildingType_edit" >
                        <div class="default text"><?php echo $TLUMACZENIA['profil80']; ?></div>
                        <i class="dropdown icon"></i>
                        <div class="menu" >
                            <div class="item rodzajZabudowyDom" data-value="0" ><?php echo $TLUMACZENIA['wyszukiwarka57'];?></div>
                            <div class="item rodzajZabudowyDom" data-value="1" ><?php echo $TLUMACZENIA['wyszukiwarka58'];?></div>
                            <div class="item rodzajZabudowyDom" data-value="2" ><?php echo $TLUMACZENIA['wyszukiwarka59'];?></div>
                            <div class="item rodzajZabudowyDom" data-value="3" ><?php echo $TLUMACZENIA['wyszukiwarka60'];?></div>
                            <div class="item rodzajZabudowyBlok" data-value="50" ><?php echo $TLUMACZENIA['wyszukiwarka61'];?></div>
                            <div class="item rodzajZabudowyBlok" data-value="51" ><?php echo $TLUMACZENIA['wyszukiwarka62'];?></div>
                            <div class="item rodzajZabudowyPokoj" data-value="100" ><?php echo $TLUMACZENIA['wyszukiwarka63'];?></div>
                            <div class="item rodzajZabudowyPokoj" data-value="101" ><?php echo $TLUMACZENIA['wyszukiwarka64'];?></div>
                            <div class="item rodzajZabudowyDzialka" data-value="150" ><?php echo $TLUMACZENIA['wyszukiwarka65'];?></div>
                            <div class="item rodzajZabudowyDzialka" data-value="151" ><?php echo $TLUMACZENIA['wyszukiwarka66'];?></div>
                            <div class="item rodzajZabudowyDzialka" data-value="152" ><?php echo $TLUMACZENIA['wyszukiwarka67'];?></div>
                            <div class="item rodzajZabudowyGaraz" data-value="200" ><?php echo $TLUMACZENIA['wyszukiwarka68'];?></div>
                            <div class="item rodzajZabudowyGaraz" data-value="201" ><?php echo $TLUMACZENIA['wyszukiwarka69'];?></div>
                            <div class="item rodzajZabudowyGaraz" data-value="202" ><?php echo $TLUMACZENIA['wyszukiwarka70'];?></div>
                            <div class="item rodzajZabudowyLokale" data-value="250" ><?php echo $TLUMACZENIA['wyszukiwarka71'];?></div>
                            <div class="item rodzajZabudowyLokale" data-value="251" ><?php echo $TLUMACZENIA['wyszukiwarka72'];?></div>
                            <div class="item rodzajZabudowyLokale" data-value="252" ><?php echo $TLUMACZENIA['wyszukiwarka73'];?></div>
                            <div class="item rodzajZabudowyLokale" data-value="253" ><?php echo $TLUMACZENIA['wyszukiwarka74'];?></div>
                            <div class="item rodzajZabudowyLokale" data-value="254" ><?php echo $TLUMACZENIA['wyszukiwarka75'];?></div>
                            <div class="item rodzajZabudowyLokale" data-value="255" ><?php echo $TLUMACZENIA['wyszukiwarka76'];?></div>
                        </div>
                    </div>
                </div>
                <div class="one wide field"></div>
                <div class="nine wide field">
                    <label>Typ ogrzewania:</label>
                    <div class="ui selection dropdown fluid">
                        <input type="hidden" id="formularz_szczegoly_propertyHeatingType_edit">
                        <div class="default text"></div>
                        <div class="default text"></div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item" data-value="0"><?php echo $TLUMACZENIA['wyszukiwarka32'];?></div>
                            <div class="item" data-value="1"><?php echo $TLUMACZENIA['wyszukiwarka31'];?></div>
                            <div class="item" data-value="2"><?php echo $TLUMACZENIA['wyszukiwarka33'];?></div>
                            <div class="item" data-value="3"><?php echo $TLUMACZENIA['wyszukiwarka78'];?></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="two fields">
                <div class="eight wide field">
                    <label><?php echo $TLUMACZENIA['przegladaj39'];?></label>
                    <div class="ui right labeled input">
                        <input  maxlength="12" type="text" id="formularz_szczegoly_propertyArea_edit" onkeypress='return validate(event)'>
                        <div class="ui basic label">
                            <?php echo $TLUMACZENIA['wyszukiwarka35'];?>
                        </div>
                    </div>
                </div>
                <div class="one wide field"></div>
                <div class="nine wide field">
                    <label><?php echo $TLUMACZENIA['przegladaj43'];?></label>
                    <input maxlength="4" type="text" id="formularz_szczegoly_propertyBuildYear_edit" onkeypress='return validate(event)' >
                </div>
            </div>

            <div class="four fields">
                <div class="four wide field">
                    <label><?php echo $TLUMACZENIA['przegladaj51'];?></label>
                    <input maxlength="4" name="rocznik" type="text" id="formularz_szczegoly_propertyMaxPerson_edit" onkeypress='return validate(event)' >
                </div>
                <div class="three wide field">
                    <label><?php echo $TLUMACZENIA['przegladaj50'];?></label>
                    <input maxlength="4" name="rocznik" type="text" id="formularz_szczegoly_propertyRoomCount_edit" onkeypress='return validate(event)' >
                </div>
                <div class="one wide field"></div>
                <div class="three wide field">
                    <label><?php echo $TLUMACZENIA['wyszukiwarka38'];?></label>
                    <input maxlength="3" name="rocznik" type="text" id="formularz_szczegoly_propertyFloor_edit" onkeypress='return validate(event)' >
                </div>

            </div>

            <h3 class="ui dividing header">
                <?php echo $TLUMACZENIA['profil93'];?>
            </h3>
            <!--            <div class="ui divider" style="margin-top: 30px;"></div>-->

            <div class="fields">
                <div class="three wide field">
                    <label><?php echo $TLUMACZENIA['wyszukiwarka40'];?></label>
                    <div class="ui selection dropdown fluid">
                        <input type="hidden" id="formularz_szczegoly_propertyIsFurnished_edit">
                        <div class="default text">Typ</div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item" data-value="true">
                                <?php echo $TLUMACZENIA['wyszukiwarka41']; ?>
                            </div>
                            <div class="item" data-value="false">
                                <?php echo $TLUMACZENIA['wyszukiwarka42']; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="three wide field">
                    <label><?php echo $TLUMACZENIA['wyszukiwarka49'];?></label>
                    <div class="ui selection dropdown fluid">
                        <input type="hidden" id="formularz_szczegoly_propertyHasBalcony_edit">
                        <div class="default text">Typ</div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item" data-value="true">
                                <?php echo $TLUMACZENIA['wyszukiwarka41']; ?>
                            </div>
                            <div class="item" data-value="false">
                                <?php echo $TLUMACZENIA['wyszukiwarka42']; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="three wide field">
                    <label><?php echo $TLUMACZENIA['wyszukiwarka44'];?></label>
                    <div class="ui selection dropdown fluid">
                        <input type="hidden" id="formularz_szczegoly_propertyHasLift_edit">
                        <div class="default text">Typ</div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item" data-value="true">
                                <?php echo $TLUMACZENIA['wyszukiwarka41']; ?>
                            </div>
                            <div class="item" data-value="false">
                                <?php echo $TLUMACZENIA['wyszukiwarka42']; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="three wide field">
                    <label><?php echo $TLUMACZENIA['wyszukiwarka45'];?></label>
                    <div class="ui selection dropdown fluid">
                        <input type="hidden" id="formularz_szczegoly_propertyHasBasement_edit">
                        <div class="default text">Typ</div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item" data-value="true">
                                <?php echo $TLUMACZENIA['wyszukiwarka41']; ?>
                            </div>
                            <div class="item" data-value="false">
                                <?php echo $TLUMACZENIA['wyszukiwarka42']; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="three wide field">
                    <label><?php echo $TLUMACZENIA['wyszukiwarka48'];?></label>
                    <div class="ui selection dropdown fluid">
                        <input type="hidden" id="formularz_szczegoly_propertyHasParking_edit">
                        <div class="default text">Typ</div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item" data-value="true">
                                <?php echo $TLUMACZENIA['wyszukiwarka41']; ?>
                            </div>
                            <div class="item" data-value="false">
                                <?php echo $TLUMACZENIA['wyszukiwarka42']; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="fields">
                <div class="three wide field">
                    <label><?php echo $TLUMACZENIA['wyszukiwarka51'];?></label>
                    <div class="ui selection dropdown fluid">
                        <input type="hidden" id="formularz_szczegoly_propertyHasGarden_edit">
                        <div class="default text">Typ</div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item" data-value="true">
                                <?php echo $TLUMACZENIA['wyszukiwarka41']; ?>
                            </div>
                            <div class="item" data-value="false">
                                <?php echo $TLUMACZENIA['wyszukiwarka42']; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="three wide field">
                    <label><?php echo $TLUMACZENIA['wyszukiwarka50'];?></label>
                    <div class="ui selection dropdown fluid">
                        <input type="hidden" id="formularz_szczegoly_propertyHasClima_edit">
                        <div class="default text">Typ</div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item" data-value="true">
                                <?php echo $TLUMACZENIA['wyszukiwarka41']; ?>
                            </div>
                            <div class="item" data-value="false">
                                <?php echo $TLUMACZENIA['wyszukiwarka42']; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="three wide field">
                    <label><?php echo $TLUMACZENIA['wyszukiwarka46'];?></label>
                    <div class="ui selection dropdown fluid">
                        <input type="hidden" id="formularz_szczegoly_propertyAllowSmoking_edit">
                        <div class="default text">Typ</div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item" data-value="true">
                                <?php echo $TLUMACZENIA['wyszukiwarka41']; ?>
                            </div>
                            <div class="item" data-value="false">
                                <?php echo $TLUMACZENIA['wyszukiwarka42']; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="three wide field">
                    <label><?php echo $TLUMACZENIA['wyszukiwarka47'];?></label>
                    <div class="ui selection dropdown fluid">
                        <input type="hidden" id="formularz_szczegoly_propertyAllowPets_edit">
                        <div class="default text">Typ</div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item" data-value="true">
                                <?php echo $TLUMACZENIA['wyszukiwarka41']; ?>
                            </div>
                            <div class="item" data-value="false">
                                <?php echo $TLUMACZENIA['wyszukiwarka42']; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>

    </div>
    <div class="actions">
        <div class="ui deny button idzDo_formularz_zdjecia2">
            <?php echo $TLUMACZENIA['profil86']; ?>
        </div>
        <div class="ui positive right labeled icon button idzDo_formularz_podsumowanie2">
            <?php echo $TLUMACZENIA['profil87']; ?>
            <i class="right arrow icon"></i>
        </div>
    </div>
</div>

<div class="ui united modal" id="formularz_podsumowanie_edit">
    <i class="icon close"></i>
    <div class="header">
        <div class="ui header">
            <?php echo $TLUMACZENIA['profil102']; ?>
        </div>
        <div class="ui huge breadcrumb">
            <a class="section active idzDo_formularz_podstawowe2" ><?php echo $TLUMACZENIA['profil72']; ?></a>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_adres2" ><?php echo $TLUMACZENIA['profil73']; ?></a>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_zdjecia2" ><?php echo $TLUMACZENIA['profil74']; ?></a>
            <i class="right chevron icon divider"></i>
            <a class="section idzDo_formularz_szczegoly2"><?php echo $TLUMACZENIA['profil75']; ?></a>
            <i class="right chevron icon divider"></i>
            <div class="section" ><?php echo $TLUMACZENIA['profil76']; ?></div>
        </div>
    </div>
    <div class="content">

        <form class="ui form" id="FORMULARZ_DODAWANIA_edit">
            <div class="field" style="display: none;">
                <label><?php echo $TLUMACZENIA['profil78'];?></label>
                <input name="tytul" id="set_formularz_TYTUL_edit" type="text">
                <input name="opis" id="set_formularz_OPIS_edit" type="text">
                <input name="rodzajT" id="set_formularz_RodzajT_edit" type="text">
                <input name="rodzajM" id="set_formularz_RodzajM_edit" type="text">
                <input name="cena" id="set_formularz_Cena_edit" type="text">
                <input name="waluta" id="set_formularz_Waluta_edit" type="text">
                <input name="ulica" id="set_formularz_Ulica_edit" type="text">
                <input name="miasto" id="set_formularz_Miasto_edit" type="text">
                <input name="kraj" id="set_formularz_Kraj_edit" type="text">
                <input name="xx" id="set_formularz_XX_edit" type="text">
                <input name="yy" id="set_formularz_YY_edit" type="text">

                <input name="glownyKontakt" id="set_formularz_glownyKontakt_edit" type="text">
                <input name="imieKontakt" id="set_formularz_imieKontakt_edit" type="text">
                <input name="emailKontakt" id="set_formularz_emailKontakt_edit" type="text">
                <input name="telKontakt" id="set_formularz_telKontakt_edit" type="text">


                <!-- SZCEGOLYYYYYY-->
                <input name="propertyBuildingType" id="set_formularz_szczegoly_propertyBuildingType_edit" type="text">
                <input name="propertyMinPerson" id="set_formularz_szczegoly_propertyMinPerson_edit" type="text">
                <input name="propertyMaxPerson" id="set_formularz_szczegoly_propertyMaxPerson_edit" type="text">
                <input name="propertyHeatingType" id="set_formularz_szczegoly_propertyHeatingType_edit" type="text">
                <input name="propertyArea" id="set_formularz_szczegoly_propertyArea_edit" type="text">
                <input name="propertyRoomCount" id="set_formularz_szczegoly_propertyRoomCount_edit" type="text">
                <input name="propertyFloor" id="set_formularz_szczegoly_propertyFloor_edit" type="text">
                <input name="propertyBuildYear" id="set_formularz_szczegoly_propertyBuildYear_edit" type="text">
                <input name="propertyIsFurnished" id="set_formularz_szczegoly_propertyIsFurnished_edit" type="text">
                <input name="propertyHasBalcony" id="set_formularz_szczegoly_propertyHasBalcony_edit" type="text">
                <input name="propertyHasLift" id="set_formularz_szczegoly_propertyHasLift_edit" type="text">
                <input name="propertyHasBasement" id="set_formularz_szczegoly_propertyHasBasement_edit" type="text">
                <input name="propertyHasParking" id="set_formularz_szczegoly_propertyHasParking_edit" type="text">
                <input name="propertyAllowPets" id="set_formularz_szczegoly_propertyAllowPets_edit" type="text">
                <input name="propertyHasGarden" id="set_formularz_szczegoly_propertyHasGarden_edit" type="text">
                <input name="propertyHasClima" id="set_formularz_szczegoly_propertyHasClima_edit" type="text">
                <input name="propertyAllowSmoking" id="set_formularz_szczegoly_propertyAllowSmoking_edit" type="text">




            </div>

            <div id="PODGLAD_edit" style="height: auto;">
                <div class="two fields" >
                    <div class="field" style="margin-right: 15px;">
                        <div id="niemaZdjec_edit" style="width: 400px;height: 300px;background: url('img/noimage.png');background-size: cover;" >
<!--                            <div class="header">ie ma zdjec ziom</div>-->
                        </div>
                        <div class="popup-gallery"  style="width: 87%;">
                            <div id="podglad_obrazy_glowne_edit" ></div>
                            <div id="podglad_obrazy_mini_edit" class="MINIATURKI" ></div>
                        </div>
                    </div>
                    <div class="field">
                        <div style="left: 48%;top: 20px;">
                            <h3 class="ui header">
                                <div id="podglad_tytul_edit"><?php echo $TLUMACZENIA['profil94'];?></div>
                            </h3>

                            <h1 class="ui header" style="margin-top: -10px;">
                                <span id="podglad_ulica_edit"><?php echo $TLUMACZENIA['profil90'];?></span>
                                <div class="sub header"><span id="podglad_kraj_edit"><?php echo $TLUMACZENIA['profil92'];?></span> - <span id="podglad_miasto_edit"><?php echo $TLUMACZENIA['profil91'];?></span> </div>
                            </h1>

                            <div class="ui clearing" style="margin-bottom: 50px;">
                                <h3 class="ui left floated header" id="podglad_offertyp_edit"><?php echo $TLUMACZENIA['profil95'];?></h3>
                                <h5 class="ui left floated header" style="margin-top: 2px;"><?php echo $TLUMACZENIA['przegladaj4'];?></h5>
                                <h3 class="ui left floated header" id="podglad_typ_edit"><?php echo $TLUMACZENIA['profil96'];?></h3>

                                <h2 class="ui right floated header" style="margin-left: 35px;margin-top: -5px;">
                                    <span id="podglad_cena_edit">100</span> <span id="podglad_cena_waluta_edit"></span>
                                </h2>
                            </div>

                            <div class="ui accordion" style="height: 100%;">
                                <div class="title">
                                    <i class="dropdown icon"></i>
                                    <?php echo $TLUMACZENIA['przegladaj7']; ?>
                                </div>
                                <div class="content">
                                    <div class="ui divided middle aligned selection list" id="podglad_szczegoly_edit" ></div>
                                </div>
                                <div class="title">
                                    <i class="dropdown icon"></i>
                                    <?php echo $TLUMACZENIA['przegladaj8']; ?>
                                </div>
                                <div class="content" id="podglad_opis_edit"></div>
                                <div class="title">
                                    <i class="dropdown icon"></i>
                                    <?php echo $TLUMACZENIA['przegladaj9']; ?>
                                </div>
                                <div class="content" id="podglad_kontakt_edit">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


            </div>

            <div id="ERORKIFORM_edit" style="margin-top: 30px;" class="ui error message"></div>

            <div class="ui middle aligned divided list" id="WYLICZ_KOSZTA_edit" style="width: 97%;margin-left: 1%;margin-top: 20px;margin-bottom: 20px;">

                <div class="two ui buttons" id="pasekDodawnieitd_przydodwaniau_edit">
                    <div class="ui labeled button" id="MOZNA_KUPOWAC_edit">
                        <div class="ui green button" style="width: 70%;">
                            <i class="checkmark icon"></i> <?php echo $TLUMACZENIA['profil98']; ?>
                        </div>
                        <a class="ui basic green left pointing label" style="width: 30%;">
                            <span style="width: 100%;" class="ile_mam_hajsu"></span>
                        </a>
                    </div>
                    <div class="ui labeled button" id="NIE_MOZNA_KUPOWAC_edit">
                        <div class="ui red button" style="width: 70%;">
                            <i class="remove icon"></i> <?php echo $TLUMACZENIA['profil99']; ?>
                        </div>
                        <a class="ui basic red left pointing label" style="width: 30%;">
                            <span style="width: 100%;" class="ile_mam_hajsu"></span>
                        </a>
                    </div>
                    <div class="ui labeled button" id="KOSZT_OPERACJI_edit" tabindex="0">
                        <div class="ui basic button" style="width: 70%;">
                            <i class="money icon"></i> <?php echo $TLUMACZENIA['profil100']; ?>
                        </div>
                        <a class="ui basic left pointing label" style="width: 30%;">
                            <span style="width: 100%;" id="koszt_suma_edit"></span>
                        </a>
                    </div>

                </div>
                <div class="ui pointing label">
                    <?php echo $TLUMACZENIA['profil101']; ?>
                </div>
            </div>


<!--            <div class="ui button fluid primary" style="margin-top: 10px;" id="WYSLIJ_FORMULARZ_edit">EDYTUJ</div>-->
        </form>






    </div>
    <div class="actions">
        <div class="ui deny button idzDo_formularz_szczegoly2">
            <?php echo $TLUMACZENIA['profil86']; ?>
        </div>
        <div class="ui positive right labeled icon button" id="WYSLIJ_FORMULARZ_edit">
            <?php echo $TLUMACZENIA['profil115']; ?>
            <i class="checkmark icon"></i>
        </div>
    </div>
</div>



<script>
    var rodzjaBtms;

    $(document).ready(function() {

        var options2 = {
            url: function(phrase) {
                var link = WYSZUKIWARKA_LINK + phrase;
                return link;
            },
            getValue: function(element) {
                var miasto = "";
                var ulica = "";
                var ulicaNR = "";

                if(element.address.city != undefined)
                    miasto = element.address.city ;
                else if(element.address.town != undefined)
                    miasto = element.address.town ;

                if(element.address.road != undefined)
                    ulica = element.address.road ;

                if(element.address.house_number != undefined)
                    ulicaNR = element.address.house_number ;


                return ""+miasto+" "+ulica+" "+ulicaNR;

            },
            list: {
                onClickEvent: function() {
                    var value = $("#wyszukiwarka_mapa2_edit").getSelectedItemData();

                    wyszukiwanie_google_formularz(value, MAPA3,false);

                }
            },
            adjustWidth: false,
            minCharNumber: 2

        };
        $("#wyszukiwarka_mapa2_edit").easyAutocomplete(options2);

        $('#wyszukiwarka_mapa2_edit').keypress(function(event){
            if(event.keyCode == 13){
                $(this).trigger('blur')
                $("#wyszukiwarka_mapa2_edit").val($('#eac-container-wyszukiwarka_mapa2_edit ul li').eq(0).text()).trigger("change");

                $.get( WYSZUKIWARKA_LINK+$('#eac-container-wyszukiwarka_mapa2_edit ul li').eq(0).text(), function(data) {
                    wyszukiwanie_google(data[0], MAPA3,false);
                })
            }
        });


        $('#formularz_podstawowe_edit').modal('attach events', '.idzDo_formularz_podstawowe2');
        $('#formularz_adres_edit').modal('attach events', '.idzDo_formularz_adres2');
        //$('#formularz_zdjecia_edit').modal('attach events', '.idzDo_formularz_zdjecia2');
        $('#formularz_szczegoly_edit').modal('attach events', '.idzDo_formularz_szczegoly2');
        $('#formularz_podsumowanie_edit').modal('attach events', '.idzDo_formularz_podsumowanie2');



        $('.idzDo_formularz_podsumowanie2').on('click', function(){

            $('#WYLICZ_KOSZTA_edit').hide();

            var getTytul = $('#formularz_TYTUL_edit').val();
            var getOpis = $('#formularz_OPIS_edit').val();
            var getRodzajT = $('#formularz_rodzajT_edit').dropdown('get value');
            var getRodzajM = $('#formularz_rodzajM_edit').dropdown('get value');

            var getCena = $('#formularz_cena_edit').val();
            var getWaluta = $('#formularz_waluta_edit').val();

            var getUlica = $('#formularz_ulica_edit').val();
            var getMiasto = $('#formularz_miasto_edit').val();
            var getKraj = $('#formularz_kraj_edit').val();

            var getXX = $('#formularz_XX_edit').val();
            var getYY = $('#formularz_YY_edit').val();

            var glownyKontakt = $('#formularz_glownyKontakt_edit').val();
            var imieKontakt = $('#formularz_imieKontakt_edit').val();
            var emailKontakt = $('#formularz_emailKontakt_edit').val();
            var telKontakt = $('#formularz_telKontakt_edit').val();

            photos_edycja = $("input[name='photos_edycja']").map(function(){
                if($(this).hasClass('zmniusem'))
                    return "-"+$(this).val();
                else
                    return $(this).val();
            }).get();

            $('#set_formularz_TYTUL_edit').val( getTytul );
            $('#set_formularz_OPIS_edit').val( getOpis );
            $('#set_formularz_RodzajT_edit').val( getRodzajT );
            $('#set_formularz_RodzajM_edit').val( getRodzajM );

            $('#set_formularz_Cena_edit').val( getCena );
            $('#set_formularz_Waluta_edit').val( getWaluta );

            $('#set_formularz_Ulica_edit').val( getUlica );
            $('#set_formularz_Miasto_edit').val( getMiasto );
            $('#set_formularz_Kraj_edit').val( getKraj );

            $('#set_formularz_XX_edit').val( getXX );
            $('#set_formularz_YY_edit').val( getYY );

            $('#set_formularz_glownyKontakt_edit').val( glownyKontakt );
            $('#set_formularz_imieKontakt_edit').val( imieKontakt );
            $('#set_formularz_emailKontakt_edit').val( emailKontakt );
            $('#set_formularz_telKontakt_edit').val( telKontakt );

            //SZCZEGOLY
            if($('#formularz_szczegoly_propertyBuildingType_edit').val()!="")
                $('#set_formularz_szczegoly_propertyBuildingType_edit').val( $('#formularz_szczegoly_propertyBuildingType_edit').val() )

            if($('#formularz_szczegoly_propertyBuildingType_edit').val()!="")
                $('#set_formularz_szczegoly_propertyMinPerson_edit').val( $('#formularz_szczegoly_propertyBuildingType_edit').val() )

            if($('#formularz_szczegoly_propertyMinPerson_edit').val()!="")
                $('#set_formularz_szczegoly_propertyMinPerson_edit').val( $('#formularz_szczegoly_propertyMinPerson_edit').val() )

            if($('#formularz_szczegoly_propertyMaxPerson_edit').val()!="")
                $('#set_formularz_szczegoly_propertyMaxPerson_edit').val( $('#formularz_szczegoly_propertyMaxPerson_edit').val() )

            if($('#formularz_szczegoly_propertyHeatingType_edit').val()!="")
                $('#set_formularz_szczegoly_propertyHeatingType_edit').val( $('#formularz_szczegoly_propertyHeatingType_edit').val() )

            if($('#formularz_szczegoly_propertyArea_edit').val()!="")
                $('#set_formularz_szczegoly_propertyArea_edit').val( $('#formularz_szczegoly_propertyArea_edit').val() )

            if($('#formularz_szczegoly_propertyRoomCount_edit').val()!="")
                $('#set_formularz_szczegoly_propertyRoomCount_edit').val( $('#formularz_szczegoly_propertyRoomCount_edit').val() )

            if($('#formularz_szczegoly_propertyFloor_edit').val()!="")
                $('#set_formularz_szczegoly_propertyFloor_edit').val( $('#formularz_szczegoly_propertyFloor_edit').val() )

            if($('#formularz_szczegoly_propertyBuildYear_edit').val()!="")
                $('#set_formularz_szczegoly_propertyBuildYear_edit').val( $('#formularz_szczegoly_propertyBuildYear_edit').val() )

            if($('#formularz_szczegoly_propertyIsFurnished_edit').val()!="")
                $('#set_formularz_szczegoly_propertyIsFurnished_edit').val( $('#formularz_szczegoly_propertyIsFurnished_edit').val() )

            if($('#formularz_szczegoly_propertyHasBalcony_edit').val()!="")
                $('#set_formularz_szczegoly_propertyHasBalcony_edit').val( $('#formularz_szczegoly_propertyHasBalcony_edit').val() )

            if($('#formularz_szczegoly_propertyHasLift_edit').val()!="")
                $('#set_formularz_szczegoly_propertyHasLift_edit').val( $('#formularz_szczegoly_propertyHasLift_edit').val() )

            if($('#formularz_szczegoly_propertyHasBasement_edit').val()!="")
                $('#set_formularz_szczegoly_propertyHasBasement_edit').val( $('#formularz_szczegoly_propertyHasBasement_edit').val() )

            if($('#formularz_szczegoly_propertyHasParking_edit').val()!="")
                $('#set_formularz_szczegoly_propertyHasParking_edit').val( $('#formularz_szczegoly_propertyHasParking_edit').val() )

            if($('#formularz_szczegoly_propertyAllowPets_edit').val()!="")
                $('#set_formularz_szczegoly_propertyAllowPets_edit').val( $('#formularz_szczegoly_propertyAllowPets_edit').val() )

            if($('#formularz_szczegoly_propertyHasGarden_edit').val()!="")
                $('#set_formularz_szczegoly_propertyHasGarden_edit').val( $('#formularz_szczegoly_propertyHasGarden_edit').val() )

            if($('#formularz_szczegoly_propertyHasClima_edit').val()!="")
                $('#set_formularz_szczegoly_propertyHasClima_edit').val( $('#formularz_szczegoly_propertyHasClima_edit').val() )

            if($('#formularz_szczegoly_propertyAllowSmoking_edit').val()!="")
                $('#set_formularz_szczegoly_propertyAllowSmoking_edit').val( $('#formularz_szczegoly_propertyAllowSmoking_edit').val() )


            //console.log( intToHeating($('#formularz_szczegoly_propertyHeatingType').val()) )
            $('#podglad_szczegoly_edit').empty();
            if($('#formularz_szczegoly_propertyBuildingType_edit').val() != "") $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj38'],intToRodzajBT($('#formularz_szczegoly_propertyBuildingType_edit').val()),''));
            $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj39'],$('#formularz_szczegoly_propertyArea_edit').val(),TLUMACZENIA['wyszukiwarka35']));
            if($('#formularz_szczegoly_propertyHeatingType_edit').val() != "") $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj40'],intToHeating($('#formularz_szczegoly_propertyHeatingType_edit').val()),''));
            $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj41'],$('#formularz_szczegoly_propertyHasBasement_edit').val(),''));
            $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj42'],$('#formularz_szczegoly_propertyHasBalcony_edit').val(),''));
            $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj43'],$('#formularz_szczegoly_propertyBuildYear_edit').val(),''));
            $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj44'],$('#formularz_szczegoly_propertyHasClima_edit').val(),''));
            $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj45'],$('#formularz_szczegoly_propertyFloor_edit').val(),''));
            $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj46'],$('#formularz_szczegoly_propertyIsFurnished_edit').val(),''));
            $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj47'],$('#formularz_szczegoly_propertyHasGarden_edit').val(),''));
            $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj48'],$('#formularz_szczegoly_propertyHasLift_edit').val(),''));
            $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj49'],$('#formularz_szczegoly_propertyAllowPets_edit').val(),''));
            $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj50'],$('#formularz_szczegoly_propertyRoomCount_edit').val(),''));
            $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj51'],$('#formularz_szczegoly_propertyMaxPerson_edit').val(),''));
            $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj52'],$('#formularz_szczegoly_propertyHasParking_edit').val(),''));


            $('#podglad_kontakt_edit').html('<table class="ui very basic table" >'+
                '<tbody>'+
                '<tr>'+
                '<td><i class="user icon"></i>'+TLUMACZENIA['przegladaj10']+'</td>'+
                '<td><span>'+$('#formularz_imieKontakt_edit').val()+'</span></td>'+
                '</tr>'+
                '<tr>'+
                '<td><i class="mail outline icon"></i>'+TLUMACZENIA['przegladaj20']+'</td>'+
                '<td><span>'+$('#formularz_emailKontakt_edit').val()+'</span></td>'+
                '</tr>'+
                '<tr>'+
                '<td><i class="call icon"></i>'+TLUMACZENIA['przegladaj12']+'</td>'+
                '<td><span>'+$('#formularz_telKontakt_edit').val()+'</span></td>'+
                '</tr>'+
                '</tbody>'+
                '</table>')

            if(getTytul!="") $('#podglad_tytul_edit').html(getTytul);
            if(getOpis!="") $('#podglad_opis_edit').html(getOpis);
            if(getRodzajT!="") $('#podglad_typ_edit').html(intToRodzajT(getRodzajT));
            if(getRodzajM!="") $('#podglad_offertyp_edit').html(intToRodzajM(getRodzajM));

            if(getCena!="") $('#podglad_cena_edit').html(getCena);

            if(getWaluta!="")
            {
                if(getRodzajT == 0)
                    $('#podglad_cena_waluta_edit').html('<sup style="font-size: 70%;">'+intToWaluta(getWaluta)+'</sup>&frasl;<sub style="font-size: 60%;">'+TLUMACZENIA['przegladaj5']+'</sub>');
                else if(getRodzajT == 1)
                    $('#podglad_cena_waluta_edit').html('<span style="font-size: 70%;">'+intToWaluta(getWaluta)+'</span>');
                else if(getRodzajT == 2)
                    $('#podglad_cena_waluta_edit').html('<sup style="font-size: 70%;">'+intToWaluta(getWaluta)+'</sup>&frasl;<sub style="font-size: 60%;">'+TLUMACZENIA['przegladaj6']+'</sub>');

            }

            if(getUlica!="") $('#podglad_ulica_edit').html(getUlica);
            if(getMiasto!="") $('#podglad_miasto_edit').html(getMiasto);
            if(getKraj!="") $('#podglad_kraj_edit').html(getKraj);


            $('#FORMULARZ_DODAWANIA_edit').submit();



        });

        var OBIEKT_edit,OBIEKT_edit2;
        $('#WYSLIJ_FORMULARZ_edit').on('click', function() {
            OBIEKT_edit["photos"] = photos_edycja;
            OBIEKT_edit["id"] = id_edytowanego;
            OBIEKT_edit["przedluzenie"] = $('#przedluzenie').val();
            OBIEKT_edit = JSON.stringify(OBIEKT_edit).replace(new RegExp("\\\\", "g"), "");

            $('#LADOWANIE_CZEKENIE').addClass('active');

            if(DEBUGOWANIE)
                console.log( OBIEKT_edit )

            $.ajax({
                url: SERWER+'person/offer/edit?id='+id_edytowanego,
                type: "POST",
                contentType: "application/json",
                data : OBIEKT_edit,
                headers : {
                    'X-Auth-Token' : localStorage.getItem('X-Auth-Token')
                },
                beforeSend : function(data){
                },
                success : function(data){
                    //location.reload();
                },
                error: function(data)
                {
                    $('#LADOWANIE_CZEKENIE').removeClass('active');
                }
            });

        });



        $('#FORMULARZ_DODAWANIA_edit').form({
            fields: {
                tytul: { identifier: 'tytul',
                    rules: [{ type   : 'empty',
                        prompt : TLUMACZENIA['profil60'] }]},
                opis: { identifier: 'opis',
                    rules: [{ type   : 'empty',
                        prompt : TLUMACZENIA['profil61'] }]},
                rodzajT: { identifier: 'rodzajT',
                    rules: [{ type   : 'empty',
                        prompt : TLUMACZENIA['profil62'] }]},
                rodzajM: { identifier: 'rodzajT',
                    rules: [{ type   : 'empty',
                        prompt : TLUMACZENIA['profil63'] }]},
                cena: { identifier: 'cena',
                    rules: [{ type   : 'empty',
                        prompt : TLUMACZENIA['profil64'] }]},
                waluta: { identifier: 'waluta',
                    rules: [{ type   : 'empty',
                        prompt : TLUMACZENIA['profil65'] }]},
                ulica: { identifier: 'ulica',
                    rules: [{ type   : 'empty',
                        prompt : TLUMACZENIA['profil66'] }]},
                miasto: { identifier: 'miasto',
                    rules: [{ type   : 'empty',
                        prompt : TLUMACZENIA['profil67'] }]},
                kraj: { identifier: 'kraj',
                    rules: [{ type   : 'empty',
                        prompt : TLUMACZENIA['profil68'] }]}
            },
            onFailure : function(formErrors, fields)
            {
                $('#WYSLIJ_FORMULARZ_edit').addClass('disabled');
                $('#WYLICZ_KOSZTA_edit').hide();

                return false;
            },
            onSuccess : function(event, fields) {

                if($('#przedluzenie').val() == "")
                    $('#przedluzenie').val(0)

                $('#WYSLIJ_FORMULARZ_edit').removeClass('disabled');
                OBIEKT_edit = fields;
                OBIEKT_edit2 = fields;
                OBIEKT_edit2["photos"] = photos_edycja;
                OBIEKT_edit2["przedluzenie"] = $('#przedluzenie').val();
                OBIEKT_edit2["id"] = id_edytowanego;
                OBIEKT_edit2 = JSON.stringify(OBIEKT_edit2).replace(new RegExp("\\\\", "g"), "");


                $.ajax({
                    url: SERWER + 'person/offer/edit-valuation',
                    type: "POST",
                    contentType: "application/json",
                    data: OBIEKT_edit2,
                    headers: {
                        'X-Auth-Token': localStorage.getItem('X-Auth-Token')
                    },
                    success: function (data) {
//                    console.log(data)
                        $('.kosztdousuniecia').remove();

                        for(var i = 0; i < data.rows.length; ++i) {
                            $('#WYLICZ_KOSZTA_edit').prepend('<div class="item kosztdousuniecia" style="width: 96%;padding-bottom: 20px;padding-top: 20px;">'+
                                '<div class="right floated content">'+
                                data.rows[i].points+' '+
                                '</div>'+
                                '<div class="content">'+
                                intToProdukt(data.rows[i].productType)+':'+
                                '</div>'+
                                '</div>');
                        }
                        if(data.rows.length != 0)
                            $('#WYLICZ_KOSZTA_edit').prepend('<h3 class="ui header kosztdousuniecia"  style="margin-bottom: 0px;" > '+TLUMACZENIA['profil70']+' </h3>');

                        $('#koszt_suma_edit').text(data.sum);
                        var koszt_suma_edit = $('#koszt_suma_edit').text();
                        $('.ile_mam_hajsu').text($('#IloscPunktow').text());
                        if( parseInt($('#IloscPunktow').text()) >= parseInt(koszt_suma_edit) )
                        {
                            $('#MOZNA_KUPOWAC_edit').show();
                            $('#NIE_MOZNA_KUPOWAC_edit').hide();
                            $('#NIE_MOZNA_KUPOWAC_edit').parent().next('.label').hide();
                            $('#WYSLIJ_FORMULARZ_edit').removeClass('disabled');
                        }
                        else
                        {
                            moge_kupowac = false;
                            $('#MOZNA_KUPOWAC_edit').hide();
                            $('#NIE_MOZNA_KUPOWAC_edit').show();

                        }
                        $('#WYLICZ_KOSZTA_edit').show();


                        return false;
                    },
                    error: function (data) {
                    }
                });
                return false;
            }
        }) ;



        $('#uzyjGlownykontakt').checkbox({
            onChecked: function () {
                $(this).parent().parent().find('.field').addClass('disabled');
                $('#formularz_imieKontakt').val( $('#imieInazwiskoMoj_input').val() );
                $('#formularz_emailKontakt').val( $('#telefonMoj_input').val() );
                $('#formularz_telKontakt').val( $('#emailMoj_input').val() );
            },
            onUnchecked: function () {
                $(this).parent().parent().find('.field').removeClass('disabled');
            }
        });

        $("#filer_input_edit").change( handleFileSelectEdit );

        $('.poczekaj_zdjecia_edit').hide();

        $('#podglad_obrazy_mini_edit').slick({
            infinite: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            variableWidth: true
        });

        $('.popup-gallery').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: TLUMACZENIA['przegladaj59']+' #%curr%...',
            tClose: TLUMACZENIA['przegladaj65'],
            mainClass: 'mfp-img-mobile',
            gallery: {
                tPrev: TLUMACZENIA['przegladaj62'], // Alt text on left arrow
                tNext: TLUMACZENIA['przegladaj63'], // Alt text on right arrow
                tCounter: '%curr% '+TLUMACZENIA['przegladaj64']+' %total%', // Markup for "1 of 7" counter
                enabled: true,
                navigateByImgClick: true,
                preload: [0,1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">'+TLUMACZENIA['przegladaj60']+' #%curr%</a> '+TLUMACZENIA['przegladaj61'],
                titleSrc: function(item) {
                    return item.el.attr('title');
                }
            }
        });

    });

    var getXX,getYY;

    var id_edytowanego;
    var zdjeciapodgladwoezaladwoano = false;
    $(document).on('click', '.editdodane', function(){
        var idektegogowna = $(this).parent().parent().find('.idtegogowna').text();
        id_edytowanego = idektegogowna;

        $.get(SERWER+'offer/get?id='+idektegogowna,function(data){

            var getTytul = data.title;
            var getOpis = data.description;
            var getRodzajT = data.offerType;
            var getRodzajM = data.property.propertyType;
            rodzjaBtms = getRodzajM;

            var getCena = data.price.value;
            var getWaluta = data.price.currency;

            var getUlica = data.property.address.street;
            var getMiasto = data.property.address.city;
            var getKraj = data.property.address.country;

            getXX = data.latitude;
            getYY = data.longitude;

            var getKontaktName = data.contact.name;
            var getKontaktEmail = data.contact.email;
            var getKontaktPhone = data.contact.phoneNumber;


            $('#formularz_TYTUL_edit').val(getTytul);
            $('#formularz_OPIS_edit').val(getOpis);
            $('#formularz_rodzajT_edit').dropdown('set value',getRodzajT);
            $('#formularz_rodzajM_edit').dropdown('set value',getRodzajM);
            $('#formularz_cena_edit').val(getCena);
            $('#formularz_waluta_edit').val(getWaluta);
            $('#formularz_ulica_edit').val(getUlica);
            $('#formularz_miasto_edit').val(getMiasto);
            $('#formularz_kraj_edit').val(getKraj);
            $('#formularz_XX_edit').val(getXX);
            $('#formularz_YY_edit').val(getYY);
            $('#formularz_imieKontakt_edit').val(getKontaktName);
            $('#formularz_emailKontakt_edit').val(getKontaktEmail);
            $('#formularz_telKontakt_edit').val(getKontaktPhone);

            $('#podglad_szczegoly_edit').empty();
            if($('#formularz_szczegoly_propertyBuildingType').val() != "") $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj38'],intToRodzajBT($('#formularz_szczegoly_propertyBuildingType_edit').val()),''));
            $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj39'],$('#formularz_szczegoly_propertyArea_edit').val(),TLUMACZENIA['wyszukiwarka35']));
            if($('#formularz_szczegoly_propertyHeatingType_edit').val() != "") $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj40'],intToHeating($('#formularz_szczegoly_propertyHeatingType_edit').val()),''));
            $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj41'],$('#formularz_szczegoly_propertyHasBasement_edit').val(),''));
            $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj42'],$('#formularz_szczegoly_propertyHasBalcony_edit').val(),''));
            $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj43'],$('#formularz_szczegoly_propertyBuildYear_edit').val(),''));
            $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj44'],$('#formularz_szczegoly_propertyHasClima_edit').val(),''));
            $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj45'],$('#formularz_szczegoly_propertyFloor_edit').val(),''));
            $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj46'],$('#formularz_szczegoly_propertyIsFurnished_edit').val(),''));
            $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj47'],$('#formularz_szczegoly_propertyHasGarden_edit').val(),''));
            $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj48'],$('#formularz_szczegoly_propertyHasLift_edit').val(),''));
            $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj49'],$('#formularz_szczegoly_propertyAllowPets_edit').val(),''));
            $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj50'],$('#formularz_szczegoly_propertyRoomCount_edit').val(),''));
            $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj51'],$('#formularz_szczegoly_propertyMaxPerson_edit').val(),''));
            $('#podglad_szczegoly_edit').append(divekSzczegolik(TLUMACZENIA['przegladaj52'],$('#formularz_szczegoly_propertyHasParking_edit').val(),''));


              $('#podglad_tytul_edit').text( getTytul );
              $('#podglad_opis_edit').text( getOpis );
              $('#podglad_typ_edit').text( intToRodzajT(getRodzajT) );
              $('#podglad_offertyp_edit').text( intToRodzajM(getRodzajM) );
              $('#podglad_cena_edit').html(getCena);


                if(getRodzajT == 0)
                    $('#podglad_cena_waluta_edit').html('<sup style="font-size: 70%;">'+intToWaluta(getWaluta)+'</sup>&frasl;<sub style="font-size: 60%;">'+TLUMACZENIA['przegladaj5']+'</sub>');
                else if(getRodzajT == 1)
                    $('#podglad_cena_waluta_edit').html('<span style="font-size: 70%;">'+intToWaluta(getWaluta)+'</span>');
                else if(getRodzajT == 2)
                    $('#podglad_cena_waluta_edit').html('<sup style="font-size: 70%;">'+intToWaluta(getWaluta)+'</sup>&frasl;<sub style="font-size: 60%;">'+TLUMACZENIA['przegladaj6']+'</sub>');


            $('#podglad_ulica_edit').html(getUlica);
            $('#podglad_miasto_edit').html(getMiasto);
            $('#podglad_kraj_edit').html(getKraj);





            //SZCZEGOLY
            if(data.property.buildingType != null)
                $('#set_formularz_szczegoly_propertyBuildingType_edit').val( $('#formularz_szczegoly_propertyBuildingType_edit').val(data.property.buildingType) )

//            if(data.property.buildingType != null)
//                $('#set_formularz_szczegoly_propertyMinPerson').val( $('#formularz_szczegoly_propertyBuildingType').val(data.property.buildingType) )

            if(data.property.maxPerson != null)
                $('#set_formularz_szczegoly_propertyMaxPerson_edit').val( $('#formularz_szczegoly_propertyMaxPerson_edit').val(data.property.maxPerson) )

            if(data.property.heatingType != null)
                $('#set_formularz_szczegoly_propertyHeatingType_edit').val( $('#formularz_szczegoly_propertyHeatingType_edit').val(data.property.heatingType) )

            if(data.property.area != null)
                $('#set_formularz_szczegoly_propertyArea_edit').val( $('#formularz_szczegoly_propertyArea_edit').val(data.property.area) )

            if(data.property.roomCount != null)
                $('#set_formularz_szczegoly_propertyRoomCount_edit').val( $('#formularz_szczegoly_propertyRoomCount_edit').val(data.property.roomCount) )

            if(data.property.floor != null)
                $('#set_formularz_szczegoly_propertyFloor_edit').val( $('#formularz_szczegoly_propertyFloor_edit').val(data.property.floor) )

            if(data.property.builtYear != null)
                $('#set_formularz_szczegoly_propertyBuildYear_edit').val( $('#formularz_szczegoly_propertyBuildYear_edit').val(data.property.builtYear) )

            if(data.property.furnished != null)
                $('#set_formularz_szczegoly_propertyIsFurnished_edit').val( $('#formularz_szczegoly_propertyIsFurnished_edit').val(data.property.furnished) )

            if(data.property.balcony != null)
                $('#set_formularz_szczegoly_propertyHasBalcony_edit').val( $('#formularz_szczegoly_propertyHasBalcony_edit').val(data.property.balcony) )

            if(data.property.lift != null)
                $('#set_formularz_szczegoly_propertyHasLift_edit').val( $('#formularz_szczegoly_propertyHasLift_edit').val(data.property.lift) )

            if(data.property.basement != null)
                $('#set_formularz_szczegoly_propertyHasBasement_edit').val( $('#formularz_szczegoly_propertyHasBasement_edit').val(data.property.basement) )

            if(data.property.parkingPlace != null)
                $('#set_formularz_szczegoly_propertyHasParking_edit').val( $('#formularz_szczegoly_propertyHasParking_edit').val(data.property.parkingPlace) )

            if(data.property.pets != null)
                $('#set_formularz_szczegoly_propertyAllowPets_edit').val( $('#formularz_szczegoly_propertyAllowPets_edit').val(data.property.pets) )

            if(data.property.garden != null)
                $('#set_formularz_szczegoly_propertyHasGarden_edit').val( $('#formularz_szczegoly_propertyHasGarden_edit').val(data.property.garden) )

            if(data.property.climatisation != null)
                $('#set_formularz_szczegoly_propertyHasClima_edit').val( $('#formularz_szczegoly_propertyHasClima_edit').val(data.property.climatisation) )


            if(data.photos.length != 0 && !zdjeciapodgladwoezaladwoano )
            {
                for (var i = 0; i < data.photos.length; ++i) {
                    var ktory;
                    if(i==0)
                        ktory = DomyslneZdj_TXT;
                    else
                        ktory = i;

                    var mini =  SERWER+'photo/thumb/'+data.photos[i].id;

                    $('#obecnieDodaneimages').append('<li class="jFiler-item">'+
                        '<div class="jFiler-item-container">'+
                        '<div class="jFiler-item-inner">'+
                        '<div class="jFiler-item-thumb" style="background: url('+mini+') no-repeat center;">'+
                        '</div>'+
                        '<div class="jFiler-item-assets jFiler-row">'+
                        '<ul class="list-inline pull-right" style="list-style-type: none;">'+
                        '<li style="float: left;margin-right: 8px;"><a class="glownezdjecie">'+ktory+'</a><input type="text" name="photos_edycja" class="kolejnosc" style="display: none;" value="'+data.photos[i].id+'" /></li>'+
                        '<li style="float: left;margin-right: 8px;"><a class="WLEWO2"><i class="chevron left icon"></i></a></li>'+
                        '<li style="float: left;margin-right: 8px;" class="WPRAWO2"><a><i class="chevron right icon"></i></a></li>'+
                        '<li style="float: left;"><a class="icon-jfi-trash jFiler-item-trash-action akcja2"><i class="icon trash"></i></a></li>'+
                        '</ul>'+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '</li>')
                }

                $('#niemaZdjec_edit').hide();
                zdjeciapodgladwoezaladwoano = true;


                $('#podglad_obrazy_glowne_edit').prepend('<a href="'+SERWER+'photo/'+data.photos[0].id+'" title="">'+
                    '<div class="glowne_zdjecie_div_back"><div class="glowne_zdjecie_div">'+
                    '<img  src="'+SERWER+'photo/'+data.photos[0].id+'" /></div></div>'+
                    '</a>');


                for (var i = 1; i < data.photos.length; ++i) {
                    var normal =  SERWER+'photo/'+data.photos[i].id;
                    var mini =  SERWER+'photo/thumb/'+data.photos[i].id;

                    $('#podglad_obrazy_mini_edit').slick('slickAdd','<a href="'+normal+'" title="">'+
                        '<div class="miniaturka_div_back"><div class="miniaturka_div"><img src="'+mini+'" /></div></div>'+
                        '</a>');
                }

                if($('#podglad_obrazy_mini_edit a').size() <= 4)
                    $('#podglad_obrazy_mini_edit').css("margin-left","-17px");



            }



            $('.dropdown').dropdown();
            $("#formularz_rodzajM_edit").dropdown({
                onChange: function (val) {
                    rodzjaBtms = val;
                }
            });


            $('#formularz_podstawowe_edit').modal('show');
        });

    });


    $(document).on('click', '.WLEWO2', function(){
        $('#podglad_obrazy_mini_edit').slick('unslick');
        if($(this).index(".WLEWO2") != 0)
        {
            var ten = $(this).parentsUntil('.jFiler-item').parent();
            var lewy = $(this).parentsUntil('.jFiler-item').parent().prev('.jFiler-item');

            var strDiv1Cont = ten.html();
            var strDiv2Cont = lewy.html();

            ten.html(strDiv2Cont);
            lewy.html(strDiv1Cont);

            if($(ten).index('.jFiler-item') == 1)
            {
                ten.find('.glownezdjecie').text($(ten).index('.jFiler-item')+1);
                lewy.find('.glownezdjecie').text(DomyslneZdj_TXT);

                var div1 = $('#podglad_obrazy_glowne_edit > a img').eq(0);
                var div2 = $('#podglad_obrazy_mini_edit > a img').eq(0);

                var tdiv1 = div1.clone();
                var tdiv2 = div2.clone();

                div1.replaceWith(tdiv2);
                div2.replaceWith(tdiv1);

            }
            else
            {
                ten.find('.glownezdjecie').text( $(ten).index('.jFiler-item')+1 );
                lewy.find('.glownezdjecie').text( $(lewy).index('.jFiler-item')+1 );

                var div1 = $('#podglad_obrazy_mini_edit > a img').eq($(ten).index('.jFiler-item')-1);
                var div2 = $('#podglad_obrazy_mini_edit > a img').eq($(lewy).index('.jFiler-item')-1);

                var tdiv1 = div1.clone();
                var tdiv2 = div2.clone();

                div1.replaceWith(tdiv2);
                div2.replaceWith(tdiv1);

            }

        }

        $('#podglad_obrazy_mini_edit').slick({
            infinite: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            variableWidth: true
        });
    });

    $(document).on('click', '.WPRAWO2', function(){
        $('#podglad_obrazy_mini_edit').slick('unslick');
        if( $(this).index(".WPRAWO2")+1 != $(".WPRAWO2").size() )
        {
            var ten = $(this).parentsUntil('.jFiler-item').parent();
            var prawy = $(this).parentsUntil('.jFiler-item').parent().next('.jFiler-item');

            var strDiv1Cont = ten.html();
            var strDiv2Cont = prawy.html();


            ten.html(strDiv2Cont);
            prawy.html(strDiv1Cont);

            if($(ten).index('.jFiler-item') == 0)
            {
                ten.find('.glownezdjecie').text(DomyslneZdj_TXT);
                prawy.find('.glownezdjecie').text($(prawy).index('.jFiler-item')+1);

                var div1 = $('#podglad_obrazy_glowne_edit > a img').eq(0);
                var div2 = $('#podglad_obrazy_mini_edit > a img').eq(0);

                var tdiv1 = div1.clone();
                var tdiv2 = div2.clone();

                div1.replaceWith(tdiv2);
                div2.replaceWith(tdiv1);
            }
            else
            {
                ten.find('.glownezdjecie').text( $(ten).index('.jFiler-item')+1 );
                prawy.find('.glownezdjecie').text( $(prawy).index('.jFiler-item')+1 );

                var div1 = $('#podglad_obrazy_mini_edit > a img').eq($(ten).index('.jFiler-item')-1);
                var div2 = $('#podglad_obrazy_mini_edit > a img').eq($(prawy).index('.jFiler-item')-1);

                var tdiv1 = div1.clone();
                var tdiv2 = div2.clone();

                div1.replaceWith(tdiv2);
                div2.replaceWith(tdiv1);
            }

        }

        $('#podglad_obrazy_mini_edit').slick({
            infinite: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            variableWidth: true
        });
    });

    var tablicaLadujacychSieEdycja = [];

    $(document).on('click', '.akcja2', function(){
        var index = $(this).index('.akcja2');
        var indexx = tablicaLadujacychSieEdycja.indexOf( index );
        tablicaLadujacychSieEdycja.splice(indexx, 1);

        if(tablicaLadujacychSieEdycja.length == 0)
        {
            $('.idzDo_formularz_podsumowanie2').show();
            $('.poczekaj_zdjecia_edit').hide();
        }


        if(index ==0 && $('.glownezdjecie').length == 1)
        {
            $('#niemaZdjec_edit').show();
            niemazdjec_edit = true;
        }

        if( index == 0)
        {
            $(this).parentsUntil('.jFiler-item').parent().remove();
            $('#podglad_obrazy_mini_edit').slick('unslick');

            var div1 = $('#podglad_obrazy_glowne_edit > a img').eq(0);
            var div2 = $('#podglad_obrazy_mini_edit > a img').eq(0);

            var tdiv1 = div1.clone();
            var tdiv2 = div2.clone();

            div1.replaceWith(tdiv2);
            div2.replaceWith(tdiv1);

            $('#podglad_obrazy_mini_edit').slick({
                infinite: false,
                slidesToShow: 4,
                slidesToScroll: 1,
                variableWidth: true
            });

            $('#podglad_obrazy_mini_edit').slick('slickRemove',index);

            if($('#podglad_obrazy_mini_edit a img').size() == 0 && $('#podglad_obrazy_glowne_edit a img').size() == 0 )
                $('#podglad_obrazy_glowne_edit').empty();

            //itemEl.next('.jFiler-item').find('.glownezdjecie').text('Głowne');
            $('.glownezdjecie').eq(index).text("GLOWNE");
            for (var i = index+1; i < $('.glownezdjecie').length; i++) {
                $('.glownezdjecie').eq(i).text(i);
            }


        }
        else
        {
            //$('.glownezdjecie').eq(0).text('Glowne');
            for (var i = index; i < $('.glownezdjecie').length; i++) {
                $('.glownezdjecie').eq(i).text(i);
            }
            $('#podglad_obrazy_mini_edit').slick('slickRemove',index-1);

           $(this).parentsUntil('.jFiler-item').parent().remove();
        }


    });



    $('.idzDo_formularz_szczegoly2').on('click', function() {
        $('.rodzajZabudowyDom,.rodzajZabudowyBlok,.rodzajZabudowyLokale, .rodzajZabudowyPokoj, .rodzajZabudowyGaraz, .rodzajZabudowyDzialka').hide();

            $('#rodzajZabudowyonczyoff_edit').removeClass('disabled');

            if(rodzjaBtms == 0)
                $('.rodzajZabudowyDom').show();
            else if(rodzjaBtms == 50)
                $('.rodzajZabudowyBlok').show();
            else if(rodzjaBtms == 100)
                $('.rodzajZabudowyPokoj').show();
            else if(rodzjaBtms == 150)
                $('.rodzajZabudowyDzialka').show();
            else if(rodzjaBtms == 200)
                $('.rodzajZabudowyGaraz').show();
            else if(rodzjaBtms == 250)
                $('.rodzajZabudowyLokale').show();


    });


    var bylo2 = false;
    $(document).on('click', '.idzDo_formularz_adres2',function(){
        if(!bylo2)
        {
            bylo2 = true;

            MAPA3 = L.map('MAPA2_edit', { center: L.latLng(50.088488, 19.937890),zoom: 12, attributionControl: false });
            window.dispatchEvent(new Event('resize'));
            L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(MAPA3);

            var srodekX = getXX;
            var srodekY = getYY;

            setTimeout(function(){
                MAPA3.invalidateSize();
                MAPA3.setView([50.07, 19.937890],12);

                MAPA3.setView([getXX,getYY], 17);

                var ostatniaDobraPozX = getXX,
                    ostatniaDobraPozY = getYY;

                MARKER_RPZECIAGNY = L.marker([getXX, getYY],{draggable:'true'}).addTo(MAPA3);

                MARKER_RPZECIAGNY.on('dragend', function(event){
                    var marker = event.target;
                    var position = marker.getLatLng();

                    var zakress = Math.sqrt(Math.abs(srodekY-position.lng)*Math.abs(srodekY-position.lng)+
                        Math.abs(srodekX-position.lat)*Math.abs(srodekX-position.lat) ) ;

                    if(zakress > zakresWyszukwianiaGoogle)
                    {
                        marker.bindPopup("<b>Kurwa ziom za daleko!</b>").openPopup();
                        marker.setLatLng(new L.LatLng(ostatniaDobraPozX,ostatniaDobraPozY),{draggable:'true'});
                        // map.panTo(new L.LatLng(ostatniaDobraPozX,ostatniaDobraPozY));
                        $('#formularz_XX_edit').val(ostatniaDobraPozX);
                        $('#formularz_YY_edit').val(ostatniaDobraPozY);
                    }
                    else
                    {
                        ostatniaDobraPozX = position.lat,
                        ostatniaDobraPozY = position.lng;
                        marker.setLatLng(new L.LatLng(position.lat, position.lng),{draggable:'true'});
                        MAPA3.panTo(new L.LatLng(position.lat, position.lng));
                        $('#formularz_XX_edit').val(position.lat);
                        $('#formularz_YY_edit').val(position.lng);
                    }


                });
                MAPA3.addLayer(MARKER_RPZECIAGNY);
            },300);
        }
    });

    $('.idzDo_formularz_zdjecia2').on('click', function(){
        $('#formularz_zdjecia_edit').modal('show');
        $('#formularz_zdjecia_edit').modal('refresh');
    });


</script>