<?php
include('../../php/tlumaczenie2.php');
?>
    <h2 class="ui header">
        <i class="users icon"></i>
        <div class="content" style="cursor: pointer;">
            <span id="zalogujSie"><?php echo $TLUMACZENIA['profil1'];?></span>
            <div class="sub header" id="zarejestrujSie"><?php echo $TLUMACZENIA['profil2'];?></div>
        </div>
    </h2>


<div class="ui buttons fluid" style="margin-top: 10px;" id="pasekDodawnieitd">
    <div class="ui labeled button" id="dodajOgloszenie" >
        <div class="ui basic button">
            <i class="icons">
                <i class="home icon"></i>
                <i class="inverted corner add icon"></i>
            </i>
            <span>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $TLUMACZENIA['profil3'];?></span>
        </div>
    </div>
    <div class="ui labeled button" id="doladujKonto" >
        <div class="ui basic button" >
            <i class="money icon"></i> <?php echo $TLUMACZENIA['profil4'];?>
        </div>
        <a class="ui basic left pointing green label">0</a>
    </div>
</div>


<h4 class="ui dividing header">
    <?php echo $TLUMACZENIA['profil5'];?>
</h4>

<div class="ui message" style="margin-top: 20px;">
    <div class="header">
        <?php echo $TLUMACZENIA['profil6'];?>
    </div>
    <?php echo $TLUMACZENIA['profil7'];?>
</div>

<!-- DOLADUJ -->
<?php
//include('modal/doladuj.php');
?>

<!-- LOGOWANIE -->
<?php
include('modal/logowanie.php');
?>


<?php
include('modal/rejestracja.php');
?>

    <?php
    include('modal/zapomnialem.php');
    ?>
<!-- KONIEC -->


<script>
    $(window).resize(function() {
        console.log($(window).width())
        if( $(window).width() < 780)
            $(body).addClass('scrolling');
    });
    $(document).ready(function() {

        $('.menu .item').tab();

        $('.checkbox').checkbox();

        $('#zalogujSie').click(function(){
            $('#modal_zalogujSie').modal('setting', {autofocus: false}).modal('show');
        });

        $('#zarejestrujSie').click(function(){
            $('#modal_zarejestrujSie').modal('setting', {autofocus: false}).modal('show');
        });

        $('#doladujKonto').click(function(){
            $('#modal_zalogujSie').modal('setting', {autofocus: false}).modal('show');
        });


        $('#modal_zarejestrujSie').modal('setting', {autofocus: false}).modal('attach events', '#idzdorejestruj');
        $('#modal_zapomnialemHasla').modal('setting', {autofocus: false}).modal('attach events', '#idzdozapomanialemhasla');
        $('#modal_zalogujSie').modal('setting', {autofocus: false}).modal('attach events', '#idzdologowania');
        $('#modal_zalogujSie').modal('setting', {autofocus: false}).modal('attach events', '#idzdologowania2');
        $('#modal_zalogujSie').modal('setting', {autofocus: false}).modal('attach events', '#idzdologowania3');

        $('#dodajOgloszenie').on('click', function(){
            $('#modal_zalogujSie').modal('setting', {autofocus: false}).modal('show');
        });

    });
</script>

