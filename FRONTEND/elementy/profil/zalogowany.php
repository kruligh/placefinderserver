<?php
include('../../php/tlumaczenie2.php');
?>
<div class="ui accordion" style="margin-bottom: 10px;">
    <div class="ui title">
        <i class="dropdown icon" style="margin-top: 10px;"></i>
        <div class="ui grey header" style="font-size: 130%;margin-left: 25px;margin-top: -22px;" id="NazwaMoj" >Login</div>
    </div>
    <div class="content ">
        <div class="ui header" style="position: absolute;left: 230px; margin-top: 15px;font-size: 80%;cursor: pointer;">
            <a style="color: grey;" id="WYLOGUJSIE"><?php echo $TLUMACZENIA['profil53'];?></a>
        </div>
        <div class="ui header" style="position: absolute;left: 140px; margin-top: 15px;font-size: 80%;cursor: pointer;">
            <a style="color: grey;" id="ZMIENHASLO"><?php echo $TLUMACZENIA['profil54'];?></a>
        </div>
        <div class="ui header" style="position: absolute;left: 20px; margin-top: 15px;font-size: 80%;cursor: pointer;">
            <a style="color: grey;" id="EDYTUJSE"><?php echo $TLUMACZENIA['profil55'];?></a>
            <a style="color: grey;" id="ZAPISZSE"><?php echo $TLUMACZENIA['profil56'];?></a>
        </div>
        <div class="ui equal width grid" style="margin-top: 35px;">
            <div class="row">
                <div class="column">
                    <div style="display: inline;" class="naInputa_div" id="imieInazwiskoMoj"><i class="user icon"></i></div>
                    <div style="display: inline;" class="naInputa_input ui left icon input" ><i class="user icon"></i><input id="imieInazwiskoMoj_input" type="text" value=""></div>
                </div>
                <div class="column"></div>
            </div>
            <div class="row" style="margin-top: -10px;">
                <div class="column">
                    <div style="white-space: nowrap;display: inline-block;overflow:hidden;text-overflow: ellipsis;width:100%;" class="naInputa_div" id="emailMoj"><i class="mail outline icon"></i></div>
                    <div style="display: inline;" class="naInputa_input ui left icon input" ><i class="mail outline icon"></i><input id="emailMoj_input" type="text" value=""></div>
                </div>
                <div class="column">
                    <div style="display: inline;" class="naInputa_div" id="telefonMoj"><i class="call icon"></i></div>
                    <div style="display: inline;" class="naInputa_input ui left icon input" ><i class="call icon"></i><input id="telefonMoj_input" type="text" value=""></div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="ui buttons fluid" style="margin-top: 10px;" id="pasekDodawnieitd">
    <div class="ui labeled button" id="dodajOgloszenie" >
        <div class="ui basic button">
            <i class="icons">
                <i class="home icon"></i>
                <i class="inverted corner add icon"></i>
            </i>
            <span>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $TLUMACZENIA['profil3'];?></span>
        </div>
    </div>
    <div class="ui labeled button" id="doladujKonto" >
        <div class="ui basic button">
            <i class="money icon"></i> <?php echo $TLUMACZENIA['profil4'];?>
        </div>
        <a class="ui basic left pointing green label" id="IloscPunktow">0</a>
    </div>
</div>



<!--    <div class="ui button"  >TEST</div>-->



<?php
include('tabki/moje.php');
?>

<!-- DOLADUJ -->
<?php
include('modal/doladuj.php');
?>

<!-- DODAWANIE NOWYCH OGLOSZEN-->
<?php

include('edytowanieofert.php');

include('dodawanieofert.php');

include('modal/usun.php');
include('modal/deaktywuj.php');
?>


<script>
    var MAPA2, MAPA3;
    var DomyslneZdj_TXT = TLUMACZENIA['profil59'];

    var photos,photos_edycja;

    var nowezdjecia = true;

    var moge_kupowac = true;
    $('#NIE_MOZNA_KUPOWAC, #NIE_MOZNA_KUPOWAC_edit').click(function () {
        $('#modal_dodaluj').modal('setting', {autofocus: false}).modal('show');
    });

    $(document).ready(function() {

        $('.naInputa_input').hide();
        $('#ZAPISZSE').hide();

        $('.accordion').accordion();
        $('.menu .item').tab();


//        $('#zalogujSie').click(function(){
//            $('#modal_zalogujSie').modal('setting', {autofocus: false}).modal('show');
//        });
//
//        $('#zarejestrujSie').click(function(){
//
//            $('#modal_zarejestrujSie').modal('setting', {autofocus: false}).modal('show');
//
//        });


        $('.strzalkagora, .strzalkadol').hide();
        $('.ui.text.menu.pierwszutkienie').on('click', 'a.item', function() {
            var co_sortowac;
            if($(this).index('a.item') == 1 || $(this).index('a.item') == 6 )
                co_sortowac = "popularity";
            else if($(this).index('a.item') == 0 || $(this).index('a.item') == 7 )
                co_sortowac = "price";
            else if($(this).index('a.item') == 2 || $(this).index('a.item') == 8 )
                co_sortowac = "activity";


            if($(this).hasClass('active'))
            {

                if($(this).hasClass('gora'))
                {

                    $(this).find('.strzalkadol').show();
                    $(this).find('.strzalkagora').hide();
                    $(this).addClass('active dol').removeClass('gora').siblings('a.item').removeClass('active');
                    //sortuje w gore
                    sortowaniko_my = "&sortBy=0&sortByValue="+co_sortowac;
                    linkacz_my = linkacz2_my+"&page="+(stronaa_my-1)+sortowaniko_my+limit_my;
                    console.log(linkacz_my)
                    dodajMarkerki_my(linkacz_my,3)
                }
                else
                {

                    $(this).find('.strzalkadol').hide();
                    $(this).find('.strzalkagora').show();
                    $(this).addClass('active gora').removeClass('dol').siblings('a.item').removeClass('active');

                    sortowaniko_my = "&sortBy=1&sortByValue="+co_sortowac;
                    linkacz_my = linkacz2_my+"&page="+(stronaa_my-1)+sortowaniko_my+limit_my;
                    console.log(linkacz_my)
                    dodajMarkerki_my(linkacz_my,3)
                }
            }
            else
            {
                $('.strzalkagora, .strzalkadol').hide();
                $(this).find('.strzalkadol').show();
                $(this).find('.strzalkagora').hide();
                $(this).addClass('active').siblings('a.item').removeClass('active');

                sortowaniko_my = "&sortBy=0&sortByValue="+co_sortowac;
                linkacz_my = linkacz2_my+"&page="+(stronaa_my-1)+sortowaniko_my+limit_my;
                console.log(linkacz_my)
                dodajMarkerki_my(linkacz_my,3)

            }


        });



        $('#doladujKonto').click(function(){
            $('#modal_dodaluj').modal('setting', {autofocus: false}).modal('show');
        });


        $('#emailwiadomosc').hide();


        $.ajax({
            url: SERWER+"/person/getMyUser",
            type: "GET",
            contentType: "application/json",
            headers : {
                'X-Auth-Token' : localStorage.getItem('X-Auth-Token')
            },
            success : function(data){

                //console.log(data)
                if(!data.activated)
                {
                    $('#emailwiadomosc').show();
                    $('#brakmieszkan').hide();
                    $('#dodajOgloszenie').addClass('disabled');
                }
                else
                {
                    $('#dodajOgloszenie').click(function(){
                        $('#modal_dodajOgloszenie').modal('setting', {autofocus: false}).modal('show');
                    });

                }

                $('#IloscPunktow').text( data.points )




                $('#NazwaMoj').html(data.name);

                $('#emailMoj').append(data.contact.email);
                $('#imieInazwiskoMoj').append(data.contact.name);
                $('#telefonMoj').append(data.contact.phoneNumber);

                $('#emailMoj_input').val(data.contact.email);
                $('#imieInazwiskoMoj_input').val(data.contact.name);
                $('#telefonMoj_input').val(data.contact.phoneNumber);


                $('#formularz_imieKontakt').val( $('#imieInazwiskoMoj_input').val() );
                $('#formularz_emailKontakt').val( $('#telefonMoj_input').val() );
                $('#formularz_telKontakt').val( $('#emailMoj_input').val() );
                $('#formularz_imieKontakt_edit').val( $('#imieInazwiskoMoj_input').val() );
                $('#formularz_emailKontakt_edit').val( $('#telefonMoj_input').val() );
                $('#formularz_telKontakt_edit').val( $('#emailMoj_input').val() );


                linkacz2_my = SERWER+"person/offer/getMyOffers?";
                linkacz_my = linkacz2_my+"page="+(stronaa_my-1)+limit_my+sortowaniko_my;



                $.ajax({
                    url: linkacz_my,
                    type: "GET",
                    contentType: "application/json",
                    headers : {
                        'X-Auth-Token' : localStorage.getItem('X-Auth-Token')
                    },
                    success : function(data){
                        //console.timeEnd("concatenation");
//                        console.log("bedize ladowanko")
                        $('#promotedHeader_my').hide();
                        $('#normalHeader_my').hide();
                        $('#mojezaznaczone_my').empty();
                        $('#mojezaznaczone_promowane_my').empty();

                        if(data.count!=0)
                            $('#brakmieszkan').hide();
                        $('#ILOSCzaznazcznocyh_my').text(data.count);
                        dodawnaie_my(data,3);


                        $('#zilustronicowanie_my').text(data.pages);
                        if( $('#ktoraStrona_my').val() == "1" )
                            $('#stronicowanie1_my').removeClass('green');
                        else
                            $('#stronicowanie1_my').addClass('green');
                        if(data.pages == 1)
                            $('#stronicowanie2_my').removeClass('green');
                        else
                            $('#stronicowanie2_my').addClass('green');

                        $('#LADOWANIE_MOICH_DODANYCH').hide();

                    },
                    error: function(data)
                    {
                    }
                });



            },
            error: function(data)
            {

                if(data.status == 403 || data.status == 500)
                {
                    localStorage.removeItem("X-Auth-Token");
                    $("#PROFIL_DIV").load("elementy/profil/niezalogowany.php");
//                    location.reload();
//                    $("#PROFIL_KLIK").click();
                }
            }
        });

        $('#dodaneogloszenia2').perfectScrollbar({
            minScrollbarLength: 20
        });

        $('#ZMIENHASLO').click(function(){
            location.href='http://164.132.57.18/flatmap/changepassword.php?token='+localStorage.getItem('X-Auth-Token');
        });

        $('#EDYTUJSE').click(function(){
            $('#EDYTUJSE').hide();
            $('#ZAPISZSE').show();

            $('.naInputa_input').show();
            $('.naInputa_div').hide();
        });

        $('#ZAPISZSE').click(function(){

            $('#LADOWANIE_CZEKENIE').addClass('active');
            var obiekt = JSON.stringify({"email" : $('#emailMoj_input').val(), "nazwa" : $('#imieInazwiskoMoj_input').val(), "telefon" : $('#telefonMoj_input').val() });
            $.ajax({
                url: SERWER+"person/editMyContact",
                type: "POST",
                contentType: "application/json",
                data : obiekt,
                headers : {
                    'X-Auth-Token' : localStorage.getItem('X-Auth-Token')
                },
                success : function(data){
                    $('#EDYTUJSE').show();
                    $('#ZAPISZSE').hide();
                    $('.naInputa_input').hide();
                    $('.naInputa_div').show();
                    location.reload();
                },
                error: function(data)
                {
                    $('#LADOWANIE_CZEKENIE').removeClass('active');
                }
            });


        });

        $('#WYLOGUJSIE').click(function(){
            $('#LADOWANIE_CZEKENIE').addClass('active');
            $.ajax({
                url: SERWER+"person/session/logout",
                type: "POST",
                contentType: "application/json",
                headers : {
                    'X-Auth-Token' : localStorage.getItem('X-Auth-Token')
                },
                success : function(data){
                    localStorage.removeItem("X-Auth-Token");
                    location.reload();
                },
                error: function(data)
                {
                    $('#LADOWANIE_CZEKENIE').removeClass('active');
                    if(data.status == 403)
                    {
                        localStorage.removeItem("X-Auth-Token");
                        location.reload();
                    }
                }
            });
        });


    });



</script>

<script src="LIB/jQuery.filer-1.3.0/js/custom.js"></script>