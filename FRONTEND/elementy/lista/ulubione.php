<div class="ui message" id="brakulubionych" >
    <div class="header">
        <?php echo $TLUMACZENIA['lista5'];?>
    </div>
</div>

<div class="ui text menu" id="sortowanieiinepierdoly" style="display:none;margin-top: -10px;border-bottom: 1px dashed #ddd8de;">
    <div class="header item"><?php echo $TLUMACZENIA['lista6'];?> </div>
    <a class="item">
        <i class="icon long arrow up strzalkagora"></i><i class="icon long arrow down strzalkadol"></i><?php echo $TLUMACZENIA['lista7'];?>
    </a>
    <a class="item">
        <i class="icon long arrow up strzalkagora"></i><i class="icon long arrow down strzalkadol"></i><?php echo $TLUMACZENIA['lista8'];?>
    </a>
</div>

<div style="height: 90%;width: 103.5%;padding-bottom: 50px;display:block;overflow:hidden!important;position: relative;padding-right: 13px;" class="ui divided items" id="mojeulubione"  ></div>


<script>
    function sorterwDol(a, b) {

        var jeden = $(a).find(''+co_sortowac).text(), dwa = $(b).find(''+co_sortowac).text();
        if( jeden.slice(-1) == "K" )
            jeden = parseInt(jeden)*1000;
        if( dwa.slice(-1) == "K" )
            dwa = parseInt(dwa)*1000;

        return jeden - dwa;
    };

    function sorterwGore(a, b) {
        var jeden = $(a).find(''+co_sortowac).text(), dwa = $(b).find(''+co_sortowac).text();
        if( jeden.slice(-1) == "K" )
            jeden = parseInt(jeden)*1000;
        if( dwa.slice(-1) == "K" )
            dwa = parseInt(dwa)*1000;

        return dwa - jeden ;
    };

    $(document).ready(function() {
        $('#mojeulubione').perfectScrollbar();

        dodajUlubionejakiemam();




        $('.strzalkagora, .strzalkadol').hide();
        $('.ui.text.menu').on('click', 'a.item', function() {
            if($(this).index('a.item') == 0)
                co_sortowac = "span.hajssortuje";
            else
                co_sortowac = "span.wyssortuje";


            if($(this).hasClass('active'))
            {
                if(!$(this).hasClass('dol'))
                {
                    $(this).find('.strzalkadol').hide();
                    $(this).find('.strzalkagora').show();
                    $(this).addClass('active dol').removeClass('gora').siblings('a.item').removeClass('active');
                    $('#mojeulubione').append($("#mojeulubione .dosortowania").toArray().sort(sorterwGore));
                }
                else
                {

                    $(this).find('.strzalkadol').show();
                    $(this).find('.strzalkagora').hide();
                    $(this).addClass('active gora').removeClass('dol').siblings('a.item').removeClass('active');
                    $('#mojeulubione').append($("#mojeulubione .dosortowania").toArray().sort(sorterwDol));
                }
            }
            else
            {
                $('.strzalkagora, .strzalkadol').hide();
                $(this).find('.strzalkadol').show();
                $(this).find('.strzalkagora').hide();
                $(this).addClass('active').siblings('a.item').removeClass('active');
                $('#mojeulubione').append($("#mojeulubione .dosortowania").toArray().sort(sorterwDol));

            }


        });


    });



</script>