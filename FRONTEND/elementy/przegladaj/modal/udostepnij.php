<div class="ui modal" id="modal_udostepnij">
    <i class="close icon"></i>

    <div class="ui center aligned header">
        <i class="share icon"></i>
        <?php echo $TLUMACZENIA['przegladaj27'];?>
    </div>
    <div class="content" style="text-align: center;">
<!--        <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Share</a></div>-->
        <?php
        $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        ?>

        <div class="ui two cards">
            <a target="_blank" class="card" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $actual_link; ?>">
                <div class="content">
                    <div class="header">
                        <i class="icon blue facebook"></i> Facebook
                    </div>
                </div>
            </a>
            <a target="_blank" class="card" href="https://twitter.com/home?status=<?php echo $actual_link; ?>">
                    <div class="content">
                        <div class="header"><i class="icon blue twitter"></i> Twitter</div>
                    </div>
            </a>
            <a target="_blank" class="card" href="https://plus.google.com/share?url=<?php echo $actual_link; ?>">
                    <div class="content">
                        <div class="header"><i class="icon red google plus"></i> Google</div>
                    </div>
            </a>
            <a target="_blank" class="card" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $actual_link; ?>&title=&summary=&source=">
                <div class="content">
                    <div class="header"><i class="icon blue linkedin"></i> Linkedin</div>
                </div>
            </a>

        </div>

    </div>
</div>

<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>