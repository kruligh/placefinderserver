
<div class="ui modal" id="modal_skontaktuj">
    <i class="close icon"></i>
    <div class="header">
        <?php echo $TLUMACZENIA['przegladaj18'];?>
    </div>
    <div class="content">

        <div class="ui form">
            <div class="field">
                <label><?php echo $TLUMACZENIA['przegladaj19'];?></label>
                <input type="text" placeholder="<?php echo $TLUMACZENIA['przegladaj20'];?>" id="emailskontaukujsie" >
            </div>
            <div class="field">
                <label><?php echo $TLUMACZENIA['profil117'];?></label>
                <input type="text" placeholder="<?php echo $TLUMACZENIA['profil25'];?>" id="telefonskontaukujsie" >
            </div>
            <div class="field">
                <label><?php echo $TLUMACZENIA['przegladaj21'];?></label>
                <textarea rows="3" id="wiadomoscEmailskontaujsie"></textarea>
            </div>
        </div>

        <div class="ui negative message" id="wpiszemail_messeage">
            <div class="header">
                <?php echo $TLUMACZENIA['przegladaj22'];?>
            </div>
        </div>
        <div class="ui negative message" id="wpiszwiadomosc_messeage">
            <div class="header">
                <?php echo $TLUMACZENIA['przegladaj13'];?>
            </div>
        </div>

        <div class="ui positive message" id="wiadomoscsukces_messeage">
            <div class="header">
                <?php echo $TLUMACZENIA['przegladaj24'];?>
            </div>
        </div>

    </div>
    <div class="actions">
        <div class="ui deny button">
            <?php echo $TLUMACZENIA['przegladaj25'];?>
        </div>
        <div class="ui right green labeled icon button" id="Skontaujsie_wyslij">
            <?php echo $TLUMACZENIA['przegladaj26'];?>
            <i class="mail outline icon"></i>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#wpiszemail_messeage').hide();
        $('#wpiszwiadomosc_messeage').hide();
        $('#wiadomoscsukces_messeage').hide();
    });

    $('#Skontaujsie_wyslij').click(function () {
        $('#wpiszemail_messeage').hide();
        $('#wpiszwiadomosc_messeage').hide();
        $('#wiadomoscsukces_messeage').hide();

        if($('#emailskontaukujsie').val() == "")
            $('#wpiszemail_messeage').show();
        else if($('#wiadomoscEmailskontaujsie').val() == "")
            $('#wpiszwiadomosc_messeage').show();
        else
        {
            $('#wpiszemail_messeage').hide();
            $('#wpiszwiadomosc_messeage').hide();
            $('#wiadomoscsukces_messeage').show();

            var obiektt = JSON.stringify({from : $('#emailskontaukujsie').val(), content : $('#wiadomoscEmailskontaujsie').val(),fromPhoneNumber : $('#telefonskontaukujsie').val(),offerId: $('#ID_TEGO').text() } );

            if(DEBUGOWANIE)
            {
                console.log("WIADOMOSC pukam po")
                console.log( obiektt  )
            }


            $.ajax({
                url: SERWER+'offer/sendMessage',
                type: "POST",
                contentType: "application/json",
                data : obiektt,
                headers : {
                    'X-Auth-Token' : localStorage.getItem('X-Auth-Token')
                },
                success : function(data){
                    $('#zgloszonosukces_messeage').show();
                },
                error: function(data)
                {
                    console.log(data)
                }
            });

            $('#emailskontaukujsie').val("");
            $('#telefonskontaukujsie').val("");
            $('#wiadomoscEmailskontaujsie').val("");
        }

    });
</script>