
<div class="ui modal" id="modal_zglos">
    <i class="close icon"></i>

    <div class="header">
        <?php echo $TLUMACZENIA['przegladaj28'];?>
    </div>
    <div class="content">
        <div class="ui form">
            <div class="field">
                <label><?php echo $TLUMACZENIA['przegladaj29'];?></label>
                <div class="ui selection dropdown">
                    <input type="hidden" id="powodid">
                    <i class="dropdown icon"></i>
                    <div class="default text"><?php echo $TLUMACZENIA['przegladaj30'];?></div>
                    <div class="menu">
                        <div class="item" data-value="0"><?php echo $TLUMACZENIA['przegladaj31'];?></div>
                        <div class="item" data-value="1"><?php echo $TLUMACZENIA['przegladaj32'];?></div>
                    </div>
                </div>
            </div>
            <div class="field">
                <label><?php echo $TLUMACZENIA['przegladaj33'];?></label>
                <textarea rows="2" id="dlaczegotxt"></textarea>
            </div>
        </div>

        <div class="ui message" id="wybierzpowod_messeage">
            <div class="header">
                <?php echo $TLUMACZENIA['przegladaj34'];?>
            </div>
        </div>
        <div class="ui positive message" id="zgloszonosukces_messeage">
            <div class="header">
                <?php echo $TLUMACZENIA['przegladaj35'];?>
            </div>
        </div>

    </div>
    <div class="actions">
        <div class="ui deny button">
            <?php echo $TLUMACZENIA['przegladaj25'];?>
        </div>
        <div class="ui right labeled icon button" id="ZGLOSZENIE_OFERTY">
            <?php echo $TLUMACZENIA['przegladaj37'];?>
            <i class="checkmark icon"></i>
        </div>
    </div>
</div>


<script>
    $('.dropdown').dropdown();


    $('#ZGLOSZENIE_OFERTY').click(function () {

        if($('#powodid').val() == "")
            $('#wybierzpowod_messeage').show();
        else
        {
            $('#wybierzpowod_messeage').hide();


            $.ajax({
                url: SERWER+'offer/report',
                type: "POST",
                contentType: "application/json",
                data : JSON.stringify({offerId : $('#ID_TEGO').text(), cause : $('#powodid').val() ,causeDesc : $('#dlaczegotxt').val() }),
                headers : {
                    'X-Auth-Token' : localStorage.getItem('X-Auth-Token')
                },
                success : function(data){
                    $('#zgloszonosukces_messeage').show();
                },
                error: function(data)
                {
                }
            });
        }

    });

    $(document).ready(function () {
        $('#wybierzpowod_messeage').hide();
        $('#zgloszonosukces_messeage').hide();


    });

</script>