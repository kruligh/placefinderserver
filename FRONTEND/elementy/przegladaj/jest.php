<div style="min-height: 98%;" >

    <div id="LADOWANIE_PRZEGLADANIA" >
        <img src="img/loading4.gif" style="width: 30%;" />
    </div>

    <div>
        <h1 style="font-size: 1.3em;" class="ui header" id="mieszkanieTYTUL">

        </h1>

        <div style="display: none;">
            <div id="mieszkanieXX"></div>
            <div id="mieszkanieYY"></div>
        </div>

        <div id="ID_TEGO" style="display: none;"></div>

        <div  style="float: right;margin-top: -35px;">
            <i id="DodajDoObserwowanych" class="empty star large icon" style="cursor: pointer"></i>
        </div>




        <div class="popup-gallery" id="obrazkipodglad" style="margin-bottom: -20px;">
            <div id="mniejszeobrazki" class="MINIATURKI" ></div>
        </div>




        <h2 class="ui header">
            <div style="position:absolute;width: 20px;margin-top:-3px;"><i id="idzNamapie" class="arrow right icon" style="cursor: pointer;font-size: 80%;"></i></div>
            <div style="display: inline-block;position:relative;left: 30px;">
                <span style="color:#8d8d8d;font-size: 65%;margin-right: 5px;font-weight: 100;"><?php echo $TLUMACZENIA['przegladaj3'];?></span><span id="mieszkanieULICA"></span>
            </div>
            <div class="sub header"><span id="mieszkanieKRAJ"></span> - <span id="mieszkanieMIASTO"></span> </div>
        </h2>

        <div class="ui clearing" style="margin-bottom: -10px;display: inline-block;width: 100%;">
            <h3 class="ui left floated header" id="mieszkanieTYP"></h3>
            <h5 class="ui left floated header" style="margin-top: 4px;"><?php echo $TLUMACZENIA['przegladaj4'];?></h5>
            <h3 class="ui left floated header" id="mieszkanieofferTYP"></h3>
            <h2 class="ui right floated header" id="mieszkanieCENA"></h2>
        </div>


        <div class="ui accordion" style="min-height: 100px;">
            <div class="title">
                <i class="dropdown icon"></i>
                <?php echo $TLUMACZENIA['przegladaj7'];?>
            </div>
            <div class="content">
                <div class="ui divided middle aligned selection list" id="mieszkanieSZCZEGOLY"></div>
            </div>
            <div class="title">
                <i class="dropdown icon"></i>
                <?php echo $TLUMACZENIA['przegladaj8'];?>
            </div>
            <div class="content" id="mieszkanieOPIS" style="word-wrap:break-word;table-layout: fixed;"></div>
            <div class="title">
                <i class="dropdown icon"></i>
                <?php echo $TLUMACZENIA['przegladaj9'];?>
            </div>
            <div class="content">
                <table class="ui very basic table" >
                    <tbody>
                    <tr>
                        <td><i class="user icon"></i><?php echo $TLUMACZENIA['przegladaj10'];?></td>
                        <td><span id="mieszkanieIMIE"></span></td>
                    </tr>
                    <tr>
                        <td>
                            <div class="ui labeled button" id="skontaktujsie">
                                <div class="ui button compact">
                                    <i class="mail outline icon"></i>
                                </div>
                                <a class="ui basic left pointing label" style="font-weight: 100;" >
                                    <?php echo $TLUMACZENIA['przegladaj11'];?>
                                </a>
                            </div>
                        </td>
                        <td><span id="mieszkanieMAIL"> </span></td>
                    </tr>
                    <tr>
                        <td><i class="call icon"></i> <?php echo $TLUMACZENIA['przegladaj12'];?></td>
                        <td><span id="mieszkanieTEL"> </span></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

    <div class="ui horizontal link list left floated" id="PASEK_Dolny-offer" >
        <a class="item" id="ZglosOferte"><?php echo $TLUMACZENIA['przegladaj13'];?> </a>
        <a class="item" id="UdostepnijOferte"><?php echo $TLUMACZENIA['przegladaj14'];?></a>
        <div class="item popupik" data-content="<?php echo $TLUMACZENIA['przegladaj15'];?>" style="color: #9c9c9c;cursor: default;" id="mieszkanieOSOBA_TYP"></div>
        <div class="item popupik" data-content="<?php echo $TLUMACZENIA['przegladaj16'];?>" style="color: #9c9c9c;cursor: default;" id="mieszkanieDATA"></div>
        <div class="item popupik" data-content="<?php echo $TLUMACZENIA['przegladaj17'];?>" style="color: #9c9c9c;cursor: default;"> <i class="icon eye" style="margin-top: -3px;margin-right: 3px;"></i><span id="mieszkanieVIEWS">0</span>  </div>
    </div>

<?php
include('modal/skontaktujsie.php');
include('modal/zglos.php');
include('modal/udostepnij.php');
?>
<script>
    $(document).ready(function() {

        $('.popupik').popup();

        $('#ZglosOferte').click(function(){
            $('#modal_zglos').modal('show');
        });

        $('#skontaktujsie').click(function(){
            $('#modal_skontaktuj').modal('show');
        });

        $('#UdostepnijOferte').click(function(){
            $('#modal_udostepnij').modal('show');
        });

        $('#idzNamapie').click(function(){
            //fieldorfa 50.087686, 19.937951
            //start  49.862417, 19.349422
            if($( window ).width() < 767)
                $("#ROZWIN_KLIK").click();

            var tymczasowaLok = L.latLng($('#mieszkanieXX').text(), $('#mieszkanieYY').text()-$( window ).width()/1300000);

            MAPA.setView(tymczasowaLok,20);

            var nowe_northWest = MAPA.getBounds().getNorthWest(),
                nowe_southEast = MAPA.getBounds().getSouthEast();

            if(!(x2 < nowe_southEast.lat  && y2 > nowe_southEast.lng && x4 > nowe_northWest.lat  && y4 < nowe_northWest.lng )  )
            {
                usunwszystkieMarkery();

                x2 = MAPA.getBounds().getSouthEast().lat,x4 = MAPA.getBounds().getNorthWest().lat;
                y2 = MAPA.getBounds().getSouthEast().lng,y4 = MAPA.getBounds().getNorthWest().lng;

                tekst = SERWER+'marker/get?x1='+x2+'&x2='+x4+'&y2='+y4+'&y1='+y2+tekst_parametry;
                stary_tekst = SERWER+'marker/get?x1='+x2+'&x2='+x4+'&y2='+y4+'&y1='+y2+'';

                wlaczone_chowanie = false;
                linkDlaListy = tekst;
                $.get(tekst,dodajMARKERY );
            }
        });

        $('#DodajDoObserwowanych').click(function(){
            if($(this).hasClass("yellow"))
            {
                $(this).removeClass("yellow").addClass("empty");
                usunzUlubione(parseInt($(this).parent().siblings('#ID_TEGO').text()));
            }
            else
            {
                $(this).addClass("yellow").removeClass("empty");
                dodajUlubione(parseInt($(this).parent().siblings('#ID_TEGO').text()));

            }
            zalodowanoliste = false;
        })


        var MIESZKANIE_OBJ;
        $.ajax({
            url: SERWER+'offer/get'+window.location.search+'&v=1',
            type: 'GET',
            success: function(data){
                TytulikPrzegladanegoMieszknia = data.title;
                document.title = "FlatMap | " + TytulikPrzegladanegoMieszknia;

                MIESZKANIE_OBJ = data;
                var tymczasowaLok = L.latLng(MIESZKANIE_OBJ.latitude, MIESZKANIE_OBJ.longitude-$( window ).width()/1300000);
                wlaczone_przerzuswanieMapy = false;
                MAPA.setView(tymczasowaLok,20);
                wlaczone_przerzuswanieMapy = true;

//                if(!wlaczone_chowanie)
//                    pobierzEl();


                // ZDJECIA
                if(MIESZKANIE_OBJ.photos.length != 0 )
                {
                    $('#mniejszeobrazki').slick({
                        infinite: false,
                        slidesToShow: 4,
                        slidesToScroll: 1,
                        variableWidth: true
                    });

                    $('#obrazkipodglad').prepend('<a href="'+SERWER+'photo/'+MIESZKANIE_OBJ.photos[0].id+'" title="">'+
                        '<div class="glowne_zdjecie_div_back"><div class="glowne_zdjecie_div">'+
                        '<img  src="'+SERWER+'photo/'+MIESZKANIE_OBJ.photos[0].id+'" /></div></div>'+
                        '</a>');


                    for (var i = 1; i < MIESZKANIE_OBJ.photos.length; ++i) {
                        var normal =  SERWER+'photo/'+MIESZKANIE_OBJ.photos[i].id;
                        var mini =  SERWER+'photo/thumb/'+MIESZKANIE_OBJ.photos[i].id;

                        $('#mniejszeobrazki').slick('slickAdd','<a href="'+normal+'" title="">'+
                            '<div class="miniaturka_div_back"><div class="miniaturka_div"><img src="'+mini+'" /></div></div>'+
                            '</a>');
                    }

                    if($('#mniejszeobrazki a').size() <= 4)
                        $('#mniejszeobrazki').css("margin-left","-17px");



                }

                $('#ID_TEGO').text( MIESZKANIE_OBJ.id );


                czyjestwUlubione(MIESZKANIE_OBJ.id);


                $('#mieszkanieTYTUL').text( MIESZKANIE_OBJ.title );

                $('#mieszkanieXX').text( MIESZKANIE_OBJ.latitude );
                $('#mieszkanieYY').text( MIESZKANIE_OBJ.longitude );

//                $('#mieszkanieOPIS').text( MIESZKANIE_OBJ.description );
//                $('#mieszkanieOPIS').html(htmlForTextWithEmbeddedNewlines('this\n has\n newlines'));
//                console.log( $('#mieszkanieOPIS').html() )
//                $('#mieszkanieOPIS').html($('#mieszkanieOPIS').html().replace(/\n/g,'<br/>'));


                var obj = $("#mieszkanieOPIS").text( MIESZKANIE_OBJ.description );
                obj.html(obj.html().replace(/\\n/g,'<br/>'));

                $('#mieszkanieTYP').text( intToRodzajM(MIESZKANIE_OBJ.property.propertyType) );
                $('#mieszkanieofferTYP').text( intToRodzajT(MIESZKANIE_OBJ.offerType) );


//                var TAGI = intToRodzajM(MIESZKANIE_OBJ.property.propertyType) + ', '
//                            + intToRodzajT(MIESZKANIE_OBJ.offerType) + ', '
//                            + MIESZKANIE_OBJ.property.address.street + ', '
//                            + MIESZKANIE_OBJ.property.address.city + ', '
//                            + intToRodzajBT(MIESZKANIE_OBJ.property.buildingType) + ', '
//                            + MIESZKANIE_OBJ.title + ', ';


                    //CENA
                $('#mieszkanieCENA').text( MIESZKANIE_OBJ.price.value );

                if(MIESZKANIE_OBJ.offerType == 1)
                    $('#mieszkanieCENA').append('<span style="font-size: 85%;"> '+intToWaluta(MIESZKANIE_OBJ.price.currency)+'</span>')
                else if(MIESZKANIE_OBJ.offerType == 0)
                    $('#mieszkanieCENA').append('<sup style="font-size: 70%;"> '+intToWaluta(MIESZKANIE_OBJ.price.currency)+'</sup>&frasl;<sub style="font-size: 60%;" >'+TLUMACZENIA['przegladaj5']+'</sub>')
                else if(MIESZKANIE_OBJ.offerType == 2)
                    $('#mieszkanieCENA').append('<sup style="font-size: 70%;"> '+intToWaluta(MIESZKANIE_OBJ.price.currency)+'</sup>&frasl;<sub style="font-size: 60%;" >'+TLUMACZENIA['przegladaj6']+'</sub>')



                //ADRES
                $('#mieszkanieULICA').text( MIESZKANIE_OBJ.property.address.street );
                $('#mieszkanieFLAT_NR').text( MIESZKANIE_OBJ.property.address.flatNumber );
                $('#mieszkanieMIASTO').text( MIESZKANIE_OBJ.property.address.city );
                $('#mieszkanieKRAJ').text( MIESZKANIE_OBJ.property.address.country );

                //ADRES
                $('#mieszkanieIMIE').text( MIESZKANIE_OBJ.contact.name );
                $('#mieszkanieMAIL').text( MIESZKANIE_OBJ.contact.email );
                $('#mieszkanieTEL').text( MIESZKANIE_OBJ.contact.phoneNumber );

                $('#mieszkanieVIEWS').text( MIESZKANIE_OBJ.views );

                //SZCZEGOLY
                $('#mieszkanieSZCZEGOLY').append(divekSzczegolik(TLUMACZENIA['przegladaj38'],intToRodzajBT(MIESZKANIE_OBJ.property.buildingType),''));
                $('#mieszkanieSZCZEGOLY').append(divekSzczegolik(TLUMACZENIA['przegladaj39'],MIESZKANIE_OBJ.property.area,TLUMACZENIA['wyszukiwarka35']));
                $('#mieszkanieSZCZEGOLY').append(divekSzczegolik(TLUMACZENIA['przegladaj40'],intToHeating(MIESZKANIE_OBJ.property.heatingType),''));
                $('#mieszkanieSZCZEGOLY').append(divekSzczegolik(TLUMACZENIA['przegladaj41'],MIESZKANIE_OBJ.property.basement,''));
                $('#mieszkanieSZCZEGOLY').append(divekSzczegolik(TLUMACZENIA['przegladaj42'],MIESZKANIE_OBJ.property.balcony,''));
                $('#mieszkanieSZCZEGOLY').append(divekSzczegolik(TLUMACZENIA['przegladaj43'],MIESZKANIE_OBJ.property.builtYear,''));
                $('#mieszkanieSZCZEGOLY').append(divekSzczegolik(TLUMACZENIA['przegladaj44'],MIESZKANIE_OBJ.property.climatisation,''));
                $('#mieszkanieSZCZEGOLY').append(divekSzczegolik(TLUMACZENIA['przegladaj45'],MIESZKANIE_OBJ.property.floor,''));
                $('#mieszkanieSZCZEGOLY').append(divekSzczegolik(TLUMACZENIA['przegladaj46'],MIESZKANIE_OBJ.property.furnished,''));
                $('#mieszkanieSZCZEGOLY').append(divekSzczegolik(TLUMACZENIA['przegladaj47'],MIESZKANIE_OBJ.property.garden,''));
                $('#mieszkanieSZCZEGOLY').append(divekSzczegolik(TLUMACZENIA['przegladaj48'],MIESZKANIE_OBJ.property.lift,''));
                $('#mieszkanieSZCZEGOLY').append(divekSzczegolik(TLUMACZENIA['przegladaj49'],MIESZKANIE_OBJ.property.pets,''));
                $('#mieszkanieSZCZEGOLY').append(divekSzczegolik(TLUMACZENIA['przegladaj50'],MIESZKANIE_OBJ.property.roomCount,''));
                $('#mieszkanieSZCZEGOLY').append(divekSzczegolik(TLUMACZENIA['przegladaj51'],MIESZKANIE_OBJ.property.maxPerson,''));
                $('#mieszkanieSZCZEGOLY').append(divekSzczegolik(TLUMACZENIA['przegladaj52'],MIESZKANIE_OBJ.property.parkingPlace,''));

                //SZCZEGOLIKI DOWN
                if( MIESZKANIE_OBJ.person.isAgency )
                    $('#mieszkanieOSOBA_TYP').text( TLUMACZENIA['przegladaj53'] );
                else
                    $('#mieszkanieOSOBA_TYP').text( TLUMACZENIA['przegladaj54'] );

                $('#mieszkanieDATA').text( new Date((MIESZKANIE_OBJ.addDate*1000)).toLocaleString([], {year: 'numeric', month: 'numeric', day: 'numeric'}) );


                $('#PRZEGLADAJ_DIV').perfectScrollbar();
                $('#LADOWANIE_PRZEGLADANIA').hide();
            },
            error: function(data) {
                console.log(data)
               if(data.status == 410)
               {
                   $('#LADOWANIE_PRZEGLADANIA').hide();

                   if(data.responseText == '1')
                        $('#PRZEGLADAJ_DIV').html('<div class="ui icon message">'+
                                               '<i class="wait icon"></i>'+
                                               '<div class="content">'+
                                               '<div class="header">'+
                                                TLUMACZENIA['przegladaj55']+
                                               '</div>'+
                                               '<p>'+TLUMACZENIA['przegladaj56']+'</p>'+
                                               '</div>'+
                                               '</div>');
                   else if(data.responseText == '0')
                       $('#PRZEGLADAJ_DIV').html('<div class="ui negative icon message">'+
                           '<i class="ban icon"></i>'+
                           '<div class="content">'+
                           '<div class="header">'+
                           TLUMACZENIA['przegladaj66']+
                           '</div>'+
                           '<p>'+TLUMACZENIA['przegladaj67']+'</p>'+
                           '</div>'+
                           '</div>');
               }
               else if(data.status == 400)
               {
                   $('#LADOWANIE_PRZEGLADANIA').hide();

                   $('#PRZEGLADAJ_DIV').html('<div class="ui negative icon message">'+
                                               '<i class="remove icon"></i>'+
                                               '<div class="content">'+
                                               '<div class="header">'+
                                                TLUMACZENIA['przegladaj57']+
                                               '</div>'+
                                               '<p>'+TLUMACZENIA['przegladaj58']+'</p>'+
                                               '</div>'+
                                               '</div>');
               }

            }
        });




        $('.accordion').accordion({
            onOpen: function () {
                $('#PRZEGLADAJ_DIV').perfectScrollbar('update');
            },
            onClose: function () {
                $('#PRZEGLADAJ_DIV').perfectScrollbar('update');
            }
        });

        $('.popup-gallery').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: TLUMACZENIA['przegladaj59']+' #%curr%...',
            tClose: TLUMACZENIA['przegladaj65'],
            mainClass: 'mfp-img-mobile',
            gallery: {
                tPrev: TLUMACZENIA['przegladaj62'], // Alt text on left arrow
                tNext: TLUMACZENIA['przegladaj63'], // Alt text on right arrow
                tCounter: '%curr% '+TLUMACZENIA['przegladaj64']+' %total%', // Markup for "1 of 7" counter
                enabled: true,
                navigateByImgClick: true,
                preload: [0,1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">'+TLUMACZENIA['przegladaj60']+' #%curr%</a> '+TLUMACZENIA['przegladaj61'],
                titleSrc: function(item) {
                    return item.el.attr('title');
                }
            }
        });
    });
</script>
