<?php
include('../php/tlumaczenie.php');
?>

<div class="ui fluid two item pointing secondary menu" style="margin-top: -10px;">
    <a class="item active" data-tab="dwa1" >
        <i class="clone icon"></i><?php echo $TLUMACZENIA['lista2'];?>
    </a>
    <a class="item " data-tab="jeden1" >
        <i class="star icon"></i><?php echo $TLUMACZENIA['lista1'];?> (<span id="ileObserwowanych">0</span>)
    </a>

</div>
<div class="ui tab " data-tab="jeden1" style="height: 100%;">
    <?php
    include('lista/ulubione.php');
    ?>
</div>
<div class="ui tab active" data-tab="dwa1" style="height: 100%;">
    <?php
    include('lista/zaznaczone.php');
    ?>
</div>




<script>
    $(document).ready(function() {
        $("#idzdowyszsukiwarki2").click(function(e){
            $('.menu .item').eq(0).click();
            window.history.pushState('MAPA', '', 'wyszukiwarka.php');
            $('#WYSZUKIWANIE_DIV').load('elementy/wyszukiwarka.php');
        });

        $('.menu .item').tab();


        if(localStorage.getItem("X-Auth-Token") !== null)
        {
            $.ajax({
                url: SERWER+"person/getFollowedOffer",
                contentType: "application/json",
                headers : {
                    'X-Auth-Token' : localStorage.getItem('X-Auth-Token')
                },
                success : function(data){
                    if(data.length != 0)
                    {
                        $('#ileObserwowanych').text(data.length);
                    }

                },
                error: function(data)
                {
                    if(ULUBIONE.length != 0)
                    {
                        $('#ileObserwowanych').text(data.length);
                    }
                }
            });
        }
        else
        {
            if(ULUBIONE.length != 0)
            {
                $('#ileObserwowanych').text(ULUBIONE.length);
            }
        }


    });
</script>