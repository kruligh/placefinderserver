<?php
include('../php/tlumaczenie.php');
?>
<div id="fb-root" ></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<div style="height: 100%;position: relative;padding-right: 15px;padding-left: 5px;" id="wyszukiwarkaikryteria">
    <div id="wyszukiwarka1" >
        <h4 class="ui dividing header"><?php echo $TLUMACZENIA['wyszukiwarka1']; ?>
            <span id="wyczscKryteria" style="font-size: 80%;float: right;cursor: pointer;"><i class="icon remove" style="margin-right: 2px;"></i>WYCZYSC</span>
        </h4>

        <div class="ui big header"><i class="world icon"></i><?php echo $TLUMACZENIA['wyszukiwarka2']; ?></div>
        <div class="ui right labeled input fluid" >
            <input type="text" placeholder="<?php echo $TLUMACZENIA['wyszukiwarka3']; ?>" id="wyszukiwarka_mapa" style="width: 100%;">
            <div class="ui dropdown label" id="rodzajZasieg" style="position: absolute;right:0;height: 40px;">
                <div class="text" style="padding-top: 2px;"><?php echo $TLUMACZENIA['wyszukiwarka53'];?></div>
                <i class="dropdown icon"></i>
                <div class="menu" >
                    <div class="item" data-value="1"><?php echo $TLUMACZENIA['wyszukiwarka52'];?></div>
                    <div class="item" data-value="2"><?php echo $TLUMACZENIA['wyszukiwarka53'];?></div>
                    <div class="item" data-value="3"><?php echo $TLUMACZENIA['wyszukiwarka54'];?></div>
                    <div class="item" data-value="4"><?php echo $TLUMACZENIA['wyszukiwarka55'];?></div>
                    <div class="item" data-value="5"><?php echo $TLUMACZENIA['wyszukiwarka56'];?></div>
                </div>
            </div>
        </div>



        <div class="ui big header"><i class="search icon"></i><?php echo $TLUMACZENIA['wyszukiwarka4']; ?></div>


        <div class="ui stackable two column grid" >
            <div class="column">
                <select data-placeholder="<?php echo $TLUMACZENIA['wyszukiwarka5'];?>" data-value="propertyType" id="rodzajNieruchomosci" multiple="multiple" style="width: 100%;"   >
                    <option value="0"><?php echo $TLUMACZENIA['wyszukiwarka6'];?></option>
                    <option value="50"><?php echo $TLUMACZENIA['wyszukiwarka7'];?></option>
                    <option value="100"><?php echo $TLUMACZENIA['wyszukiwarka8'];?></option>
                    <option value="150"><?php echo $TLUMACZENIA['wyszukiwarka9'];?></option>
                    <option value="200"><?php echo $TLUMACZENIA['wyszukiwarka10'];?></option>
                    <option value="250"><?php echo $TLUMACZENIA['wyszukiwarka11'];?></option>
                </select>
            </div>
            <div class="column">
                <select data-placeholder="<?php echo $TLUMACZENIA['wyszukiwarka12'];?>" data-value="offerType" id="rodzajTransakcji" multiple="multiple" style="width: 100%;"  >
                    <option value="0"><?php echo $TLUMACZENIA['wyszukiwarka13'];?></option>
                    <option value="1"><?php echo $TLUMACZENIA['wyszukiwarka14'];?></option>
                    <option value="2"><?php echo $TLUMACZENIA['wyszukiwarka15'];?></option>
                </select>
            </div>
        </div>

    </div>


    <div id="wyszukiwarka2" >

        <div class="ui accordion" style="min-height: 100%;position: relative;">
            <div class=" title">
                <div class="ui header" ><i class="dropdown icon"></i><?php echo $TLUMACZENIA['wyszukiwarka16'];?></div>
            </div>
            <div class="content" id="wiecejpokazschow">
                <div class="field fieldmarginBottom">
                    <div class="ui header tiny">
                        <i class="money icon"></i>
                        <div class="content">
                            <?php echo $TLUMACZENIA['wyszukiwarka17'];?>
                        </div>
                    </div>
                    <div class="ui action input fluid">
                        <input  maxlength="10" type="text" placeholder="<?php echo $TLUMACZENIA['wyszukiwarka18'];?>" id="cenaOd" onkeypress='return validate(event)'>

                        <input maxlength="10" type="text" placeholder="<?php echo $TLUMACZENIA['wyszukiwarka19'];?>" id="cenaDo" onkeypress='return validate(event)'>


                        <select data-placeholder="<?php echo $TLUMACZENIA['wyszukiwarka20'];?>" class="rodzajWalutki" style="width: 15%;">
                            <option value="0"><?php echo $TLUMACZENIA['wyszukiwarka20'];?></option>
                            <option value="1"><?php echo $TLUMACZENIA['wyszukiwarka21'];?></option>
                            <option value="2"><?php echo $TLUMACZENIA['wyszukiwarka22'];?></option>
                        </select>
                    </div>

                    <span class="wiadomoscwarn" id="cenaWiadomosc" style="color: red;"><i class="warning sign icon"></i><?php echo $TLUMACZENIA['wyszukiwarka23'];?></span>

                </div>

                <div class="field fieldmarginBottom" >
                    <div class="ui header tiny">
                        <i class="building  icon"></i>
                        <div class="content">
                            <?php echo $TLUMACZENIA['wyszukiwarka24'];?>
                        </div>
                    </div>
                    <select data-placeholder="<?php echo $TLUMACZENIA['wyszukiwarka25'];?>" id="RodzjaBudynkuu" data-value="buildingType" class="KryteriumWybierzMultiple"  multiple style="width: 100%;"></select>
                </div>


                <div class="field fieldmarginBottom">
                    <div class="ui header tiny">
                        <i class="wait icon"></i>
                        <div class="content">
                            <?php echo $TLUMACZENIA['wyszukiwarka26'];?>
                        </div>
                    </div>
                    <select data-value="lastAdded" class="KryteriumWybierz" data-placeholder="<?php echo $TLUMACZENIA['wyszukiwarka25'];?>" style="width: 100%;">
                        <option></option>
                        <option value="-100"><?php echo $TLUMACZENIA['wyszukiwarka25'];?></option>
                        <option value="0"><?php echo $TLUMACZENIA['wyszukiwarka27'];?></option>
                        <option value="1"><?php echo $TLUMACZENIA['wyszukiwarka28'];?></option>
                        <option value="2"><?php echo $TLUMACZENIA['wyszukiwarka29'];?></option>

                    </select>
                </div>

                <div class="field fieldmarginBottom">
                    <div class="ui header tiny">
                        <i class="fire icon"></i>
                        <div class="content">
                            <?php echo $TLUMACZENIA['wyszukiwarka30'];?>
                        </div>
                    </div>
                    <select data-placeholder="<?php echo $TLUMACZENIA['wyszukiwarka25'];?>" data-value="heatingtype" class="KryteriumWybierzMultiple"  multiple style="width: 100%;">
                        <option value="0"><?php echo $TLUMACZENIA['wyszukiwarka32'];?></option>
                        <option value="1"><?php echo $TLUMACZENIA['wyszukiwarka31'];?></option>
                        <option value="2"><?php echo $TLUMACZENIA['wyszukiwarka33'];?></option>
                        <option value="3"><?php echo $TLUMACZENIA['wyszukiwarka78'];?></option>
                    </select>
                </div>


                <div class="field fieldmarginBottom">
                    <div class="ui header tiny">
                        <i class="cube icon"></i>
                        <div class="content">
                            <?php echo $TLUMACZENIA['wyszukiwarka34'];?>
                        </div>
                    </div>
                    <div class="ui action input fluid">
                        <input type="text" placeholder="<?php echo $TLUMACZENIA['wyszukiwarka18'];?>" id="areaOd" maxlength="5" onkeypress='return validate(event)'>

                        <input type="text" placeholder="<?php echo $TLUMACZENIA['wyszukiwarka19'];?>" id="areaDo" maxlength="5" onkeypress='return validate(event)'>

                        <div class="ui basic label"  > <?php echo $TLUMACZENIA['wyszukiwarka35'];?></div>
                    </div>
                    <span class="wiadomoscwarn" id="powierzchniaWiadomosc" style="color: red;"><i class="warning sign icon"></i><?php echo $TLUMACZENIA['wyszukiwarka23'];?></span>
                </div>

                <div class="field fieldmarginBottom">
                    <div class="ui header tiny">
                        <i class="hourglass end icon"></i>
                        <div class="content">
                            <?php echo $TLUMACZENIA['wyszukiwarka36'];?>
                        </div>
                    </div>
                    <div class="ui action input fluid">
                        <input type="text" placeholder="<?php echo $TLUMACZENIA['wyszukiwarka18'];?>" id="rokOd" maxlength="4" onkeypress='return validate(event)'>
                        <input type="text" placeholder="<?php echo $TLUMACZENIA['wyszukiwarka19'];?>" id="rokDo" maxlength="4" onkeypress='return validate(event)'>

                    </div>
                    <span class="wiadomoscwarn" id="rokWiadomosc" style="color: red;"><i class="warning sign icon"></i><?php echo $TLUMACZENIA['wyszukiwarka37'];?></span>
                </div>

                <div class="field fieldmarginBottom">
                    <div class="ui header tiny">
                        <i class="angle double up icon"></i>
                        <div class="content" >
                            <?php echo $TLUMACZENIA['wyszukiwarka38'];?>
                        </div>
                    </div>
                    <div class="ui action input fluid">
                        <input type="text" placeholder="<?php echo $TLUMACZENIA['wyszukiwarka18'];?>" id="pietroOd" maxlength="2" onkeypress='return validate(event)'>
                        <input type="text" placeholder="<?php echo $TLUMACZENIA['wyszukiwarka19'];?>" id="pietroDo" maxlength="2" onkeypress='return validate(event)'>

                    </div>
                    <span class="wiadomoscwarn" id="pietroWiadomosc" style="color: red;"><i class="warning sign icon"></i><?php echo $TLUMACZENIA['wyszukiwarka23'];?></span>

                </div>

                <div class="field fieldmarginBottom">
                    <div class="ui header tiny">
                        <i class="users icon"></i>
                        <div class="content">
                            <?php echo $TLUMACZENIA['wyszukiwarka39'];?>
                        </div>
                    </div>
                    <div class="ui action input fluid">
                        <input type="text" placeholder="<?php echo $TLUMACZENIA['wyszukiwarka18'];?>" id="iloscosobOd" maxlength="2" onkeypress='return validate(event)'>
                        <input type="text" placeholder="<?php echo $TLUMACZENIA['wyszukiwarka19'];?>" id="iloscosobDo" maxlength="2" onkeypress='return validate(event)'>

                    </div>
                    <span class="wiadomoscwarn" id="iloscOsobWiadomosc" style="color: red;"><i class="warning sign icon"></i><?php echo $TLUMACZENIA['wyszukiwarka23'];?></span>

                </div>

                <div class="field fieldmarginBottom">
                    <div class="ui header tiny">
                        <i class="hotel icon"></i>
                        <div class="content">
                            <?php echo $TLUMACZENIA['wyszukiwarka40'];?>
                        </div>
                    </div>
                    <div class="ui action input fluid">
                        <input type="text" placeholder="<?php echo $TLUMACZENIA['wyszukiwarka18'];?>" id="pokoiOd" maxlength="2" onkeypress='return validate(event)'>
                        <input type="text" placeholder="<?php echo $TLUMACZENIA['wyszukiwarka19'];?>" id="pokoiDo" maxlength="2" onkeypress='return validate(event)'>

                    </div>
                    <span class="wiadomoscwarn" id="iloscPokoiWiadomosc" style="color: red;"><i class="warning sign icon"></i><?php echo $TLUMACZENIA['wyszukiwarka23'];?></span>

                </div>



                <div class="ui divider" style="margin-top: 25px;margin-bottom: 25px;"></div>

                <div class="ui grid">
                    <div style="width: 50%" class="column">
                        <label><?php echo $TLUMACZENIA['wyszukiwarka40'];?></label>
                        <select data-placeholder="<?php echo $TLUMACZENIA['wyszukiwarka25'];?>" data-value="furnished" class="KryteriumWybierzTakNie" style="width: 100%;" >
                            <option></option>
                            <option value="true"><?php echo $TLUMACZENIA['wyszukiwarka41'];?></option>
                            <option value="false"><?php echo $TLUMACZENIA['wyszukiwarka42'];?></option>
                            <option value="-100"><?php echo $TLUMACZENIA['wyszukiwarka25'];?></option>
                        </select>
                    </div>
                    <div style="width: 50%" class="column">
                        <label><?php echo $TLUMACZENIA['wyszukiwarka43'];?></label>
                        <select data-placeholder="<?php echo $TLUMACZENIA['wyszukiwarka25'];?>" data-value="isFromAgency" class="KryteriumWybierzTakNie" style="width: 100%;">
                            <option></option>
                            <option value="true"><?php echo $TLUMACZENIA['wyszukiwarka41'];?></option>
                            <option value="false"><?php echo $TLUMACZENIA['wyszukiwarka42'];?></option>
                            <option value="-100"><?php echo $TLUMACZENIA['wyszukiwarka25'];?></option>
                        </select>
                    </div>
                    <div style="width: 50%" class="column">
                        <label><?php echo $TLUMACZENIA['wyszukiwarka44'];?></label>
                        <select data-placeholder="<?php echo $TLUMACZENIA['wyszukiwarka25'];?>" data-value="lift" class="KryteriumWybierzTakNie" style="width: 100%;" >
                            <option></option>
                            <option value="true"><?php echo $TLUMACZENIA['wyszukiwarka41'];?></option>
                            <option value="false"><?php echo $TLUMACZENIA['wyszukiwarka42'];?></option>
                            <option value="-100"><?php echo $TLUMACZENIA['wyszukiwarka25'];?></option>
                        </select>
                    </div>
                    <div style="width: 50%" class="column">
                        <label><?php echo $TLUMACZENIA['wyszukiwarka45'];?></label>
                        <select data-placeholder="<?php echo $TLUMACZENIA['wyszukiwarka25'];?>" data-value="basement" class="KryteriumWybierzTakNie" style="width: 100%;">
                            <option></option>
                            <option value="true"><?php echo $TLUMACZENIA['wyszukiwarka41'];?></option>
                            <option value="false"><?php echo $TLUMACZENIA['wyszukiwarka42'];?></option>
                            <option value="-100"><?php echo $TLUMACZENIA['wyszukiwarka25'];?></option>
                        </select>
                    </div>
                    <div style="width: 50%" class="column">
                        <label><?php echo $TLUMACZENIA['wyszukiwarka46'];?></label>
                        <select data-placeholder="<?php echo $TLUMACZENIA['wyszukiwarka25'];?>" data-value="smoking" class="KryteriumWybierzTakNie" style="width: 100%;" >
                            <option></option>
                            <option value="true"><?php echo $TLUMACZENIA['wyszukiwarka41'];?></option>
                            <option value="false"><?php echo $TLUMACZENIA['wyszukiwarka42'];?></option>
                            <option value="-100"><?php echo $TLUMACZENIA['wyszukiwarka25'];?></option>
                        </select>
                    </div>
                    <div style="width: 50%" class="column">
                        <label><?php echo $TLUMACZENIA['wyszukiwarka47'];?></label>
                        <select data-placeholder="<?php echo $TLUMACZENIA['wyszukiwarka25'];?>" data-value="pets" class="KryteriumWybierzTakNie" style="width: 100%;">
                            <option></option>
                            <option value="true"><?php echo $TLUMACZENIA['wyszukiwarka41'];?></option>
                            <option value="false"><?php echo $TLUMACZENIA['wyszukiwarka42'];?></option>
                            <option value="-100"><?php echo $TLUMACZENIA['wyszukiwarka25'];?></option>
                        </select>
                    </div>
                    <div style="width: 50%" class="column">
                        <label><?php echo $TLUMACZENIA['wyszukiwarka48'];?></label>
                        <select data-placeholder="<?php echo $TLUMACZENIA['wyszukiwarka25'];?>" data-value="parking" class="KryteriumWybierzTakNie" style="width: 100%;" >
                            <option></option>
                            <option value="true"><?php echo $TLUMACZENIA['wyszukiwarka41'];?></option>
                            <option value="false"><?php echo $TLUMACZENIA['wyszukiwarka42'];?></option>
                            <option value="-100"><?php echo $TLUMACZENIA['wyszukiwarka25'];?></option>
                        </select>
                    </div>
                    <div style="width: 50%" class="column">
                        <label><?php echo $TLUMACZENIA['wyszukiwarka49'];?></label>
                        <select data-placeholder="<?php echo $TLUMACZENIA['wyszukiwarka25'];?>" data-value="balcony" class="KryteriumWybierzTakNie" style="width: 100%;">
                            <option></option>
                            <option value="true"><?php echo $TLUMACZENIA['wyszukiwarka41'];?></option>
                            <option value="false"><?php echo $TLUMACZENIA['wyszukiwarka42'];?></option>
                            <option value="-100"><?php echo $TLUMACZENIA['wyszukiwarka25'];?></option>
                        </select>
                    </div>
                    <div style="width: 50%" class="column">
                        <label><?php echo $TLUMACZENIA['wyszukiwarka50'];?></label>
                        <select data-placeholder="<?php echo $TLUMACZENIA['wyszukiwarka25'];?>" data-value="clima" class="KryteriumWybierzTakNie" style="width: 100%;">
                            <option></option>
                            <option value="true"><?php echo $TLUMACZENIA['wyszukiwarka41'];?></option>
                            <option value="false"><?php echo $TLUMACZENIA['wyszukiwarka42'];?></option>
                            <option value="-100"><?php echo $TLUMACZENIA['wyszukiwarka25'];?></option>
                        </select>
                    </div>
                    <div style="width: 50%" class="column">
                        <label><?php echo $TLUMACZENIA['wyszukiwarka51'];?></label>
                        <select data-placeholder="<?php echo $TLUMACZENIA['wyszukiwarka25'];?>" data-value="garden" class="KryteriumWybierzTakNie" style="width: 100%;">
                            <option></option>
                            <option value="true"><?php echo $TLUMACZENIA['wyszukiwarka41'];?></option>
                            <option value="false"><?php echo $TLUMACZENIA['wyszukiwarka42'];?></option>
                            <option value="-100"><?php echo $TLUMACZENIA['wyszukiwarka25'];?></option>
                        </select>
                    </div>
                </div>


            </div>

            <div id="legenda" style="width:100%;position: absolute;bottom: 0;overflow: hidden;">
                <div class="fb-like" style="bottom: -10px;height: 25px;" data-href="https://www.facebook.com/flatmapp/" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>

                <div style="margin-top: 10px;">
                    <div class="ui divider" style="margin-bottom: 10px;width: 105%;"></div>

                    <div class="ui three column grid"  style="height: 100%;width: 120%;" >
                        <div class="column">
                            <div class="row" >
                                <div class="column" ><div style="width: 20px;padding-bottom: 5px;" class="ui red label"></div> <?php echo $TLUMACZENIA['wyszukiwarka14'];?> </div>
                                <div class="column" style="padding-top: 5px;"><div style="width: 20px;padding-bottom: 5px;" class="ui green label"></div> <?php echo $TLUMACZENIA['wyszukiwarka15'];?> </div>
                                <div class="column" style="padding-top: 5px;"><div style="width: 20px;padding-bottom: 5px;" class="ui blue label"></div> <?php echo $TLUMACZENIA['wyszukiwarka13'];?> </div>
                            </div>
                        </div>
                        <div style="border-left:1px solid #c9c9c9;height:80px;width: 5px;margin-right: -20px;margin-left: -10px;margin-top: 3px;"></div>
                        <div class="column">
                            <div class="row">
                                <div class="column"><i class="icon building"></i><?php echo $TLUMACZENIA['wyszukiwarka7'];?></div>
                                <div class="column" style="padding-top: 5px;"><i class="icon home"></i><?php echo $TLUMACZENIA['wyszukiwarka6'];?></div>
                                <div class="column" style="padding-top: 5px;"><i class="icon coffee" ></i><?php echo $TLUMACZENIA['wyszukiwarka11'];?></div>
                            </div>
                        </div>
                        <div class="column">
                            <div class="row" >
                                <div class="column"><i class="icon bed"></i><?php echo $TLUMACZENIA['wyszukiwarka8'];?></div>
                                <div class="column" style="padding-top: 5px;"><i class="icon car"></i><?php echo $TLUMACZENIA['wyszukiwarka10'];?></div>
                                <div class="column" style="padding-top: 5px;"><i class="icon tree"></i><?php echo $TLUMACZENIA['wyszukiwarka9'];?></div>
                            </div>
                        </div>



                    </div>

                </div>

            </div>
        </div>


    </div>

</div>


<script src="js/wyszukiwarka.js" defer></script>
