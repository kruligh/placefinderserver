<html>
    <head>
        <?php
        include ('ustawienia.php');
        include ('php/wybierzJezyk.php');
        ?>
        <title><?php echo $TLUMACZENIA['meta14']; ?></title>
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <meta name="Description" content="<?php echo $TLUMACZENIA['meta5']; ?>" />
        <meta name="Keywords" content="<?php echo $TLUMACZENIA['meta6']; ?>" />
        <script src="LIB/jquery/jquery-1.12.4.min.js"></script>
        <link rel="stylesheet" type="text/css" href="LIB/semantic/semantic.min.css">
        <script src="LIB/semantic/semantic.min.js"></script>
        <script src="LIB/sha3.js"></script>
        <script>

            var SERWER,LINK;
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {

                if (this.readyState == 4 && this.status == 200) {
                    SERWER = JSON.parse(this.responseText).serwer;
                    LINK  = JSON.parse(this.responseText).link ;

                    $(document).ready(function() {

                        $('#juzktoswycofal').hide();
                        $('.ui.form').form({
                            fields: {
                                password: {
                                    identifier: 'password',
                                    rules: [
                                        {
                                            type   : 'empty',
                                            prompt : '<?php echo $TLUMACZENIA['inne12']; ?>'
                                        },
                                        {
                                            type   : 'minLength[6]',
                                            prompt : '<?php echo $TLUMACZENIA['profil39']; ?>'
                                        }
                                    ]
                                },
                                password2: {
                                    identifier: 'password2',
                                    rules: [
                                        {
                                            type   : 'match[password]',
                                            prompt : '<?php echo $TLUMACZENIA['profil38']; ?>'
                                        }
                                    ]
                                }
                            },
                            onFailure : function(formErrors, fields)
                            {
                                return false;
                            },
                            onSuccess : function(event, fields)
                            {
                                var obiekt = JSON.stringify({"password" : CryptoJS.SHA3(fields.password).toString(),"token" : "<?php echo $_GET['token']; ?>", "requestId" : "<?php echo $_GET['u']; ?>" });


                                $.ajax({
                                    url: SERWER+'forgottenPassword/changePassword',
                                    type: "POST",
                                    contentType: "application/json",
                                    data : obiekt ,
                                    success : function(data){
                                        location.href=LINK;
                                    },
                                    error: function(data)
                                    {
                                        if(data.responseText == "INVALID_DATA")
                                        {
                                            $('#juzktoswycofal').show();
                                        }
                                    }
                                });

                                return false;
                            }
                        }) ;

                    }) ;
                }
            }
            xhttp.open("GET", "php/serwer.php", true);
            xhttp.send();


        </script>

        <style type="text/css">
            body {
                background-color: #DADADA;
            }
            body > .grid {
                height: 100%;
            }
            .image {
                margin-top: -100px;
            }
            .column {
                max-width: 450px;
            }
        </style>
    </head>
    <body>
    <div class="ui middle aligned center aligned grid">
        <div class="column">
            <h2 class="ui teal header">
                <div class="content">
                    <?php echo $TLUMACZENIA['inne10']; ?>
                </div>
            </h2>
            <form class="ui large form">
                <div class="ui stacked segment">
                    <div class="field">
                        <div class="ui left icon input">
                            <i class="user icon"></i>
                            <input type="password" name="password" placeholder="<?php echo $TLUMACZENIA['inne4']; ?>">
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui left icon input">
                            <i class="lock icon"></i>
                            <input type="password" name="password2" placeholder="<?php echo $TLUMACZENIA['inne5']; ?>">
                        </div>
                    </div>
                    <div class="ui fluid large teal submit button"><?php echo $TLUMACZENIA['inne6']; ?></div>
                </div>

                <div class="ui error message"></div>
                <div style="width: 90%;margin: auto;" class="ui error message" id="juzktoswycofal">
                    <div class="header">
                        <?php echo $TLUMACZENIA['inne11']; ?>
                    </div>
                </div>

            </form>

        </div>
    </div>


    </body>
</html>