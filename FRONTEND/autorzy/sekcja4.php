
<div style="width: 70%;margin: auto;" class="ui raised segment">
    <div class="ui header">Skontaktuj się z nami!</div>

    <div class="ui header blue"  >
        <a target="_blank" href="https://www.facebook.com/adachooo">
            <i class="facebook icon"></i>
            <div class="content">
                Facebook
            </div>
        </a>
    </div>
    <div class="ui horizontal divider header">
        LUB
    </div>
    <form class="ui form " style="text-align: left;" method="post" >
        <div class="fields">
            <div class="ten wide field">
                <label>Twój E-mail</label>
                <input type="text" name="email" class="czysc">
            </div>
            <div class="six wide field">
                <label>Imie</label>
                <input type="text" name="imie" class="czysc">
            </div>
        </div>
        <div class="field">
            <label>Text</label>
            <textarea name="text" class="czysc"></textarea>
        </div>
        <div class="ui divider"></div>
        <div class="ui positive message" id="wyslalsiemeil"><i class="icon mail"></i>E-mail został wyslany, wkrótce sie odezwiemy!</div>

        <div class="ui primary submit button fluid" id="WYSLIJ_MEILA">WYSLIJ</div>
        <div class="ui error message"></div>
    </form>
</div>
