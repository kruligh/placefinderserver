$(document).ready(function() {
    $('#fullpage').fullpage({
        verticalCentered: true,
        anchors: ['home', 'pluginy', 'autorzy', 'kontakt'],
        sectionsColor: ['#EDF7D7', '#ffffff', '#ffffff', '#ffffff'],
        navigation: true,
        navigationPosition: 'right',
        navigationTooltips: ['Home', 'Autorzy', 'Pluginy', 'Kontakt'],
        responsiveWidth: 700,
        afterResponsive: function(isResponsive){

        },
        menu: '#menu, #menu_repsonsive'
    });

    $('#pokazschowzawawrtosc ~ li').hide();

    $('#pokazschowzawawrtosc').click(function () {
        $('#pokazschowzawawrtosc ~ li').toggle();
    })

    $('#wyslalsiemeil').hide();

    $('.ui.form').form({
        fields: {
            email: {
                identifier: 'email',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Puste email'
                    }
                ]
            },
            password: {
                identifier: 'text',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Pusty teskt'
                    }
                ]
            }
        },
        onSuccess : function(event, fields)
        {
            $.ajax({
                type: "POST",
                url: "meil.php",
                data: fields,
                success: function(phpReturnResult){
                    // alert('Success: ' + phpReturnResult);
                    // jQuery(".email-us").html("<div id='email-sent'><p>Thank you for the message.</p><p>We will reply as soon as possible. PHP Script's return result: " + phpReturnResult + "</p></div>");
                },
                error: function(errormessage) {
                    //you would not show the real error to the user - this is just to see if everything is working
                    // alert('Sendmail failed possibly php script: ' + errormessage);
                }
            });

            $('#wyslalsiemeil').show();
            $('.czysc').val('')
            return false;
        }
    });

});