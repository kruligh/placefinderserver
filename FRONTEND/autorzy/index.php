<html>
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="LIB/jquery.fullPage.css" />

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>


    <script type="text/javascript" src="LIB/jquery.fullPage.min.js"></script>

    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.3.0/css/font-awesome.min.css" />

    <link rel="stylesheet" type="text/css" href="LIB/semantic/semantic.min.css">
    <script src="LIB/semantic/semantic.min.js"></script>

    <link rel="stylesheet" type="text/css" href="main.css">

    <script src="main.js" ></script>

</head>
<body>
<ul id="menu_repsonsive">
    <li style="cursor: pointer;" id="pokazschowzawawrtosc"><a><i class="icon align justify"></i></a></li>
    <li data-menuanchor="home"><a href="#home"><i class="fa fa-home" aria-hidden="true"></i></a></li>
    <li data-menuanchor="pluginy"><a href="#pluginy">Pluginy</a></li>
    <li data-menuanchor="autorzy"><a href="#autorzy">Autorzy</a></li>
    <li data-menuanchor="kontakt"><a href="#kontakt">Kontakt</a></li>
</ul>

<ul id="menu">
    <li data-menuanchor="home"><a href="#home"><i class="fa fa-home" aria-hidden="true"></i></a></li>
    <li data-menuanchor="pluginy"><a href="#pluginy">Pluginy</a></li>
    <li data-menuanchor="autorzy"><a href="#autorzy">Autorzy</a></li>
    <li data-menuanchor="kontakt"><a href="#kontakt">Kontakt</a></li>
</ul>

<div id="fullpage">

    <div class="section" id="section0"  >
        <div id="obrazek_video"></div>
        <video id="myVideo" loop muted data-autoplay>
            <source src="tlo.mp4" type="video/mp4">
            <source src="tlo.webm" type="video/webm">
        </video>
        <div class="layer">
            <?php
             include ('sekcja1.php');
            ?>
        </div>
    </div>


    <div class="section" id="section1" >
        
        <div class="slide" id="slide1">
            <?php
            include ('sekcja3.php');
            ?>
        </div>
    </div>
    <div class="section" id="section2">
        <?php
        include ('sekcja2.php');
        ?>
    </div>
    <div class="section" id="section3" >
         <?php
        include ('sekcja4.php');
        ?>
    </div>
</div>

</body>
</html>