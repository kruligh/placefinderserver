<div class="ui doubling three column grid">
    <div class="column" >
        <a target="_blank" href="http://leafletjs.com/">
            <h2 class="ui header">
                <img class="ui image obrazki_responsywne" src="ikony/leaflet_icon.png" >
                <div class="content" style="font-size: 170%;">
                    Leaflet
                </div>
            </h2>
        </a>
    </div>
    <div class="column" style="font-size: 250%;">
        <a target="_blank" href="http://semantic-ui.com/">
            <h2 class="ui header">
                <img class="ui image obrazki_responsywne" src="ikony/semantic_icon.png" >
                <div class="content" style="font-size: 170%;">
                    SemanticUI
                </div>
            </h2>
        </a>
    </div>
    <div class="column" style="font-size: 250%;">
        <a target="_blank" href="https://jquery.com/">
            <h2 class="ui header">
                <img class="ui image obrazki_responsywne" src="ikony/jquery_icon.png" >
                <div class="content" style="font-size: 170%;">
                    JQuery
                </div>
            </h2>
        </a>
    </div>
    <div class="column" style="font-size: 170%;"><a target="_blank" href="https://github.com/Leaflet/Leaflet.markercluster">Leaflet.markercluster</a></div>
    <div class="column" style="font-size: 170%;"><a target="_blank" href="http://fontawesome.io/">Font Awesome</a></div>
    <div class="column" style="font-size: 170%;"><a target="_blank" href="http://filer.grandesign.md/">JQuery Filer</a></div>
    <div class="column" style="font-size: 140%;"><a target="_blank" href="http://dimsemenov.com/plugins/magnific-popup/">Magnific Popup</a></div>
    <div class="column" style="font-size: 140%;"><a target="_blank" href="https://github.com/kenwheeler/slick/">Slick</a></div>
    <div class="column" style="font-size: 120%;"><a target="_blank" href="http://easyautocomplete.com/">EasyAutocomplete</a></div>
    <div class="column" style="font-size: 110%;"><a target="_blank" href="https://github.com/Leaflet/Leaflet.draw">Leaflet.draw</a></div>
    <div class="column" style="font-size: 110%;"><a target="_blank" href="https://github.com/noraesae/perfect-scrollbar">Perfect ScrollBar</a></div>
    <div class="column" style="font-size: 110%;"><a target="_blank" href="https://github.com/CliffCloud/Leaflet.EasyButton">L.EasyButton</a></div>
    <div class="column" style="font-size: 100%;"><a target="_blank" href="https://github.com/domoritz/leaflet-locatecontrol">Leaflet.Locate</a></div>
</div>

