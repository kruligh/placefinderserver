$(document).ready(function () {
    inicjuj_mapke();
});


// MARKER KLIK
function markerOnClick()
{
    wlaczone_chowanie = true;
    zalodowanoprzegladanie = false;
    byloprzegladania = '?id='+this.options.id;

    if( !rozwiniete )
        $("#ROZWIN_KLIK2").click();

    $("#PRZEGLADAJ_KLIK").click();
}

var raz= true;
var pochowane_markery = [];
var wlaczone_chowanie = false;

function usunwszystkieMarkery()
{
    if(DEBUGOWANIE_wiadomosci)
        console.log("Usuwam " + markers.length )

    markers.forEach(function(mark) {
        MARKERY.removeLayer(mark);
    });
    markers = [];
    DoDodania = [];
    pochowane_markery = [];
}

var tekst_zaznaczone;
function pochowajMarkery(SW,NE)
{
    tekst_zaznaczone = 'x1='+SW.lat+'&x2='+NE.lat+'&y2='+SW.lng+'&y1='+NE.lng;

    wlaczone_chowanie = true;
    markers.forEach(function(mark) {
        var mypozycjaX = mark.getLatLng().lat,mypozycjaY = mark.getLatLng().lng;
        if(!(mypozycjaX > SW.lat && mypozycjaX < NE.lat && mypozycjaY > SW.lng && mypozycjaY < NE.lng))
        {
            pochowane_markery.push(mark);
            MARKERY.removeLayer(mark);
        }
    });
    Array.prototype.push.apply(DoDodania, pochowane_markery);

    // var kliknij = false;
    // if(zalodowanoliste)
    //     kliknij = true;
    //
    // zalodowanoliste = false;
    if(tekst_zaznaczone)
        byloprzegladanialista = '?'+tekst_zaznaczone+tekst_parametry;
    else
        byloprzegladanialista = '';

    if(DEBUGOWANIE_wiadomosci)
        console.log("Chowam " + DoDodania.length )

    linkDlaListy = SERWER+'marker/getList'+byloprzegladanialista;
    dodajMarkeerywidocznezlinku(linkDlaListy);

    // if(kliknij)
    // {
    //     //console.log("klik")
    //     $('#LIST_KLIK').click();
    // }


}

function pokaz_pochowaneMarkery()
{
    if(DEBUGOWANIE_wiadomosci)
        console.log("Odsłaniam " + DoDodania.length )
    linkDlaListy = stary_tekst;
    dodajMarkeerywidocznezlinku(linkDlaListy);
    
    wlaczone_chowanie = false;
    DoDodania.forEach(function(mark) {
        MARKERY.addLayer(mark);
    });
    DoDodania = [];
}


function dodajMARKERY(data)
{
    if(DEBUGOWANIE_wiadomosci)
        console.log("Dodaje " + data.length)


    for (var i = 0; i < data.length; ++i) {
        var typ,rodzajTransakcji,rodzajMieszkania;
        rodzajTransakcji = data[i]['offerType'];
        rodzajMieszkania = data[i]['propertyType'];

        if(rodzajTransakcji == 0)
            typ = new L.AwesomeMarkers.icon({
                markerColor: 'blue',
                prefix: 'fa',
                iconColor: 'black'
            });
        else if(rodzajTransakcji == 1)
            typ = new L.AwesomeMarkers.icon({
                markerColor: 'red',
                prefix: 'fa',
                iconColor: 'black',
                icon : ''
            });
        else if(rodzajTransakcji == 2)
            typ = new L.AwesomeMarkers.icon({
                markerColor: 'green',
                prefix: 'fa',
                iconColor: 'black'
            });



        if(rodzajMieszkania == 0)
            typ.options.icon = "home";
        else if(rodzajMieszkania == 50)
            typ.options.icon = "building";
        else if(rodzajMieszkania == 100)
            typ.options.icon = "bed";
        else if(rodzajMieszkania == 150)
            typ.options.icon = "tree";
        else if(rodzajMieszkania == 200)
            typ.options.icon = "car";
        else if(rodzajMieszkania == 250)
            typ.options.icon = "coffee";


        var Marker = new L.Marker([data[i]['latitude'], data[i]['longitude']], {
            id : data[i]['id'],
            icon : typ,
            rodzajTransakcji : rodzajTransakcji,
            rodzajMieszkania : rodzajMieszkania
        });

        Marker.on('click', markerOnClick);
        markers.push(Marker);
    }

    if(!raz)
        dodajMarkeerywidocznezlinku(linkDlaListy);
    else
        raz = false;


    //console.log("markerow jest: " + markers.length  )
    if(wlaczone_chowanie )
    {
        Array.prototype.push.apply(DoDodania, markers);
    }
    else
    {
        if(razwlaczone_chowanie)
        {
            MARKERY.addLayers(markers);
            MAPA.addLayer(MARKERY);
        }
        else
        {
            razwlaczone_chowanie = true;
            wlaczone_chowanie = true;
            MARKERY.addLayers(markers);
            MAPA.addLayer(MARKERY);
        }

    }

    $('#LADOWANIE').hide();

}

