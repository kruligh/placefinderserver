var MAPA;
var kosz;
var narysowanenaMapie = new L.FeatureGroup();
var narysowanaFiguradousueniecia;
var MARKERY;
var markers = [];
var DoDodania = [];
var tekst = '';
var tekst_parametry = '';
var stary_tekst;
var lokalizacja;
var ruch = false;
var stary_polygonSW,stary_polygonNE;
var tekstjestgotowy = false;
var drawControl;

var x2,y2,x4,y4;
var ruch = false;
var maksymalnyzoom = 10;
var razzaladujstrone = true;
var razzzaldwowwnostorne = true;
var zaladowanaListajest = false;
var RozmiarListy;
var wlaczone_przerzuswanieMapy = true;

var linkDlaListy = '';
var linkDlaListyKolko = '';

function inicjuj_mapke() {

    var southwest = L.latLng(-90, -180),
        northeast = L.latLng(90, 180),
        bounds = L.latLngBounds(southwest, northeast);


    MAPA = L.map('MAPA', { center: L.latLng(50.061797, 19.931332),  //
                            zoom: 15,
                            minZoom : 5,
                            zoomSnap : 0.1,
                            zoomDelta : 0.25,
                            maxBounds: bounds,
                            attributionControl: false });





    // L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(MAPA);
    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(MAPA);
    // L.tileLayer('https://a.tiles.mapbox.com/v3/mi.0ad4304c/{z}/{x}/{y}.png').addTo(MAPA);


    L.drawLocal.draw.handlers.rectangle.tooltip.start = 'Zaznacz interesujący Cie obszar';
    L.drawLocal.draw.handlers.simpleshape.tooltip.end = 'zaznaczony obszar';
    L.drawLocal.draw.toolbar.actions.text = 'Anuluj';
    L.drawLocal.draw.toolbar.actions.title = 'Anuluj rysowanie';

        dodatek_KOSZ();
        dodatek_RYSOWANIE();
        dodatek_LEGENDA();


    pobierzEl();

        $('#chowajalbopokazikonke').hide();


        $('.leaflet-draw-draw-rectangle').append("<i style='font-size: 150%;margin-left: 3px;' class='write square icon'></i>")
        $('.leaflet-draw-draw-rectangle').parent().append("<div id='rysujTooltip' >"+TLUMACZENIA['mapa1']+"</div>");

        $('#akceptujCookie').click(function () {
            $(this).parent().hide();
            setCookie("akceptuje", true, 30);
        });





}


function pobierzEl()
{

    MAPA.on("zoom",function(){

        if(wlaczone_przerzuswanieMapy)
        {
            if(MAPA.getZoom() < maksymalnyzoom)
            {
                usunwszystkieMarkery();
                x2 = 0,y2 = 0,x4=0, y4= 0;
                narysowanenaMapie.removeLayer(narysowanaFiguradousueniecia);
                kosz.removeFrom(MAPA);
                pokaz_pochowaneMarkery();
                MAPA.removeControl(drawControl);
                $('#chowajalbopokazikonke').hide();
            }
            else
            {
                //MAPA.addControl(drawControl)
                var nowe_northWest = MAPA.getBounds().getNorthWest() ,
                    nowe_southEast = MAPA.getBounds().getSouthEast() ;

                //console.log( (!czyJestwsrodku(nowe_northWest, nowe_southEast) && !ruch) )
                if (!czyJestwsrodku(nowe_northWest, nowe_southEast) && !ruch) {
                    wyslijPunkty(nowe_southEast, nowe_northWest, {lat: x2,lng: y2}, {lat: x4,lng: y4}  );

                    if(x2 > nowe_southEast.lat)
                        x2 = nowe_southEast.lat;
                    if(y2 < nowe_southEast.lng)
                        y2 = nowe_southEast.lng;

                    if(x4 < nowe_northWest.lat)
                        x4 = nowe_northWest.lat;
                    if(y4 > nowe_northWest.lng)
                        y4 = nowe_northWest.lng;


                }
                else {
                    // console.log("jest w srodku wiec nic nie robie")
                    var P1 = MAPA.getBounds().getSouthEast(), P2 = MAPA.getBounds().getNorthWest();
                    stary_tekst = SERWER+'marker/get?x1='+P1.lat+'&y1='+P1.lng+'&x2='+P2.lat+'&y2='+P2.lng+'';
                    linkDlaListy = stary_tekst+tekst_parametry;


                    dodajMarkeerywidocznezlinku(linkDlaListy);
                }
            }

        }
        //console.log( MAPA.getZoom() )


    });



    MAPA.on('dragend', function() {

        if(wlaczone_przerzuswanieMapy)
        {
            if(MAPA.getZoom() < maksymalnyzoom)
            {
                usunwszystkieMarkery();
                x2 = 0,y2 = 0,x4=0, y4= 0;
            }
            else
            {
                var nowe_northWest = MAPA.getBounds().getNorthWest(),
                    nowe_southEast = MAPA.getBounds().getSouthEast();

                if (!czyJestwsrodku(nowe_northWest, nowe_southEast) && !ruch) {

                    wyslijPunkty(nowe_southEast, nowe_northWest, {lat: x2,lng: y2}, {lat: x4,lng: y4}  );

                    if(x2 > nowe_southEast.lat)
                        x2 = nowe_southEast.lat;
                    if(y2 < nowe_southEast.lng)
                        y2 = nowe_southEast.lng;

                    if(x4 < nowe_northWest.lat)
                        x4 = nowe_northWest.lat;
                    if(y4 > nowe_northWest.lng)
                        y4 = nowe_northWest.lng;

                }
                else {
                    //console.log("jest w srodku wiec nic nie robie")
                    var P1 = MAPA.getBounds().getSouthEast(), P2 = MAPA.getBounds().getNorthWest();
                    stary_tekst = SERWER+'marker/get?x1='+P1.lat+'&y1='+P1.lng+'&x2='+P2.lat+'&y2='+P2.lng+'';
                    linkDlaListy = stary_tekst+tekst_parametry;
                    dodajMarkeerywidocznezlinku(linkDlaListy);
                }
            }
        }


    });


    var str = window.location.search;
    // tekst_parametry = str.substring(83,str.length);

    // console.log( tekst_parametry )

    tekst = SERWER+'marker/get?x1='+MAPA.getBounds().getSouthEast().lat+
        '&x2='+MAPA.getBounds().getNorthWest().lat+
        '&y2='+MAPA.getBounds().getNorthWest().lng+
        '&y1='+MAPA.getBounds().getSouthEast().lng;


    //PIerwsze pobranie markerow
    linkDlaListy = tekst;

    $.get(tekst,dodajMARKERY ).done(function () {
        // if(byloprzegladanialista != "")
        // {
        //     var xx1 = getParameterByName('x1',window.location.search);
        //     var xx2 = getParameterByName('x2',window.location.search);
        //     var yy1 = getParameterByName('y1',window.location.search);
        //     var yy2 = getParameterByName('y2',window.location.search);
        //
        //     var polygon = L.polygon([
        //         [xx1, yy1],
        //         [xx1, yy2],
        //         [xx2, yy2],
        //         [xx2, yy1]
        //     ],{
        //         weight: 5,
        //         color: '#4acb40',
        //         fillOpacity : 0.15
        //     }).addTo(MAPA);
        //
        //     wlaczone_chowanie = true;
        //     narysowanenaMapie.addLayer(polygon);
        //     narysowanaFiguradousueniecia = polygon;
        //
        //     ruch = true;
        //     MAPA.fitBounds(polygon.getBounds(), {paddingTopLeft: [350,0]});
        //     ruch = false;
        //
        //
        //     setTimeout(function () {
        //         usunwszystkieMarkery();
        //
        //         x2 = MAPA.getBounds().getSouthEast().lat,x4 = MAPA.getBounds().getNorthWest().lat;
        //         y2 = MAPA.getBounds().getSouthEast().lng,y4 = MAPA.getBounds().getNorthWest().lng;
        //
        //         tekst = SERWER+'marker/get?x1='+x2+'&x2='+x4+'&y2='+y4+'&y1='+y2+tekst_parametry;
        //         stary_tekst = SERWER+'marker/get?x1='+x2+'&x2='+x4+'&y2='+y4+'&y1='+y2+'';
        //
        //         linkDlaListy = tekst;
        //         wlaczone_chowanie = false;
        //         $.get(tekst,dodajMARKERY ).done(function(data) {
        //             RozmiarListy = data.length;
        //
        //
        //             stary_polygonSW = polygon.getBounds()._southWest, stary_polygonNE = polygon.getBounds()._northEast;
        //
        //             pochowajMarkery(stary_polygonSW,stary_polygonNE);
        //
        //
        //             narysowanenaMapie.addLayer(polygon);
        //             narysowanaFiguradousueniecia = polygon;
        //
        //             kosz.addTo(MAPA);
        //             if(!rozwiniete)
        //                 $('.easy-button-container').css({ left: 45});
        //
        //
        //         });
        //
        //     },500)
        //
        //
        //
        // }
    });

    x2 = MAPA.getBounds().getSouthEast().lat,x4 = MAPA.getBounds().getNorthWest().lat;
    y2 = MAPA.getBounds().getSouthEast().lng,y4 = MAPA.getBounds().getNorthWest().lng;

    MARKERY = L.markerClusterGroup({
        chunkedLoading: true,
        removeOutsideVisibleBounds : true,
        maxClusterRadius : 100
    });

}

// '<a style="text-decoration: none;margin-right: 5px;" target="_blank" href="http://flatmap-blog.16mb.com/">Blog </a>'+
var TekstDolnypasek =  '<a style="text-decoration: none;margin-right: 5px;" target="_blank" href="autorzy/index.php">Autorzy </a>  '+
    '<a href="http://leafletjs.com/">Leaflet</a> &copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors</span>';

var TekstDolnypasek2 = '2017 © FlatMap. BETA All rights reserved.';

// +
//     '<span style="font-size: 130%;float:right;margin-top: 14px;">'+
// '<a style="text-decoration: none" target="_blank" href="http://flatmap-blog.16mb.com/">Blog •</a> '+
// '<a style="text-decoration: none" target="_blank" href="autorzy/index.php">Autorzy </a>  '+
// '</span><br>';

var TekstDolnypasekCookies = '<div style="font-size: 87%;">Używamy cookies w celu podniesienia jakości tej strony. '+
    ' Przegladając, zgadzasz się na korzystanie z <a style="color: black;cursor: pointer;">cookies</a> <a style="cursor: pointer;font-size: 120%;" id="akceptujCookie">Zgadzam się</a>  </div>';


function dodatek_LEGENDA() {
    // if(getCookie("akceptuje"))
    // {
        L.control.attribution({position: 'bottomright'})
            .setPrefix(TekstDolnypasek)
            .addTo(MAPA);
        L.control.attribution({position: 'bottomright'})
            .setPrefix(TekstDolnypasek2)
            .addTo(MAPA);
    // }
    // else
    // {
    //     L.control.attribution({position: 'bottomright'})
    //         .setPrefix(TekstDolnypasekCookies)
    //         .addTo(MAPA);
    //     L.control.attribution({position: 'bottomright'})
    //         .setPrefix(TekstDolnypasek)
    //         .addTo(MAPA);
    //     L.control.attribution({position: 'bottomright'})
    //         .setPrefix(TekstDolnypasek2)
    //         .addTo(MAPA);
    // }


}

var usunieterysowane = [];

function dodatek_KOSZ(){
    kosz = L.easyButton( '<i style="margin-left: 2px;" class="icon trash"></i>', function(){
        narysowanenaMapie.removeLayer(narysowanaFiguradousueniecia);
        this.removeFrom(MAPA);

        pokaz_pochowaneMarkery();

        zalodowanoliste = false;
        zaladowanaListajest = false;
        byloprzegladanialista = '';

        if( !rozwiniete )
            $("#ROZWIN_KLIK2").click();

        // $("#LIST_KLIK").click();
        //  $("#WYSZUKIWANIE_KLIK").click();

        //$('#mojezaznaczone').html('');

    });
}


function dodatek_RYSOWANIE() {

    L.drawLocal.draw.toolbar.buttons.rectangle = TLUMACZENIA['mapa2'];
    L.drawLocal.draw.handlers.rectangle.tooltip.start = TLUMACZENIA['mapa3'];

    MAPA.addLayer(narysowanenaMapie);

    drawControl = new L.Control.Draw({
        position: 'topleft',
        draw: {
            polyline: false,
            polygon: false,
            circle: false,
            marker: false
        },
        edit: {
            featureGroup: narysowanenaMapie,
            remove: false,
            edit: false
        }
    });

    drawControl.setDrawingOptions({
        rectangle: {
            shapeOptions: {
                weight: 5,
                color: '#4acb40',
                fillOpacity : 0.15
            }
        }
    });

    MAPA.addControl(drawControl);

    MAPA.on('draw:created', function (e) {
        narysowanenaMapie.addLayer(e.layer);
        narysowanaFiguradousueniecia = e.layer;

        kosz.addTo(MAPA);
        if(!rozwiniete)
            $('.easy-button-container').css({ left: 45});

        stary_polygonSW = e.layer.getBounds()._southWest, stary_polygonNE = e.layer.getBounds()._northEast;

        pochowajMarkery(stary_polygonSW,stary_polygonNE);

    });

    MAPA.on('draw:drawstart', function () {

        if(Object.keys(narysowanenaMapie._layers).length == 1 )
        {
            kosz.removeFrom(MAPA);
            narysowanenaMapie.removeLayer(narysowanaFiguradousueniecia);

            pokaz_pochowaneMarkery();
        }


    });

}
