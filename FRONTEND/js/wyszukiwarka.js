var czyszcze = false, czyszcze2 = false, usuwanie = true;
stary_tekst = tekst;
function wygenerujlinkzparametrami(parametr, wartosc) {
    if (!czyszcze && !czyszcze2) {
        if (!usuwanie) {
            usuwanie = true;

            tekst = tekst.split('&')[0] + '&' + tekst.split('&')[1] + '&' + tekst.split('&')[2] + '&' + tekst.split('&')[3];


            linkDlaListy = tekst;
            $.get(tekst, dodajMARKERY).done(function () {
                if (Object.keys(narysowanenaMapie._layers).length == 1)
                    pochowajMarkery(stary_polygonSW, stary_polygonNE);

            });
        }
        else {
            var nazwa = '&' + parametr + '=';
            tekst = removeParam(parametr, tekst);

            tekst_parametry += nazwa + wartosc;
            var tablica = parseQuery(tekst_parametry);

            if (wartosc == -100) {
                delete tablica[parametr];
            }
            else {

            }

            delete tablica[''];
            var blkstr = [];
            $.each(tablica, function (idx2, val2) {
                var str = idx2 + "=" + val2;
                if (!(parametr == 'propertyType' && idx2 == 'buildingType'))
                    blkstr.push(str);
            });

            tekst_parametry = '&' + blkstr.join("&");
            if (stary_tekst != "")
                tekst = stary_tekst + tekst_parametry;
            else
                tekst += tekst_parametry;

            //console.log(tekst_parametry)

            usunwszystkieMarkery();
            wlaczone_chowanie = false;
            $('#LADOWANIE').show();

            linkDlaListy = tekst;
            $.get(tekst, dodajMARKERY).done(function () {
                if (Object.keys(narysowanenaMapie._layers).length == 1)
                    pochowajMarkery(stary_polygonSW, stary_polygonNE);

            });

            if (tekst_parametry == "&")
                $('#wyczscKryteria').hide();

            if(DEBUGOWANIE)
                console.log(tekst)

        }

    }

}


function rodzajmieszkania(state) {
    if (!state.id) {
        return state.text;
    }


    if (state.id == 0)
        var $state = $('<span><i class="home icon"></i>'+TLUMACZENIA['wyszukiwarka6']+'</span>');
    else if (state.id == 50)
        var $state = $('<span><i class="building icon"></i>'+TLUMACZENIA['wyszukiwarka7']+'</span>');
    else if (state.id == 100)
        var $state = $('<span><i class="bed icon"></i>'+TLUMACZENIA['wyszukiwarka8']+'</span>');
    else if (state.id == 150)
        var $state = $('<span><i class="tree icon"></i>'+TLUMACZENIA['wyszukiwarka9']+'</span>');
    else if (state.id == 200)
        var $state = $('<span><i class="car icon"></i>'+TLUMACZENIA['wyszukiwarka10']+'</span>');
    else if (state.id == 250)
        var $state = $('<span><i class="coffee icon"></i>'+TLUMACZENIA['wyszukiwarka11']+'</span>');

    return $state;
}
function rodzajTakNie(state) {
    if (!state.id) {
        return state.text;
    }

    if (state.id == "true")
        var $state = $('<span><i class="checkmark green icon"></i>'+TLUMACZENIA['wyszukiwarka41']+'</span>');
    else if (state.id == "false")
        var $state = $('<span><i class="remove red icon"></i>'+TLUMACZENIA['wyszukiwarka42']+'</span>');
    else
        var $state = $('<span>'+TLUMACZENIA['wyszukiwarka25']+'</span>');

    return $state;
}
function rodzajTranskacji(state) {
    if (!state.id) {
        return state.text;
    }

    if (state.id == 0)
        var $state = $('<span><i class="blue square icon"></i>'+TLUMACZENIA['wyszukiwarka13']+'</span>');
    else if (state.id == 1)
        var $state = $('<span><i class="red square icon"></i>'+TLUMACZENIA['wyszukiwarka14']+'</span>');
    else if (state.id == 2)
        var $state = $('<span><i class="green square icon"></i>'+TLUMACZENIA['wyszukiwarka15']+'</span>');

    return $state;
}
function Zmianawartosci() {
    $('#wyczscKryteria').show();

    var foo = [];
    $(this).find(':selected').each(function (i, selected) {
        foo[i] = $(selected).val();
    });

    if (foo.length != 0)
        wygenerujlinkzparametrami($(this).data("value"), foo.join("-"));
    else
        wygenerujlinkzparametrami($(this).data("value"), -100);
}

$(document).ready(function () {


    $("#rodzajNieruchomosci").select2({
        minimumResultsForSearch: Infinity,
        templateResult: rodzajmieszkania
    }).on("change", function () {

        var tab2 = [];
        $(this).find(':selected').each(function (i, selected) {
            tab2[i] = $(selected).val();
        });

        if (tab2.length != 0) {
            $('#RodzjaBudynkuu').empty();

            for (var i = 0; i < tab2.length; ++i) {
                var value = tab2[i];
                if (value == 0) {
                    var string = '<option value="0">'+TLUMACZENIA['wyszukiwarka57']+'</option>' +
                        '<option value="1">'+TLUMACZENIA['wyszukiwarka58']+'</option>' +
                        '<option value="2">'+TLUMACZENIA['wyszukiwarka59']+'</option>' +
                        '<option value="2">'+TLUMACZENIA['wyszukiwarka60']+'</option>';

                }
                else if (value == 50) {
                    var string = '<option value="50">'+TLUMACZENIA['wyszukiwarka61']+'</option>' +
                        '<option value="51">'+TLUMACZENIA['wyszukiwarka62']+'</option>';
                }
                else if (value == 100) {
                    var string = '<option value="100">'+TLUMACZENIA['wyszukiwarka63']+'</option>' +
                        '<option value="101">'+TLUMACZENIA['wyszukiwarka64']+'</option>';
                }
                else if (value == 150) {
                    var string = '<option value="150">'+TLUMACZENIA['wyszukiwarka65']+'</option>' +
                        '<option value="151">'+TLUMACZENIA['wyszukiwarka66']+'</option>' +
                        '<option value="152">'+TLUMACZENIA['wyszukiwarka67']+'</option>';
                }
                else if (value == 200) {
                    var string = '<option value="200">'+TLUMACZENIA['wyszukiwarka68']+'</option>' +
                        '<option value="201">'+TLUMACZENIA['wyszukiwarka69']+'</option>' +
                        '<option value="202">'+TLUMACZENIA['wyszukiwarka70']+'</option>';
                }
                else if (value == 250) {
                    var string = '<option value="250">'+TLUMACZENIA['wyszukiwarka71']+'</option>' +
                        '<option value="251">'+TLUMACZENIA['wyszukiwarka72']+'</option>' +
                        '<option value="252">'+TLUMACZENIA['wyszukiwarka73']+'</option>' +
                        '<option value="253">'+TLUMACZENIA['wyszukiwarka74']+'</option>' +
                        '<option value="254">'+TLUMACZENIA['wyszukiwarka75']+'</option>' +
                        '<option value="255">'+TLUMACZENIA['wyszukiwarka76']+'</option>';
                }

                $('#RodzjaBudynkuu').append(string);
            }

        }
        else
            $('#RodzjaBudynkuu').empty();

        $('#wyczscKryteria').show();

        var foo = [];
        $(this).find(':selected').each(function (i, selected) {
            foo[i] = $(selected).val();
        });

        if (foo.length != 0)
            wygenerujlinkzparametrami($(this).data("value"), foo.join("-"));
        else
            wygenerujlinkzparametrami($(this).data("value"), -100);
    });

    $("#rodzajTransakcji").select2({
        minimumResultsForSearch: Infinity,
        templateResult: rodzajTranskacji
    }).on("change", Zmianawartosci);

    $(".KryteriumWybierz").select2({
        minimumResultsForSearch: Infinity
    }).on("change", Zmianawartosci);

    $(".rodzajWalutki").select2({
        minimumResultsForSearch: Infinity
    }).on("change", function () {
        var foo = [];
        $(this).find(':selected').each(function (i, selected) {
            foo[i] = $(selected).val();
        });

        waluta = foo[0];
        cena = cenamin + '-' + cenamax + '-' + waluta;
        if (cena == "--1" || cena == "--2" || cena == "--3")
            wygenerujlinkzparametrami("price", -100);
        else
            wygenerujlinkzparametrami("price", cena);

//            Zmianawartosci();
    });


    $(".KryteriumWybierzMultiple").select2({
        minimumResultsForSearch: Infinity,
        "language": {
            "noResults": function () {
                return TLUMACZENIA['wyszukiwarka77'];
            }
        },
        escapeMarkup: function (markup) {
            return markup;
        }
    }).on("change", Zmianawartosci);

    $(".KryteriumWybierzTakNie").select2({
        minimumResultsForSearch: Infinity,
        templateResult: rodzajTakNie
    }).on("change", Zmianawartosci);

    $('li.select2-search.select2-search--inline > input').attr({"readonly": "readonly"});


    $('#wyczscKryteria').click(function () {
        usunwszystkieMarkery();
        $('#RodzjaBudynkuu').empty();

        czyszcze2 = true;
        $("#rodzajNieruchomosci").val(null).trigger("change");
        $("#rodzajTransakcji").val(null).trigger("change");
        $(".KryteriumWybierz").val(null).trigger("change");
        //$(".rodzajWalutki").val(null).trigger("change");
        $(".KryteriumWybierzMultiple").val(null).trigger("change");
        $(".KryteriumWybierzTakNie").val(null).trigger("change");
        czyszcze2 = false;

        $("#wyszukiwarka_mapa").val("");

        $("#cenaOd").val("");
        $("#cenaDo").val("");
        $("#areaOd").val("");
        $("#areaDo").val("");
        $("#rokOd").val("");
        $("#rokDo").val("");
        $("#pietroOd").val("");
        $("#pietroDo").val("");
        $("#iloscosobOd").val("");
        $("#iloscosobDo").val("");
        $("#pokoiOd").val("");
        $("#pokoiDo").val("");
        kosz.removeFrom(MAPA);
        narysowanenaMapie.removeLayer(narysowanaFiguradousueniecia);

        tekst_parametry = "";
        $('#wyczscKryteria').hide();
        usuwanie = false;
        wygenerujlinkzparametrami('propertyType', -100); // tak dla przykaldu

    });

    var options = {
        url: function (phrase) {
            var link = WYSZUKIWARKA_LINK + phrase;
            return link;
        },
        getValue: function (element) {
            var miasto = "";
            var ulica = "";
            var ulicaNR = "";

            if (element.address.city != undefined)
                miasto = element.address.city;
            else if (element.address.town != undefined)
                miasto = element.address.town;

            if (element.address.road != undefined)
                ulica = element.address.road;

            if (element.address.house_number != undefined)
                ulicaNR = element.address.house_number;

            if( ("" + miasto + " " + ulica + " " + ulicaNR).length != 2 )
                return "" + miasto + " " + ulica + " " + ulicaNR;
            else
                return '';


        },
        list: {
            onChooseEvent: function () {
                var value = $("#wyszukiwarka_mapa").getSelectedItemData();

                wyszukiwanie_google(value, MAPA);

            }
        },
        minCharNumber: 2

    };
    $("#wyszukiwarka_mapa").easyAutocomplete(options);


    $('#wyszukiwarka_mapa').keypress(function (event) {
        if (event.keyCode == 13) {
            $(this).trigger('blur');
            $("#wyszukiwarka_mapa").val($('#eac-container-wyszukiwarka_mapa ul li').eq(0).text()).trigger("change");

            $.get(WYSZUKIWARKA_LINK + $('#eac-container-wyszukiwarka_mapa ul li').eq(0).text(), function (data) {
                wyszukiwanie_google(data[0], MAPA);
            })
        }
    });


    $("#wyszukiwarka_mapa").change(function () {
        tekstjestgotowy = false;
        $('#wyczscKryteria').show();
    });


    $('#wyczscKryteria').hide();

    $('.wiadomoscwarn').hide();
    // Przygotowane jak jest zaznaczona lista zeby pozaznazcac jak trzeba
//        if(byloprzegladanialista != "")
//        {
//            $('#wyczscKryteria').show();
//            var propertyType = getParameterByName('propertyType',byloprzegladanialista);
//            $('#rodzajNieruchomosci').dropdown('set selected', propertyType);
//
//            var offerType = getParameterByName('offerType',byloprzegladanialista);
//            $('#rodzajTransakcji').dropdown('set selected', offerType);
//
//            var buildtingtype = getParameterByName('buildingType',byloprzegladanialista);
//            $('.rodzajBudynku').dropdown('set selected', buildtingtype);
//            if(propertyType ==0)
//                $('#rodzajZabudowyOnOff_HOUSE').show();
//            else if(propertyType == 50)
//                $('#rodzajZabudowyOnOff_FLAT').show();
//            else if(propertyType == 100)
//                $('#rodzajZabudowyOnOff_ROOM').show();
//            else if(propertyType == 150)
//                $('#rodzajZabudowyOnOff_PARCEL').show();
//            else if(propertyType == 200)
//                $('#rodzajZabudowyOnOff_GARAGE').show();
//            else if(propertyType == 250)
//                $('#rodzajZabudowyOnOff_LOCAL').show();
//
//            var price = getParameterByName('price',byloprzegladanialista);
//            if(price != null)
//            {
//                var cenki = price.split("-");
//                $('#cenaOd').val(cenki[0]);
//                $('#cenaDo').val(cenki[1]);
//                $('#rodzajWaluta').val(cenki[2]);
//            }
//
//            var lastAdded = getParameterByName('lastAdded',byloprzegladanialista);
//            $('#aktualnoscogloszenia').dropdown('set selected', lastAdded);
//
//            var iloscosob = getParameterByName('personcount',byloprzegladanialista);
//            if(iloscosob != null)
//            {
//                var iloscosobtab = iloscosob.split("-");
//                for(var i=0;i<iloscosobtab.length;i++)
//                    $('#iloscosob').dropdown('set selected', iloscosobtab[i]);
//            }
//
//            var typogrzewania = getParameterByName('heatingtype',byloprzegladanialista);
//            if(typogrzewania != null)
//            {
//                var typogrzewaniatab = typogrzewania.split("-");
//                for(var i=0;i<typogrzewaniatab.length;i++)
//                    $('#typogrzewania').dropdown('set selected', typogrzewaniatab[i]);
//            }
//
//            var rokk = getParameterByName('year',byloprzegladanialista);
//            if(rokk != null)
//            {
//                var rokktab = rokk.split("-");
//                $('#rokOd').val(rokktab[0]);
//                $('#rokDo').val(rokktab[1]);
//            }
//
//            var areaa = getParameterByName('area',byloprzegladanialista);
//            if(areaa != null)
//            {
//                var areaatab = areaa.split("-");
//                $('#areaOd').val(areaatab[0]);
//                $('#areaDo').val(areaatab[1]);
//            }
//
//
//            var iloscpokoi = getParameterByName('roomcount',byloprzegladanialista);
//            if(iloscpokoi != null)
//            {
//                var iloscpokoitab = iloscpokoi.split("-");
//                for(var i=0;i<iloscpokoitab.length;i++)
//                    $('#iloscpokoi').dropdown('set selected', iloscpokoitab[i]);
//            }
//
//            var pietro = getParameterByName('floor',byloprzegladanialista);
//            if(pietro != null)
//            {
//                var pietrotab = pietro.split("-");
//                for(var i=0;i<pietrotab.length;i++)
//                    $('#pietro').dropdown('set selected', pietrotab[i]);
//            }
//
//            $('#umeblowane').dropdown('set selected', getParameterByName('furnished',byloprzegladanialista));
//            $('#balkon').dropdown('set selected', getParameterByName('balcony',byloprzegladanialista));
//            $('#winda').dropdown('set selected', getParameterByName('lift',byloprzegladanialista));
//            $('#piwnica').dropdown('set selected', getParameterByName('basement',byloprzegladanialista));
//            $('#palacy').dropdown('set selected', getParameterByName('smoking',byloprzegladanialista));
//            $('#zwierzeta').dropdown('set selected', getParameterByName('pets',byloprzegladanialista));
//            $('#parking').dropdown('set selected', getParameterByName('parking',byloprzegladanialista));
//            $('#ogrod').dropdown('set selected', getParameterByName('garden',byloprzegladanialista));
//            $('#klimatyzacja').dropdown('set selected', getParameterByName('clima',byloprzegladanialista));
//            $('#zagencji').dropdown('set selected', getParameterByName('isFromAgency',byloprzegladanialista));
//
//        }

    $('#wyszukiwarkaikryteria').perfectScrollbar();

    $('.accordion').accordion({
        onOpen: function () {
            $('#wyszukiwarkaikryteria').perfectScrollbar('update');
        },
        onClosing: function () {
            $('#wyszukiwarkaikryteria').perfectScrollbar('update');
        }
    });

    $('#przejdzdolisty').on('click',function () {
        var win = window.open('lista.php?'+linkDlaListy.split('?')[1], '_blank');
        win.focus();

    });


    $('#rodzajZasieg').dropdown({
        onChange: function (value, text, $choice) {
            if (value == 1)
                $('#ZASIEG').text('0.01');
            else if (value == 2)
                $('#ZASIEG').text('0.02');
            else if (value == 3)
                $('#ZASIEG').text('0.05');
            else if (value == 4)
                $('#ZASIEG').text('0.10');
            else if (value == 5)
                $('#ZASIEG').text('0.20');

            if (tekstjestgotowy) {
                narysowanenaMapie.removeLayer(narysowanaFiguradousueniecia);
                wyszukiwanie_google(lokalizacja, MAPA);
            }


        }
    });


    // w zaleznosci do jezyka
//        $('#rodzajWaluta').dropdown('set selected', 1);

    waluta = 0;


    var zmianaCena = false;
    $("#cenaOd,#cenaDo").keyup(function () {
        zmianaCena = true;
    });

    var cena = '', cenamin = '', cenamax = '', waluta;


    $("#cenaOd").focusout(function (event) {
        $('#wyczscKryteria').show();
        if (zmianaCena) {
            zmianaCena = false;
            $('#cenaWiadomosc').hide();
            cenamin = $(this).val();
            cena = cenamin + '-' + cenamax + '-' + waluta;

            if (cenamax == '') {
                if (cena.substring(0, 2) != "--")
                    wygenerujlinkzparametrami("price", cena);
                else
                    wygenerujlinkzparametrami("price", -100);
            }
            else {
                if (cenamin == "")
                    wygenerujlinkzparametrami("price", cena);
                else if ((parseInt(cenamin) < parseInt(cenamax) ))
                    if (cena.substring(0, 2) != "--")
                        wygenerujlinkzparametrami("price", cena);
                    else
                        wygenerujlinkzparametrami("price", -100);
                else {
                    wygenerujlinkzparametrami("price", -100);
                    $('#cenaWiadomosc').show();
                }
            }
        }
    });

    $("#cenaDo").focusout(function (event) {
        $('#wyczscKryteria').show();
        if (zmianaCena) {
            zmianaCena = false;
            $('#cenaWiadomosc').hide();
            cenamax = $(this).val();
            cena = cenamin + '-' + cenamax + '-' + waluta;

            if (cenamax == '') {
                wygenerujlinkzparametrami("price", cena);
            }
            else if (cenamin == '') {
                if (cena.substring(0, 2) != "--")
                    wygenerujlinkzparametrami("price", cena);
                else
                    wygenerujlinkzparametrami("price", -100);
            }
            else {
                if ((parseInt(cenamin) < parseInt(cenamax) ))
                    if (cena.substring(0, 2) != "--")
                        wygenerujlinkzparametrami("price", cena);
                    else
                        wygenerujlinkzparametrami("price", -100);
                else {
                    wygenerujlinkzparametrami("price", -100);
                    $('#cenaWiadomosc').show();
                }

            }

        }
    });

    var zmianaRok = false;
    $("#rokOd,#rokDo").keyup(function () {
        zmianaRok = true;
    });
    var rok = '', rokmin = '', rokmax = '';
    $("#rokOd").focusout(function (event) {
        $('#wyczscKryteria').show();
        if (zmianaRok) {
            zmianaRok = false;
            $('#rokWiadomosc').hide();
            if (!$(this).val() == "" && $(this).val() < 1950)
                $(this).val(1950);

            rokmin = $(this).val();
            rok = rokmin + '-' + rokmax;

            if (rokmax == '') {
                if (rok.length != 1)
                    wygenerujlinkzparametrami("year", rok);
                else
                    wygenerujlinkzparametrami("year", -100);
            }
            else {

                if (rokmin == "")
                    wygenerujlinkzparametrami("year", rok);
                else if ((parseInt(rokmin) < parseInt(rokmax) && !(parseInt(rokmin) < 1950 || parseInt(rokmax) > 2017)  ))
                    if (rok.length != 1)
                        wygenerujlinkzparametrami("year", rok);
                    else
                        wygenerujlinkzparametrami("year", -100);
                else {
                    wygenerujlinkzparametrami("year", -100);
                    $('#rokWiadomosc').show();
                }
            }
        }
    });

    $("#rokDo").focusout(function (event) {
        $('#wyczscKryteria').show();
        if (zmianaRok) {
            zmianaRok = false;
            $('#rokWiadomosc').hide();


            if (!$(this).val() == "" && $(this).val() < 1950)
                if ($(this).val() < rokmin)
                    $(this).val(parseInt(rokmin));
                else
                    $(this).val(1950);

            rokmax = $(this).val();
            rok = rokmin + '-' + rokmax;

            if (rokmax == '') {
                wygenerujlinkzparametrami("year", rok);
            }
            else if (rokmin == '') {
                if (rok.length != 1)
                    wygenerujlinkzparametrami("year", rok);
                else
                    wygenerujlinkzparametrami("year", -100);
            }
            else {

                if ((parseInt(rokmin) < parseInt(rokmax) && !(parseInt(rokmin) < 1950 || parseInt(rokmax) > 2017) ))
                    if (rok.length != 1)
                        wygenerujlinkzparametrami("year", rok);
                    else
                        wygenerujlinkzparametrami("year", -100);
                else {
                    wygenerujlinkzparametrami("year", -100);
                    $('#rokWiadomosc').show();
                }
            }
        }
    });

    var zmianaArea = false;
    $("#areaOd,#areaDo").keyup(function () {
        zmianaArea = true;
    });
    var area = '', areamin = '', areamax = '';
    $("#areaOd").focusout(function (event) {
        $('#wyczscKryteria').show();
        if (zmianaArea) {
            zmianaArea = false;
            $('#powierzchniaWiadomosc').hide();
            areamin = $(this).val();
            area = areamin + '-' + areamax;

            if (areamax == '') {
                if (area.length != 1)
                    wygenerujlinkzparametrami("area", area);
                else
                    wygenerujlinkzparametrami("area", -100);
            }
            else {

                if (areamin == "")
                    wygenerujlinkzparametrami("area", area);
                else if ((parseInt(areamin) < parseInt(areamax) ))
                    if (area.length != 1)
                        wygenerujlinkzparametrami("area", area);
                    else
                        wygenerujlinkzparametrami("area", -100);
                else {
                    wygenerujlinkzparametrami("area", -100);
                    $('#powierzchniaWiadomosc').show();
                }
            }
        }
    });

    $("#areaDo").focusout(function (event) {
        $('#wyczscKryteria').show();
        if (zmianaArea) {
            zmianaArea = false;
            $('#powierzchniaWiadomosc').hide();
            areamax = $(this).val();
            area = areamin + '-' + areamax;

            if (areamax == '') {
                wygenerujlinkzparametrami("area", area);
            }
            else if (areamin == '') {
                if (area.length != 1)
                    wygenerujlinkzparametrami("area", area);
                else
                    wygenerujlinkzparametrami("area", -100);
            }
            else {

                if ((parseInt(areamin) < parseInt(areamax) ))
                    if (area.length != 1)
                        wygenerujlinkzparametrami("area", area);
                    else
                        wygenerujlinkzparametrami("area", -100);
                else {
                    wygenerujlinkzparametrami("area", -100);
                    $('#powierzchniaWiadomosc').show();
                }
            }
        }
    });


    var zmianapietro = false;
    $("#pietroOd,#pietroDo").keyup(function () {
        zmianapietro = true;
    });
    var pietro = '', pietromin = '', pietromax = '';
    $("#pietroOd").focusout(function (event) {
        $('#wyczscKryteria').show();
        if (zmianapietro) {
            zmianapietro = false;
            $('#pietroWiadomosc').hide();
            pietromin = $(this).val();
            pietro = pietromin + '-' + pietromax;

            if (pietromax == '') {
                if (pietro.length != 1)
                    wygenerujlinkzparametrami("floor", pietro);
                else
                    wygenerujlinkzparametrami("floor", -100);
            }
            else {

                if (pietromin == "")
                    wygenerujlinkzparametrami("floor", pietro);
                else if ((parseInt(pietromin) < parseInt(pietromax) ))
                    if (pietro.length != 1)
                        wygenerujlinkzparametrami("floor", pietro);
                    else
                        wygenerujlinkzparametrami("floor", -100);
                else {
                    wygenerujlinkzparametrami("floor", -100);
                    $('#pietroWiadomosc').show();
                }
            }
        }
    });

    $("#pietroDo").focusout(function (event) {
        $('#wyczscKryteria').show();
        if (zmianapietro) {
            zmianapietro = false;
            $('#pietroWiadomosc').hide();
            pietromax = $(this).val();
            pietro = pietromin + '-' + pietromax;

            if (pietromax == '') {
                wygenerujlinkzparametrami("floor", pietro);
            }
            else if (pietromin == '') {
                if (pietro.length != 1)
                    wygenerujlinkzparametrami("floor", pietro);
                else
                    wygenerujlinkzparametrami("floor", -100);
            }
            else {

                if ((parseInt(pietromin) < parseInt(pietromax) ))
                    if (pietro.length != 1)
                        wygenerujlinkzparametrami("floor", pietro);
                    else
                        wygenerujlinkzparametrami("floor", -100);
                else {
                    wygenerujlinkzparametrami("floor", -100);
                    $('#pietroWiadomosc').show();
                }
            }
        }
    });


    var zmianailoscosob = false;
    $("#iloscosobOd,#iloscosobDo").keyup(function () {
        zmianailoscosob = true;
    });
    var iloscosob = '', iloscosobmin = '', iloscosobmax = '';
    $("#iloscosobOd").focusout(function (event) {
        $('#wyczscKryteria').show();
        if (zmianailoscosob) {
            zmianailoscosob = false;
            $('#iloscOsobWiadomosc').hide();
            iloscosobmin = $(this).val();
            iloscosob = iloscosobmin + '-' + iloscosobmax;

            if (iloscosobmax == '') {
                if (iloscosob.length != 1)
                    wygenerujlinkzparametrami("personcount", iloscosob);
                else
                    wygenerujlinkzparametrami("personcount", -100);
            }
            else {

                if (iloscosobmin == "")
                    wygenerujlinkzparametrami("personcount", iloscosob);
                else if ((parseInt(iloscosobmin) < parseInt(iloscosobmax) ))
                    if (iloscosob.length != 1)
                        wygenerujlinkzparametrami("personcount", iloscosob);
                    else
                        wygenerujlinkzparametrami("personcount", -100);
                else {
                    wygenerujlinkzparametrami("personcount", -100);
                    $('#iloscOsobWiadomosc').show();
                }
            }
        }
    });

    $("#iloscosobDo").focusout(function (event) {
        $('#wyczscKryteria').show();
        if (zmianailoscosob) {
            zmianailoscosob = false;
            $('#iloscOsobWiadomosc').hide();
            iloscosobmax = $(this).val();
            iloscosob = iloscosobmin + '-' + iloscosobmax;

            if (iloscosobmax == '') {
                wygenerujlinkzparametrami("personcount", iloscosob);
            }
            else if (iloscosobmin == '') {
                if (iloscosob.length != 1)
                    wygenerujlinkzparametrami("personcount", iloscosob);
                else
                    wygenerujlinkzparametrami("personcount", -100);
            }
            else {

                if ((parseInt(iloscosobmin) < parseInt(iloscosobmax) ))
                    if (iloscosob.length != 1)
                        wygenerujlinkzparametrami("personcount", iloscosob);
                    else
                        wygenerujlinkzparametrami("personcount", -100);
                else {
                    wygenerujlinkzparametrami("personcount", -100);
                    $('#iloscOsobWiadomosc').show();
                }
            }
        }
    });


    var zmianailoscpokoi = false;
    $("#pokoiOd,#pokoiDo").keyup(function () {
        zmianailoscpokoi = true;
    });
    var iloscpokoi = '', iloscpokoimin = '', iloscpokoimax = '';
    $("#pokoiOd").focusout(function (event) {
        $('#wyczscKryteria').show();
        if (zmianailoscpokoi) {
            zmianailoscpokoi = false;
            $('#iloscPokoiWiadomosc').hide();
            iloscpokoimin = $(this).val();
            iloscpokoi = iloscpokoimin + '-' + iloscpokoimax;

            if (iloscpokoimax == '') {
                if (iloscpokoi.length != 1)
                    wygenerujlinkzparametrami("roomcount", iloscpokoi);
                else
                    wygenerujlinkzparametrami("roomcount", -100);
            }
            else {

                if (iloscpokoimin == "")
                    wygenerujlinkzparametrami("roomcount", iloscpokoi);
                else if ((parseInt(iloscpokoimin) < parseInt(iloscpokoimax) ))
                    if (iloscpokoi.length != 1)
                        wygenerujlinkzparametrami("roomcount", iloscpokoi);
                    else
                        wygenerujlinkzparametrami("roomcount", -100);
                else {
                    wygenerujlinkzparametrami("roomcount", -100);
                    $('#iloscPokoiWiadomosc').show();
                }
            }
        }
    });

    $("#pokoiDo").focusout(function (event) {
        $('#wyczscKryteria').show();
        if (zmianailoscpokoi) {
            zmianailoscpokoi = false;
            $('#iloscPokoiWiadomosc').hide();
            iloscpokoimax = $(this).val();
            iloscpokoi = iloscpokoimin + '-' + iloscpokoimax;

            if (iloscpokoimax == '') {
                wygenerujlinkzparametrami("roomcount", iloscpokoi);
            }
            else if (iloscpokoimin == '') {
                if (iloscpokoi.length != 1)
                    wygenerujlinkzparametrami("roomcount", iloscpokoi);
                else
                    wygenerujlinkzparametrami("roomcount", -100);
            }
            else {

                if ((parseInt(iloscpokoimin) < parseInt(iloscpokoimax) ))
                    if (iloscpokoi.length != 1)
                        wygenerujlinkzparametrami("roomcount", iloscpokoi);
                    else
                        wygenerujlinkzparametrami("roomcount", -100);
                else {
                    wygenerujlinkzparametrami("roomcount", -100);
                    $('#iloscPokoiWiadomosc').show();
                }
            }
        }
    });


});