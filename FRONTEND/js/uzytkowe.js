var byloprzegladania = "";
var byloprzegladanialista = "";
var zalodowanoprzegladanie = false, zalodowanowyszukiwarke = false, zalodowanowyprofil = false, zalodowanoliste = false;
var rozwiniete = true;
if ($(window).width() < 765)
    rozwiniete = false;

$('.menu .item').tab();
$("#ROZWIN_KLIK2").hide();

var bez = true;
$(window).on('popstate', function () {
    // location.reload();
    bez = false
    var co = location.pathname.substring(location.pathname.lastIndexOf("/") + 1);
    if (co == 'wyszukiwarka.php')
        $('.menu .item').eq(0).click();
    else if (co == 'przegladaj.php')
        $('.menu .item').eq(1).click();
    else if (co == 'lista.php')
        $('.menu .item').eq(2).click();
    else if (co == 'profil.php')
        $('.menu .item').eq(3).click();

});


$(document).ready(function () {
    //$("#ROZWIN_KLIK").click();


    if (localStorage.getItem("X-Auth-Token") !== null) {
        $.ajax({
            url: SERWER + "person/getFollowedOffer",
            contentType: "application/json",
            headers: {
                'X-Auth-Token': localStorage.getItem('X-Auth-Token')
            },
            success: function (data) {
                if (data.length != 0) {
                    $('#iloscUlubionych').show();
                    $('#iloscUlubionych').text(data.length);
                }

            },
            error: function (data) {
                if (ULUBIONE.length != 0) {
                    $('#iloscUlubionych').show();
                    $('#iloscUlubionych').text(data.length);
                }
            }
        });
    }
    else {
        if (ULUBIONE.length != 0) {
            $('#iloscUlubionych').show();
            $('#iloscUlubionych').text(ULUBIONE.length);
        }
    }


});

$("#ROZWIN_KLIK").click(function (e) {
    $("#PASEK").transition('slide right')
    rozwiniete = false;
    $('#MAPA .leaflet-control-zoom').animate({left: 45});
    $('.leaflet-draw-section').animate({left: 45});
    $('.easy-button-container').animate({left: 45});
    $("#ROZWIN_KLIK2").show();
    $('#ROZWIN_KLIK2').animate({left: 10});
});

$("#ROZWIN_KLIK2").click(function (e) {
    $("#PASEK").transition('slide right')
    rozwiniete = true;
    $('#MAPA .leaflet-control-zoom').animate({left: 480});
    $('.leaflet-draw-section').animate({left: 480});
    $('.easy-button-container').animate({left: 480});
    $("#ROZWIN_KLIK2").hide();
    $('#ROZWIN_KLIK2').animate({left: 480});
});


$("#WYSZUKIWANIE_KLIK").click(function (e) {

    document.title = TLUMACZENIA['meta1'];
    if (bez)
        window.history.pushState('MAPA', '', 'wyszukiwarka.php');
    if (!zalodowanowyszukiwarke)
        $('#WYSZUKIWANIE_DIV').load('elementy/wyszukiwarka.php');

    zalodowanowyszukiwarke = true;
    bez = true;
    jestwliscie = false;
});

$("#LIST_KLIK").click(function (e) {

    document.title = TLUMACZENIA['meta2'];
    if (bez)
        window.history.pushState('MAPA', '', 'lista.php' + byloprzegladanialista);
    if (!zalodowanoliste)
        $('#LIST_DIV').load('elementy/lista.php' + byloprzegladanialista);

    zalodowanoliste = true;
    bez = true;
    jestwliscie = true;

});

$("#PRZEGLADAJ_KLIK").click(function (e) {

    if (bez)
        window.history.pushState('MAPA', '', 'przegladaj.php' + byloprzegladania);

    if (!zalodowanoprzegladanie)
        $('#PRZEGLADAJ_DIV').load('elementy/przegladaj.php' + byloprzegladania);
    else {
        if ($('#DodajDoObserwowanych').hasClass('yellow'))
            if (czyjestwUlubione($('#ID_TEGO').text()))
                $('#DodajDoObserwowanych').removeClass("yellow").addClass("empty");

        if (TytulikPrzegladanegoMieszknia !== undefined)
            document.title = "FlatMap | " + TytulikPrzegladanegoMieszknia;
        else
            document.title = TLUMACZENIA['meta3'];
    }


    zalodowanoprzegladanie = true;
    bez = true;
    jestwliscie = false;
});

var TytulikPrzegladanegoMieszknia;
$("#PROFIL_KLIK").click(function (e) {
    document.title = TLUMACZENIA['meta4'];
    if (bez)
        window.history.pushState('MAPA', '', 'profil.php');
    if (!zalodowanowyprofil)
        $('#PROFIL_DIV').load('elementy/profil.php');

    zalodowanowyprofil = true;
    bez = true;
    jestwliscie = false;


});

var linkacz = '', linkacz2 = '';
var stronaa = 1, sortowaniko = '';
var limit = '&limit=10';

function dodawnaie_my(data, ile) {
    for (var i = 0; i < data.content.length; ++i) {


        var ikonka = dodajIkonkedoListy(
            data.content[i].status,
            data.content[i].actualTo,
            data.content[i].views,
            data.content[i].id,
            data.content[i].title,
            data.content[i].property.propertyType,
            data.content[i].offerType,
            data.content[i].photos,
            data.content[i].price.value,
            data.content[i].price.currency,
            data.content[i].property.address.street,
            data.content[i].property.address.country,
            data.content[i].property.address.city, ile);

        if (data.content[i].promotionList == 1) {
            if (!$('#promotedHeader_my').is(':visible'))
                $('#promotedHeader_my').show()
            $('#mojezaznaczone_promowane_my').append(ikonka);
        }
        else if (data.content[i].promotionList == 0) {
            if (!$('#normalHeader_my').is(':visible'))
                $('#normalHeader_my').show()
            $('#mojezaznaczone_my').append(ikonka);
        }


        if (data.content.length - 1 == i)
            if (data.content[i].promotionList == 1)
                $('#mojezaznaczone_my').append('<div id="stronicowanie_my" style="text-align: center;margin-right: 10%;position:relative;top: 50px;">' +
                    '<div id="stronicowanie1_my" class="ui large green label" style="position:relative;top: 2px;cursor: pointer;">' +
                    'Poprzednie' +
                    '</div>' +
                    '<div class="ui mini input" style="width: 44px;">' +
                    '<input type="text" value="1" maxlength="3" id="ktoraStrona_my" onkeypress="validate(event)">' +
                    '</div>' +
                    '<label style="position:relative;top: 2px;left: 3px;margin-right: 5px;">z <span id="zilustronicowanie_my">100</span></label>' +
                    '<div id="stronicowanie2_my" class="ui large green label" style="position:relative;top: 2px;cursor: pointer;">' +
                    'Następna' +
                    '</div>' +
                    '</div>');
            else
                $('#mojezaznaczone_my').append('<div id="stronicowanie_my" style="text-align: center;margin-right: 10%;">' +
                    '<div id="stronicowanie1_my" class="ui large green label" style="position:relative;top: 2px;cursor: pointer;">' +
                    'Poprzednie' +
                    '</div>' +
                    '<div class="ui mini input" style="width: 44px;">' +
                    '<input type="text" value="1" maxlength="3" id="ktoraStrona_my" onkeypress="validate(event)">' +
                    '</div>' +
                    '<label style="position:relative;top: 2px;left: 3px;margin-right: 5px;">z <span id="zilustronicowanie_my">100</span></label>' +
                    '<div id="stronicowanie2_my" class="ui large green label" style="position:relative;top: 2px;cursor: pointer;">' +
                    'Następna' +
                    '</div>' +
                    '</div>');

    }
}


function dodawnaie(data, ile) {
    for (var i = 0; i < data.content.length; ++i) {


        var ikonka = dodajIkonkedoListy(
            data.content[i].status,
            data.content[i].actualTo,
            data.content[i].views,
            data.content[i].id,
            data.content[i].title,
            data.content[i].property.propertyType,
            data.content[i].offerType,
            data.content[i].photos,
            data.content[i].price.value,
            data.content[i].price.currency,
            data.content[i].property.address.street,
            data.content[i].property.address.country,
            data.content[i].property.address.city, ile);

        if (data.content[i].promotionList == 1) {
            if (!$('#promotedHeader').is(':visible'))
                $('#promotedHeader').show()
            $('#mojezaznaczone_promowane').append(ikonka);
        }
        else if (data.content[i].promotionList == 0) {
            if (!$('#normalHeader').is(':visible'))
                $('#normalHeader').show()
            $('#mojezaznaczone').append(ikonka);
        }


        if (data.content.length - 1 == i)
            if (data.content[i].promotionList == 1)
                $('#mojezaznaczone').append('<div id="stronicowanie" style="text-align: center;margin-right: 10%;position:relative;top: 50px;">' +
                    '<div id="stronicowanie1" class="ui large green label" style="position:relative;top: 2px;cursor: pointer;">' +
                    'Poprzednie' +
                    '</div>' +
                    '<div class="ui mini input" style="width: 44px;">' +
                    '<input type="text" value="1" maxlength="3" id="ktoraStrona" onkeypress="validate(event)">' +
                    '</div>' +
                    '<label style="position:relative;top: 2px;left: 3px;margin-right: 5px;">z <span id="zilustronicowanie">100</span></label>' +
                    '<div id="stronicowanie2" class="ui large green label" style="position:relative;top: 2px;cursor: pointer;">' +
                    'Następna' +
                    '</div>' +
                    '</div>');
            else
                $('#mojezaznaczone').append('<div id="stronicowanie" style="text-align: center;margin-right: 10%;">' +
                    '<div id="stronicowanie1" class="ui large green label" style="position:relative;top: 2px;cursor: pointer;">' +
                    'Poprzednie' +
                    '</div>' +
                    '<div class="ui mini input" style="width: 44px;">' +
                    '<input type="text" value="1" maxlength="3" id="ktoraStrona" onkeypress="validate(event)">' +
                    '</div>' +
                    '<label style="position:relative;top: 2px;left: 3px;margin-right: 5px;">z <span id="zilustronicowanie">100</span></label>' +
                    '<div id="stronicowanie2" class="ui large green label" style="position:relative;top: 2px;cursor: pointer;">' +
                    'Następna' +
                    '</div>' +
                    '</div>');

    }
}

function dodajMarkerki_my(link, ile) {
    $.ajax({
        url: link,
        contentType: "application/json",
        headers: {
            'X-Auth-Token': localStorage.getItem('X-Auth-Token')
        },
        success: function (data) {
            // $('#mojezaznaczone').html(JSON.stringify(data));
            // console.log(data)]
            $('#promotedHeader_my').hide();
            $('#normalHeader_my').hide();
            $('#mojezaznaczone_my').empty();
            $('#mojezaznaczone_promowane_my').empty();
            dodawnaie_my(data, ile);


            $('#zilustronicowanie_my').text(data.pages);
            $('#ktoraStrona_my').val(stronaa_my);
            if ($('#ktoraStrona_my').val() == "1")
                $('#stronicowanie1_my').removeClass('green');
            else
                $('#stronicowanie1_my').addClass('green');
            if (data.pages == 1)
                $('#stronicowanie2_my').removeClass('green');
            else
                $('#stronicowanie2_my').addClass('green');

        },
        error: function (request, textStatus, errorThrown) {

        }
    });
}

function dodajMarkerki(link, ile) {
    console.log(link)
    $.ajax({
        url: link,
        contentType: "application/json",
        headers: {
            'X-Auth-Token': localStorage.getItem('X-Auth-Token')
        },
        success: function (data) {
            // $('#mojezaznaczone').html(JSON.stringify(data));
            // console.log(data)]
            $('#promotedHeader').hide();
            $('#normalHeader').hide();
            $('#mojezaznaczone').empty();
            $('#mojezaznaczone_promowane').empty();
            dodawnaie(data, ile);

            $('#zilustronicowanie').text(data.pages);
            $('#ktoraStrona').val(stronaa);
            if ($('#ktoraStrona').val() == "1")
                $('#stronicowanie1').removeClass('green');
            else
                $('#stronicowanie1').addClass('green');
            if (data.pages == 1)
                $('#stronicowanie2').removeClass('green');
            else
                $('#stronicowanie2').addClass('green');

        },
        error: function (request, textStatus, errorThrown) {

        }
    });
}

var listazrysowania = false;
var kolkoblokujace = false;


 var razwlaczone_chowanie = true;

function dodajMarkeerywidocznezlinku(link) {
    // console.log(   Object.keys(narysowanenaMapie._layers).length )
    // console.log(listazrysowania + " " + jestwliscie + " " + Object.keys(narysowanenaMapie._layers).length != 1) )
    if(!kolkoblokujace)
    {
        if( (jestwliscie && Object.keys(narysowanenaMapie._layers).length != 1) || !listazrysowania )
        {
            if(linkDlaListyKolko != "")
            {
                linkacz2 = SERWER + linkDlaListyKolko;
                linkDlaListyKolko = "";
            }
            else
                linkacz2 = SERWER + 'offer/getList?' + link.split("?")[1];

            linkacz = linkacz2 + limit + "&page=" + (stronaa - 1) + sortowaniko;
            if (DEBUGOWANIE)
                console.log(linkacz)

            if(!$("#LIST_DIV").is(":hidden"))
            {
                byloprzegladanialista = link.split("?")[1]+ limit + "&page=" + (stronaa - 1) + sortowaniko;
                window.history.pushState('MAPA', '', 'lista.php?' + byloprzegladanialista);
            }


            $.ajax({
                url: linkacz2 + "&limit=1000000000&page=" + (stronaa - 1) + sortowaniko,
                contentType: "application/json",
                headers: {
                    'X-Auth-Token': localStorage.getItem('X-Auth-Token')
                },
                success: function (data) {
                    $('#ILOSCzaznazcznocyh').html(data.content.length);
                }
            });

            $.ajax({
                url: linkacz,
                contentType: "application/json",
                headers: {
                    'X-Auth-Token': localStorage.getItem('X-Auth-Token')
                },
                success: function (data) {
                    // $('#mojezaznaczone').html(JSON.stringify(data));
                    $('#promotedHeader').hide();
                    $('#normalHeader').hide();
                    $('#mojezaznaczone').empty();
                    $('#mojezaznaczone_promowane').empty();

                    dodawnaie(data, 2);


                    $('#zilustronicowanie').text(data.pages);
                    if ($('#ktoraStrona').val() == "1")
                        $('#stronicowanie1').removeClass('green');
                    else
                        $('#stronicowanie1').addClass('green');
                    if (data.pages == 1)
                        $('#stronicowanie2').removeClass('green');
                    else
                        $('#stronicowanie2').addClass('green');

                    $('#LADOWANIE_MOJEJ_LISTY').hide();

                },
                error: function (request, textStatus, errorThrown) {

                }
            });
        }
    }



}

$(document).on('click', '#stronicowanie1.green', function () {
    stronaa--;
    $('#ktoraStrona').val(stronaa);
    if ($('#ktoraStrona').val() == 1)
        $('#stronicowanie1').removeClass('green');

    if (!$('#stronicowanie2').hasClass('green'))
        $('#stronicowanie2').addClass('green');

    linkacz = linkacz2 + limit + "&page=" + (stronaa - 1) + sortowaniko;
    dodajMarkerki(linkacz, 2)
})

$(document).on('click', '#stronicowanie2.green', function () {
    stronaa++;
    $('#ktoraStrona').val(stronaa);
    if ($('#ktoraStrona').val() == parseInt($('#zilustronicowanie').text()))
        $('#stronicowanie2').removeClass('green');

    if (!$('#stronicowanie1').hasClass('green'))
        $('#stronicowanie1').addClass('green');

    linkacz = linkacz2 + limit + "&page=" + (stronaa - 1) + sortowaniko;
    dodajMarkerki(linkacz, 2)
})

function sprawdzwarunki() {
    if (parseInt($('#zilustronicowanie').text()) != 1) {
        if ($('#ktoraStrona').val() > parseInt($('#zilustronicowanie').text()))
            $('#ktoraStrona').val($('#zilustronicowanie').text())
        else if ($('#ktoraStrona').val() == 0)
            $('#ktoraStrona').val(1);

        stronaa = $('#ktoraStrona').val();
        if ($('#ktoraStrona').val() == $('#zilustronicowanie').text()) {
            $('#stronicowanie2').removeClass('green');

            if (!$('#stronicowanie1').hasClass('green'))
                $('#stronicowanie1').addClass('green');
        }
        else if ($('#ktoraStrona').val() == 1) {
            $('#stronicowanie1').removeClass('green');

            if (!$('#stronicowanie2').hasClass('green'))
                $('#stronicowanie2').addClass('green');
        }
    }
    else
        $('#ktoraStrona').val(1);

}


$(document).on('focusout', '#ktoraStrona', function () {
    sprawdzwarunki();

    linkacz = linkacz2 + limit + "&page=" + (stronaa - 1) + sortowaniko;
    dodajMarkerki(linkacz, 2)

})


$(document).on('keypress', '#ktoraStrona', function (e) {
    if (e.which == 13) {
        $('#ktoraStrona').blur();
    }
})

function dodajUlubionejakiemam() {


    if (localStorage.getItem("X-Auth-Token") !== null) {
        $.ajax({
            url: SERWER + "person/getFollowedOffer",
            contentType: "application/json",
            headers: {
                'X-Auth-Token': localStorage.getItem('X-Auth-Token')
            },
            success: function (data) {
                if (data.length != 0) {
                    $('#brakulubionych').hide();
                    $('#sortowanieiinepierdoly').show();

                    for (var i = 0; i < data.length; i++) {
                        $('#mojeulubione').append(dodajIkonkedoListy(
                            '', '',
                            data[i].views,
                            data[i].id,
                            data[i].title,
                            data[i].property.propertyType,
                            data[i].offerType,
                            data[i].photos,
                            data[i].price.value,
                            data[i].price.currency,
                            data[i].property.address.street,
                            data[i].property.address.country,
                            data[i].property.address.city, 1));
                    }
                }
            },
            error: function (request, textStatus, errorThrown) {
                Ulubin2();
            }
        });

    }
    else {
        Ulubin2()
    }

}


function Ulubin2() {
    if (ULUBIONE.length != 0) {
        $('#brakulubionych').hide();
        for (var i = 0; i < ULUBIONE.length; i++) {
            $.ajax({
                url: SERWER + 'offer/get?id=' + ULUBIONE[i],
                type: 'GET',
                success: function (data) {
                    $('#mojeulubione').append(dodajIkonkedoListy(
                        '', '',
                        data.views,
                        data.id,
                        data.title,
                        data.property.propertyType,
                        data.offerType,
                        data.photos,
                        data.price.value,
                        data.price.currency,
                        data.property.address.street,
                        data.property.address.country,
                        data.property.address.city, 1));
                },
                beforeSend: function (jqXHR, settings) {
                    jqXHR.url = settings.url;
                },
                error: function (jqXHR, exception) {
                    var removeItem = getParameterByName('id', jqXHR.url);

                    ULUBIONE = jQuery.grep(ULUBIONE, function (value) {
                        return value != removeItem;
                    });
                    localStorage.setItem("ULUBIONE", JSON.stringify(ULUBIONE));
                }
            });
        }
    }
}


function intToWyswietlenia(num) {

    if (num >= 1000000000) {
        return (num / 1000000000).toFixed(1).replace(/\.0$/, '') + 'G';
    }
    if (num >= 1000000) {
        return (num / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
    }
    if (num >= 1000) {
        return (num / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
    }
    return num;
}


function dodajIkonkedoListy(stausik, dokiedy, wyswietlen, id, tytul, rodzajM, rodzajT, zdjecia, hajs, waluta, ulica, kraj, miasto, podobrazkiemnr) {
    var adres = '<h4 class="ui grey  header">' + ulica + '<br><div class="sub header">' + kraj + ' ' + miasto + '</div></h4>';

    var dodatekk = '', dodatekk2 = '', dodatekk3 = '';
    var podlinki;
    if (podobrazkiemnr == 1) {
        // ULUBIONE
        podlinki = '<span style="font-size: 80%;">' +
            '<span style="cursor: pointer;float: left;" class="UsunzUlubionych" data-value="' + id + '"><i class="star half empty icon"></i>' + TLUMACZENIA['lista9'] + '</span>' +
            '<span class="wyssortuje" style="background-color:white;display: inline-block;float: right;right:0;margin-right: 25px;position: absolute;"><i class="eye icon"></i>' + intToWyswietlenia(wyswietlen) + '</span>' +
            '</span><span class="idtegogowna" style="display: none">' + id + '</span>'

        dodatekk = '<div style="margin-left: -5px;">' + podlinki + '</div>';
    }
    else if (podobrazkiemnr == 2) {
        // ZAZNACZONE
        var jestwulubionych = false;
        for (var i = 0; i < ULUBIONE.length; i++)
            if (ULUBIONE[i] == id)
                jestwulubionych = true;

        if (!jestwulubionych) {
            podlinki = '<span style="font-size: 80%;margin-left: 2px;">' +
                '<span style="cursor: pointer;float:left;" class="dodajdoobs" data-value="' + id + '"><i class="empty star icon"></i>' + TLUMACZENIA['lista10'] + '</span>' +
                '<span class="wyssortuje" style="width:50px;margin-left: 5px;float:right;margin-right: 15px;background-color:white;display: inline-block;position: absolute;right:0;"><i class="eye icon"></i>' + intToWyswietlenia(wyswietlen) + '</span>' +
                '</span><span class="idtegogowna" style="display: none">' + id + '</span>'
        }
        else {
            podlinki = '<span style="font-size: 80%;margin-left: 2px;">' +
                '<span data-value="' + id + '"><i class="full star icon"></i>' + TLUMACZENIA['lista11'] + '</span>' +
                '<span class="wyssortuje" style="width:50px;margin-left: 5px;float:right;margin-right:10px;background-color:white;display: inline-block;position: absolute;right:0;"><i class="eye icon"></i>' + intToWyswietlenia(wyswietlen) + '</span>' +
                '</span><span class="idtegogowna" style="display: none">' + id + '</span>'
        }
        dodatekk = '<div style="margin-left: -5px;">' + podlinki + '</div>';

    }
    else if (podobrazkiemnr == 3) {
        var cozrob;


        if (stausik == 2) {
            dodatekk2 = '<div style="color: red;float: left;">' + TLUMACZENIA['lista12'] + '</div>'
            cozrob = TLUMACZENIA['lista15'];
            cozrobK = 'UsunzMoich';
            podlinki = '<span style="font-size: 70%;display: inline-block;white-space: nowrap;">' +
                '<span style="cursor: pointer;" class="' + cozrobK + '" data-value="' + id + '"><i class="remove icon"></i>' + cozrob + ' &nbsp;&nbsp;</span>' +
                '<span class="wyssortuje"><i class="eye icon"></i>' + intToWyswietlenia(wyswietlen) + '</span>' +
                '</span><span class="idtegogowna" style="display: none">' + id + '</span>'
        }
        else if (stausik == 1) {
            dodatekk2 = '<div style="color: silver;float: left;">' + TLUMACZENIA['lista13'] + '</div>'
            cozrob = TLUMACZENIA['lista15'];
            cozrobK = 'UsunzMoich';
            podlinki = '<span style="font-size: 70%;display: inline-block;white-space: nowrap;">' +
                '<span style="cursor: pointer;" class="' + cozrobK + '" data-value="' + id + '"><i class="remove icon"></i>' + cozrob + ' &nbsp;&nbsp;</span>' +
                '<span class="editdodane" style="cursor: pointer;"><i class="edit icon"></i>' + TLUMACZENIA['lista19'] + ' </span>' +
                '<span class="wyssortuje"><i class="eye icon"></i>' + intToWyswietlenia(wyswietlen) + '</span>' +
                '</span><span class="idtegogowna" style="display: none">' + id + '</span>'
        }
        else if (stausik == 0) {
            dodatekk2 = '<div class="aktynwpopup" style="color: green;float: left;cursor: pointer;" data-content="Aktywne do ' + new Date((dokiedy * 1000)).toLocaleString([], {
                    year: 'numeric',
                    month: 'numeric',
                    day: 'numeric'
                }) + '">' + TLUMACZENIA['lista14'] + '</div>'
            cozrob = TLUMACZENIA['lista16'];
            cozrobK = 'Deaktywuj';
            podlinki = '<span style="font-size: 70%;display: inline-block;white-space: nowrap;">' +
                '<span style="cursor: pointer;" class="' + cozrobK + '" data-value="' + id + '"><i class="remove icon"></i>' + cozrob + ' &nbsp;&nbsp;</span>' +
                '<span class="editdodane" style="cursor: pointer;"><i class="edit icon"></i>' + TLUMACZENIA['lista17'] + ' </span>' +
                '<span class="wyssortuje"><i class="eye icon"></i>' + intToWyswietlenia(wyswietlen) + '</span>' +
                '</span><span class="idtegogowna" style="display: none">' + id + '</span>'
        }


        // MOJE


        dodatekk3 = '<div class="opcjepokazschowaikonke" style="display:none;float: left;margin-left: -5px;">' + podlinki + '</div>';

        dodatekk = '<div style="font-size: 95%;"><span class="opcjepokazschowaikonke_klik" style="float: left;margin-left: -5px;margin-right: 5px;color: grey;cursor: pointer;">' +
            '<i class="icon configure"></i>' + TLUMACZENIA['lista18'] + '</span>' + dodatekk2 + '</div>' +
            dodatekk3;


    }

    var podobrazkiem = '<div class="podobrazkiem " style="height: 25px;float: left;margin-top: 10px;margin-bottom: -10px;">' + podlinki + '</div>'

    var waluta;
    if (rodzajT == 0) {
        waluta = '<span style="margin-left: 3px;"><span style="font-size: 80%;">' + intToWaluta(waluta) + '/' + TLUMACZENIA['przegladaj5'] + '</span><span style="font-size: 70%;"></span></span>';
    }
    else if (rodzajT == 1) {
        waluta = '<span style="margin-left: 3px;"><span style="font-size: 80%;">' + intToWaluta(waluta) + '</span></span>';
    }
    else if (rodzajT == 2) {
        waluta = '<span style="margin-left: 3px;"><span style="font-size: 80%;">' + intToWaluta(waluta) + '/' + TLUMACZENIA['przegladaj6'] + '</span><span style="font-size: 70%;"></span></span>';
    }


    var obr = '';
    if (zdjecia.length != 0)
        obr = SERWER + 'photo/' + zdjecia[0].id + '.jpg';
    else
        obr = 'img/noimage.png';


// '<div style="width: 50%;margin-bottom: 20px;float: left;" class="image " >'+
    // '<img src="'+obr+'" class="linkItem2" style="cursor:pointer;max-height: 135px;" >'+
    // podobrazkiem+
    // '</div>'+
    return '<div class="dosortowania " style="width: 105%;">' +
        '<div style="cursor: pointer;float:left;" class="lista_div_back linkItem2"><div class="lista_div">' +
        '<img style="margin-bottom: -5px;"  src="' + obr + '" /></div></div>' +
        '<div class="content obiektlista" style="width: 42%;float:left;margin-top: -3px;margin-bottom: 40px;">' +
        '<span class="tytulaktywny"><a class="ui header " >' + tytul + '</a></span><hr style="border-top: 1px solid #dcdcdc;margin-right: 15px;">' +
        '<div class="meta Odstpedol">' +
        adres +
        '</div>' +
        '<div class="extra Odstpedol">' +
        '<div style="width:90%;overflow: hidden;font-size: 14px;line-height: 1.5;color: #858585;text-overflow: ellipsis;white-space: nowrap;" class="ui left floated">' +
        intToRodzajM(rodzajM) + ' &nbsp;na&nbsp; ' + intToRodzajT(rodzajT) +
        '</div>' +
        '</div>' +
        '<div style="font-size: 120%;top: -8px;" class="extra Odstpedol">' +
        '<span class="hajssortuje">' + hajs + '</span>' + waluta +
        '</div>' +
        dodatekk +
        '</div>';


}

$(document).on('click', '.opcjepokazschowaikonke_klik', function () {
    $('.opcjepokazschowaikonke').toggle();
});

$(document).on('click', '.tytulaktywny', function () {
    if ($(this).hasClass("aktywny"))
        $(this).removeClass("aktywny");
    else
        $(this).addClass("aktywny");
});

$(document).on('click', '.linkItem, .linkItem2', function () {

    var idek;
    if ($(this).hasClass('linkItem2'))
        idek = $(this).parent().find('.idtegogowna').text()
    else
        idek = $(this).parent().parent().find('.idtegogowna').text()

    if (!wlaczone_chowanie)
        usunwszystkieMarkery();

    zalodowanoprzegladanie = false;
    byloprzegladania = '?id=' + idek;

    if (!rozwiniete)
        $("#ROZWIN_KLIK2").click();

    $("#PRZEGLADAJ_KLIK").click();

    //console.log("linkitem" + $(this).parent().parent().find('.idtegogowna').text() )
});

$(document).on('click', '.UsunzUlubionych', function () {
    usunzUlubione($(this).data("value"));
    $(this).parentsUntil('.dosortowania').parent().remove();
    zalodowanoliste = false;
    $('#LIST_KLIK').click();
});

$(document).on('click', '.UsunzMoich', function () {
    var iderewk = $(this).data("value");
    $('#idtegodousuniecia').text(iderewk);
    $('#UsunModal').modal('show');
});

$(document).on('click', '.Deaktywuj', function () {
    var iderewk = $(this).data("value");
    $('#idtegododeaktywacji').text(iderewk);
    $('#DeaktywujModal').modal('show');
});

$(document).on('click', '#potwierdzUsuniecie', function () {
    $.ajax({
        url: SERWER + 'person/offer/deleteMyOffer',
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify({offerId: $('#idtegodousuniecia').text()}),
        headers: {
            'X-Auth-Token': localStorage.getItem('X-Auth-Token')
        },
        success: function (data) {
            location.reload();
        },
        error: function (data) {
        }
    });

});

$(document).on('click', '#potwierdzDeaktywacje', function () {
    $.ajax({
        url: SERWER + 'person/offer/deleteMyOffer',
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify({offerId: $('#idtegododeaktywacji').text()}),
        headers: {
            'X-Auth-Token': localStorage.getItem('X-Auth-Token')
        },
        success: function (data) {
            location.reload();
        },
        error: function (data) {
        }
    });

});

$(document).on('click', '.dodajdoobs', function () {


    dodajUlubione(parseInt($(this).data("value")));
    zalodowanoliste = false;
    $('#LIST_KLIK').click();


});


var ULUBIONE = [];
// OBSerwowane
if (localStorage.getItem("ULUBIONE")) {
    ULUBIONE = JSON.parse(localStorage.getItem("ULUBIONE"));
} else {
    localStorage.setItem("ULUBIONE", JSON.stringify(ULUBIONE));
}

function czyjestwUlubione(nr) {
    if (localStorage.getItem("X-Auth-Token") !== null) {


        $.ajax({
            url: SERWER + "person/getFollowedOffer",
            contentType: "application/json",
            headers: {
                'X-Auth-Token': localStorage.getItem('X-Auth-Token')
            },
            success: function (data) {
                if (data.length != 0) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].id == nr)
                            $('#DodajDoObserwowanych').addClass("yellow").removeClass("empty");
                    }

                }
            },
            error: function (data) {
                for (var i = 0; i < ULUBIONE.length; i++)
                    if (ULUBIONE[i] == nr)
                        $('#DodajDoObserwowanych').addClass("yellow").removeClass("empty");
            }
        });
    }
    else {
        for (var i = 0; i < ULUBIONE.length; i++)
            if (ULUBIONE[i] == nr)
                $('#DodajDoObserwowanych').addClass("yellow").removeClass("empty");

    }


}

function dodajUlubione(nr) {

    if (localStorage.getItem("X-Auth-Token") !== null) {
        $.ajax({
            url: SERWER + "person/followOffer",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({offerId: nr}),
            headers: {
                'X-Auth-Token': localStorage.getItem('X-Auth-Token')
            },
            success: function (data) {

            },
            error: function (data) {
                var jestwulubionych = false;
                for (var i = 0; i < ULUBIONE.length; i++)
                    if (ULUBIONE[i] == nr)
                        jestwulubionych = true;

                if (!jestwulubionych) {
                    ULUBIONE.push(nr);
                    localStorage.setItem("ULUBIONE", JSON.stringify(ULUBIONE));


                }

            }
        });
    }
    else {
        var jestwulubionych = false;
        for (var i = 0; i < ULUBIONE.length; i++)
            if (ULUBIONE[i] == nr)
                jestwulubionych = true;

        if (!jestwulubionych) {
            ULUBIONE.push(nr);
            localStorage.setItem("ULUBIONE", JSON.stringify(ULUBIONE));
        }
    }
    $('#iloscUlubionych').show();
    var ilejest = parseInt($('#iloscUlubionych').text()) || 0;
    $('#iloscUlubionych').text(ilejest + 1);
}

function usunzUlubione(nr) {
    if (localStorage.getItem("X-Auth-Token") !== null) {
        $.ajax({
            url: SERWER + "person/unfollowOffer",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({offerId: nr}),
            headers: {
                'X-Auth-Token': localStorage.getItem('X-Auth-Token')
            },
            success: function (data) {

            },
            error: function (data) {
                var index = ULUBIONE.indexOf(nr);
                if (index > -1) {
                    ULUBIONE.splice(index, 1);
                    localStorage.setItem("ULUBIONE", JSON.stringify(ULUBIONE));
                }
            }
        });
    }
    else {
        var index = ULUBIONE.indexOf(nr);
        if (index > -1) {
            ULUBIONE.splice(index, 1);
            localStorage.setItem("ULUBIONE", JSON.stringify(ULUBIONE));
        }
    }

    var ilejest = parseInt($('#iloscUlubionych').text()) || 0;
    if (ilejest == 1)
        $('#iloscUlubionych').hide();
    $('#iloscUlubionych').text(ilejest - 1);

}

function rysujnaobecnym() {
    // tekstjestgotowy = true;
    // lokalizacja= place;
    // var xx = parseFloat(place.lat) , yy = parseFloat(place.lon);
    // var ZASIEG = parseFloat($('#ZASIEG').text());
    //
    // if(narysowanaFiguradousueniecia != undefined)
    // {
    //     narysowanenaMapie.removeLayer(narysowanaFiguradousueniecia);
    //     pokaz_pochowaneMarkery();
    // }
    //

    //
    // ZASIEG *= 50000;
    // console.log(ZASIEG)


}


function wyszukiwanie_google(place, map) {
    tekstjestgotowy = true;
    lokalizacja = place;
    var xx = parseFloat(place.lat), yy = parseFloat(place.lon);
    var ZASIEG = parseFloat($('#ZASIEG').text());

    if (narysowanaFiguradousueniecia != undefined) {
        narysowanenaMapie.removeLayer(narysowanaFiguradousueniecia);
        pokaz_pochowaneMarkery();
    }

    // [xx-ZASIEG/2, yy-ZASIEG],
    //     [xx+ZASIEG/2, yy-ZASIEG],
    //     [xx+ZASIEG/2, yy+ZASIEG],
    //     [xx-ZASIEG/2, yy+ZASIEG]

    ZASIEG *= 50000;
    var polygon = L.circle([xx, yy], {
        weight: 5,
        color: '#4acb40',
        fillOpacity: 0.15,
        radius: ZASIEG
    }).addTo(map);

    ruch = true;
    if ($(document).width() < 765)
        MAPA.fitBounds(polygon.getBounds());
    else
        MAPA.fitBounds(polygon.getBounds(), {paddingTopLeft: [350, 0]});
    ruch = false;


    if (!(x2 < xx - ZASIEG / 2 && y2 > yy + ZASIEG && x4 > xx + ZASIEG / 2 && y4 < yy - ZASIEG)) {

        usunwszystkieMarkery();


        /*  Dominik To zmienił na to poniżej i naprawił ale nie wie czy czegoś innego nie zepsuł xD:D
         wszystko wskazuje na to ze wszystko gra
         x2 = MAPA.getBounds().getSouthEast().lat,x4 = MAPA.getBounds().getNorthWest().lat;
         y2 = MAPA.getBounds().getSouthEast().lng,y4 = MAPA.getBounds().getNorthWest().lng;*/
        x2 = polygon.getBounds().getSouthEast().lat, x4 = polygon.getBounds().getNorthWest().lat;
        y2 = polygon.getBounds().getSouthEast().lng, y4 = polygon.getBounds().getNorthWest().lng;

        tekst = SERWER + 'marker/getCircle?circleLat=' + xx + '&circleLng=' + yy + '&circleR=' + ZASIEG + '&x1=' + x2 + '&x2=' + x4 + '&y2=' + y4 + '&y1=' + y2 + tekst_parametry;

        stary_tekst = SERWER + 'marker/get?x1=' + x2 + '&x2=' + x4 + '&y2=' + y4 + '&y1=' + y2 + '';

        wlaczone_chowanie = false;

        linkDlaListyKolko = 'offer/getCircleList?circleLat=' + xx + '&circleLng=' + yy + '&circleR=' + ZASIEG + '&x1=' + x2 + '&x2=' + x4 + '&y2=' + y4 + '&y1=' + y2 + "&limit=10&page=0"+ tekst_parametry;;

        kolkoblokujace = true;

        console.log(linkDlaListy);
        $.get(tekst, dodajMARKERY).done(function () {
            stary_polygonSW = polygon.getBounds()._southWest, stary_polygonNE = polygon.getBounds()._northEast;

            pochowajMarkery(stary_polygonSW, stary_polygonNE);

            // dodajMarkeerywidocznezlinku(linkDlaListy);
            narysowanenaMapie.addLayer(polygon);
            narysowanaFiguradousueniecia = polygon;

            kosz.addTo(MAPA);
            if (!rozwiniete)
                $('.easy-button-container').css({left: 45});

        });

        setTimeout(function () {
            kolkoblokujace = false;
        },500)

    }
    else {
        // W srodku, chowam jakie trzeba
        //console.log("Wsrodku")


        stary_polygonSW = polygon.getBounds()._southWest, stary_polygonNE = polygon.getBounds()._northEast;

        pochowajMarkery(stary_polygonSW, stary_polygonNE);

        narysowanenaMapie.addLayer(polygon);
        narysowanaFiguradousueniecia = polygon;

        kosz.addTo(MAPA);
        if (!rozwiniete)
            $('.easy-button-container').css({left: 45});

    }


}

var zakresWyszukwianiaGoogle = 0.1;

var MARKER_RPZECIAGNY;
function wyszukiwanie_google_formularz(place, map, zdodawania) {
    var srodekX = (parseFloat(place.boundingbox[0]) + parseFloat(place.boundingbox[1]) ) / 2;
    var srodekY = (parseFloat(place.boundingbox[2]) + parseFloat(place.boundingbox[3]) ) / 2;


    if (zdodawania) {

        // $('#ADRESY').show();

        $('#formularz_kraj').val(place.address.country);
        if (place.address.town == undefined)
            $('#formularz_miasto').val(place.address.city);
        else
            $('#formularz_miasto').val(place.address.town);
        if (place.address.house_number == undefined)
            $('#formularz_ulica').val(place.address.road);
        else
            $('#formularz_ulica').val(place.address.road + " " + place.address.house_number);


        if ($('#formularz_ulica').val() == "")
            $('#PUSTA_ULICA').show();
        else {
            map.eachLayer(function (layer) {
                if (layer.options.draggable)
                    map.removeLayer(layer);
            });
            var xxx = parseFloat(place.lat), yyy = parseFloat(place.lon);
            var marker = L.marker([xxx, yyy], {draggable: 'true'}).addTo(map);
            marker.bindPopup("<b>Przeciagnij aby lepiej<br>okreslic miejsce!</b>").openPopup();

            $('#formularz_XX').val(xxx);
            $('#formularz_YY').val(yyy);

            map.setView([xxx, yyy], 15);

            var ostatniaDobraPozX = xxx,
                ostatniaDobraPozY = yyy;

            marker.on('dragend', function (event) {
                var marker = event.target;
                var position = marker.getLatLng();

                var zakress = Math.sqrt(Math.abs(srodekY - position.lng) * Math.abs(srodekY - position.lng) +
                    Math.abs(srodekX - position.lat) * Math.abs(srodekX - position.lat));

                if (zakress > zakresWyszukwianiaGoogle) {
                    marker.bindPopup("<b>Za daleko od wpisanego adresu!</b>").openPopup();
                    marker.setLatLng(new L.LatLng(ostatniaDobraPozX, ostatniaDobraPozY), {draggable: 'true'});
                    // map.panTo(new L.LatLng(ostatniaDobraPozX,ostatniaDobraPozY));
                    $('#formularz_XX').val(ostatniaDobraPozX);
                    $('#formularz_YY').val(ostatniaDobraPozY);
                }
                else {
                    ostatniaDobraPozX = position.lat,
                        ostatniaDobraPozY = position.lng;
                    marker.setLatLng(new L.LatLng(position.lat, position.lng), {draggable: 'true'});
                    map.panTo(new L.LatLng(position.lat, position.lng));
                    $('#formularz_XX').val(position.lat);
                    $('#formularz_YY').val(position.lng);
                }


            });
            map.addLayer(marker);
            $('#PUSTA_ULICA').hide();

        }


    }
    else {
        //$('#ADRESY_edit').show();

        $('#formularz_kraj_edit').val(place.address.country);
        if (place.address.town == undefined)
            $('#formularz_miasto_edit').val(place.address.city);
        else
            $('#formularz_miasto_edit').val(place.address.town);
        if (place.address.house_number == undefined)
            $('#formularz_ulica_edit').val(place.address.road);
        else
            $('#formularz_ulica_edit').val(place.address.road + " " + place.address.house_number);

        if ($('#formularz_ulica_edit').val() == "")
            $('#PUSTA_ULICA_edit').show();
        else {
            map.removeLayer(MARKER_RPZECIAGNY);
            var xxx = parseFloat(place.lat), yyy = parseFloat(place.lon);
            MARKER_RPZECIAGNY = L.marker([xxx, yyy], {draggable: 'true'}).addTo(map);
            MARKER_RPZECIAGNY.bindPopup("<b>Przeciagnij aby lepiej<br>okreslic miejsce!</b>").openPopup();

            $('#formularz_XX_edit').val(xxx);
            $('#formularz_YY_edit').val(yyy);

            map.setView([xxx, yyy], 15);

            var ostatniaDobraPozX = xxx,
                ostatniaDobraPozY = yyy;

            MARKER_RPZECIAGNY.on('dragend', function (event) {
                var marker = event.target;
                var position = marker.getLatLng();

                var zakress = Math.sqrt(Math.abs(srodekY - position.lng) * Math.abs(srodekY - position.lng) +
                    Math.abs(srodekX - position.lat) * Math.abs(srodekX - position.lat));

                if (zakress > zakresWyszukwianiaGoogle) {
                    marker.bindPopup("<b>Za daleko od wpisanego adresu!!</b>").openPopup();
                    marker.setLatLng(new L.LatLng(ostatniaDobraPozX, ostatniaDobraPozY), {draggable: 'true'});
                    map.panTo(new L.LatLng(ostatniaDobraPozX, ostatniaDobraPozY));
                    $('#formularz_XX_edit').val(ostatniaDobraPozX);
                    $('#formularz_YY_edit').val(ostatniaDobraPozY);
                }
                else {
                    ostatniaDobraPozX = position.lat,
                        ostatniaDobraPozY = position.lng;
                    marker.setLatLng(new L.LatLng(position.lat, position.lng), {draggable: 'true'});
                    map.panTo(new L.LatLng(position.lat, position.lng));
                    $('#formularz_XX_edit').val(position.lat);
                    $('#formularz_YY_edit').val(position.lng);
                }


            });
            map.addLayer(MARKER_RPZECIAGNY);
            $('#PUSTA_ULICA_edit').hide();

        }


    }


}


function getParameterByName(name, url) {
    if (!url) {
        url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}


$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });

    if (Object.prototype.toString.call(o['photos']) !== '[object Array]')
        if (o['photos'] === undefined)
            o['photos'] = Array();
        else
            o['photos'] = [o['photos']];

    return o;
};

var cossiewrzuilo = 1;
function handleFileSelect(evt) {
    $('#niemaZdjec').hide();
    niemazdjec = false;
    cossiewrzuilo++;
    var files = evt.target.files;
    for (var i = 0, f; f = files[i]; i++) {
        //console.log("Start Uploading...")

        if (!f.type.match('image.*')) {
            continue;
        }
        var reader = new FileReader();
        raz = true;
        reader.onload = (function (theFile, count) {
            return function (e) {
                //console.log(count + " " + raz + " " +cossiewrzuilo )

                if (count == 1 && raz && cossiewrzuilo == 2) {
                    raz = false;
                    $('#podglad_obrazy_glowne').prepend('<a href="' + e.target.result + '" title="The">' +
                        '<div class="glowne_zdjecie_div_back"><div class="glowne_zdjecie_div"><img src="' + e.target.result + '" /></div></div>' +
                        '</a>');
                }
                else {
                    $('#podglad_obrazy_mini').slick('slickAdd', '<a href="' + e.target.result + '" title="The">' +
                        '<div class="miniaturka_div_back"><div class="miniaturka_div"><img src="' + e.target.result + '" /></div></div>' +
                        '</a>');
                }
            };
        })(f, i + 1);


        reader.readAsDataURL(f);
    }

}

function handleFileSelectEdit(evt) {
    $('#niemaZdjec_edit').hide();
    niemazdjec_edit = false;
    cossiewrzuilo++;
    var files = evt.target.files;
    for (var i = 0, f; f = files[i]; i++) {
        //console.log("Start Uploading...")

        if (!f.type.match('image.*')) {
            continue;
        }
        var reader = new FileReader();
        raz = true;
        reader.onload = (function (theFile, count) {
            return function (e) {
                //console.log(count + " " + raz + " " +cossiewrzuilo )

                if (count == 1 && raz && cossiewrzuilo == 2) {
                    raz = false;
                    //console.log( $('#podglad_obrazy_glowne_edit').children().size() )
                    //if($('#podglad_obrazy_glowne_edit').children().size() == 0)

                    $('#podglad_obrazy_mini_edit').prepend('<a href="' + e.target.result + '" title="The">' +
                        '<div class="miniaturka_div_back"><div class="miniaturka_div"><img src="' + e.target.result + '" /></div></div>' +
                        '</a>');

                    if ($('#podglad_obrazy_glowne_edit').children().size() != 0) {
                        $('#podglad_obrazy_mini_edit').slick('unslick');
                        var div1 = $('#podglad_obrazy_glowne_edit > a img').eq(0);
                        var div2 = $('#podglad_obrazy_mini_edit > a img').eq(0);

                        var tdiv1 = div1.clone();
                        var tdiv2 = div2.clone();

                        div1.replaceWith(tdiv2);
                        div2.replaceWith(tdiv1);

                        $('#podglad_obrazy_mini_edit').slick({
                            infinite: false,
                            slidesToShow: 4,
                            slidesToScroll: 1,
                            variableWidth: true
                        });


                    }

                }
                else {
                    $('#podglad_obrazy_mini_edit').slick('slickAdd', '<a href="' + e.target.result + '" title="The">' +
                        '<div class="miniaturka_div_back"><div class="miniaturka_div"><img src="' + e.target.result + '" /></div></div>' +
                        '</a>');
                }
            };
        })(f, i + 1);


        reader.readAsDataURL(f);
    }

}


function parseQuery(qstr) {
    var query = {};
    var a = (qstr[0] === '?' ? qstr.substr(1) : qstr).split('&');
    for (var i = 0; i < a.length; i++) {
        var b = a[i].split('=');
        query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
    }
    return query;
}


function removeParam(key, sourceURL) {
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";


    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
}


function intToProdukt(val) {
    if (val == 0)
        return TLUMACZENIA['profil106'];
    else if (val == 1)
        return TLUMACZENIA['profil107'];
    else if (val == 2)
        return TLUMACZENIA['profil108'];
    else if (val == 3)
        return TLUMACZENIA['profil109'];
    else if (val == 4)
        return TLUMACZENIA['profil110'];
    else if (val == 5)
        return TLUMACZENIA['profil111'];
    else if (val == 6)
        return TLUMACZENIA['profil112'];
    else if (val == 7)
        return TLUMACZENIA['profil113'];

}

function intToRodzajM(val) {
    if (val == 0)
        return TLUMACZENIA['wyszukiwarka6'];
    else if (val == 50)
        return TLUMACZENIA['wyszukiwarka7'];
    else if (val == 100)
        return TLUMACZENIA['wyszukiwarka8'];
    else if (val == 150)
        return TLUMACZENIA['wyszukiwarka9'];
    else if (val == 200)
        return TLUMACZENIA['wyszukiwarka10'];
    else if (val == 250)
        return TLUMACZENIA['wyszukiwarka11'];
}

function intToRodzajBT(val) {
    if (val == 0)
        return TLUMACZENIA['wyszukiwarka57'];
    else if (val == 1)
        return TLUMACZENIA['wyszukiwarka58'];
    else if (val == 2)
        return TLUMACZENIA['wyszukiwarka59'];
    else if (val == 3)
        return TLUMACZENIA['wyszukiwarka60'];
    else if (val == 50)
        return TLUMACZENIA['wyszukiwarka61'];
    else if (val == 51)
        return TLUMACZENIA['wyszukiwarka62'];
    else if (val == 10)
        return TLUMACZENIA['wyszukiwarka63'];
    else if (val == 101)
        return TLUMACZENIA['wyszukiwarka64'];
    else if (val == 150)
        return TLUMACZENIA['wyszukiwarka65'];
    else if (val == 151)
        return TLUMACZENIA['wyszukiwarka66'];
    else if (val == 152)
        return TLUMACZENIA['wyszukiwarka67'];
    else if (val == 200)
        return TLUMACZENIA['wyszukiwarka68'];
    else if (val == 201)
        return TLUMACZENIA['wyszukiwarka69'];
    else if (val == 202)
        return TLUMACZENIA['wyszukiwarka70'];
    else if (val == 250)
        return TLUMACZENIA['wyszukiwarka71'];
    else if (val == 251)
        return TLUMACZENIA['wyszukiwarka72'];
    else if (val == 252)
        return TLUMACZENIA['wyszukiwarka73'];
    else if (val == 253)
        return TLUMACZENIA['wyszukiwarka74'];
    else if (val == 254)
        return TLUMACZENIA['wyszukiwarka75'];
    else if (val == 255)
        return TLUMACZENIA['wyszukiwarka76'];
}


function intToRodzajT(val) {
    if (val == 0)
        return TLUMACZENIA['wyszukiwarka13'];
    else if (val == 1)
        return TLUMACZENIA['wyszukiwarka14'];
    else if (val == 2)
        return TLUMACZENIA['wyszukiwarka15'];
}

function intToWaluta(val) {
    if (val == 0)
        return TLUMACZENIA['wyszukiwarka20'];
    else if (val == 1)
        return TLUMACZENIA['wyszukiwarka21'];
    else if (val == 2)
        return TLUMACZENIA['wyszukiwarka22'];
}

function intToHeating(val) {
    if (val == 0)
        return TLUMACZENIA['wyszukiwarka32'];
    else if (val == 1)
        return TLUMACZENIA['wyszukiwarka31'];
    else if (val == 2)
        return TLUMACZENIA['wyszukiwarka33'];
    else if (val == 3)
        return TLUMACZENIA['wyszukiwarka78'];

}

function divekSzczegolik(tytul, wart, extra) {
    if ((typeof wart === 'string' || wart instanceof String) && wart != '' && wart != "true" && wart != "false") {
        return '<div class="item"><div class="header">' + tytul + ':&emsp;' +
            '<h4 style="display: inline;" class="ui grey header">' +
            wart + ' ' + extra + '</h4></div></div>'
    }
    else {
        if (wart != null && wart != '') {

            if ($.parseJSON(wart) === true)
                wart = '<i class="green checkmark icon"></i>';
            else if ($.parseJSON(wart) === false)
                wart = '<i class="red remove icon"></i>';

            return '<div class="item"><div class="header">' + tytul + ':&emsp;' +
                '<h4 style="display: inline;" class="ui grey header">' +
                wart + ' ' + extra + '</h4></div></div>'
        }
    }


}

var ruszaj = false;
function czyJestwsrodku(gornyzachod, dolnywschod) {
    //console.log((dolnywschod.lat) + " " + (dolnywschod.lng) + " " + (gornyzachod.lat) + " " + ( gornyzachod.lng) )
    return (x2 < dolnywschod.lat && y2 > dolnywschod.lng && x4 > gornyzachod.lat && y4 < gornyzachod.lng )
}

function validate(event) {
    var key = window.event ? event.keyCode : event.which;
    if (event.keyCode == 8 || event.keyCode == 46
        || event.keyCode == 37 || event.keyCode == 39) {
        return true;
    }
    else if ( key < 48 || key > 57 ) {
        return false;
    }
    else return true;
}


function wyslijPunkty(P1, P2, P3, P4) {

    // L.polygon([
    //     [P1.lat, P1.lng],
    //     [P1.lat, P2.lng],
    //     [P2.lat, P2.lng],
    //     [P2.lat, P1.lng]
    // ], {color: 'black'}).addTo(MAPA);

    // L.polygon([
    //     [P3.lat, P3.lng],
    //     [P3.lat, P4.lng],
    //     [P4.lat, P4.lng],
    //     [P4.lat, P3.lng]
    // ], {color: 'black'}).addTo(MAPA);

    //tekst_parametry = '&'+tekst_parametry.substring(1,tekst_parametry.length);

    stary_tekst = SERWER + 'marker/get?x1=' + P1.lat + '&y1=' + P1.lng + '&x2=' + P2.lat + '&y2=' + P2.lng + '';

    tekst = tekst.split('?')[0] + '?' + 'x1=' + P1.lat + '&y1=' + P1.lng + '&x2=' + P2.lat +
        '&y2=' + P2.lng + '&x3=' + P3.lat + '&y3=' + P3.lng + '&x4=' + P4.lat + '&y4=' + P4.lng + '' + tekst_parametry;

    //console.log(tekst_parametry)

    // if (DEBUGOWANIE)
    //     console.log('http://localhost:9999/marker/getNew?x1=' + P1.lat + '&y1=' + P1.lng + '&x2=' + P2.lat +
    //         '&y2=' + P2.lng + '&x3=' + P3.lat + '&y3=' + P3.lng + '&x4=' + P4.lat + '&y4=' + P4.lng + '' + tekst_parametry)

    $('#LADOWANIE').show();

    linkDlaListy = stary_tekst + tekst_parametry;

    $.get(SERWER + 'marker/getNew?x1=' + P1.lat + '&y1=' + P1.lng + '&x2=' + P2.lat +
        '&y2=' + P2.lng + '&x3=' + P3.lat + '&y3=' + P3.lng + '&x4=' + P4.lat + '&y4=' + P4.lng + '' + tekst_parametry, dodajMARKERY);


}


function lista_lazyLoading(data, gdzie, ile) {
    var tablica_prz = [];
    for (var i = 0; i < data.length; ++i) {

        var status = '', dokiedy = '';
        status = data[i].status;
        dokiedy = data[i].actualTo;

        $.get(SERWER + 'offer/get?id=' + data[i].id, function (data2) {

            var ikonka = dodajIkonkedoListy(
                status,
                dokiedy,
                data2.views,
                data2.id,
                data2.title,
                data2.property.propertyType,
                data2.offerType,
                data2.photos,
                data2.price.value,
                data2.price.currency,
                data2.property.address.street,
                data2.property.address.country,
                data2.property.address.city, ile);

            //tablica_prz.push(ikonka);

            $('#' + gdzie).append(ikonka);

            $('.aktynwpopup').popup();
        });
    }


    //console.log(tablica_prz)
    // new Clusterize({
    //     rows: tablica_prz,
    //     scrollId: gdzie+'2',
    //     contentId: gdzie
    // });

}


function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie() {
    var user = getCookie("akceptuje");
    return user;
}



















