<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html lang="pl">
    <head>
        <?php
        include('szkielet/biblioteki.php');
        if(isset($_GET['id']) && $_GET['id'] != "")
        {

            $json = @file_get_contents('http://164.132.57.18:9999/offer/get?id='.$_GET['id']);
            if ($json !== FALSE) {
                $obj = json_decode($json);

                include('php/funkcje.php');
                include ('php/wybierzJezyk.php');



                $TAGI =  intToRodzajM($obj->property->propertyType).' '.$TLUMACZENIA['przegladaj4'].' '
                    .intToRodzajT($obj->offerType).', '
                    .$obj->property->address->street.', '
                    .$obj->property->address->city.', '
                    .intToRodzajBT($obj->property->buildingType).', '
                    .$obj->title.', '.$TLUMACZENIA['meta6'];


                ?>


                <title><?php echo $TLUMACZENIA['meta11']." ".$obj->title; ?></title>
                <meta name="Description" content="<?php echo $obj->description; ?>" />
                <meta name="Keywords" content="<?php echo $TAGI; ?>i" />
                <meta property="og:url" content="http://flat-map.com/przegladaj.php?id=<?php echo $_GET['id']; ?>"/>
                <meta property="og:title" content="<?php echo $obj->title; ?>" />
                <meta property="og:description" content="<?php echo $obj->description; ?>" />



                <?php
                if(isset($obj->photos[0]->id))
                    echo '<meta property="og:image" content="'.$SERWER.'photo/' .$obj->photos[0]->id.'" />';
            }

        }
        else
        {
            ?>
            <meta name="Description" content="<?php echo $TLUMACZENIA['meta5']; ?>" />
            <meta name="Keywords" content="<?php echo $TLUMACZENIA['meta6']; ?>" />
            <?php
        }


        ?>
    </head>
    </head>
    <body>
        <?php
        include('szkielet/pasek_mapa.php');

        include('szkielet/skrypty.php');

        ?>
    </body>
</html>