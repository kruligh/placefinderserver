<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html lang="pl">
    <head>
        <title>MAPA</title>

        <meta name="Description" content="FlatMap to mapa ogłoszeń nieruchomości - aplikacja internetowa ułatwiająca poszukiwanie mieszkania lub dowolnej nieruchomości ze względu na położenie za pomocą wygodnej mapy." />
        <meta name="Keywords" content="FlatMap Flat Map flat-map.com mapa nieruchomości mieszkania dla studenta młodych na wynajem" />




        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-92979749-1', 'auto');
            ga('send', 'pageview');

        </script>


        <meta http-equiv="refresh" content="0; URL='wyszukiwarka.php'" />
    </head>
    <body>
    </body>

</html>