<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html lang="pl">
    <head>
        <?php
        include('szkielet/biblioteki.php');
        ?>
        <title><?php echo $TLUMACZENIA['meta2']; ?></title>
        <meta name="Description" content="<?php echo $TLUMACZENIA['meta5']; ?>" />
        <meta name="Keywords" content="<?php echo $TLUMACZENIA['meta6']; ?>" />
        <meta property="og:url" content="<?php echo $TLUMACZENIA['meta7']; ?>"/>
        <meta property="og:title" content="<?php echo $TLUMACZENIA['meta8']; ?>" />
        <meta property="og:description" content="<?php echo $TLUMACZENIA['meta9']; ?>" />
        <meta property="og:image" content="<?php echo $TLUMACZENIA['meta10']; ?>" />




    </head>
    <body>
        <?php
        include('szkielet/pasek_mapa.php');

        include('szkielet/skrypty.php');
        ?>
    </body>
</html>