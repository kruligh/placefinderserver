$(document).ready(function(){
	$("#filer_input").filer({
		limit: null,
		maxSize: null,
        fileMaxSize : 6,
		extensions: ["jpg", "png", "jpeg"],
		changeInput: '<div id="DragDropDiv" class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon cloud upload"></i></div><span style="font-size: 110%;">Przeciagnij tu zdjęcia</span><div class="jFiler-input-text" style="margin-top: 20px;margin-bottom: 20px;">Maksymalny rozmiar pliku: 6 MB </div><a class="jFiler-input-choose-btn blue">Wybierz plik</a></div></div>',
		showThumbs: true,
		theme: "dragdropbox",
		templates: {
			box: '<ul id="wrzucaneObrazki" class="jFiler-items-list jFiler-items-grid"></ul>',
			item: '<li class="jFiler-item">\
						<div class="jFiler-item-container">\
							<div class="jFiler-item-inner">\
								<div class="jFiler-item-thumb">\
									<div class="jFiler-item-status"></div>\
									<div class="jFiler-item-thumb-overlay">\
										<div class="jFiler-item-info">\
											<div style="display:table-cell;vertical-align: middle;">\
												<span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
												<span class="jFiler-item-others">{{fi-size2}}</span>\
											</div>\
										</div>\
									</div>\
									{{fi-image}}\
								</div>\
								<div class="jFiler-item-assets jFiler-row">\
									<ul class="list-inline pull-left">\
										<li>{{fi-progressBar}}</li>\
									</ul>\
									<ul class="list-inline pull-right">\
										<li><a class="glownezdjecie"></a><input type="text" name="photos" class="kolejnosc" style="display: none;" /></li>\
										<li><a class="WLEWO"><i class="chevron left icon"></i></a></li>\
										<li class="WPRAWO"><a><i class="chevron right icon"></i></a></li>\
										<li><a class="icon-jfi-trash jFiler-item-trash-action akcja1"><i class="icon trash"></i></a></li>\
									</ul>\
								</div>\
							</div>\
						</div>\
					</li>',
			itemAppend: '<li class="jFiler-item">\
							<div class="jFiler-item-container">\
								<div class="jFiler-item-inner">\
									<div class="jFiler-item-thumb">\
										<div class="jFiler-item-status"></div>\
										<div class="jFiler-item-thumb-overlay">\
											<div class="jFiler-item-info">\
												<div style="display:table-cell;vertical-align: middle;">\
													<span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
													<span class="jFiler-item-others">{{fi-size2}}</span>\
												</div>\
											</div>\
										</div>\
										{{fi-image}}\
									</div>\
									<div class="jFiler-item-assets jFiler-row">\
										<ul class="list-inline pull-left">\
											<li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
										</ul>\
										<ul class="list-inline pull-right">\
											<li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
										</ul>\
									</div>\
								</div>\
							</div>\
						</li>',
			progressBar: '<div class="bar" ></div>',
			itemAppendToEnd: true,
			canvasImage: true,
			removeConfirmation: false,
			_selectors: {
				list: '.jFiler-items-list',
				item: '.jFiler-item',
				progressBar: '.bar',
				remove: '.jFiler-item-trash-action'
			}
		},
		dragDrop: {
			dragEnter: null,
			dragLeave: null,
			drop: null,
			dragContainer: null,
		},
		uploadFile: {
			url: SERWER+"person/offer/add/photo/upload",
			data: null,
			type: 'POST',
			enctype: 'multipart/form-data',
			synchron: true,
			success: function(data, itemEl, listEl, boxEl, newInputEl, inputEl, id){
                nowezdjecia = true;

                itemEl.find('.kolejnosc').attr("value", data);

                var index = tablicaLadujacychSie.indexOf(itemEl.find('.kolejnosc').attr("value", data));
                tablicaLadujacychSie.splice(index, 1);

				if(tablicaLadujacychSie.length == 0)
				{
                    $('.idzDo_formularz_podsumowanie').show();
                    $('.poczekaj_zdjecia').hide();
				}
			},
			error: function(el){

                el.find('.glownezdjecie').html('<span style="color:red">ERROR!!</span>');

				setTimeout(function () {
                    el.find('.akcja1').click();
                },1000);
			},
			statusCode: null,
			onProgress: null,
			onComplete: null
		},
		files: null,
		addMore: true,
		allowDuplicates: true,
		clipBoardPaste: true,
		excludeName: null,
		beforeRender: null,
		afterRender: null,
		beforeShow: null,
		beforeSelect: null,
		onSelect: function(){
            $('#formularz_zdjecia').modal('refresh');
            $('.idzDo_formularz_podsumowanie').hide();
            $('.poczekaj_zdjecia').show();

            $('.glownezdjecie').text("");
			$('.glownezdjecie').first().text(DomyslneZdj_TXT);

            for (var i = 1; i < $('.glownezdjecie').length; ++i) {
                $('.glownezdjecie').eq(i).text(i+1);
            }
            tablicaLadujacychSie.push(i++);

			$('#modal_dodajOgloszenie').modal('refresh');

		},
		afterShow: null,
		onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
			var filerKit = inputEl.prop("jFiler"),
		        file_name = filerKit.files_list[id].name;

		},
		onEmpty: null,
		options: null,
		dialogs: {
			alert: function(text) {
				return alert(text);
			},
			confirm: function (text, callback) {
				confirm(text) ? callback() : null;
			}
		},
		captions: {
			button: "Wybierz pliki",
			feedback: "Wybierz pliki do wrzucenia",
			feedback2: "pliki zostały wybrane",
			drop: "Upuść plik tutaj",
			removeConfirmation: "Napewno chcesz usunąć?",
			errors: {
				filesLimit: "Limit zdjęc przekroczono",
				filesType: "Można wrzucac tylko zdjecia!!",
				filesSize: "Wybrany plik jest za duży! Wybierz plik do 6 MB",
				filesSizeAll: "Wybrany plik jest za duży! Wybierz plik do 6 MB."
			}
		}
	});

	var wrzucDoglownego = false;

	$("#filer_input_edit").filer({
		limit: null,
		maxSize: null,
        fileMaxSize : 6,
        extensions: ["jpg", "png", "jpeg"],
        changeInput: '<div id="DragDropDiv_edit" class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon cloud upload"></i></div><span style="font-size: 110%;">Przeciagnij tu zdjęcia</span><div class="jFiler-input-text" style="margin-top: 20px;margin-bottom: 20px;">Maksymalny rozmiar pliku: 6 MB </div><a class="jFiler-input-choose-btn blue">Wybierz plik</a></div></div>',
        showThumbs: true,
		theme: "dragdropbox",
		templates: {
			box: '<ul id="wrzucaneObrazki_edit" class="jFiler-items-list jFiler-items-grid"></ul>',
			item: '<li class="jFiler-item">\
						<div class="jFiler-item-container">\
							<div class="jFiler-item-inner">\
								<div class="jFiler-item-thumb">\
									<div class="jFiler-item-status"></div>\
									<div class="jFiler-item-thumb-overlay">\
										<div class="jFiler-item-info">\
											<div style="display:table-cell;vertical-align: middle;">\
												<span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
												<span class="jFiler-item-others">{{fi-size2}}</span>\
											</div>\
										</div>\
									</div>\
									{{fi-image}}\
								</div>\
								<div class="jFiler-item-assets jFiler-row">\
									<ul class="list-inline pull-left">\
										<li>{{fi-progressBar}}</li>\
									</ul>\
									<ul class="list-inline pull-right">\
										<li><a class="glownezdjecie"></a><input type="text" name="photos_edycja" class="kolejnosc zmniusem" style="display: none;" /></li>\
										<li><a class="WLEWO2"><i class="chevron left icon"></i></a></li>\
										<li class="WPRAWO2"><a><i class="chevron right icon"></i></a></li>\
										<li><a style="cursor: pointer;" class="icon-jfi-trash akcja2"></a></li>\
									</ul>\
								</div>\
							</div>\
						</div>\
					</li>',
			itemAppend: '<li class="jFiler-item">\
							<div class="jFiler-item-container">\
								<div class="jFiler-item-inner">\
									<div class="jFiler-item-thumb">\
										<div class="jFiler-item-status"></div>\
										<div class="jFiler-item-thumb-overlay">\
											<div class="jFiler-item-info">\
												<div style="display:table-cell;vertical-align: middle;">\
													<span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
													<span class="jFiler-item-others">{{fi-size2}}</span>\
												</div>\
											</div>\
										</div>\
										{{fi-image}}\
									</div>\
									<div class="jFiler-item-assets jFiler-row">\
										<ul class="list-inline pull-left">\
											<li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
										</ul>\
										<ul class="list-inline pull-right">\
											<li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
										</ul>\
									</div>\
								</div>\
							</div>\
						</li>',
			progressBar: '<div class="bar" ></div>',
			itemAppendToEnd: true,
			canvasImage: true,
			removeConfirmation: false,
			_selectors: {
				list: '.jFiler-items-list',
				item: '.jFiler-item',
				progressBar: '.bar',
				remove: '.jFiler-item-trash-action'
			}
		},
		dragDrop: {
			dragEnter: null,
			dragLeave: null,
			drop: null,
			dragContainer: null,
		},
		uploadFile: {
			url: SERWER+"person/offer/add/photo/upload",
			data: null,
			type: 'POST',
			enctype: 'multipart/form-data',
			synchron: true,
			success: function(data, itemEl, listEl, boxEl, newInputEl, inputEl, id){
                nowezdjecia = true;

                itemEl.find('.kolejnosc').attr("value", data);

                var index = tablicaLadujacychSieEdycja.indexOf(itemEl.find('.kolejnosc').attr("value", data));
                tablicaLadujacychSieEdycja.splice(index, 1);

				if(tablicaLadujacychSieEdycja.length == 0)
				{
                    $('.idzDo_formularz_podsumowanie2').show();
                    $('.poczekaj_zdjecia_edit').hide();
				}

			},
			error: function(el){
                el.find('.glownezdjecie').html('<span style="color:red">ERROR!!</span>');

                setTimeout(function () {
                    el.find('.akcja2').click();
                },1000);
			},
			statusCode: null,
			onProgress: null,
			onComplete: null
		},
		files: null,
		addMore: true,
		allowDuplicates: true,
		clipBoardPaste: true,
		excludeName: null,
		beforeRender: null,
		afterRender: null,
		beforeShow: null,
		beforeSelect: null,
		onSelect: function(){
            $('#formularz_zdjecia2').modal('refresh');
            $('.idzDo_formularz_podsumowanie2').hide();
            $('.poczekaj_zdjecia_edit').show();

            $('.glownezdjecie').text("");
			$('.glownezdjecie').first().text(DomyslneZdj_TXT);


            for (var i = 1; i < $('.glownezdjecie').length; ++i) {
                $('.glownezdjecie').eq(i).text(i+1);
            }
            tablicaLadujacychSie.push(i++);
            if(!wrzucDoglownego)
            {
                $('#wrzucaneObrazki_edit').append( $('#obecnieDodaneimages').html() );
                $('#obecnieDodaneimages').remove();
                wrzucDoglownego = true;
            }
            $('#formularz_zdjecia_edit').modal('refresh');

		},
		afterShow: null,
		onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){

			var filerKit = inputEl.prop("jFiler"),
		        file_name = filerKit.files_list[id].name;

		},
		onEmpty: null,
		options: null,
		dialogs: {
			alert: function(text) {
				return alert(text);
			},
			confirm: function (text, callback) {
				confirm(text) ? callback() : null;
			}
		},
        captions: {
            button: "Wybierz pliki",
            feedback: "Wybierz pliki do wrzucenia",
            feedback2: "pliki zostały wybrane",
            drop: "Upuść plik tutaj",
            removeConfirmation: "Napewno chcesz usunąć?",
            errors: {
                filesLimit: "Limit zdjęc przekroczono",
                filesType: "Można wrzucac tylko zdjecia!!",
                filesSize: "Wybrany plik jest za duży! Wybierz plik do 6 MB",
                filesSizeAll: "Wybrany plik jest za duży! Wybierz plik do 6 MB."
            }
        }
	});
})
