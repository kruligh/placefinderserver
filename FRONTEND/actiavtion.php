<html>
    <head>
        <?php
        include ('ustawienia.php');
        include ('php/wybierzJezyk.php');
        ?>
        <title><?php echo $TLUMACZENIA['meta12']; ?></title>
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <meta name="Description" content="<?php echo $TLUMACZENIA['meta5']; ?>" />
        <meta name="Keywords" content="<?php echo $TLUMACZENIA['meta6']; ?>" />
        <script src="LIB/jquery/jquery-1.12.4.min.js"></script>
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
    </head>
    <body style="background-color: #efefef">


        <div style="position: absolute;left:27%;top:15vw;width:50vw;text-align: center;">
            <img src="img/flatmap_logo2.png" style="width: 100%;">
            <span style="font-family: 'Open Sans';top: 20px;position: relative;"><?php echo $TLUMACZENIA['inne1']; ?></span>

        </div>
        <script >
            var SERWER,LINK;
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    SERWER  = JSON.parse(this.responseText).serwer ;
                    LINK  = JSON.parse(this.responseText).link ;

                    var obiekt = JSON.stringify({"token" : "<?php echo $_GET['token']; ?>", "u" : "<?php echo $_GET['u']; ?>" });

                    $.ajax({
                        url: SERWER+'register/activateMail',
                        type: "POST",
                        contentType: "application/json",
                        data : obiekt,
                        success : function(data){
                            location.href=LINK;
                        },
                        error: function(data)
                        {
                            location.href=LINK;
                        }
                    });
                }
            };
            xhttp.open("GET", "php/serwer.php", true);
            xhttp.send();


        </script>
    </body>
</html>