<div class="w3-card-2 w3-margin">
    <div class="w3-container w3-padding">
        <h4>Popularne artykuły</h4>
    </div>
    <ul class="w3-ul w3-hoverable w3-white" >
        <li class="w3-padding-16" >
            <a href="post1.php" style="text-decoration: none;">
                <span class="w3-large">Mieszkanie dla studenta</span><br>
                <span>Na co zwrócic uwage przy wyborze mieszkania</span>
            </a>

        </li>
        <li class="w3-padding-16">
            <a href="post2.php" style="text-decoration: none;">
                <span class="w3-large">Typ ogrzewania</span><br>
                <span>Czy rodzaj ogrzewania odgrywa tak duża wage?</span>
            </a>
        </li>
    </ul>
</div>