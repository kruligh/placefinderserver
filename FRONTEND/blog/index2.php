<!DOCTYPE html>
<html>
<head>
    <title>FlatMap | BLOG</title>
    <meta name="description" content="FlatMap Blog opis itd">
    <meta name="keywords" content="FlatMap, Blog">
    <meta name="author" content="Adam Gąsiorek">
    <link rel="icon" href="../favicon.ico" type="image/x-icon" />

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/lib/w3.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    <style>
        body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
    </style>

</head>

<body class="w3-light-grey">

    <div id="fb-root"></div>
    <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.8";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <!-- w3-content defines a container for fixed size centered content,
    and is wrapped around the whole page content, except for the footer in this example -->
    <div class="w3-content" style="max-width:1400px">


    <header class="w3-container w3-center w3-padding-32">
        <a href="index.php" style="text-decoration: none;">
            <img src="../img/flatmap_logo2.png" style="width: 200px;" alt="Flat Map" />
        </a>
        <p>Blog, artykuły, wszystko o nieruchomościach</p>
    </header>

    <div class="w3-row">
        <div class="w3-col l8 s12">
            <?php
            include ('all_posty1.php');
            ?>
        </div>
        <div class="w3-col l4">
            <?php

            include ('fb.php');
            include ('popularne_artykuly.php');

            ?>
        </div>
    </div>
</div>

<footer class="w3-container w3-dark-grey w3-padding-32 w3-margin-top">
    <button class="w3-btn w3-disabled w3-padding-large w3-margin-bottom">Poprzednia  strona</button>
<!--    <a href="strona2.php" >-->
<!--        <button class="w3-btn w3-disabled w3-padding-large w3-margin-bottom">Następna strona »</button>-->
<!--    </a>-->
    <button class="w3-btn w3-disabled w3-padding-large w3-margin-bottom">Następna strona »</button>

    <p style="margin-bottom: -15px;">FlatMap ⓒ 2017 Powered by <a style="text-decoration: none;" href="https://www.w3schools.com/">Adacho</a> and <a style="text-decoration: none;" href="https://www.w3schools.com/w3css/default.asp" target="_blank">w3.css</a></p>

</footer>

    <script id="dsq-count-scr" src="//flatmap-blog.disqus.com/count.js" async></script>
</body>
</html>
