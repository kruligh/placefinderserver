<!DOCTYPE html>
<html>
<head>
    <title>FlatMap | BLOG</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/lib/w3.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    <style>
        body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
    </style>
    <script src="lib/jquery-1.12.4.min.js"></script>

</head>

<body class="w3-light-grey">

    <div id="fb-root"></div>
    <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.8";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <!-- w3-content defines a container for fixed size centered content,
    and is wrapped around the whole page content, except for the footer in this example -->
    <div class="w3-content" style="max-width:1400px">


    <header class="w3-container w3-center w3-padding-32">
        <img src="logo.png" style="width: 200px;" alt="Flat Map" />
        <p>Blog, artykuły, wszystko o nieruchomościach</p>
    </header>

    <div class="w3-row">
        <div class="w3-col l8 s12">
            <?php
            include ('all_posty2.php');
            ?>
        </div>
        <div class="w3-col l4">
            <?php

            include ('fb.php');
            include ('popularne_artykuly.php');

            ?>
        </div>
    </div>
</div>

<footer class="w3-container w3-dark-grey w3-padding-32 w3-margin-top">
    <a href="index.php" style="text-decoration: none;">
        <button class="w3-btn w3-padding-large w3-margin-bottom">« Poprzednia strona</button>
    </a>

    <button class="w3-btn w3-disabled w3-padding-large w3-margin-bottom">Następna strona</button>

    <p>Powered by <a href="https://www.w3schools.com/w3css/default.asp" target="_blank">w3.css</a></p>
</footer>

</body>
</html>
