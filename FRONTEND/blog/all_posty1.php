
<!---------------------------   POST1     ------------------------------------>
<?php

$link = 'post1.php';
$TYTUL = 'Mieszkanie dla studenta';
$KROTKI_OPIS = 'Mauris neque quam, fermentum ut nisl vitae, convallis maximus nisl. Sed mattis nunc id lorem euismod placerat. Vivamus porttitor magna enim, ac accumsan tortor cursus at. Phasellus sed ultricies mi non congue ullam corper. Praesent tincidunt sed
            tellus ut rutrum. Sed vitae justo condimentum, porta lectus vitae, ultricies congue gravida diam non fringilla';

?>
<div class="w3-card-4 w3-margin w3-white">
    <a href="<?php echo $link; ?>" style="text-decoration: none;">
        <img src="zdjecia/mieszkanie.jpg" alt="Nature" style="width:100%">
        <div class="w3-container w3-padding-8">
            <h3><b><?php echo $TYTUL; ?></b></h3>
        </div>
    </a>
    <div class="w3-container">
        <p><?php echo $KROTKI_OPIS; ?></p>
        <div class="w3-row">
            <a href="<?php echo $link; ?>" style="text-decoration: none;">
                <div class="w3-col m8 s12">
                    <p><button class="w3-btn w3-padding-large w3-white w3-border w3-hover-border-black"><b>CZYTAJ WIĘCEJ »</b></button></p>
                </div>
            </a>
            <div class="w3-col m4 w3-hide-small" style="position: relative">
                <p><span class="w3-padding-large w3-right" style="position: absolute;right: 40px;margin-top: 7px;"><b>Komentarzy  </b></span>
                        <a href="post1.php#disqus_thread" style="right: 20px;bottom: -40px;text-align: center;color:white;background-color: black;text-decoration: none;position: absolute;overflow: hidden;height: 20px;width: 30px;"></a>
            </div>
        </div>
    </div>
</div>

<hr>
<!--------------------------- KONIEC POST1     -------------------------------->



<!---------------------------   POST2     ------------------------------------>
<?php
$link = 'post2.php';
$TYTUL = 'Typ ogrzewania';
$KROTKI_OPIS = 'Mauris neque quam, fermentum ut nisl vitae, convallis maximus nisl. Sed mattis nunc id lorem euismod placerat. Vivamus porttitor magna enim, ac accumsan tortor cursus at. Phasellus sed ultricies mi non congue ullam corper. Praesent tincidunt sed
            tellus ut rutrum. Sed vitae justo condimentum, porta lectus vitae, ultricies congue gravida diam non fringilla';

?>
<div class="w3-card-4 w3-margin w3-white">
    <a href="<?php echo $link; ?>" style="text-decoration: none;">
        <img src="zdjecia/grzejnik.jpeg" alt="Nature" style="width:100%">
        <div class="w3-container w3-padding-8">
            <h3><b><?php echo $TYTUL; ?></b></h3>
        </div>
    </a>

    <div class="w3-container">
        <p><?php echo $KROTKI_OPIS; ?></p>
        <div class="w3-row">
            <a href="<?php echo $link; ?>" style="text-decoration: none;">
                <div class="w3-col m8 s12">
                    <p><button class="w3-btn w3-padding-large w3-white w3-border w3-hover-border-black"><b>CZYTAJ WIĘCEJ »</b></button></p>
                </div>
            </a>
            <div class="w3-col m4 w3-hide-small" style="position: relative">
                <p><span class="w3-padding-large w3-right" style="position: absolute;right: 40px;margin-top: 7px;"><b>Komentarzy  </b></span>
                    <a href="post2.php#disqus_thread" style="right: 20px;bottom: -40px;text-align: center;color:white;background-color: black;text-decoration: none;position: absolute;overflow: hidden;height: 20px;width: 30px;"></a>
            </div>
        </div>
    </div>
</div>
<hr>
<!--------------------------- KONIEC POST2     -------------------------------->
