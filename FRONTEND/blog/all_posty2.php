
<!---------------------------   POST1     ------------------------------------>
<div class="w3-card-4 w3-margin w3-white scrollaable">
    <img src="woods.jpg" alt="Nature" style="width:100%">
    <div class="w3-container w3-padding-8">
        <h3><b>Mieszkanie dla studenta 2 strona</b></h3>
    </div>

    <div class="w3-container">
        <p>Mauris neque quam, fermentum ut nisl vitae, convallis maximus nisl. Sed mattis nunc id lorem euismod placerat. Vivamus porttitor magna enim, ac accumsan tortor cursus at. Phasellus sed ultricies mi non congue ullam corper. Praesent tincidunt sed
            tellus ut rutrum. Sed vitae justo condimentum, porta lectus vitae, ultricies congue gravida diam non fringilla.</p>
        <div class="w3-row">
            <div class="w3-col m8 s12">
                <p><button class="w3-btn w3-padding-large w3-white w3-border w3-hover-border-black"><b>CZYTAJ WIĘCEJ »</b></button></p>
            </div>
        </div>
    </div>
</div>
<hr>
<!--------------------------- KONIEC POST1     -------------------------------->



<!---------------------------   POST2     ------------------------------------>
<div class="w3-card-4 w3-margin w3-white scrollaable">
    <img src="woods.jpg" alt="Nature" style="width:100%">
    <div class="w3-container w3-padding-8">
        <h3><b>Typ ogrzewania 2 strona</b></h3>
    </div>

    <div class="w3-container">
        <p>Mauris neque quam, fermentum ut nisl vitae, convallis maximus nisl. Sed mattis nunc id lorem euismod placerat. Vivamus porttitor magna enim, ac accumsan tortor cursus at. Phasellus sed ultricies mi non congue ullam corper. Praesent tincidunt sed
            tellus ut rutrum. Sed vitae justo condimentum, porta lectus vitae, ultricies congue gravida diam non fringilla.</p>
        <div class="w3-row">
            <div class="w3-col m8 s12">
                <p><button class="w3-btn w3-padding-large w3-white w3-border w3-hover-border-black"><b>CZYTAJ WIĘCEJ »</b></button></p>
            </div>
        </div>
    </div>
</div>
<hr>
<!--------------------------- KONIEC POST2     -------------------------------->
