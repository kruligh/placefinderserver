
    <title><?php echo $TYTUL; ?></title>
    <meta name="description" content="<?php echo $GOOGLE_opis; ?>">
    <meta name="keywords" content="<?php echo $GOOGLE_tagi; ?>">
    <meta name="author" content="Adam Gąsiorek">

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/lib/w3.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    <style>
        body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
    </style>

</head>

<body class="w3-light-grey" style="margin-bottom: 60px;">

<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.8";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<!-- w3-content defines a container for fixed size centered content,
and is wrapped around the whole page content, except for the footer in this example -->
<div class="w3-content" style="max-width:1400px;">


    <header class="w3-container w3-center w3-padding-32">
        <a href="index.php" style="text-decoration: none;">
            <img src="../img/flatmap_logo2.png" style="width: 200px;" alt="Flat Map" />
        </a>
        <p>Blog, artykuły, wszystko o nieruchomościach</p>

    </header>



    <div class="w3-row">
        <div class="w3-col l8 s12">



            <div class="w3-card-4 w3-margin w3-white">
                <img src="<?php echo $ZDJECIE_GLOWNE; ?>" alt="Nature" style="width:100%">
                <div class="w3-container w3-padding-8">
                    <h3><b><?php echo $TYTUL; ?></b></h3>
                </div>

                <div class="w3-container" >
                    <p style="margin-bottom: 40px;">
                        <?php echo $OPIS; ?>

                    </p>


                </div>


                <div id="disqus_thread" style="margin-left: 2%;width: 96%;margin-bottom: 50px;"></div>
                <script>

                    var disqus_config = function () {
//                 this.page.url = 'flatmap-blog.16.mb.com';  // Replace PAGE_URL with your page's canonical URL variable
                        this.page.identifier = '<?php echo $TYTUL; ?>'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                    };
                    (function() { // DON'T EDIT BELOW THIS LINE
                        var d = document, s = d.createElement('script');
                        s.src = '//flatmap-blog.disqus.com/embed.js';
                        s.setAttribute('data-timestamp', +new Date());
                        (d.head || d.body).appendChild(s);
                    })();
                </script>
                <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>



            </div>
</div>
<div class="w3-col l4">
    <?php

    include ('fb.php');
    include ('popularne_artykuly.php');

    ?>
</div>
</div>
</div>



</body>
</html>
