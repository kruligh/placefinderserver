<html>
<head>
    <title>REGULAMIN</title>
</head>
</html>
<body>
<img src="img/flatmap_logo.png" style="width: 200px;">
<p><strong>REGULAMIN PORTALU OGŁOSZENIOWEGO FlatMap</strong></p>
<p><strong>I.     Definicje</strong></p>
<p>Użyte w Regulaminie pojęcia oznaczają: </p>
<p>1.    <strong>Klient</strong> – osoba fizyczna, osoba prawna lub jednostka organizacyjna niebędąca osobą prawną, której przepisy szczególne przyznają zdolność prawną, która dokonuje Zamówienia w ramach Sklepu;</p>
<p>2.     <strong>Kodeks Cywilny</strong> – ustawa z dnia 23 kwietnia 1964 r. (Dz.U. Nr 16, poz. 93 ze zm.);</p>
<p>3.     <strong>Regulamin</strong> – niniejszy Regulamin świadczenia usług drogą elektroniczną w ramach portalu internetowego FlatMap;</p>
<p>4.     <strong>Sklep internetowy (Sklep)</strong> – serwis internetowy dostępny pod www.flat-map.com, za pośrednictwem którego Klient może w szczególności składać Zamówienia; </p>
<p>5. <strong>    Towar</strong> – produkty prezentowane w Sklepie Internetowym;</p>
<p>6. <strong>   Umowa sprzedaży</strong> – umowa sprzedaży Towarów w rozumieniu Kodeksu Cywilnego, zawarta pomiędzy [podmiotem prowadzącym sklep internetowy] a Klientem, zawierana z wykorzystaniem serwisu internetowego Sklepu; </p>
<p>7.     <strong>Ustawa o prawach konsumenta</strong> – ustawa z dnia 30 maja 2014 r. o prawach konsumenta (Dz.U. z 2014 r. poz. 827);</p>
<p>8.     <strong>Ustawa o świadczeniu usług drogą elektroniczną</strong> – ustawa z dnia 18 lipca 2002 r. o świadczeniu usług drogą elektroniczną (Dz. U. Nr 144, poz. 1204 ze zm.);</p>
<p>9.     <strong>Zamówienie</strong> – oświadczenie woli Klienta, zmierzające bezpośrednio do zawarcia Umowy sprzedaży, określające w szczególności rodzaj i liczbę Towaru. <br /><br />
    <strong>II.     Postanowienia ogólne</strong></p>
<p>2.1.     Niniejszy Regulamin określa zasady korzystania ze sklepu internetowego dostępnego pod flat-map.com. </p>
<p>2.2.     Niniejszy Regulamin jest regulaminem, o którym mowa w art. 8 Ustawy o świadczeniu usług drogą elektroniczną. </p>
<p>2.3.     Sklep internetowy, działający pod flat-map.com, prowadzony jest przez [oznaczenie zgodne z częścią I. podręcznika].</p>
<p>2.4.     Niniejszy Regulamin określa w szczególności [w zależności od dostępnych możliwości]:</p>
<p>a)     zasady dokonywania rejestracji i korzystania z konta w ramach sklepu internetowego; </p>
<p>b)     warunki i zasady dokonywania elektronicznej rezerwacji produktów dostępnych w ramach sklepu internetowego;</p>
<p>c)     warunki i zasady składania drogą elektroniczną Zamówień w ramach sklepu internetowego;</p>
<p>d)     zasady zawierania Umów sprzedaży z wykorzystaniem usług świadczonych w ramach Sklepu Internetowego.</p>
<p>2.5.    Korzystanie ze sklepu internetowego jest możliwe pod warunkiem spełnienia przez system teleinformatyczny, z którego korzysta Klient następujących minimalnych wymagań technicznych:</p>
<p>a)     Internet Explorer w wersji 9.0 lub nowszej z włączoną obsługą Javascript, lub</p>
<p>2.6.    W celu korzystania ze sklepu internetowego Klient powinien we własnym zakresie uzyskać dostęp do stanowiska komputerowego lub urządzenia końcowego, z dostępem do Internetu.</p>
<p>2.7.     Zgodnie z obowiązującymi przepisami prawa [prowadzący Sklep] zastrzega sobie możliwość ograniczenia świadczenia usług za pośrednictwem Sklepu internetowego do osób, które ukończyły wiek 18 lat. W takim przypadku potencjalni Klienci zostaną o powyższym powiadomieni. </p>
<p>2.8.     Klienci mogą uzyskać dostęp do niniejszego Regulaminu w każdym czasie za pośrednictwem odsyłacza zamieszczonego na stronie flat-map.com/regulamin.php oraz pobrać go i sporządzić jego wydruk.</p>
<p>2.9.     Informacje o Towarach podane na stronach internetowych Sklepu, w szczególności ich opisy, parametry techniczne i użytkowe oraz ceny, stanowią zaproszenie do zawarcia umowy, w rozumieniu art. 71 Kodeksu Cywilnego.<br /><br /><strong>III.    Zasady korzystania ze Sklepu Internetowego</strong></p>
<p>3.1.    Warunkiem rozpoczęcia korzystania ze Sklepu internetowego jest rejestracja w jego ramach. </p>
<p>3.2.    Rejestracja następuje poprzez wypełnienie i zaakceptowanie formularza rejestracyjnego, udostępnianego na jednej ze stron Sklepu. </p>
<p>3.3.    Warunkiem rejestracji jest wyrażenie zgody na treść Regulaminu oraz podanie danych osobowych oznaczonych jako obowiązkowe.</p>
<p>3.4.    Administrator może pozbawić Klienta prawa do korzystania ze Sklepu Internetowego, jak również może ograniczyć jego dostęp do części lub całości zasobów Sklepu Internetowego, ze skutkiem natychmiastowym, w przypadku naruszenia przez Klienta Regulaminu, a w szczególności, gdy Klient:</p>
<p>a)    podał w trakcie rejestracji w sklepie internetowym dane niezgodne z prawdą, niedokładne lub nieaktualne, wprowadzające w błąd lub naruszające prawa osób trzecich,</p>
<p>b)     dopuścił się za pośrednictwem sklepu internetowego naruszenia dóbr osobistych osób trzecich, w szczególności dóbr osobistych innych klientów sklepu internetowego,</p>
<p>c)     dopuści się innych zachowań, które zostaną uznane przez administratora za zachowania niezgodne z obowiązującymi przepisami prawa lub ogólnymi zasadami korzystania z sieci Internet lub godzące w dobre imię flat-map.com.</p>
<p>3.5.     Osoba, która została pozbawiona prawa do korzystania ze sklepu internetowego, nie może dokonać powtórnej rejestracji bez uprzedniej zgody administratora.</p>
<p>3.6.    W celu zapewnienia bezpieczeństwa przekazu komunikatów i danych w związku ze świadczonymi w ramach Witryny usługami, Sklep internetowy podejmuje środki techniczne i organizacyjne odpowiednie do stopnia zagrożenia bezpieczeństwa świadczonych usług, w szczególności środki służące zapobieganiu pozyskiwania i modyfikacji przez osoby nieuprawnione danych osobowych przesyłanych w Internecie.</p>
<p>3.7.    Klient zobowiązany jest w szczególności do:</p>
<p>a)    niedostarczania i nieprzekazywania treści zabronionych przez przepisy prawa, np. treści propagujących przemoc, zniesławiających lub naruszających dobra osobiste i inne prawa osób trzecich,</p>
<p>b)    korzystania ze Sklepu internetowego w sposób nie zakłócający jego funkcjonowania, w szczególności poprzez użycie określonego oprogramowania lub urządzeń, </p>
<p>c)     niepodejmowania działań takich jak: rozsyłanie lub umieszczanie w ramach Sklepu internetowego niezamówionej informacji handlowej (spam),</p>
<p>d)     korzystania ze Sklepu internetowego w sposób nieuciążliwy dla innych klientów oraz dla flat-map.com,</p>
<p>e)     korzystania z wszelkich treści zamieszczonych w ramach Sklepu internetowego jedynie w zakresie własnego użytku osobistego,</p>
<p>f)     korzystania ze Sklepu internetowego w sposób zgodny z przepisami obowiązującego na terytorium Rzeczypospolitej Polskiej prawa, postanowieniami Regulaminu, a także z ogólnymi zasadami korzystania z sieci Internet.<br /><br />
    <strong>IV.     Procedura zawarcia Umowy sprzedaży </strong></p>
<p>4.1.     W celu zawarcia Umowy sprzedaży za pośrednictwem Sklepu internetowego należy wejść na stronę internetową flat-map.com, dokonać wyboru pakietu dostępnego w zakladce profil i po przycisnieciu przycisku "Doładuj" podejmując kolejne czynności techniczne w oparciu o wyświetlane Klientowi komunikaty oraz informacje dostępne na stronie.</p>
<p>4.2.    Wybór zamawianych Towarów przez Klienta jest dokonywany poprzez ich odpowiedni wybór.</p>
<p>4.3.    W trakcie składania Zamówienia – do momentu naciśnięcia przycisku „Zamawiam” – Klient ma możliwość modyfikacji wprowadzonych danych oraz w zakresie wyboru Towaru. W tym celu należy kierować się wyświetlanymi Klientowi komunikatami oraz informacjami dostępnymi na stronie. </p>
<p>4.4.    Po podaniu przez Klienta korzystającego ze Sklepu internetowego wszystkich niezbędnych danych, wyświetlone zostanie podsumowanie złożonego Zamówienia. Podsumowanie złożonego Zamówienia będzie zawierać informacje dotyczące: </p>
<p>a)    przedmiotu zamówienia, </p>
<p>b)    jednostkowej oraz łącznej ceny zamawianych produktów lub usług, w tym kosztów dostawy oraz dodatkowych kosztów (jeśli występują), </p>
<p>c)    wybranej metody płatności, </p>
<p>d)    wybranego sposobu dostawy,</p>
<p>4.5.    W celu wysłania Zamówienia konieczne jest dokonanie akceptacji treści Regulaminu, podanie danych osobowych oznaczonych jako obowiązkowe oraz naciśnięcie przycisku „Zamawiam z obowiązkiem zapłaty”. </p>
<p>4.6.    Wysłanie przez Klienta Zamówienia stanowi oświadczenie woli zawarcia z [prowadzącym Sklep] Umowy sprzedaży, zgodnie z treścią Regulaminu. 1</p>
<p>4.7.    Po złożeniu Zamówienia, Klient otrzymuje wiadomość e-mail, zawierającą ostateczne potwierdzenie wszystkich istotnych elementów Zamówienia.</p>
<p>4.8.    Umowę traktuje się za zawartą z momentem otrzymania przez Klienta wiadomości e-mail, o której mowa powyżej.</p>
<p>4.9.    Umowa sprzedaży zawierana jest w języku polskim, o treści zgodnej z Regulaminem. <br /><br />
   <strong>V.     Ceny i metody płatności</strong></p>
<p>6.1.    Ceny Towarów podawane są w złotych polskich i zawierają wszystkie składniki, w tym podatek VAT (z wyróżnieniem wysokości stawki), cła oraz wszelkie inne składniki. </p>
<p>6.2.    Klient ma możliwość uiszczenia ceny:</p>
<p>b)    płatnością w systemie Payu,</p>
<p>8.1.    [Podmiot prowadzący sklep internetowy] jako sprzedawca odpowiada wobec Klienta będącego konsumentem w rozumieniu art. 22[1] Kodeksu Cywilnego, z tytułu rękojmi za wady w zakresie określonym w Kodeksie Cywilnym, w szczególności w art. 556 oraz art. 556[1]  -  556[3] Kodeksu Cywilnego.</p>
<p>8.2.    Reklamacje, wynikające z naruszenia praw Klienta gwarantowanych prawnie, lub na podstawie niniejszego Regulaminu, należy kierować na adres flatmapp@gmail.com. [Podmiot prowadzący sklep internetowy] zobowiązuje się do rozpatrzenia każdej reklamacji w terminie do [14] dni, a gdyby to nie było możliwe, do poinformowania w tym okresie Klienta, kiedy reklamacja zostanie rozpatrzona.</p>
<p><strong>VI.    Reklamacje w zakresie świadczenia usług drogą elektroniczną</strong></p>
<p>9.1.    [Podmiot prowadzący sklep internetowy] podejmuje działania w celu zapewnienia w pełni poprawnego działania Sklepu, w takim zakresie, jaki wynika z aktualnej wiedzy technicznej i zobowiązuje się usunąć w rozsądnym terminie wszelkie nieprawidłowości zgłoszone przez Klientów.</p>
<p>9.2.    Klient zobowiązany jest niezwłocznie powiadomić [podmiot prowadzący sklep internetowy] o wszelkich nieprawidłowościach lub przerwach w funkcjonowaniu serwisu Sklepu Internetowego.</p>
<p>9.3.    Nieprawidłowości związane z funkcjonowaniem Sklepu Klient może zgłaszać pisemnie na adres: [adres], mailowo pod adres [adres e-mail] lub przy użyciu formularza kontaktowego.</p>
<p>9.4.    W reklamacji Klient powinien podać swoje imię i nazwisko, adres do korespondencji, rodzaj i datę wystąpienia nieprawidłowości związanej z funkcjonowaniem Sklepu.</p>
<p>9.5.    [Podmiot prowadzący sklep internetowy] zobowiązuje się do rozpatrzenia każdej reklamacji w terminie do [14] dni, a gdyby to nie było możliwe, do poinformowania w tym okresie Klienta, kiedy reklamacja zostanie rozpatrzona.</p>
<p><strong>VII.         Postanowienia końcowe</strong></p>
<p>10.1.    Rozstrzyganie ewentualnych sporów powstałych pomiędzy [podmiotem prowadzącym sklep internetowy] a Klientem, który jest konsumentem w rozumieniu art. 22[1] Kodeksu Cywilnego, zostaje poddane sądom właściwym zgodnie z postanowieniami właściwych przepisów Kodeksu postępowania cywilnego. </p>
<p>10.2.    Rozstrzyganie ewentualnych sporów powstałych pomiędzy [podmiotem prowadzącym sklep internetowy] a Klientem, który nie jest konsumentem w rozumieniu art. 22[1] Kodeksu Cywilnego Kodeksu Cywilnego, zostaje poddane sądowi właściwemu ze względu na siedzibę [podmiotu prowadzącego sklep internetowy].</p>
<p>10.3.    W sprawach nieuregulowanych w niniejszym Regulaminie mają zastosowanie przepisy Kodeksu cywilnego, przepisy Ustawy o świadczeniu usług drogą elektroniczną oraz inne właściwe przepisy prawa polskiego.</p>
<p>

</body>
</html>