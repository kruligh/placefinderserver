CREATE TABLE IF NOT EXISTS contact (
  id BIGINT NOT NULL AUTO_INCREMENT,
  name VARCHAR(32),
  email VARCHAR(32),
  phone_number VARCHAR(32),
  PRIMARY KEY (id)
) CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS address (
  id BIGINT NOT NULL AUTO_INCREMENT,
  street VARCHAR(32) NOT NULL,
  flat_number VARCHAR(6),
  city VARCHAR(32) NOT NULL,
  country VARCHAR(32) NOT NULL,
  PRIMARY KEY (id)
) CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS person (
  id BIGINT NOT NULL AUTO_INCREMENT,
  email VARCHAR(64) UNIQUE,
  password VARCHAR(255),
  name VARCHAR(64),
  is_activated TINYINT NOT NULL DEFAULT 0,
  contact_id BIGINT NOT NULL,
  is_agency TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (id),
  FOREIGN KEY (contact_id) REFERENCES contact(id)
) CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS person_points_account (
  id BIGINT NOT NULL AUTO_INCREMENT,
  person_id BIGINT NOT NULL,
  points_amount BIGINT NOT NULL DEFAULT 0,
  PRIMARY KEY (id),
  FOREIGN KEY (person_id) REFERENCES person(id)
)CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS admin (
  id BIGINT NOT NULL AUTO_INCREMENT,
  email VARCHAR(64) UNIQUE,
  password VARCHAR(255),
  name VARCHAR(64) UNIQUE,
  PRIMARY KEY (id)
) CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS activation_token (
  id BIGINT NOT NULL AUTO_INCREMENT,
  token VARCHAR(64),
  person_id BIGINT NOT NULL,
  generate_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  FOREIGN KEY (person_id) REFERENCES person(id)
) CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS forgotten_password_token (
  id BIGINT NOT NULL AUTO_INCREMENT,
  token VARCHAR(64),
  person_id BIGINT NOT NULL,
  generate_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  FOREIGN KEY (person_id) REFERENCES person(id)
) CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS price (
  id BIGINT NOT NULL AUTO_INCREMENT,
  amount INT,
  currency VARCHAR(16),
  type TINYINT UNSIGNED,
  PRIMARY KEY (id)
) CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;


CREATE TABLE IF NOT EXISTS property (
  id BIGINT NOT NULL AUTO_INCREMENT,
  property_type TINYINT UNSIGNED,
  building_type TINYINT UNSIGNED,
  address_id BIGINT NOT NULL,
  area INT UNSIGNED,
  room_count SMALLINT UNSIGNED,
  min_person SMALLINT UNSIGNED,
  max_person SMALLINT UNSIGNED,
  floor INT,
  lift TINYINT,
  balcony TINYINT,
  furnished TINYINT,
  heating_type TINYINT,
  parking_place TINYINT,
  pets TINYINT,
  built_year INT,
  basement TINYINT,
  garden TINYINT,
  climatisation TINYINT,
  smoking TINYINT,
  PRIMARY KEY (id),
  FOREIGN KEY (address_id) REFERENCES address(id)
) CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS offer (
  id BIGINT NOT NULL AUTO_INCREMENT,
  alias VARCHAR(128) UNIQUE,
  offer_type TINYINT UNSIGNED,
  fake_latitude DOUBLE NOT NULL,
  fake_longitude DOUBLE NOT NULL,
  offer_actual_to_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  add_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  active TINYINT DEFAULT 1,
  title VARCHAR(128),
  description VARCHAR(2000),
  price_id BIGINT NOT NULL,
  property_id BIGINT NOT NULL,
  person_id BIGINT NOT NULL,
  contact_id BIGINT NOT NULL,
  views BIGINT NOT NULL DEFAULT 0,
  is_consumed_by_admin TINYINT NOT NULL DEFAULT 0,
  was_ending_notified TINYINT NOT NULL DEFAULT 0,
  is_deleted TINYINT NOT NULL DEFAULT 0,
  promotion_list TINYINT NOT NULL DEFAULT 0,
  promotion_map TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (id),
  FOREIGN KEY (price_id) REFERENCES price(id),
  FOREIGN KEY (property_id) REFERENCES property(id),
  FOREIGN KEY (person_id) REFERENCES person(id),
  FOREIGN KEY (contact_id) REFERENCES contact(id)
) CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS offers_photo(
  id BIGINT NOT NULL AUTO_INCREMENT,
  offer_id BIGINT NOT NULL,
  url VARCHAR(255) NOT NULL,
  thumb_url VARCHAR(255) NOT NULL,
  store_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  priority INTEGER DEFAULT 0,
  PRIMARY KEY (id),
  FOREIGN KEY (offer_id) REFERENCES offer(id)
) CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS temporary_photo(
  id BIGINT NOT NULL AUTO_INCREMENT,
  person_id BIGINT NOT NULL,
  url VARCHAR(255) NOT NULL,
  thumb_url VARCHAR(255) NOT NULL,
  store_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  FOREIGN KEY (person_id) REFERENCES person(id)
) CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS deleted_photo(
  id BIGINT NOT NULL AUTO_INCREMENT,
  offer_id BIGINT NOT NULL,
  url VARCHAR(255) NOT NULL,
  thumb_url VARCHAR(255) NOT NULL,
  store_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  delete_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  FOREIGN KEY (offer_id) REFERENCES offer(id)
) CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS currency(
  currency_code VARCHAR(16) NOT NULL,
  currency_value DOUBLE NOT NULL,
  PRIMARY KEY(currency_code)
) CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS reported_offer(
  id BIGINT NOT NULL AUTO_INCREMENT,
  offer_id BIGINT NOT NULL,
  report_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  cause TINYINT NOT NULL DEFAULT 0,
  cause_desc VARCHAR(255),
  consumed TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (id),
  FOREIGN KEY (offer_id) REFERENCES offer(id)
) CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS points_packet(
  id BIGINT NOT NULL AUTO_INCREMENT,
  packet_name VARCHAR(255) NOT NULL,
  points_count INT NOT NULL,
  price_pln INT NOT NULL,
  PRIMARY KEY (id)
) CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS points_packet_payment(
  id BIGINT NOT NULL AUTO_INCREMENT,
  person_id BIGINT NOT NULL,
  token VARCHAR(255),
  add_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  points_packet_id BIGINT NOT NULL,
  currency VARCHAR(16) NOT NULL,
  consumed TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (id),
  FOREIGN KEY (person_id) REFERENCES person(id),
  FOREIGN KEY (points_packet_id) REFERENCES points_packet(id)
) CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS offer_payment(
  id BIGINT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (id)
) CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS offer_history(
  id BIGINT NOT NULL AUTO_INCREMENT,
  offer_id BIGINT NOT NULL,
  action_type TINYINT NOT NULL,
  event_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  admin_id BIGINT DEFAULT NULL,
  reason VARCHAR(255) DEFAULT NULL,
  report_id BIGINT DEFAULT NULL,
  payment_id BIGINT DEFAULT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (offer_id) REFERENCES offer(id),
  FOREIGN KEY (admin_id) REFERENCES admin(id),
  FOREIGN KEY (report_id) REFERENCES reported_offer(id),
  FOREIGN KEY (payment_id) REFERENCES offer_payment(id)
) CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS price_list(
  id BIGINT NOT NULL AUTO_INCREMENT,
  country_short VARCHAR(64),
  product_type TINYINT UNSIGNED,
  points_amount INTEGER NOT NULL,
  PRIMARY KEY (id)
) CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS follow_offer(
  id BIGINT NOT NULL AUTO_INCREMENT,
  offer_id BIGINT NOT NULL,
  person_id BIGINT NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (offer_id) REFERENCES offer(id),
  FOREIGN KEY (person_id) REFERENCES person(id)
) CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS send_mail_request(
  id BIGINT NOT NULL AUTO_INCREMENT,
  mail_from VARCHAR(255) DEFAULT NULL,
  mail_to VARCHAR(255) DEFAULT NULL,
  subject VARCHAR(255) DEFAULT NULL,
  body TEXT NOT NULL,
  send_date TIMESTAMP,
  attemps INT DEFAULT 0,
  consumed TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (id)
)CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;


INSERT INTO price_list(product_type, points_amount) VALUES(0,10);
INSERT INTO price_list(product_type, points_amount) VALUES(1,50);
INSERT INTO price_list(product_type, points_amount) VALUES(2,35);
INSERT INTO price_list(product_type, points_amount) VALUES(3,5);
INSERT INTO price_list(product_type, points_amount) VALUES(4,10);
INSERT INTO price_list(product_type, points_amount) VALUES(5,10);
INSERT INTO price_list(product_type, points_amount) VALUES(6,50);
INSERT INTO price_list(product_type, points_amount) VALUES(7,35);

INSERT INTO price_list(country_short, product_type, points_amount) VALUES('PL',0,10);
INSERT INTO price_list(country_short, product_type, points_amount) VALUES('PL',1,50);
INSERT INTO price_list(country_short, product_type, points_amount) VALUES('PL',2,35);
INSERT INTO price_list(country_short, product_type, points_amount) VALUES('PL',3,5);
INSERT INTO price_list(country_short, product_type, points_amount) VALUES('PL',4,10);
INSERT INTO price_list(country_short, product_type, points_amount) VALUES('PL',5,10);
INSERT INTO price_list(country_short, product_type, points_amount) VALUES('PL',6,50);
INSERT INTO price_list(country_short, product_type, points_amount) VALUES('PL',7,35);

INSERT INTO `points_packet` VALUES (1,'pakiet MINI',10,1000);
INSERT INTO `points_packet` VALUES (2,'pakiet STANDARD',25,2500);
INSERT INTO `points_packet` VALUES (3,'pakiet EXTRA',50,4000);
INSERT INTO `points_packet` VALUES (4,'pakiet MEGA',100,7000);
INSERT INTO `points_packet` VALUES (5,'pakiet PREMIUM',200,10000);

INSERT INTO admin(email, password, name) VALUES('admin','dupagowno','admin name');




