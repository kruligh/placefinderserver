
INSERT INTO contact(id,name,email,phone_number) VALUES(1,'Dominik','firmowy@mail.com','888 888 888');

INSERT INTO person(id,email, password, name, is_activated, contact_id) VALUES(1,'tubedzieemail@email.com','asd','Domino Krolino',1,1);
INSERT INTO `person_points_account` VALUES (1,1,690);

INSERT INTO `address` VALUES ('1', 'Rondo Mogilskie', null, 'Kraków', 'Polska');
INSERT INTO `price` VALUES ('1', '69', '1', '0');
INSERT INTO `property` VALUES ('1', '50', '51', '1', '73', '2', '0', '10', '3', '0', '1', '1', '1', '0', '1', '1995', '0', '1', '1', '0');
INSERT INTO `offer` VALUES ('1', null, '2', '140.0641833', '199.96140230000003', '2017-05-16 01:14:05', '2017-01-16 01:14:05', '1', 'Księstwo mogilskie', 'Ksiestwo mogilskie zaprasza na turnieje birpongowe w doborowym towarzystkie, można zagrać w szachy, uprać pranie w pralce, pogadać na balkonie na fajce, w lodówce mamy kapuste a kieszenie mamy puste ale uśmiech mamy na twarzy kto wie co dzi? się zdarzy. Specjalnie na potrzeby zdjęć zrobiliśmy tego syfa, utrzymanie porzadku to praca syzyfa', '1', '1', '1', '1', '13214','0','0','0','1','1');
INSERT INTO `offer` VALUES ('2', null, '2', '140.0641833', '199.96140230000003', '2017-01-01 12:01:05', '2017-01-16 01:14:05', '0', 'nieaktywna', 'opis', '1', '1', '1', '1', '13214','0','0','0','0','0');
INSERT INTO `offer` VALUES ('3', null, '2', '140.0641833', '199.96140230000003', '2017-05-16 01:14:05', '2017-01-16 01:14:05', '0', 'niezaakceptowana przez admina', 'opis', '1', '1', '1', '1', '13214','0','0','0','0','0');
INSERT INTO `offer` VALUES ('4', null, '2', '140.0641833', '199.96140230000003', '2017-05-16 01:14:05', '2017-01-16 01:14:05', '0', 'zbanowana bo była zgłoszona', 'opis', '1', '1', '1', '1', '13214','0','0','0','0','0');

INSERT INTO `reported_offer`(offer_id,cause, cause_desc, consumed) VALUES ('1',1,'Brzydka oferta, proszę o usunięcie bo jest rasistowska i wgl usuń to bla bla bla', '1');

INSERT INTO `offer_history` VALUES ('1','2','0','2017-01-01 12:01:05',null,null,null,null);
INSERT INTO `offer_history` VALUES ('2','2','1','2017-01-01 12:08:59',null,null,null,null);
INSERT INTO `offer_history` VALUES ('3','2','2','2017-01-10 16:45:59',1,null,null,null);
INSERT INTO `offer_history` VALUES ('4','2','3','2017-02-01 12:01:59',null,null,null,null);
INSERT INTO `offer_history` VALUES ('5','2','4','2017-02-01 12:01:59',1,'adminowi się nie spodobała',null,null);
INSERT INTO `offer_history` VALUES ('6','2','5','2017-02-19 16:45:59',1,'Treść raportu została potwierdzona',1,null);

INSERT INTO `offer_history` VALUES ('7','3','4','2017-02-20 16:45:59','1','BRZYDKA BYLA',null,null);
INSERT INTO `offer_history` VALUES ('8','4','5','2017-02-19 16:45:59','1','Treść raportu została potwierdzona',1,null);

INSERT INTO `offers_photo` VALUES ('1', '1', '/home/placefinder/photos/1-a.jpg', '/home/placefinder/photos/thumb/thumb1-a.jpg', '2017-01-16 01:14:05', '0');
INSERT INTO `offers_photo` VALUES ('2', '1', '/home/placefinder/photos/1-b.jpg', '/home/placefinder/photos/thumb/thumb1-b.jpg', '2017-01-16 01:14:06', '1');
INSERT INTO `offers_photo` VALUES ('3', '1', '/home/placefinder/photos/1-c.jpg', '/home/placefinder/photos/thumb/thumb1-c.jpg', '2017-01-16 01:14:06', '2');
INSERT INTO `offers_photo` VALUES ('4', '1', '/home/placefinder/photos/1-d.jpg', '/home/placefinder/photos/thumb/thumb1-d.jpg', '2017-01-16 01:14:06', '3');
INSERT INTO `offers_photo` VALUES ('5', '1', '/home/placefinder/photos/1-e.jpg', '/home/placefinder/photos/thumb/thumb1-e.jpg', '2017-01-16 01:14:06', '4');

INSERT INTO `offers_photo` VALUES ('6', '3', '/home/placefinder/photos/2017/2/21/1/1487688509966-0.jpg', '/home/placefinder/photos/2017/2/21/1/thumb/1487688509966-th-1.jpg', '2017-02-21 15:49:30', '0');
INSERT INTO `offers_photo` VALUES ('7', '2', '/home/placefinder/photos/2017/2/21/1/1487688513145-2.jpg', '/home/placefinder/photos/2017/2/21/1/thumb/1487688513145-th-3.jpg', '2017-02-21 15:49:30', '0');
INSERT INTO `offers_photo` VALUES ('8', '4', '/home/placefinder/photos/2017/2/21/1/1487688519499-4.jpg', '/home/placefinder/photos/2017/2/21/1/thumb/1487688519499-th-5.jpg', '2017-02-21 15:49:30', '0');
INSERT INTO `offers_photo` VALUES ('9', '4', '/home/placefinder/photos/2017/2/21/1/1487688536414-6.jpg', '/home/placefinder/photos/2017/2/21/1/thumb/1487688536414-th-7.jpg', '2017-02-21 15:49:30', '1');