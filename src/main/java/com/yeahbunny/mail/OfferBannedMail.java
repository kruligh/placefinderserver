package com.yeahbunny.mail;

import java.time.ZonedDateTime;


public class OfferBannedMail {
    private String userName;
    private String offerTitle;
    private String banReason;
    private ZonedDateTime banDate;

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setOfferTitle(String offerTitle) {
        this.offerTitle = offerTitle;
    }

    public String getOfferTitle() {
        return offerTitle;
    }

    public void setBanReason(String banReason) {
        this.banReason = banReason;
    }

    public String getBanReason() {
        return banReason;
    }

    public void setBanDate(ZonedDateTime banDate) {
        this.banDate = banDate;
    }

    public ZonedDateTime getBanDate() {
        return banDate;
    }
}
