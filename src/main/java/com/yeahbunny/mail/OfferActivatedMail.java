package com.yeahbunny.mail;


public class OfferActivatedMail {
    private String userName;
    private String offerUrl;

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setOfferUrl(String offerUrl) {
        this.offerUrl = offerUrl;
    }

    public String getOfferUrl() {
        return offerUrl;
    }
}
