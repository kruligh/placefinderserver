package com.yeahbunny.mail;


import com.yeahbunny.domain.AbstractEntity;

public class SendMailRequest extends AbstractEntity {
    private Object body;
    private String subject;
    private String to;
    private String from;
    private String templateFile;


    private SendMailRequest() {
    }

    public static SendMailRequest init(String templateFile ) {
        SendMailRequest request = new SendMailRequest();
        request.templateFile = templateFile;
        return request;
    }

    public SendMailRequest to(String email) {
        this.to =email;
        return this;
    }

    public SendMailRequest from(String from) {
        this.from = from;
        return this;
    }


    public SendMailRequest withSubject(String subject) {
        this.subject =  subject;
        return this;
    }

    public SendMailRequest withBody(Object body) {
        this.body = body;
        return this;
    }

    public Object getBody() {
        return body;
    }

    public String getSubject() {
        return subject;
    }

    public String getTo() {
        return to;
    }

    public String getTemplateFile() {
        return templateFile;
    }

    public String getFrom() {
        return from;
    }
}
