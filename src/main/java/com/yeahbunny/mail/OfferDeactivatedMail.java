package com.yeahbunny.mail;


public class OfferDeactivatedMail {
    private String offerTitle;
    private String userName;

    public void setOfferTitle(String offerTitle) {
        this.offerTitle = offerTitle;
    }

    public String getOfferTitle() {
        return offerTitle;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }
}
