package com.yeahbunny.mail;

public class AddedPointsMail {
    private String userName;
    private int addedPointsCount;
    private int actualPointsCount;

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public int getAddedPointsCount() {
        return addedPointsCount;
    }

    public void setAddedPointsCount(int addedPointsCount) {
        this.addedPointsCount = addedPointsCount;
    }

    public int getActualPointsCount() {
        return actualPointsCount;
    }

    public void setActualPointsCount(int actualPointsCount) {
        this.actualPointsCount = actualPointsCount;
    }
}
