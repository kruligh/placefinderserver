package com.yeahbunny.mail;


import java.time.ZonedDateTime;

public class EndOfferIncomingMail {
    private String userName;
    private String offerUrl;
    private ZonedDateTime endDate;
    private String offerTitle;

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setOfferUrl(String offerUrl) {
        this.offerUrl = offerUrl;
    }

    public String getOfferUrl() {
        return offerUrl;
    }

    public void setEndDate(ZonedDateTime endDate) {
        this.endDate = endDate;
    }

    public ZonedDateTime getEndDate() {
        return endDate;
    }

    public void setOfferTitle(String offerTitle) {
        this.offerTitle = offerTitle;
    }

    public String getOfferTitle() {
        return offerTitle;
    }
}
