package com.yeahbunny;


public class SizeConstraintsConfiguration {

    public static final int phoneNumber = 20;
    public static final int contactName = 40;
    public static final int reportCause = 1000;

    public static final int offerTitle = 36;
    public static final int street = 120;
    public static final int city = 120;
    public static final int country = 120;
    public static final int offerDescription = 351;
    public static final int email = 120;
    public static final int messageBody = 2500;
}
