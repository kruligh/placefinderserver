package com.yeahbunny.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="geocoding exception")
public class GeocodingException extends RuntimeException{
}
