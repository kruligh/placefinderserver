package com.yeahbunny.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.EXPECTATION_FAILED, reason="Google service error")
public class GoogleServiceException extends RuntimeException{
}
