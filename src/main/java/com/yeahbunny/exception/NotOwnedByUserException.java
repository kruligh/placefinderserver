package com.yeahbunny.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.FORBIDDEN, reason="Its not your offer")
public class NotOwnedByUserException extends SecurityException {

    public NotOwnedByUserException(String s) {
        super(s);
    }
}
