package com.yeahbunny.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.BAD_REQUEST, reason="Photo does not Exception")
public class PhotoDoesntExistException extends RuntimeException {

    public PhotoDoesntExistException(String message) {
        super(message);
    }
}