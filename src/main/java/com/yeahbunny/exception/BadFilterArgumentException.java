package com.yeahbunny.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.BAD_REQUEST, reason="Bad Filter Argument Exception")
public class BadFilterArgumentException extends NumberFormatException{

    public BadFilterArgumentException(String key, String value, Class<?> targetClass) {
        super("Cannot convert param with key: " + key + " and value: " + value + " to " + targetClass.getName());
    }

    public BadFilterArgumentException(String key) {
        super("Unknown key: " + key);
    }
}
