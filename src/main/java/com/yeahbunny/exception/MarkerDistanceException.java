package com.yeahbunny.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.BAD_REQUEST, reason="marker distance exception")
public class MarkerDistanceException extends RuntimeException{
}
