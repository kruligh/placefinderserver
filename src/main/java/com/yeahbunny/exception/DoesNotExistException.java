package com.yeahbunny.exception;

import javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="Offer does not exist")
public class DoesNotExistException extends RuntimeException{

    public DoesNotExistException(){
        super("");
    }

    public DoesNotExistException(String msg) {
        super(msg);
    }

    public DoesNotExistException(String msg, Exception e) {
        super(msg, e);
    }
}
