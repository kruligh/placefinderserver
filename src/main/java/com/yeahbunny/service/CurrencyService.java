package com.yeahbunny.service;

import com.yeahbunny.dao.CurrencyRepository;
import com.yeahbunny.domain.CurrencyEntity;
import com.yeahbunny.domain.type.CurrencyType;
import com.yeahbunny.exception.BadFilterArgumentException;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service
public class CurrencyService {

    @Inject
    private CurrencyRepository currencyRepository;

    public double getCurrencyMultiplier(String currencyCode){

        if(currencyCode.equals(CurrencyType.PLN.name())){
            return 1;
        }

        CurrencyEntity entity = currencyRepository.findOne(currencyCode);
        if(entity == null){
            throw new BadFilterArgumentException("PRICE");

        }else{

            return entity.getValue();
        }
    }

    public int calculateFromZlToAmount(CurrencyType currencyType, int pricePln) {
        if(currencyType == CurrencyType.PLN){
            return pricePln;
        }else{//todo ale to brzydkie XDDD zal
            float zl = pricePln/100.0f;
            float output  = zl * (float)getCurrencyMultiplier(currencyType.name());
            return Math.round(output * 100);
        }
    }
}
