package com.yeahbunny.service;

import com.yeahbunny.exception.GeocodingException;
import com.yeahbunny.exception.GoogleServiceException;
import com.yeahbunny.exception.MarkerDistanceException;
import com.yeahbunny.service.google.GoogleAddressComponent;
import com.yeahbunny.service.google.GoogleDistance;
import com.yeahbunny.service.google.GoogleDistanceResponse;
import com.yeahbunny.service.google.GoogleGeocodingResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.inject.Inject;
import java.util.List;

@Service
public class GeocodingService {

    private static final Logger LOG = LoggerFactory.getLogger(GeocodingService.class);

    @Value("${app.google.apikey}")
    private String googleApiKey;

    @Value("${app.offer.marker.max_distance}")
    private double maxMarkerToGeolocationDistance;

    private String googleGeocodingUrl="https://maps.googleapis.com/maps/api/geocode/json?address=";

    private String googleDistanceMatrixApiUrl="https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=";

    @Inject
    private RestTemplate restTemplate;

    public String checkOfferAndGetCountryCode(String street, String city, String country, double latitude, double longitude) {

        GoogleGeocodingResponse geocodingResponse = getGeocodingResponse(street,city,country);

        if(geocodingResponse.getAddressResultList().isEmpty()){
            throw new GeocodingException();
        }

        checkIsMarkerInRange(geocodingResponse, latitude,longitude);


        for (GoogleAddressComponent googleAddressComponent:geocodingResponse.getAddressResultList().get(0).getAddressComponentList()) {
            if(googleAddressComponent.getTypes().contains("country")){
                LOG.debug("Country code {}", googleAddressComponent.getShortName());
                return googleAddressComponent.getShortName();
            }
        }

        LOG.error("Country code not found. Street: {} city: {} country: {}",street,city,country);

        return null;
    }

    private void checkIsMarkerInRange(GoogleGeocodingResponse geocodingResponse, double markerLatitude, double markerLongitude) {
        double addressLatitude = geocodingResponse.getAddressResultList().get(0).getAddressGeometry().getGoogleAddressLocation().getLatitude();
        double addressLongitude = geocodingResponse.getAddressResultList().get(0).getAddressGeometry().getGoogleAddressLocation().getLongitude();

        long distance = getDistance(addressLatitude,addressLongitude, markerLatitude, markerLongitude);

        LOG.info("Distance between marker and geocoding result = {}", distance);

        if(distance>this.maxMarkerToGeolocationDistance){
            throw new MarkerDistanceException();
        }
    }

    private long getDistance(double addressLatitude, double addressLongitude, double markerLatitude, double markerLongitude) {
        String paramsString = addressLatitude + "," + addressLongitude + "&destinations=" + markerLatitude + "," + markerLongitude;
        String completeDistanceUrl = this.googleDistanceMatrixApiUrl+paramsString+"&key="+this.googleApiKey;
        LOG.debug("Distance url {}", completeDistanceUrl);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>(headers);

        ResponseEntity<GoogleDistanceResponse> response;
        try{
            response = restTemplate.exchange(completeDistanceUrl, HttpMethod.GET, request , GoogleDistanceResponse.class );
        }catch (RestClientException e) {
            if (e instanceof HttpClientErrorException) {
                HttpClientErrorException httpClientException = (HttpClientErrorException) e;
                LOG.error("Distance matrix error code {} ", httpClientException.getStatusCode().value());
                throw new GoogleServiceException();
            }else{
                e.printStackTrace();
                throw new GoogleServiceException();
            }
        }

        GoogleDistanceResponse result = response.getBody();

        return result.getRows().get(0).getElements().get(0).getDistance().getValue();
    }

    private GoogleGeocodingResponse getGeocodingResponse(String street, String city, String country) {
        String paramsString = street + ",+" + city + ",+" + country;
        String completeGeocodingUrl = this.googleGeocodingUrl+paramsString+"&key="+this.googleApiKey;
        LOG.debug("Geocoding url {}", completeGeocodingUrl);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<String>(headers);

        ResponseEntity<GoogleGeocodingResponse> response;
        try{
            response = restTemplate.exchange(completeGeocodingUrl, HttpMethod.GET, request , GoogleGeocodingResponse.class );
        }catch (RestClientException e) {
            if (e instanceof HttpClientErrorException) {
                HttpClientErrorException httpClientException = (HttpClientErrorException) e;
                LOG.error("Geocoding error code {} ", httpClientException.getStatusCode().value());
                throw new GoogleServiceException();
            }else{
                e.printStackTrace();
                throw new GoogleServiceException();
            }
        }
        return response.getBody();
    }
}
