package com.yeahbunny.service;

import com.yeahbunny.domain.PointsPacketPaymentEntity;

import java.time.ZonedDateTime;
import java.util.UUID;

public class TokenBuilderService {

    public static String buildToken() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    public static String paymentToken(PointsPacketPaymentEntity pointsPacketPaymentEntity) {
        return pointsPacketPaymentEntity.getPerson().getId()+ "-" + pointsPacketPaymentEntity.getId() + "-" + ZonedDateTime.now().toInstant().toEpochMilli();
    }
}
