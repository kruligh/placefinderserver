package com.yeahbunny.service;

import com.yeahbunny.domain.*;
import com.yeahbunny.mail.*;
import com.yeahbunny.ws.dto.request.AddPointsRequest;
import com.yeahbunny.ws.dto.request.SendMessageRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service
public class NotifyUserService {

    private final static Logger LOG = LoggerFactory.getLogger(NotifyUserService.class);

    @Value("${app.host}")
    String appHost;

    @Value("${app.mail.link.offer}")
    String offerLink;

    @Value("${app.mail.link.verification}")
    String mailAccountVerificationLink;

    @Value("${app.mail.link.forgotten_password_change}")
    String forgottenPasswordChangeLink;

    @Value("${app.mail.link.forgotten_password_cancel}")
    String forgottenPasswordCancelLink;

    @Value("${app.mail.from}")
    String mailFrom;

    @Inject
    SendMailService sendMailService;

    public void notifyOfferActivated(OfferEntity offerEntity) {

        OfferActivatedMail mail = new OfferActivatedMail();
        mail.setUserName(offerEntity.getContact().getName());
        mail.setOfferUrl(appHost + offerLink + "?id=" +offerEntity.getId());

        LOG.debug("Sending notify offer extended to  {} with link {}", offerEntity.getContact().getEmail(), mail.getOfferUrl());

        SendMailRequest mailRequest =
                SendMailRequest.init("mail_offer-activated.vm")
                        .withSubject("Aktywacja oferty")
                        .to(offerEntity.getContact().getEmail())
                        .from(mailFrom)
                        .withBody(mail);


        //sendMailService.toExecute(mailRequest);

    }

    public void notifyOfferDeativated(OfferEntity offerEntity) {
        LOG.debug("Sending notify offer deactivated to  {}", offerEntity.getContact().getEmail());

        OfferDeactivatedMail mail = new OfferDeactivatedMail();
        mail.setUserName(offerEntity.getContact().getName());
        mail.setOfferTitle(offerEntity.getTitle());

        SendMailRequest mailRequest =
                SendMailRequest.init("mail_offer-deactivated.vm")
                        .withSubject("Deaktywacja oferty")
                        .to(offerEntity.getContact().getEmail())
                        .from(mailFrom)
                        .withBody(mail);

        //sendMailService.toExecute(mailRequest);


    }

    public void notifyEndOfOfferIncoming(OfferEntity offerEntity) {
        LOG.debug("Sending notify end offer incoming to {}", offerEntity.getContact().getEmail());

        EndOfferIncomingMail mail = new EndOfferIncomingMail();
        mail.setUserName(offerEntity.getContact().getName());
        mail.setOfferTitle(offerEntity.getTitle());
        mail.setOfferUrl(buildOfferUrl(offerEntity.getId()));
        mail.setEndDate(offerEntity.getOfferActualToDate());

        SendMailRequest mailRequest =
                SendMailRequest.init("mail_offer-end-incoming.vm")
                        .withSubject("Nadchodzący termin ważności oferty")
                        .to(offerEntity.getContact().getEmail())
                        .from(mailFrom)
                        .withBody(mail);

        //sendMailService.toExecute(mailRequest);
    }

    private String buildOfferUrl(Long id) {
        return appHost + offerLink + "?id=" + id;
    }

    public void sendActivationMail(PersonEntity personEntity, ActivationTokenEntity activationTokenEntity) {

        EmailActivationMail mail = new EmailActivationMail();
        mail.setUserName(personEntity.getName());
        mail.setUrl(appHost + mailAccountVerificationLink + "?token=" + activationTokenEntity.getToken() + "&u=" + activationTokenEntity.getId());

        LOG.debug("Generate activation email with link {}", mail.getUrl());

        SendMailRequest mailRequest =
                SendMailRequest.init("mail_activation.vm")
                        .withSubject("Potwierdzenie adresu email")
                        .to(personEntity.getEmail())
                        .from(mailFrom)
                        .withBody(mail);

       sendMailService.toExecute(mailRequest);

    }

    public void sendForgottenPasswordEmail(ForgottenPasswordTokenEntity forgottenPasswordTokenEntity) {
        ForgottenPasswordMail mail = new ForgottenPasswordMail();
        mail.setUserName(forgottenPasswordTokenEntity.getPerson().getName());
        mail.setUrl(appHost + forgottenPasswordChangeLink + "?token=" + forgottenPasswordTokenEntity.getToken() + "&u=" + forgottenPasswordTokenEntity.getId());
        mail.setCancelUrl(appHost + forgottenPasswordCancelLink + "?token=" + forgottenPasswordTokenEntity.getToken() + "&u=" + forgottenPasswordTokenEntity.getId());
        LOG.debug("Generate forgotten password email with link {}", mail.getUrl());

        SendMailRequest mailRequest =
                SendMailRequest.init("mail_forgotten_password.vm")
                        .withSubject("Zmiana hasła")
                        .to(forgottenPasswordTokenEntity.getPerson().getEmail())
                        .from(mailFrom)
                        .withBody(mail);

        sendMailService.toExecute(mailRequest);
    }

    public void notifyOfferRejectedByAdmin(OfferHistoryEntity historyEntity) {
        LOG.debug("Sending notify offer banned{}", historyEntity.getOffer().getContact().getEmail());

        OfferEntity offerEntity = historyEntity.getOffer();

        OfferBannedMail mail = new OfferBannedMail();
        mail.setUserName(offerEntity.getContact().getName());
        mail.setOfferTitle(offerEntity.getTitle());
        mail.setBanDate(historyEntity.getEventDate());
        mail.setBanReason(historyEntity.getReason());


        SendMailRequest mailRequest =
                SendMailRequest.init("mail_offer-banned.vm")
                        .withSubject("Zablokowanie oferty")
                        .to(offerEntity.getContact().getEmail())
                        .from(mailFrom)
                        .withBody(mail);

        sendMailService.toExecute(mailRequest);

    }

    public void notifyOfferBannedByReport(OfferHistoryEntity historyEntity) {
        this.notifyOfferRejectedByAdmin(historyEntity);
    }

    public void notifyAddedPointsByAdmin(AdminEntity adminEntity, PersonEntity personEntity, AddPointsRequest addPointsRequest) {
        LOG.error("todo : sending mail points added by admin");

    }

    public void notifyAddedPoints(PersonEntity personEntity, int addedPointsCount, int actualPointsCount){
        AddedPointsMail mail = new AddedPointsMail();
        mail.setUserName(personEntity.getName());
        mail.setAddedPointsCount(addedPointsCount);
        mail.setActualPointsCount(actualPointsCount);

        SendMailRequest mailRequest =
                SendMailRequest.init("mail_points-added.vm")
                        .withSubject("Zakończenie transakcji")
                        .to(personEntity.getContact().getEmail())
                        .from(mailFrom)
                        .withBody(mail);

        sendMailService.toExecute(mailRequest);
    }

    public void sendOfferMessage(SendMessageRequest sendMessageRequest, ContactEntity contact) {
        OfferMessageMail mail = new OfferMessageMail();
        mail.setUserName(contact.getName());
        mail.setOfferUrl(buildOfferUrl(sendMessageRequest.getOfferId()));
        mail.setContent(sendMessageRequest.getContent());
        mail.setPhoneNumber(sendMessageRequest.getFromPhoneNumber());
        mail.setFrom(sendMessageRequest.getFrom());

        SendMailRequest mailRequest =
                SendMailRequest.init("mail_offer-message.vm")
                        .withSubject("Zapytanie o ofertę")
                        .to(contact.getEmail())
                        .from(sendMessageRequest.getFrom())
                        .withBody(mail);

        sendMailService.toExecute(mailRequest);
    }

    @Async
    public void sendActivationMailAsync(PersonEntity personEntity, ActivationTokenEntity activationTokenEntity) {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.sendActivationMail(personEntity,activationTokenEntity);
    }
}
