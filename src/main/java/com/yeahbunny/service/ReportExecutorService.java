package com.yeahbunny.service;

import com.yeahbunny.dao.OfferHistoryRepository;
import com.yeahbunny.domain.AdminEntity;
import com.yeahbunny.domain.OfferHistoryEntity;
import com.yeahbunny.domain.ReportedOfferEntity;
import com.yeahbunny.domain.type.OfferActionType;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.time.ZonedDateTime;

@Service
public class ReportExecutorService {

    @Inject
    private OfferHistoryRepository offerHistoryRepository;

    @Inject
    private NotifyUserService notifyUserService;

    public void ignoreReport(ReportedOfferEntity reportedOfferEntity, AdminEntity adminEntity) {
        reportedOfferEntity.setConsumed(true);

        OfferHistoryEntity historyEntity = new OfferHistoryEntity();
        historyEntity.setAction(OfferActionType.IGNORED_REPORT);
        historyEntity.setEventDate(ZonedDateTime.now());
        historyEntity.setOffer(reportedOfferEntity.getOffer());
        historyEntity.setReason(null);
        historyEntity.setAdmin(adminEntity);
        historyEntity.setReport(reportedOfferEntity);

        this.offerHistoryRepository.save(historyEntity);
    }

    public void executeReport(ReportedOfferEntity reportedOfferEntity, AdminEntity adminEntity, String banReason) {

        reportedOfferEntity.setConsumed(true);

        OfferHistoryEntity historyEntity = new OfferHistoryEntity();
        historyEntity.setAction(OfferActionType.BANNED_BY_REPORT);
        historyEntity.setOffer(reportedOfferEntity.getOffer());
        historyEntity.setEventDate(ZonedDateTime.now());
        historyEntity.setReport(reportedOfferEntity);
        historyEntity.setAdmin(adminEntity);
        historyEntity.setReason(banReason);

        this.offerHistoryRepository.save(historyEntity);

        notifyUserService.notifyOfferBannedByReport(historyEntity);


    }
}
