package com.yeahbunny.service;

import com.yeahbunny.dao.PriceListRepository;
import com.yeahbunny.domain.PriceListEntity;
import com.yeahbunny.domain.type.ProductType;
import com.yeahbunny.ws.dto.request.AddOfferRequest;
import com.yeahbunny.ws.dto.request.EditOfferRequest;
import com.yeahbunny.ws.dto.request.OfferRequest;
import com.yeahbunny.ws.dto.response.OfferValuationResponseDto;
import com.yeahbunny.ws.dto.response.OfferValuationRow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Service
public class OfferValuationService {

    private static final Logger LOG = LoggerFactory.getLogger(OfferValuationService.class);

    @Inject
    private PriceListRepository priceListRepository;

    @Value("${app.offer.photo.max_free_count}")
    private int maxFreePhotos;

    public OfferValuationResponseDto valuateAdd(AddOfferRequest offerRequest, String countryCode) {
        OfferValuationResponseDto responseDto = new OfferValuationResponseDto();

        List<OfferValuationRow> rows = new ArrayList<>();

        rows.add(generateValuationRow(getAddOfferPrice(offerRequest, countryCode)));

        int additionalPhotos = offerRequest.getPhotos().length - maxFreePhotos;

        for(int i = 0; i <additionalPhotos; i++){
            rows.add(generateValuationRow(getAddAdditionalPhotoPrice(countryCode)));
        }

        responseDto.setRows(rows);
        return responseDto;
    }

    public OfferValuationResponseDto valuateEdit(EditOfferRequest editOfferRequest, String countryCode) {
        OfferValuationResponseDto responseDto = new OfferValuationResponseDto();

        List<OfferValuationRow> rows = new ArrayList<>();

        if(editOfferRequest.getExtendOffer()>0){
            rows.add(generateValuationRow(getExtendOfferPrice(editOfferRequest, countryCode)));
        }

        int newPhotos = 0;
        for (long photoId : editOfferRequest.getPhotos()) {
            //wtedy jest dodawane podczas edycji
            if(photoId<0){
                newPhotos++;
            }
        }

        int additionalPhotos = editOfferRequest.getPhotos().length - maxFreePhotos;

        for(int i = 0; i <additionalPhotos && i<newPhotos; i++){
            rows.add(generateValuationRow(getAddAdditionalPhotoPrice(countryCode)));
        }

        responseDto.setRows(rows);
        return responseDto;
    }

    private PriceListEntity getExtendOfferPrice(EditOfferRequest editOfferRequest, String countryCode) {
        ProductType productType;

        switch (editOfferRequest.getOfferType()){
            case HIRE:
                productType = ProductType.EXTEND_HIRE;
                break;
            case SELL:
                productType = ProductType.EXTEND_SELL;
                break;
            case VISIT:
                productType = ProductType.EXTEND_VISIT;
                break;
            default:
                LOG.error("Product type {} not recognized", editOfferRequest.getOfferType().name());
                productType = ProductType.EXTEND_SELL;
                break;
        }

        PriceListEntity entity = this.priceListRepository.findByCountryCodeAndProductType(countryCode, productType);

        if(entity == null){
            LOG.error("Probably not recognized country code in database. Country code: {}, product type: {} ", countryCode, productType.name());
            entity = this.priceListRepository.findByCountryCodeIsNullAndProductType(productType);
        }

        return entity;
    }

    private PriceListEntity getAddAdditionalPhotoPrice(String countryCode) {

        ProductType additionalPhotoType = ProductType.ADDITIONAL_PHOTO;

        PriceListEntity entity = this.priceListRepository.findByCountryCodeAndProductType(countryCode, additionalPhotoType);

        if(entity == null){
            LOG.error("Probably not recognized country code in database. Country code: {}, product type: {} ", countryCode, additionalPhotoType);
            entity = this.priceListRepository.findByCountryCodeIsNullAndProductType(additionalPhotoType);
        }

        return entity;
    }

    private OfferValuationRow generateValuationRow(PriceListEntity addOfferPrice) {
        OfferValuationRow result = new OfferValuationRow();
        result.setPoints(addOfferPrice.getPointsAmount());
        result.setProductType(addOfferPrice.getProductType());

        return result;
    }

    private PriceListEntity getAddOfferPrice(OfferRequest offerRequest, String countryCode) {

        ProductType productType;

        switch (offerRequest.getOfferType()){
            case HIRE:
                productType = ProductType.HIRE;
                break;
            case SELL:
                productType = ProductType.SELL;
                break;
            case VISIT:
                productType = ProductType.VISIT;
                break;
            default:
                LOG.error("Product type {} not recognized", offerRequest.getOfferType().name());
                productType = ProductType.SELL;
                break;
        }


        PriceListEntity entity = this.priceListRepository.findByCountryCodeAndProductType(countryCode, productType);

        if(entity == null){
            LOG.error("Probably not recognized country code in database. Country code: {}, product type: {} ", countryCode, productType.name());
            entity = this.priceListRepository.findByCountryCodeIsNullAndProductType(productType);
        }

        return entity;
    }

}
