package com.yeahbunny.service;

import com.yeahbunny.dao.SendMailRequestRepository;
import com.yeahbunny.domain.SendMailRequestEntity;
import com.yeahbunny.mail.SendMailRequest;
import com.yeahbunny.ws.RegisterController;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.velocity.VelocityEngineUtils;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

@Service
public class SendMailService{

    @Inject
    private JavaMailSender mailSender;

    @Inject
    private VelocityEngine velocityEngine;

    @Inject
    private SendMailRequestRepository sendMailRequestRepository;

    private static final Logger LOG = LoggerFactory.getLogger(SendMailService.class);

    public void toExecute(SendMailRequest sendMailRequest) {

        Map model = new HashMap();
        model.put("body", sendMailRequest.getBody());
        String text = VelocityEngineUtils.mergeTemplateIntoString(
                velocityEngine, sendMailRequest.getTemplateFile(), "utf-8", model);

        SendMailRequestEntity entity = new SendMailRequestEntity();
        entity.setConsumed(false);
        entity.setAttemps(0);
        entity.setBody(text);
        entity.setFrom(sendMailRequest.getFrom());
        entity.setSubject(sendMailRequest.getSubject());
        entity.setTo(sendMailRequest.getTo());

        this.sendMailRequestRepository.save(entity);

        this.execute(entity);

    }

    public void execute(SendMailRequestEntity sendMailRequest) {

        sendMailRequest.setAttemps(sendMailRequest.getAttemps()+1);
        MimeMessage msg = mailSender.createMimeMessage();

        try {
            MimeMessageHelper helper = new MimeMessageHelper(msg, true);
            helper.setText(sendMailRequest.getBody(), true);
            helper.setSubject(sendMailRequest.getSubject());
            helper.setTo(sendMailRequest.getTo());
            helper.setFrom(sendMailRequest.getFrom());
        } catch (MessagingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        try{
            this.mailSender.send(msg);
            sendMailRequest.setConsumed(true);
            sendMailRequest.setSendDate(ZonedDateTime.now());
            LOG.info("Mail sent to {} from {}",sendMailRequest.getTo(), sendMailRequest.getFrom());
        }catch (MailException e){
            e.printStackTrace();
            LOG.error("Sending mail to {} from {} failed {}",sendMailRequest.getTo(), sendMailRequest.getFrom(), e.getMessage());
            sendMailRequest.setConsumed(false);
        }
        this.sendMailRequestRepository.save(sendMailRequest);
    }
}
