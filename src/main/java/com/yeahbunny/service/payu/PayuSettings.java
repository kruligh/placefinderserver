package com.yeahbunny.service.payu;

public class PayuSettings {

    private boolean invoiceDisabled;

    public boolean isInvoiceDisabled() {
        return invoiceDisabled;
    }

    public void setInvoiceDisabled(boolean invoiceDisabled) {
        this.invoiceDisabled = invoiceDisabled;
    }
}
