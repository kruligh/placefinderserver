package com.yeahbunny.service.payu;

import java.util.List;

public class PayuStandardOffer {

    private String notifyUrl;

    private String continueUrl;

    private String extOrderId;

    private String customerIp;

    private String merchantPosId;

    private String description;
    private String currencyCode;
    private String totalAmount;
    private PayuSettings settings;
    private List<PayuProduct> products;

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getContinueUrl() {
        return continueUrl;
    }

    public void setContinueUrl(String continueUrl) {
        this.continueUrl = continueUrl;
    }

    public String getExtOrderId() {
        return extOrderId;
    }

    public void setExtOrderId(String extOrderId) {
        this.extOrderId = extOrderId;
    }

    public String getCustomerIp() {
        return customerIp;
    }

    public void setCustomerIp(String customerIp) {
        this.customerIp = customerIp;
    }

    public String getMerchantPosId() {
        return merchantPosId;
    }

    public void setMerchantPosId(String merchantPosId) {
        this.merchantPosId = merchantPosId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public List<PayuProduct> getProducts() {
        return products;
    }

    public void setProducts(List<PayuProduct> products) {
        this.products = products;
    }

    public PayuSettings getSettings() {
        return settings;
    }

    public void setSettings(PayuSettings settings) {
        this.settings = settings;
    }
}
