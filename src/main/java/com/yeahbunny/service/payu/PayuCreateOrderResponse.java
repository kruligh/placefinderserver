package com.yeahbunny.service.payu;

import org.springframework.http.HttpStatus;


public class PayuCreateOrderResponse {

    private String orderId;

    private PayuStatus status;

    private String redirectUri;

    private String extOrderId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public PayuStatus getStatus() {
        return status;
    }

    public void setStatus(PayuStatus status) {
        this.status = status;
    }

    public String getRedirectUri() {
        return redirectUri;
    }

    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }

    public String getExtOrderId() {
        return extOrderId;
    }

    public void setExtOrderId(String extOrderId) {
        this.extOrderId = extOrderId;
    }

    @Override
    public String toString() {
        return "PayuCreateOrderResponse{" +
                "orderId='" + orderId + '\'' +
                ", status=" + status +
                ", redirectUri='" + redirectUri + '\'' +
                ", extOrderId='" + extOrderId + '\'' +
                '}';
    }
}
