package com.yeahbunny.service;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yeahbunny.domain.PointsPacketEntity;
import com.yeahbunny.domain.PointsPacketPaymentEntity;
import com.yeahbunny.domain.type.CurrencyType;
import com.yeahbunny.exception.BadArgumentsException;
import com.yeahbunny.exception.PaymentException;
import com.yeahbunny.service.payu.*;
import com.yeahbunny.ws.dto.request.PayuOrderNotificationRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class PayuService {

    private static final Logger LOG = LoggerFactory.getLogger(PayuService.class);

    @Value("${app.payment.payu.url.host}")
    private String payuHostUrl;

    @Value("${app.payment.payu.url.authorize}")
    private String payuAuthorizeUrl;

    @Value("${app.payment.payu.url.un_authorize}")
    private String payuUnAuthorizeUrl;

    @Value("${app.payment.payu.url.create_order}")
    private String payuCreateOrderUrl;

    @Value("${app.payment.payu.pos_id}")
    private String payuPosId;

    @Value("${app.payment.payu.client_secret}")
    private String payuSecret;

    @Value("${app.payment.payu.md5key}")
    private String payuMd5key;

    @Value("${app.payment.payu.description}")
    private String paymentDescription;

    @Value("${app.payment.payu.notifyUrl}")
    private String payuPaymentNotifyUrl;

    @Value("${app.payment.payu.continueUrl}")
    private String payuPaymentContinueUrl;

    @Value("${app.back.host}")
    private String backHost;

    @Value("${app.host}")
    private String appHost;

    @Inject
    private CurrencyService currencyService;

    @Inject
    private RestTemplate restTemplate;

    private String payuAuthHeaderName = "Authorization";

    private PayuAuthorizeResponse payuAuth;

    /***
     *
     * @param pointsPacketPaymentEntity
     * @param remoteAddress of user for payu
     * @return String url to redirect user to make payment
     */
    public String buyPoints(PointsPacketPaymentEntity pointsPacketPaymentEntity, String remoteAddress) {
        this.login();
        PayuCreateOrderResponse createOrderResponse = this.createStandardOrder(pointsPacketPaymentEntity, remoteAddress);
        this.logout();
        return createOrderResponse.getRedirectUri();
    }


    /***
     * Reject order
     * @param payuOrderNotificationRequest
     */
    public void reject(PayuOrderNotificationRequest payuOrderNotificationRequest) {
        //todo reject
    }

    private PayuCreateOrderResponse createStandardOrder(PointsPacketPaymentEntity pointsPacketPaymentEntity, String remoteAddress) {
        LOG.info("Create standard order");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add(this.payuAuthHeaderName, createAuthHeaderValue());

        PayuStandardOffer payuStandardOffer = generatePayuCreateStandardRequest(pointsPacketPaymentEntity, remoteAddress);
        String jsonBody = generateJson(payuStandardOffer);
        LOG.info("Json created {} ",jsonBody);
        HttpEntity<String> request = new HttpEntity<String>(jsonBody,headers);

        ResponseEntity<PayuCreateOrderResponse> response;
        try{
            response = restTemplate.exchange(this.payuHostUrl+this.payuCreateOrderUrl,HttpMethod.POST, request , PayuCreateOrderResponse.class );
        }catch (RestClientException e) {
            if (e instanceof HttpClientErrorException) {
                HttpClientErrorException httpClientException = (HttpClientErrorException) e;
                String responseMsg = readFromJson("status.statusCode", httpClientException.getResponseBodyAsString());
                LOG.error("Payment error code {} {}, request json {}", httpClientException.getStatusCode().value(), responseMsg, jsonBody);
                throw new PaymentException();
            }else{
                e.printStackTrace();
                throw new PaymentException();
            }
        }
        LOG.info("PayU create order response: code: {} body: {}", response.getStatusCodeValue(), response.getBody());

        return response.getBody();
    }

    private void login() {
        LOG.info("PayU authorization");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
        map.add("grant_type", "client_credentials");
        map.add("client_id", payuPosId);
        map.add("client_secret", payuSecret);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

        ResponseEntity<PayuAuthorizeResponse> response;
        try{
            response = restTemplate.postForEntity(this.payuHostUrl+this.payuAuthorizeUrl, request , PayuAuthorizeResponse.class );
        }catch (RestClientException e){
            e.printStackTrace();
            throw new PaymentException();
        }
        this.payuAuth = response.getBody();
        LOG.info("PayU auth response: code: {} body: {}", response.getStatusCodeValue(), response.getBody());

    }

    private void logout() {
        LOG.info("PayU logout");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.TEXT_PLAIN);
        headers.add(this.payuAuthHeaderName, this.payuAuth.getTokenType() + " " + payuAuth.getAccesToken());

        HttpEntity<Object> request = new HttpEntity<>(headers);

        ResponseEntity<String> response;
        try{

            response = restTemplate.exchange(this.payuHostUrl + this.payuUnAuthorizeUrl + "/" + this.payuAuth.getAccesToken(),
                    HttpMethod.DELETE,
                    request,
                    String.class);
            LOG.info("PayU logout response: code: {} body: {}", response.getStatusCodeValue(), response.getBody());
        }catch (RestClientException e){
            if (e instanceof HttpClientErrorException) {
                HttpClientErrorException httpClientException = (HttpClientErrorException) e;
                String responseMsg = readFromJson("status.statusCode", httpClientException.getResponseBodyAsString());
                LOG.error("Payment error code {} {}, request json {}", httpClientException.getStatusCode().value(), responseMsg);
                //throw new PaymentException();
            }else{
                e.printStackTrace();
                throw new PaymentException();
            }
        }
        this.payuAuth = null;

    }

    private String readFromJson(String field, String json) {
        try {
            JsonFactory factory = new JsonFactory();
            ObjectMapper mapper = new ObjectMapper(factory);
            JsonNode rootNode = mapper.readTree(json);

            String[] fields = field.split("\\.");
            for(int i =0; i<fields.length; i++){
                rootNode = rootNode.get(fields[i]);
                if(rootNode == null){
                    return "";
                }
            }
            return rootNode.textValue();

        }catch (IOException e){
            return "";
        }
    }
    private String generateJson(PayuStandardOffer payuStandardOffer) {
        ObjectMapper mapper = new ObjectMapper();
        String json;
        try{
            json =  mapper.writeValueAsString(payuStandardOffer);
        }catch (JsonProcessingException e){
            e.printStackTrace();
            throw new BadArgumentsException("PayuStandardOffer json exception");
        }
        return json;
    }

    private PayuStandardOffer generatePayuCreateStandardRequest(PointsPacketPaymentEntity pointsPacketPaymentEntity, String remoteAddress) {
        PayuStandardOffer result = new PayuStandardOffer();

        result.setNotifyUrl(this.backHost + this.payuPaymentNotifyUrl);
        result.setContinueUrl(this.appHost + this.payuPaymentContinueUrl);
        result.setExtOrderId(pointsPacketPaymentEntity.getToken());
        result.setCustomerIp(remoteAddress);
        result.setMerchantPosId(this.payuPosId);
        result.setDescription(this.paymentDescription);
        result.setCurrencyCode(pointsPacketPaymentEntity.getCurrency().name());
        result.setTotalAmount(Integer.toString(pointsPacketPaymentEntity.getPointsPacket().getPricePln()));

        PayuSettings payuSettings = new PayuSettings();
        payuSettings.setInvoiceDisabled(true);
        result.setSettings(payuSettings);

        List<PayuProduct> products= new ArrayList<>();
         products.add(generateProduct(pointsPacketPaymentEntity.getPointsPacket(), pointsPacketPaymentEntity.getCurrency()));
        result.setProducts(products);

        return result;
    }

    private PayuProduct generateProduct(PointsPacketEntity pointsPacket, CurrencyType currencyType) {

        PayuProduct product = new PayuProduct();
        product.setName(pointsPacket.getName());
        product.setUnitPrice(Integer.toString(currencyService.calculateFromZlToAmount(currencyType, pointsPacket.getPricePln())));
        product.setQuantity("1");
        product.setVirtual(true);

        return product;
    }

    private String createAuthHeaderValue() {

        String result = this.payuAuth.getTokenType() + " " + payuAuth.getAccesToken();
        LOG.info(this.payuAuthHeaderName + " : " + result);
        return result;
    }
}
