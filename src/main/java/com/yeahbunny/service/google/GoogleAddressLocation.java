package com.yeahbunny.service.google;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GoogleAddressLocation {

    @JsonProperty(value = "lat")
    private double latitude;

    @JsonProperty(value = "lng")
    private double longitude;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
