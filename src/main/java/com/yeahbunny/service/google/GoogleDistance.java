package com.yeahbunny.service.google;


import com.fasterxml.jackson.annotation.JsonProperty;

public class GoogleDistance {

    @JsonProperty(value = "value")
    private long value;

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }
}
