package com.yeahbunny.service.google;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class GoogleAddressResult {

    @JsonProperty(value = "address_components")
    List<GoogleAddressComponent> addressComponentList;

    @JsonProperty(value = "geometry")
    GoogleAddressGeometry addressGeometry;

    public List<GoogleAddressComponent> getAddressComponentList() {
        return addressComponentList;
    }

    public void setAddressComponentList(List<GoogleAddressComponent> addressComponentList) {
        this.addressComponentList = addressComponentList;
    }

    public GoogleAddressGeometry getAddressGeometry() {
        return addressGeometry;
    }

    public void setAddressGeometry(GoogleAddressGeometry addressGeometry) {
        this.addressGeometry = addressGeometry;
    }
}
