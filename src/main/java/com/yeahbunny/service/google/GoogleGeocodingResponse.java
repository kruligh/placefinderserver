package com.yeahbunny.service.google;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class GoogleGeocodingResponse {

    @JsonProperty(value = "results")
    private List<GoogleAddressResult> addressResultList;

    @JsonProperty(value = "status")
    private String status;

    public List<GoogleAddressResult> getAddressResultList() {
        return addressResultList;
    }

    public void setAddressResultList(List<GoogleAddressResult> addressResultList) {
        this.addressResultList = addressResultList;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
