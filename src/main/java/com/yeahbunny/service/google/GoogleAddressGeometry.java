package com.yeahbunny.service.google;


import com.fasterxml.jackson.annotation.JsonProperty;

public class GoogleAddressGeometry {

    @JsonProperty(value = "location")
    GoogleAddressLocation googleAddressLocation;

    public GoogleAddressLocation getGoogleAddressLocation() {
        return googleAddressLocation;
    }

    public void setGoogleAddressLocation(GoogleAddressLocation googleAddressLocation) {
        this.googleAddressLocation = googleAddressLocation;
    }
}
