package com.yeahbunny.service.google;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class GoogleDistanceRow {

    @JsonProperty(value = "elements")
    private List<GoogleDistanceElement> elements;

    public List<GoogleDistanceElement> getElements() {
        return elements;
    }

    public void setElements(List<GoogleDistanceElement> elements) {
        this.elements = elements;
    }
}
