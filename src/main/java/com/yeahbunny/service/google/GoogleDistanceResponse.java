package com.yeahbunny.service.google;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class GoogleDistanceResponse {

    @JsonProperty(value = "rows")
    private List<GoogleDistanceRow> rows;

    @JsonProperty(value = "status")
    private String status;

    public List<GoogleDistanceRow> getRows() {
        return rows;
    }

    public void setRows(List<GoogleDistanceRow> rows) {
        this.rows = rows;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
