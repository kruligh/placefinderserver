package com.yeahbunny.service.google;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GoogleDistanceElement {

    @JsonProperty(value = "distance")
    private GoogleDistance distance;

    @JsonProperty(value = "status")
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public GoogleDistance getDistance() {
        return distance;
    }

    public void setDistance(GoogleDistance distance) {
        this.distance = distance;
    }
}
