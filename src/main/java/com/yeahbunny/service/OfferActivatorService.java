package com.yeahbunny.service;

import com.yeahbunny.dao.OfferHistoryRepository;
import com.yeahbunny.dao.OfferRepository;
import com.yeahbunny.domain.AdminEntity;
import com.yeahbunny.domain.OfferHistoryEntity;
import com.yeahbunny.domain.OfferEntity;
import com.yeahbunny.domain.type.OfferActionType;
import com.yeahbunny.ws.dto.request.RejectOfferRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.time.ZonedDateTime;

@Service
public class OfferActivatorService {

    private static final Logger LOG = LoggerFactory.getLogger(OfferActivatorService.class);

    @Inject
    private NotifyUserService notifyUserService;

    @Inject
    private OfferRepository offerRepository;

    @Inject
    private OfferHistoryRepository activationHistoryRepository;

    @Transactional
    public void deactivateByRejectOffer(OfferEntity offerEntity, RejectOfferRequest rejectOfferRequest, AdminEntity adminEntity) {
        LOG.info("Offer {} deactivating", offerEntity.getId());
        offerEntity.setActive(false);
        offerEntity.setConsumedByAdmin(true);
        this.offerRepository.save(offerEntity);

        OfferHistoryEntity historyEntity = new OfferHistoryEntity();
        historyEntity.setAction(OfferActionType.UNACCEPTED_BY_ADMIN);
        historyEntity.setOffer(offerEntity);
        historyEntity.setEventDate(ZonedDateTime.now());
        historyEntity.setAdmin(adminEntity);
        historyEntity.setReason(rejectOfferRequest.getReason());

        this.activationHistoryRepository.save(historyEntity);

        this.notifyUserService.notifyOfferRejectedByAdmin(historyEntity);

    }

    public void acceptOffer(OfferEntity offerEntity, AdminEntity adminEntity) {
        offerEntity.setConsumedByAdmin(true);
        this.offerRepository.save(offerEntity);

        OfferHistoryEntity historyEntity = new OfferHistoryEntity();
        historyEntity.setOffer(offerEntity);
        historyEntity.setAction(OfferActionType.ACCEPTED_BY_ADMIN);
        historyEntity.setEventDate(ZonedDateTime.now());
        historyEntity.setAdmin(adminEntity);

        this.activationHistoryRepository.save(historyEntity);
    }

    public void deactivateByTime(OfferEntity offerEntity) {
        offerEntity.setActive(false);
        this.offerRepository.save(offerEntity);

        OfferHistoryEntity historyEntity = new OfferHistoryEntity();
        historyEntity.setOffer(offerEntity);
        historyEntity.setAction(OfferActionType.OUT_OF_DATE);
        historyEntity.setEventDate(ZonedDateTime.now());

        this.activationHistoryRepository.save(historyEntity);

        this.notifyUserService.notifyOfferDeativated(offerEntity);
    }
}
