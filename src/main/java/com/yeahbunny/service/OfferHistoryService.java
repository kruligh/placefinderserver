package com.yeahbunny.service;

import com.yeahbunny.dao.OfferHistoryRepository;
import com.yeahbunny.domain.OfferEntity;
import com.yeahbunny.domain.OfferHistoryEntity;
import com.yeahbunny.domain.type.OfferActionType;
import com.yeahbunny.ws.dto.response.MyOfferStatus;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

@Service
public class OfferHistoryService {

    @Inject
    OfferHistoryRepository offerHistoryRepository;

    public MyOfferStatus getOfferStatus(OfferEntity source) {

        if(source.isActive()){
            return MyOfferStatus.ACTIVE;
        }else{
            List<OfferHistoryEntity> historyEntities = offerHistoryRepository.findAllByOfferIdAndActions(source.getId(), OfferActionType.BANNED_BY_REPORT, OfferActionType.UNACCEPTED_BY_ADMIN);
            if(historyEntities.isEmpty()){
                return MyOfferStatus.INACTIVE;
            }else{
                return MyOfferStatus.BANNED;
            }
        }
    }
}
