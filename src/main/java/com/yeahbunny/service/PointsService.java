package com.yeahbunny.service;

import com.yeahbunny.dao.PointsAccountRepository;
import com.yeahbunny.domain.*;
import com.yeahbunny.exception.CantAffordException;
import com.yeahbunny.ws.util.PersonPrincipalProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service
public class PointsService {

    private static final Logger LOG = LoggerFactory.getLogger(PointsService.class);

    @Value("${app.points.start}")
    private int activationEmailPoints;

    @Inject
    NotifyUserService notifyUserService;

    @Inject
    private PointsAccountRepository pointsAccountRepository;

    @Inject
    private PersonPrincipalProvider principalProvider;

    public void executeSuccessPayment(PointsPacketPaymentEntity pointsPacketPaymentEntity) {

        pointsPacketPaymentEntity.setConsumed(true);

        PersonEntity personEntity = pointsPacketPaymentEntity.getPerson();

        PointsPacketEntity packetEntity = pointsPacketPaymentEntity.getPointsPacket();

       addPoints(personEntity,packetEntity.getPointsCount());
        int actualPoints = pointsAccountRepository.findByPerson(personEntity).getAmount();
        int addedPoints = packetEntity.getPointsCount();
        notifyUserService.notifyAddedPoints(pointsPacketPaymentEntity.getPerson(),addedPoints,actualPoints);
    }

    public void executePointsPayment(PersonEntity pe, int offerPrice) {
        addPoints(pe,-offerPrice);
    }

    public void addActivationEmailPoints(PersonEntity personEntity) {
        addPoints(personEntity,activationEmailPoints);
    }

    public void adminAddPoints(PersonEntity personEntity, int pointsCount) {
       addPoints(personEntity,pointsCount);

    }

    private void addPoints(PersonEntity personEntity, int count) {
        PointsAccountEntity accountEntity = this.pointsAccountRepository.findByPerson(personEntity);
        accountEntity.addPoints(count);
        if(accountEntity.getAmount()<0){
            LOG.error("Cant afford");
            throw new CantAffordException();
        }
    }
}
