package com.yeahbunny.ws;

import com.yeahbunny.dao.AdminRepository;
import com.yeahbunny.domain.AdminEntity;
import com.yeahbunny.ws.dto.generator.AdminDtoGenerator;
import com.yeahbunny.ws.dto.response.AdminResponseDto;
import com.yeahbunny.ws.util.AdminPrincipalProvider;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping(value = "/admin/account")
public class AdminAccountController {

    @Inject
    AdminPrincipalProvider principalProvider;

    @Inject
    AdminRepository adminRepository;

    @Inject
    AdminDtoGenerator adminDtoGenerator;

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    @ResponseBody
    public AdminResponseDto getMyAdminAccount(){
    AdminEntity adminEntity = principalProvider.getEntity();
    //todo statystyki
    return this.adminDtoGenerator.generate(adminEntity);
    }
}
