package com.yeahbunny.ws;

import com.yeahbunny.dao.ReportedOfferRepository;
import com.yeahbunny.domain.*;
import com.yeahbunny.service.ReportExecutorService;
import com.yeahbunny.ws.dto.generator.ReportDtoGenerator;
import com.yeahbunny.ws.dto.request.*;
import com.yeahbunny.ws.dto.response.ReportResponseDto;
import com.yeahbunny.ws.util.AdminPrincipalProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/admin")
public class AdminReportController {

    private static final Logger LOG = LoggerFactory.getLogger(AdminReportController.class);

    @Inject
    private AdminPrincipalProvider adminPrincipalProvider;

    @Inject
    private ReportExecutorService reportExecutorService;

    @Inject
    private ReportedOfferRepository reportedOfferRepository;

    @Inject
    private ReportDtoGenerator reportDtoGenerator;

    @RequestMapping(value = "/getReports", method = RequestMethod.GET)
    @ResponseBody
    private List<ReportResponseDto> getReports(){
        List<ReportedOfferEntity> reportList = this.reportedOfferRepository.findAllByConsumedFalseOrderByReportDate();

        return  reportList.stream().map(reportedOfferEntity -> {
            return reportDtoGenerator.generate(reportedOfferEntity);
        }).collect(Collectors.toList());
    }

    @RequestMapping(value = "/ignoreReport", method = RequestMethod.POST)
    @ResponseBody
    private ResponseEntity<String> ignoreReport(@RequestBody @Valid IgnoreReportRequest ignoreReportRequest){
        ReportedOfferEntity reportedOfferEntity = this.reportedOfferRepository.findOne(ignoreReportRequest.getReportId());
        if(reportedOfferEntity == null){
            return ResponseEntity.status(404).body("REPORT_DOESNT_EXISTS");
        }

        if(reportedOfferEntity.getConsumed() == true){
            return ResponseEntity.status(410).body("ALREADY_CONSUMED");
        }
        LOG.info("Report {} ignoring", reportedOfferEntity.getId());

        AdminEntity adminEntity = this.adminPrincipalProvider.getEntity();

        this.reportExecutorService.ignoreReport(reportedOfferEntity,adminEntity);

        return ResponseEntity.ok("OK");
    }

    @RequestMapping(value = "/banOfferByReport", method = RequestMethod.POST)
    @ResponseBody
    private ResponseEntity<String> banOfferByReportReport(@RequestBody @Valid BanOfferByReportRequest banRequest){
        ReportedOfferEntity reportedOfferEntity = this.reportedOfferRepository.findOne(banRequest.getReportId());
        if(reportedOfferEntity == null){
            return ResponseEntity.status(404).body("REPORT_DOESNT_EXISTS");
        }

        if(reportedOfferEntity.getConsumed() == true){
            return ResponseEntity.status(410).body("ALREADY_CONSUMED");
        }

        LOG.info("Report {} executing cause: {}", reportedOfferEntity.getId(), banRequest.getBanReason());

        AdminEntity adminEntity = this.adminPrincipalProvider.getEntity();

        this.reportExecutorService.executeReport(reportedOfferEntity,adminEntity, banRequest.getBanReason());

        return ResponseEntity.ok("OK");
    }
}
