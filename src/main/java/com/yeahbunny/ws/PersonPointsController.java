package com.yeahbunny.ws;

import com.yeahbunny.dao.PointsPacketRepository;
import com.yeahbunny.ws.dto.generator.PointsPacketDtoGenerator;
import com.yeahbunny.ws.dto.response.PointsPacketResponseDto;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/person/points")
public class PersonPointsController {

    @Inject
    private PointsPacketRepository pointsPacketRepository;

    @Inject
    private PointsPacketDtoGenerator pointsPacketDtoGenerator;

    @RequestMapping(value = "/packets", method = RequestMethod.GET)
    public List<PointsPacketResponseDto> getPackets(){

        List<PointsPacketResponseDto> result = new ArrayList<>();
        this.pointsPacketRepository.findAll().forEach(item->{
            result.add(pointsPacketDtoGenerator.generate(item));
        });

        return result;
    }
}
