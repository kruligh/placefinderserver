package com.yeahbunny.ws;

import com.yeahbunny.dao.PointsPacketRepository;
import com.yeahbunny.domain.PointsPacketEntity;
import com.yeahbunny.domain.PriceListEntity;
import com.yeahbunny.exception.DoesNotExistException;
import com.yeahbunny.ws.dto.generator.PointsPacketDtoGenerator;
import com.yeahbunny.ws.dto.request.EditPacketRequest;
import com.yeahbunny.ws.dto.request.EditPriceListRequest;
import com.yeahbunny.ws.dto.response.PointsPacketResponseDto;
import com.yeahbunny.ws.dto.response.PriceListItemResponseDto;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/admin/packets_list")
public class AdminPacketsController {

    @Inject
    private PointsPacketRepository packetRepository;

    @Inject
    private PointsPacketDtoGenerator pointsPacketDtoGenerator;

    @RequestMapping(value="/get", method = RequestMethod.GET)
    @ResponseBody
    public List<PointsPacketResponseDto> getPackets(){
        List<PointsPacketResponseDto> result = new ArrayList<>();
        this.packetRepository.findAll().forEach(item->{
            result.add(pointsPacketDtoGenerator.generate(item));
        });

        return result;
    }

    @Transactional
    @RequestMapping(value="/edit", method = RequestMethod.POST)
    @ResponseBody
    public List<PointsPacketResponseDto> editPacket(@RequestBody @Valid EditPacketRequest editPacketRequest){

        PointsPacketEntity packetEntity = this.packetRepository.findOne(editPacketRequest.getId());

        if(packetEntity== null){
            throw new DoesNotExistException();
        }

        packetEntity.setPointsCount(editPacketRequest.getPointsCount());
        packetEntity.setPricePln(editPacketRequest.getPricePln());
        packetEntity.setName(editPacketRequest.getName());

        List<PointsPacketResponseDto> result = new ArrayList<>();
        this.packetRepository.findAll().forEach(item->{
            result.add(pointsPacketDtoGenerator.generate(item));
        });

        return result;
    }

}
