package com.yeahbunny.ws;

import com.yeahbunny.dao.FollowOfferRepository;
import com.yeahbunny.dao.OfferRepository;
import com.yeahbunny.dao.PersonRepository;
import com.yeahbunny.domain.ContactEntity;
import com.yeahbunny.domain.FollowOfferEntity;
import com.yeahbunny.domain.OfferEntity;
import com.yeahbunny.domain.PersonEntity;
import com.yeahbunny.ws.dto.generator.MyUserDtoGenerator;
import com.yeahbunny.ws.dto.generator.OfferDtoGenerator;
import com.yeahbunny.ws.dto.request.ChangeMyPasswordRequest;
import com.yeahbunny.ws.dto.request.EditMyContactRequest;
import com.yeahbunny.ws.dto.request.FollowOfferRequest;
import com.yeahbunny.ws.dto.response.MyUserResponseDto;
import com.yeahbunny.ws.dto.response.ContactResponseDto;
import com.yeahbunny.ws.dto.generator.DtoReflectionGenerator;
import com.yeahbunny.ws.dto.response.OfferResponseDto;
import com.yeahbunny.ws.util.PersonPrincipalProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/person")
public class PersonController {
    private static final Logger LOG = LoggerFactory.getLogger(PersonController.class);
    @Inject
    PersonPrincipalProvider personPrincipalProvider;

    @Inject
    PersonRepository personRepository;

    @Inject
    private MyUserDtoGenerator myUserDtoGenerator;

    @Inject
    private FollowOfferRepository followOfferRepository;

    @Inject
    private OfferRepository offerRepository;

    @Inject
    private OfferDtoGenerator offerDtoGenerator;

    @RequestMapping(value = "/getMyUser", method = RequestMethod.GET)
    @ResponseBody
    public MyUserResponseDto getMyUser(){
        PersonEntity personEntity = personPrincipalProvider.getEntity();

        return this.myUserDtoGenerator.generate(personEntity);
    }

    @RequestMapping(value = "/getMyContact", method = RequestMethod.GET)
    @ResponseBody
    public ContactResponseDto getMyContact(){
        ContactEntity contactEntity = this.personPrincipalProvider.getEntity().getContact();
        return new DtoReflectionGenerator<ContactResponseDto>().generate(ContactResponseDto.class,contactEntity);
    }

    @Transactional
    @RequestMapping(value = "/editMyContact", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> editMyContact(@RequestBody @Valid EditMyContactRequest editMyContactRequest){
        ContactEntity contactEntity = this.personPrincipalProvider.getEntity().getContact();
        contactEntity.setEmail(editMyContactRequest.getContactEmail());
        contactEntity.setName(editMyContactRequest.getContactName());
        contactEntity.setPhoneNumber(editMyContactRequest.getContactPhoneNumber());
        return ResponseEntity.ok("OK");
    }

    @RequestMapping(value = "/changePassword", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> changePassword(@RequestBody @Valid ChangeMyPasswordRequest changeMyPasswordRequest){
        PersonEntity personEntity = this.personPrincipalProvider.getEntity();
        LOG.info("User {} changing password", personEntity.getEmail());
        if(!personEntity.getPassword().equals(changeMyPasswordRequest.getOldPassword())){
            LOG.info("User {} send invalid old password", personEntity.getEmail());
            return ResponseEntity.status(403).body("OLD_PASSWORD_INVALID");
        }

        personEntity.setPassword(changeMyPasswordRequest.getNewPassword());
        personRepository.save(personEntity);
        return ResponseEntity.ok("");
    }

    @Transactional
    @RequestMapping(value = "/followOffer", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> followOffer(@RequestBody @Valid FollowOfferRequest followOfferRequest){
        LOG.info("Follow offer {}", followOfferRequest.getOfferId());
        PersonEntity personEntity = this.personPrincipalProvider.getEntity();
        OfferEntity offerEntity = this.offerRepository.findOne(followOfferRequest.getOfferId());

        if(offerEntity==null){
            return ResponseEntity.status(404).body("Offer does not exists");
        }

        if(this.followOfferRepository.findByPersonAndOffer(personEntity,offerEntity)!=null){
            return ResponseEntity.ok("Already followed");
        }

        FollowOfferEntity followOfferEntity = new FollowOfferEntity();
        followOfferEntity.setPerson(personEntity);
        followOfferEntity.setOffer(offerEntity);

        this.followOfferRepository.save(followOfferEntity);

        return ResponseEntity.ok(followOfferEntity.getId() + "");
    }

    @Transactional
    @RequestMapping(value = "/unfollowOffer", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> unfollowOffer(@RequestBody @Valid FollowOfferRequest followOfferRequest){
        LOG.info("Unfollow offer {}", followOfferRequest.getOfferId());
        OfferEntity offerEntity = this.offerRepository.findOne(followOfferRequest.getOfferId());

        if(offerEntity==null){
            LOG.error("Offer does not exist");
            return ResponseEntity.status(404).body("Offer does not exists");
        }

        FollowOfferEntity followOfferEntity = this.followOfferRepository.findByPersonAndOffer(personPrincipalProvider.getEntity(), offerEntity);

        if(followOfferEntity==null){
            LOG.error("Follow entity does not exist");
            return ResponseEntity.status(404).body("Subscription does not exists");
        }

        this.followOfferRepository.delete(followOfferEntity);

        return ResponseEntity.ok("ok");
    }

    @RequestMapping(value = "/getFollowedOffer", method = RequestMethod.GET)
    @ResponseBody
    public List<OfferResponseDto> getFollowedOffer(){

        List<FollowOfferEntity> followedOffers = this.followOfferRepository.findAllByPerson(personPrincipalProvider.getEntity());

        List<OfferResponseDto> result = new ArrayList<>();

        followedOffers.forEach(followOfferEntity -> {
            result.add(this.offerDtoGenerator.generate(followOfferEntity.getOffer()));
        });

        return result;
    }


}
