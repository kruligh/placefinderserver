package com.yeahbunny.ws;

import com.yeahbunny.dao.OfferRepository;
import com.yeahbunny.dao.ReportedOfferRepository;
import com.yeahbunny.domain.OfferEntity;
import com.yeahbunny.domain.ReportedOfferEntity;
import com.yeahbunny.exception.BadArgumentsException;
import com.yeahbunny.service.NotifyUserService;
import com.yeahbunny.service.OfferHistoryService;
import com.yeahbunny.ws.dto.generator.OfferDtoGenerator;
import com.yeahbunny.ws.dto.request.ReportOfferRequest;
import com.yeahbunny.ws.dto.request.SendMessageRequest;
import com.yeahbunny.ws.dto.response.MyOfferStatus;
import com.yeahbunny.ws.dto.response.OfferResponseDto;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;

import javax.inject.Inject;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;

@RestController
@RequestMapping(value = "/offer")
public class OfferController {
    private static final Logger LOG = LoggerFactory.getLogger(OfferController.class);

    @Inject
    OfferRepository offerRepository;

    @Inject
    ReportedOfferRepository reportedOfferRepository;

    @Inject
    OfferDtoGenerator offerDtoGenerator;

    @Inject
    OfferHistoryService historyService;

    @Inject
    private NotifyUserService notifyUserService;

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public @ResponseBody
    OfferResponseDto getOffer(@RequestParam(required = true) String id, @RequestParam(required = false) String v,HttpServletResponse response) throws IOException{
        OfferEntity entity = getByIdOrAlias(id);

        if(entity == null){
            throw new BadArgumentsException("Offer does not exists");
        }

        MyOfferStatus offerStatus = historyService.getOfferStatus(entity);
        if(offerStatus == MyOfferStatus.BANNED){
           response.getWriter().write("0");
           response.setStatus(410);
           return null;
        }else if(offerStatus == MyOfferStatus.INACTIVE){
           response.getWriter().write("1");
           response.setStatus(410);
           return null;
        }

        if(v!=null){
            addView(entity);
        }

        OfferResponseDto result = this.offerDtoGenerator.generate(entity);
        return result;
    }

    private void addView(OfferEntity entity) {
        entity.addView();
        offerRepository.save(entity);
    }

    private OfferEntity getByIdOrAlias(String idOrAlias) {
        if(idOrAlias.matches("[0-9]*")){
            return offerRepository.findOne(new Long(idOrAlias));
        }else{
            return offerRepository.findByAlias(idOrAlias);
        }
    }

    @ApiOperation(value = "Send message to offer owner")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 400, message = "Email from is invalid"),
            @ApiResponse(code = 404, message = "Offer does not exist"),
            @ApiResponse(code = 410, message = "Offer is not active"),
            @ApiResponse(code = 500, message = "Internal server error")}
    )
    @RequestMapping(value = "/sendMessage", method = RequestMethod.POST)
    public ResponseEntity<String> sendMessage(@RequestBody @Valid SendMessageRequest sendMessageRequest){
        LOG.info("Sending offer message offer {}, from {}", sendMessageRequest.getOfferId(), sendMessageRequest.getFrom());
       OfferEntity offerEntity =  this.offerRepository.findOne(sendMessageRequest.getOfferId());

        if(offerEntity == null){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("NOT_FOUND");
        }

        if(!offerEntity.isActive()){
            return ResponseEntity.status(HttpStatus.GONE).body("NOT_ACTIVE_OFFER");
        }

        try {
            InternetAddress adress = new InternetAddress(sendMessageRequest.getFrom());
            adress.validate();
        } catch (AddressException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("INVALID_EMAIL");
        }

        notifyUserService.sendOfferMessage(sendMessageRequest, offerEntity.getContact());
        return ResponseEntity.ok("OK");
    }

    @RequestMapping(value = "/report", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<String> reportOffer(@RequestBody @Valid ReportOfferRequest reportOfferRequest){
        LOG.info("Reporting offer {} cause {}",reportOfferRequest.getOfferId(), reportOfferRequest.getCause());

        OfferEntity offerEntity = getByIdOrAlias(reportOfferRequest.getOfferId());

        if(offerEntity == null){
            LOG.info("Offer {} does not exists",reportOfferRequest.getOfferId());
            return ResponseEntity.status(400).body("OFFER_DOESNT_EXISTS");
        }

        ReportedOfferEntity reportedOfferEntity = new ReportedOfferEntity();
        reportedOfferEntity.setCauseDesc(reportOfferRequest.getCauseDesc());
        reportedOfferEntity.setCause(reportOfferRequest.getCause());
        reportedOfferEntity.setOffer(offerEntity);

        reportedOfferRepository.save(reportedOfferEntity);

        return ResponseEntity.ok("OK");
    }


}
