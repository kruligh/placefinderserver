package com.yeahbunny.ws;

import com.yeahbunny.dao.PersonRepository;
import com.yeahbunny.domain.AdminEntity;
import com.yeahbunny.domain.PersonEntity;
import com.yeahbunny.service.NotifyUserService;
import com.yeahbunny.service.PointsService;
import com.yeahbunny.ws.dto.generator.DtoReflectionGenerator;
import com.yeahbunny.ws.dto.generator.MyUserDtoGenerator;
import com.yeahbunny.ws.dto.request.AddPointsRequest;
import com.yeahbunny.ws.dto.response.MyUserResponseDto;
import com.yeahbunny.ws.util.AdminPrincipalProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/admin")
public class AdminUserController {

    private static final Logger LOG = LoggerFactory.getLogger(AdminUserController.class);

    @Inject
    private AdminPrincipalProvider adminPrincipalProvider;

    @Inject
    private PersonRepository personRepository;

    @Inject
    private PointsService pointsService;

    @Inject
    private NotifyUserService notifyUserService;

    @Inject
    private MyUserDtoGenerator myUserDtoGenerator;

    @RequestMapping(value = "/getUsers", method = RequestMethod.GET)
    @ResponseBody
    public List<MyUserResponseDto> getUsers(@RequestParam String name){

        LOG.debug("Search user by email: {}", name );

        if(name == null || name.length()<3){
            return Collections.emptyList();
        }

        List<PersonEntity> personEntityList = personRepository.findByEmailContaining(name);

        return personEntityList.stream().map(item->{
            LOG.info("Found user {}", item.getEmail());
            return myUserDtoGenerator.generate(item);
        }).collect(Collectors.toList());
    }

    @RequestMapping(value = "/getUser", method = RequestMethod.GET)
    @ResponseBody
    public MyUserResponseDto getUser(@RequestParam Long id){

        LOG.debug("Search user by id: {}", id );

        PersonEntity personEntity = personRepository.findOne(id);

        if(personEntity == null){
            return null;
        }else{
            return myUserDtoGenerator.generate(personEntity);
        }
    }

    @Transactional
    @RequestMapping(value = "/addPoints", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> addPoints(@RequestBody @Valid AddPointsRequest addPointsRequest){
        LOG.debug("Adding points");
        AdminEntity adminEntity = this.adminPrincipalProvider.getEntity();

        PersonEntity personEntity = this.personRepository.findOne(addPointsRequest.getUserId());
        this.pointsService.adminAddPoints(personEntity,addPointsRequest.getPointsCount());
        personRepository.save(personEntity);

        LOG.info("Added {} points to user {} by {}",addPointsRequest.getPointsCount(),personEntity.getEmail(),adminEntity.getEmail());

        this.notifyUserService.notifyAddedPointsByAdmin(adminEntity,personEntity,addPointsRequest);

        return ResponseEntity.ok("OK");

    }
}
