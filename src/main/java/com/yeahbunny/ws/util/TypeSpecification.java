package com.yeahbunny.ws.util;

import com.yeahbunny.domain.OfferEntity;
import com.yeahbunny.domain.type.BuildingType;
import com.yeahbunny.domain.type.PropertyType;
import com.yeahbunny.exception.BadArgumentsException;
import com.yeahbunny.exception.BadFilterArgumentException;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

public class TypeSpecification extends FilterSpecification {

    private int[] values;
    private String[] path;

    /**
     *
     * @param multiValues array with acceptable values
     * @param path path to field in OfferEntity example: property.propertyType
     */
    public TypeSpecification(int[] multiValues, String path) {
        this.values = multiValues;
        this.path = path.split("\\.");
    }

    @Override
    public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
        Predicate[] predicateList = new Predicate[values.length];

        Path<?> field = getFields(root,path);

        for(int i = 0; i < values.length; i++) {
            if(field.getJavaType().equals(PropertyType.class)){
                predicateList[i] = criteriaBuilder.equal(field, PropertyType.get(values[i]));
            }else if(field.getJavaType().equals(BuildingType.class)){
                predicateList[i] = criteriaBuilder.equal(field, BuildingType.get(values[i]));
            }else{
                predicateList[i] = criteriaBuilder.equal(field,values[i]);
            }
        }

        return criteriaBuilder.or(predicateList);
    }
}

