package com.yeahbunny.ws.util;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;


public class LatLngSpecification implements Specification {

    AreaBoundary areaBoundary;
    AreaBoundary oldAreaBoundary;

    public LatLngSpecification(AreaBoundary areaBoundary, AreaBoundary oldAreaBoundary) {
        this.areaBoundary = areaBoundary;
        this.oldAreaBoundary = oldAreaBoundary;
    }

    @Override
    public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicateList = new ArrayList<>();


        if(oldAreaBoundary!=null){
            //getNew markers
            predicateList.add(criteriaBuilder.between(root.get("fakeLatitude").as(Double.class),
                    Math.min(areaBoundary.getMin().getFakeLatitude(),oldAreaBoundary.getMin().getFakeLatitude()),
                    Math.max(areaBoundary.getMax().getFakeLatitude(),oldAreaBoundary.getMax().getFakeLatitude())));

            predicateList.add(criteriaBuilder.between(root.get("fakeLongitude").as(Double.class),
                    Math.min(areaBoundary.getMin().getFakeLongitude(),oldAreaBoundary.getMin().getFakeLongitude()),
                    Math.max(areaBoundary.getMax().getFakeLongitude(),oldAreaBoundary.getMax().getFakeLongitude())));

        }else{
            //get markers
            predicateList.add(criteriaBuilder.between(root.get("fakeLatitude").as(Double.class),
                    areaBoundary.getMin().getFakeLatitude(),
                    areaBoundary.getMax().getFakeLatitude()));

            predicateList.add(criteriaBuilder.between(root.get("fakeLongitude").as(Double.class),
                    areaBoundary.getMin().getFakeLongitude(),
                    areaBoundary.getMax().getFakeLongitude()));
        }

        predicateList.add(criteriaBuilder.equal(root.get("active").as(Boolean.class),true));

        return criteriaBuilder.and(predicateList.toArray(new Predicate[predicateList.size()]));
    }
}
