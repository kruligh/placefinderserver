package com.yeahbunny.ws.util;

import com.yeahbunny.domain.OfferEntity;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kroli on 12.03.2017.
 */
public class OldLatLngSpecification implements Specification<OfferEntity> {
    AreaBoundary oldAreaBoundary;

    public OldLatLngSpecification(AreaBoundary oldAreaBoundary) {
        this.oldAreaBoundary = oldAreaBoundary;
    }
    @Override
    public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {

            Predicate p1 = criteriaBuilder.between(root.get("fakeLatitude").as(Double.class),
                    oldAreaBoundary.getMin().getFakeLatitude(),
                    oldAreaBoundary.getMax().getFakeLatitude()).not();

            Predicate p2 = criteriaBuilder.between(root.get("fakeLongitude").as(Double.class),
                    oldAreaBoundary.getMin().getFakeLongitude(),
                    oldAreaBoundary.getMax().getFakeLongitude()).not();

        return criteriaBuilder.or(p1,p2);
    }
}
