package com.yeahbunny.ws.util;


import com.yeahbunny.exception.BadFilterArgumentException;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

public abstract class FilterSpecification implements Specification {
    protected Path<?> getFields(Root root, String[] path) {
        Path<?> field = root;
        for(int i =0;i<path.length;i++){
            field = field.get(path[i]);
            if(field == null){
                throw new BadFilterArgumentException(path[i] + "does not exists in " + root.getJavaType().getName());
            }
        }
        return field;
    }
}
