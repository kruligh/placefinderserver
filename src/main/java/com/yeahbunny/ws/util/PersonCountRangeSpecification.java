package com.yeahbunny.ws.util;

import javax.persistence.criteria.*;

public class PersonCountRangeSpecification extends FilterSpecification{

    private int[] values;
    private String[] pathMin;
    private String[] pathMax;

    public PersonCountRangeSpecification(int[] multiValues, String pathMin, String pathMax) {
        this.values = multiValues;
        this.pathMin = pathMin.split("\\.");
        this.pathMax = pathMax.split("\\.");
    }

    @Override
    public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
        Path<?> fieldMin = getFields(root, pathMin);
        Path<?> fieldMax = getFields(root, pathMax);

        Predicate p1 = criteriaBuilder.greaterThanOrEqualTo(fieldMax.as(Integer.class),values[0]);

        if(values.length == 1){
            //min criteria -> bigger than val[0]
            return p1;
        }

        Predicate p2 = criteriaBuilder.lessThanOrEqualTo(fieldMin.as(Integer.class),values[1]);

        if(values[0]==0 ){
            //max criteria -> lower than val[1]
            // bo to co robic where min person count > 0
            return p2;
        }else{
            // min - max criteria
            return criteriaBuilder.and(p1,p2);
        }
    }


}
