package com.yeahbunny.ws.util;

import com.yeahbunny.ws.dto.request.LastAddedType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;


public class LastAddedSpecification extends FilterSpecification {

    private LastAddedType lastAddedType;
    private String[] path;

    private static final Logger LOG = LoggerFactory.getLogger(LastAddedSpecification.class);

    public LastAddedSpecification(LastAddedType lastAddedType, String path) {
        this.lastAddedType = lastAddedType;
        this.path = path.split("\\.");
    }

    @Override
    public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
        Path<?> field = getFields(root,path);

        switch(lastAddedType){
            case DAY:
                return criteriaBuilder.greaterThanOrEqualTo(field.as(ZonedDateTime.class),ZonedDateTime.now().minus(1, ChronoUnit.DAYS));
            case THREE_DAYS:
                return criteriaBuilder.greaterThanOrEqualTo(field.as(ZonedDateTime.class),ZonedDateTime.now().minus(3, ChronoUnit.DAYS));
            case WEEK:
                return criteriaBuilder.greaterThanOrEqualTo(field.as(ZonedDateTime.class),ZonedDateTime.now().minus(7, ChronoUnit.DAYS));
            default:
                LOG.error(lastAddedType + " option does not exist");
                return criteriaBuilder.conjunction();
        }
    }
}
