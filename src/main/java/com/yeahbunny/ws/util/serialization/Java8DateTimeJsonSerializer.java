package com.yeahbunny.ws.util.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.GregorianCalendar;

public class Java8DateTimeJsonSerializer extends JsonSerializer<ZonedDateTime> {



    @Override
    public void serialize(ZonedDateTime zonedDateTime, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {

        jsonGenerator.writeNumber(convertDate(zonedDateTime));
    }

    public long convertDate(ZonedDateTime zonedDateTime){
        if (zonedDateTime == null) {
            return 0;
        }

        GregorianCalendar date = GregorianCalendar.from(zonedDateTime);
        return date.getTimeInMillis()/1000;
    }
}
