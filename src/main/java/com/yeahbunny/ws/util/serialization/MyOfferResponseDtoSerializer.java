package com.yeahbunny.ws.util.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.yeahbunny.ws.dto.response.MyOfferResponseDto;
import com.yeahbunny.ws.dto.response.OfferResponseDto;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/***
 * Adding field status to json from OfferResponseDto
 */
public class MyOfferResponseDtoSerializer extends JsonSerializer<MyOfferResponseDto> {

    @Override
    public void serialize(MyOfferResponseDto value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        Java8DateTimeJsonSerializer dateTimeJsonSerializer = new Java8DateTimeJsonSerializer();
        ObjectMapper mapper = new ObjectMapper();
        String temp = mapper.writeValueAsString(value.getOffer());
        temp = temp.substring(0,temp.lastIndexOf("}")) +
                ", \"status\":" + value.getMyOfferStatus().ordinal() +
                ", \"actualTo\":" + dateTimeJsonSerializer.convertDate(value.getActualTo()) +
                " }";
        jsonGenerator.writeRawValue(temp);


    }






}
