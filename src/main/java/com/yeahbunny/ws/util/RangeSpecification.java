package com.yeahbunny.ws.util;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

public class RangeSpecification extends FilterSpecification {

    private int[] values;
    private String[] path;

    public RangeSpecification(int[] rangeValues, String path) {
        this.values = rangeValues;
        this.path= path.split("\\.");
    }

    @Override
    public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
        Path<?> field = getFields(root, path);

        if(values.length == 1){
            return criteriaBuilder.greaterThanOrEqualTo(field.as(Integer.class),values[0]);
        }else if ( values[0] == 0){
            return criteriaBuilder.lessThanOrEqualTo(field.as(Integer.class),values[1]);
        }else{
            return criteriaBuilder.between(field.as(Integer.class), values[0], values[1]);
        }
    }
}
