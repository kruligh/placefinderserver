package com.yeahbunny.ws.util;

import com.yeahbunny.exception.BadFilterArgumentException;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

public class BooleanSpecification extends FilterSpecification{

    private boolean value;
    private String[] path;


    public BooleanSpecification(boolean b, String s) {
        this.value = b;
        this.path = s.split("\\.");
    }

    @Override
    public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {

        Path<?> field = getFields(root,path);

        return criteriaBuilder.equal(field.as(Boolean.class),value);
    }
}
