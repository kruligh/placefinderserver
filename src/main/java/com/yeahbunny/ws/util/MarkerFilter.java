package com.yeahbunny.ws.util;

import com.yeahbunny.domain.LazyOfferEntity;
import com.yeahbunny.domain.OfferEntity;
import com.yeahbunny.domain.PriceEntity;
import com.yeahbunny.domain.type.CurrencyType;
import com.yeahbunny.exception.BadArgumentsException;
import com.yeahbunny.exception.BadFilterArgumentException;
import com.yeahbunny.service.CurrencyService;
import com.yeahbunny.ws.dto.LocalizedPoint;
import com.yeahbunny.ws.dto.generator.MarkerDtoGenerator;
import com.yeahbunny.ws.dto.request.LastAddedType;
import com.yeahbunny.ws.dto.response.MarkerResponseDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.persistence.criteria.CriteriaBuilder;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class MarkerFilter {

    @Inject
    private CurrencyService currencyService;

    private static final Logger LOG = LoggerFactory.getLogger(MarkerFilter.class);

    public boolean matchPrice(PriceEntity priceEntity, String entry) {
        try{
            int[] values = new int[3];

            String[] temp = entry.split("-");
            for(int i =0 ;i<temp.length; i++){
                if(temp[i] != null && !(temp[i].trim().isEmpty())){
                    values[i] = Integer.parseInt(temp[i]);
                }
            }

            CurrencyType currencyType = CurrencyType.values()[values[2]];
            double paramsCurrencyMultiplier = this.currencyService.getCurrencyMultiplier(currencyType.name());
            double targetCurrencyMultiplier = this.currencyService.getCurrencyMultiplier(priceEntity.getCurrency().name());



            double minPrice = values[0] * paramsCurrencyMultiplier;
            double maxPrice = values[1] * paramsCurrencyMultiplier;
            double targetPrice  = priceEntity.getAmount() * targetCurrencyMultiplier;

           // LOG.debug("Price bounduary {} {} pln from {} target price {} pln from {}", minPrice,maxPrice,currencyType.name(), targetPrice, priceEntity.getCurrency().name());

            if((maxPrice>0 && targetPrice > maxPrice )|| targetPrice <minPrice){
                return false;
            }else {
                return true;
            }
        }catch(NumberFormatException e){
            throw new BadFilterArgumentException("price",entry,Integer.class);
        }
    }

    public Specification createSpecification(Map<String, String> params) {
        AreaBoundary areaBoundary = createAreaBoundary(params);
        Specification latLngSpec = new LatLngSpecification(areaBoundary,null);
        return specificationByParams(Specifications.where(latLngSpec), params);

    }

    public Specification createSpecificationNewMarkers(Map<String, String> params) {
        AreaBoundary areaBoundary = createAreaBoundary(params);
        AreaBoundary oldAreaBoundary = createOldAreaBoundAry(params);

        Specification latLngSpec = new LatLngSpecification(areaBoundary,oldAreaBoundary);
        Specification oldLatLngSpec  = new OldLatLngSpecification(oldAreaBoundary);

        return specificationByParams(Specifications.where(latLngSpec).and(oldLatLngSpec), params);

    }

    private AreaBoundary createOldAreaBoundAry(Map<String, String> params) {
        try {
            return new AreaBoundary(
                    new MarkerPoint(Double.parseDouble(params.remove("x3")),Double.parseDouble(params.remove("y4"))),
                    new MarkerPoint(Double.parseDouble(params.remove("x4")),Double.parseDouble(params.remove("y3"))));
        }catch (NullPointerException e){
            return null;
        }catch (NumberFormatException e){
            throw new BadArgumentsException("CANNOT CONVERT REQUIRED PARAMETERS TO DOUBLE");
        }
    }

    private AreaBoundary createAreaBoundary(Map<String, String> params) {
        try {
            return new AreaBoundary(
                    new MarkerPoint(Double.parseDouble(params.remove("x1")),Double.parseDouble(params.remove("y2"))),
                    new MarkerPoint(Double.parseDouble(params.remove("x2")),Double.parseDouble(params.remove("y1"))));
        }catch (NullPointerException e){
            throw new BadArgumentsException("NOT FOUND REQUIRED PARAMETERS");
        }catch (NumberFormatException e){
            throw new BadArgumentsException("CANNOT CONVERT REQUIRED PARAMETERS TO DOUBLE");
        }
    }

    private Specification specificationByParams(Specifications query, Map<String, String> params) {

        for(Map.Entry<String, String> entry : params.entrySet()){
            switch (entry.getKey().toLowerCase()){
                case "offertype":{
                    query = query.and(new TypeSpecification(getMultiValues(entry.getValue()),"offerType"));
                    break;
                }
                case "propertytype":{
                    query = query.and(new TypeSpecification(getMultiValues(entry.getValue()),"property.propertyType"));
                    break;
                }
                case "price":{
                    //query = query.and(new PriceSpecification()) moze kiedys
                    break;
                }
                case "isfromagency":{
                    query = query.and(new BooleanSpecification(entry.getValue().charAt(0) == 't', "person.isAgency"));
                    break;
                }
                case "lastadded":{
                    query = query.and(new LastAddedSpecification(getLastAddedType(entry.getValue()), "addDate"));
                    break;
                }
                case "buildingtype":{
                    query = query.and(new TypeSpecification(getMultiValues(entry.getValue()),"property.buildingType"));
                    break;
                }
                case "personcount":{
                    query = query.and(new PersonCountRangeSpecification(getRangeValues(entry.getValue()),"property.minPerson", "property.maxPerson"));
                    break;
                }
                case "heatingtype":{
                    query = query.and(new TypeSpecification(getMultiValues(entry.getValue()),"property.heatingType"));
                    break;
                }
                case "area":{
                    query = query.and(new RangeSpecification(getRangeValues(entry.getValue()),"property.area"));
                    break;
                }
                case "roomcount":{
                    query = query.and(new RangeSpecification(getRangeValues(entry.getValue()),"property.roomCount"));
                    break;
                }
                case "floor":{
                    query = query.and(new RangeSpecification(getRangeValues(entry.getValue()),"property.floor"));
                    break;
                }
                case "year":{
                    query = query.and(new RangeSpecification(getRangeValues(entry.getValue()),"property.builtYear"));
                    break;
                }
                case "furnished":{
                    query = query.and(new BooleanSpecification(entry.getValue().charAt(0) == 't', "property.furnished"));
                    break;
                }
                case "balcony":{
                    query = query.and(new BooleanSpecification(entry.getValue().charAt(0) == 't', "property.balcony"));
                    break;
                }
                case "lift":{
                    query = query.and(new BooleanSpecification(entry.getValue().charAt(0) == 't', "property.lift"));
                    break;
                }
                case "basement":{
                    query = query.and(new BooleanSpecification(entry.getValue().charAt(0) == 't', "property.basement"));
                    break;
                }
                case "parking":{
                    query = query.and(new BooleanSpecification(entry.getValue().charAt(0) == 't', "property.parkingPlace"));
                    break;
                }
                case "pets":{
                    query = query.and(new BooleanSpecification(entry.getValue().charAt(0) == 't', "property.pets"));
                    break;
                }
                case "garden":{
                    query = query.and(new BooleanSpecification(entry.getValue().charAt(0) == 't', "property.garden"));
                    break;
                }
                case "clima":{
                    query = query.and(new BooleanSpecification(entry.getValue().charAt(0) == 't', "property.climatisation"));
                    break;
                }
                case "smoking":{
                    query = query.and(new BooleanSpecification(entry.getValue().charAt(0) == 't', "property.smoking"));
                    break;
                }
                default:{
                    LOG.error("filter {} doesnt exists", entry.getKey());
                    //throw new BadFilterArgumentException(entry.getKey());
                }
            }
        }
        return query;
    }

    private LastAddedType getLastAddedType(String value) {
        return LastAddedType.values()[Integer.parseInt(value)];
    }

    private int[] getMultiValues(String allValues) {
        String[] values = allValues.split("-");
        int[] valu2= new int[values.length];
        for(int i = 0; i < values.length; i++){
            valu2[i] = Integer.parseInt(values[i]);
        }
        return valu2;
    }


    private int[] getRangeValues(String allValues) {

        String[] values = allValues.split("-");
        if(values.length==0 || values.length>2){
            throw new BadFilterArgumentException("Range arguments invalid: " +allValues);
        }
        int[] valu2= new int[values.length];
        for(int i = 0; i < values.length; i++){
            if(values[i]== null || values[i].trim().isEmpty()){
                valu2[i] = 0;
            }else{
                valu2[i] = Integer.parseInt(values[i].trim());
            }
        }
        return valu2;
    }

    public<T extends LocalizedPoint> List<T> circle(List<T> offerEntities, double circleCenterLat, double circleCenterLng, double circleR) {

        return offerEntities.stream().filter(item ->{
            double distance = distance(circleCenterLat, item.getLatitude(),circleCenterLng,item.getLongitude());
            if(distance <= circleR){
               // LOG.debug("TAK offer {} lat {} lng {} circle lat {} lng {} r {} DIST {} ", item.getId(),item.getFakeLatitude(), item.getFakeLongitude(),circleCenterLat,circleCenterLng,circleR,distance);
                return true;
            }else{
               // LOG.debug("NOPE offer {} lat {} lng {} circle lat {} lng {} r {} DIST {}", item.getId(),item.getFakeLatitude(), item.getFakeLongitude(),circleCenterLat,circleCenterLng,circleR,distance);
                return false;
            }
        }).collect(Collectors.toList());
    }

    /**
     * Calculate distance between two points in latitude
     * @returns Distance in Meters
     */
    public static double distance(double lat1, double lat2, double lon1, double lon2) {

        final int R = 6371; // Radius of the earth

        Double latDistance = Math.toRadians(lat2 - lat1);
        Double lonDistance = Math.toRadians(lon2 - lon1);
        Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return R * c * 1000; // convert to meters
    }

}
