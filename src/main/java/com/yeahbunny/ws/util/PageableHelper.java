package com.yeahbunny.ws.util;

import com.yeahbunny.domain.AbstractEntity;
import com.yeahbunny.domain.OfferEntity;
import com.yeahbunny.exception.BadArgumentsException;
import com.yeahbunny.ws.dto.generator.DtoGenerator;
import com.yeahbunny.ws.dto.generator.MyOfferDtoGenerator;
import com.yeahbunny.ws.dto.response.PageResponseDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public class PageableHelper {

    private static final Logger LOG = LoggerFactory.getLogger(PageableHelper.class);

    public static Pageable generatePageable(Integer page, Integer limit, String sortByString, String sortByValue){

        if(page == null || page < 0){
            page = 0;
        }

        if(limit == null || limit <1){
            limit = 20;
        }

        int sortBy = 0;

        Pageable pageRequest;
        Sort promotionSort = new Sort(Sort.Direction.DESC,"promotionList");
        if(sortByString != null && sortByValue !=null){

            sortBy = Integer.parseInt(sortByString);

            Sort.Direction direction;
            if(sortBy == 1){
                direction = Sort.Direction.ASC;
            }else{
                direction = Sort.Direction.DESC;
            }

            String orderProperties;
            switch(sortByValue){
                case "popularity":
                    orderProperties = "views";
                    break;
                case "price":
                    orderProperties = "price.amount";
                    break;
                case "addDate":
                    orderProperties = "addDate";
                    break;
                default:
                    throw new BadArgumentsException("cannot resolve sort by " + sortByValue);
            }
            LOG.info("Get List, page {} sorted by {} {}" , page,orderProperties,direction.name());
            Sort sort = new Sort(direction,orderProperties);
            pageRequest= new PageRequest(page, limit,promotionSort.and(sort));

        }else{
            LOG.info("Get List, page {}" , page);
            pageRequest= new PageRequest(page, limit,promotionSort);
        }
        return pageRequest;
    }

    public static Pageable generatePageableMyOffers(Integer page, Integer limit, String sortBy, String sortByValue) {
        Pageable pageRequest = generatePageable(page,limit,sortBy,sortByValue);

        Sort activeSort = new Sort(Sort.Direction.DESC,"active");

        return new PageRequest(page,limit,pageRequest.getSort().and(activeSort));
    }

    public static<T extends AbstractEntity> PageResponseDto generateResponseDto(DtoGenerator dtoGenerator, Page<T> offerEntityPage, String url, int page, int limit, String orderBy, String orderByValue) {
        PageResponseDto resultPage = new PageResponseDto();

        offerEntityPage.forEach((offerEntity -> {
            resultPage.add(dtoGenerator.generate(offerEntity));
        }));

        if(offerEntityPage.isFirst()){
            resultPage.setPrev(null);
        }else{
            resultPage.setPrev(generateMyOfferLink(url, page-1,limit, orderBy, orderByValue));
        }

        if(offerEntityPage.isLast()){
            resultPage.setNext(null);
        }else{
            resultPage.setNext(generateMyOfferLink(url, page+1,limit, orderBy, orderByValue));
        }

        resultPage.setPages(offerEntityPage.getTotalPages());
        resultPage.setCount(offerEntityPage.getTotalElements());

        return resultPage;
    }

    private static String generateMyOfferLink(String url, int i, int limit, String orderBy, String orderByValue) {
        if(orderBy == null){
            return url +"?page="+i+"&limit="+limit;
        }else{
            return url +"?page="+i+"&limit="+limit + "&orderBy=" + orderBy + "&orderByValue=" +orderByValue;
        }

    }


}
