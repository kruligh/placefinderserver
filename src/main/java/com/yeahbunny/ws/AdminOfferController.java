package com.yeahbunny.ws;

import com.yeahbunny.dao.OfferRepository;
import com.yeahbunny.domain.AdminEntity;
import com.yeahbunny.domain.OfferEntity;
import com.yeahbunny.exception.BadArgumentsException;
import com.yeahbunny.service.OfferActivatorService;
import com.yeahbunny.ws.dto.generator.OfferDtoGenerator;
import com.yeahbunny.ws.dto.request.AcceptOfferRequest;
import com.yeahbunny.ws.dto.request.RejectOfferRequest;
import com.yeahbunny.ws.dto.response.OfferResponseDto;
import com.yeahbunny.ws.util.AdminPrincipalProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/admin")
public class AdminOfferController {
    private static final Logger LOG = LoggerFactory.getLogger(AdminOfferController.class);

    @Inject
    private OfferRepository offerRepository;

    @Inject
    private OfferDtoGenerator offerDtoGenerator;

    @Inject
    private AdminPrincipalProvider adminPrincipalProvider;

    @Inject
    private OfferActivatorService offerActivatorService;

    @Transactional
    @RequestMapping(value = "/acceptOffer", method = RequestMethod.POST)
    @ResponseBody
    private ResponseEntity<String> acceptOffer(@RequestBody @Valid AcceptOfferRequest acceptOfferRequest){
        OfferEntity offerEntity = this.offerRepository.findOne(acceptOfferRequest.getOfferId());
        if(offerEntity == null){
            return ResponseEntity.status(404).body("OFFER_DOESNT_EXISTS");
        }

        if(offerEntity.isConsumedByAdmin() == true){
            return ResponseEntity.status(410).body("ALREADY_CONSUMED");
        }

        LOG.info("Offer {} accepting", offerEntity.getId());
        AdminEntity adminEntity = this.adminPrincipalProvider.getEntity();

        this.offerActivatorService.acceptOffer(offerEntity,adminEntity);

        return ResponseEntity.ok("OK");
    }

    @RequestMapping(value = "/rejectOffer", method = RequestMethod.POST)
    @ResponseBody
    private ResponseEntity<String> rejectOffer(@RequestBody @Valid RejectOfferRequest rejectOfferRequest){
        OfferEntity offerEntity = this.offerRepository.findOne(rejectOfferRequest.getOfferId());
        if(offerEntity == null){
            return ResponseEntity.status(404).body("OFFER_DOESNT_EXISTS");
        }

        if(offerEntity.isConsumedByAdmin() == true){
            return ResponseEntity.status(410).body("ALREADY_CONSUMED");
        }

        LOG.info("Offer {} rejecting", offerEntity.getId());
        AdminEntity adminEntity = this.adminPrincipalProvider.getEntity();

        offerActivatorService.deactivateByRejectOffer(offerEntity, rejectOfferRequest, adminEntity);
        return ResponseEntity.ok("OK");

    }

    @RequestMapping(value = "/getNotAcceptedOffers", method = RequestMethod.GET)
    @ResponseBody
    private List<OfferResponseDto> getNotAcceptedOffers(@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer limit){
        if(page == null || page < 0){
            page =0;
        }

        if(limit == null || limit <1 ){
            limit = 100;
        }

        Pageable pageRequest = new PageRequest(page, limit, Sort.Direction.DESC, "addDate");

        List<OfferEntity> offerEntityList = offerRepository.findAllByConsumedByAdminFalseAndActiveTrue(pageRequest);

        return offerEntityList.stream().map(item->{
            return offerDtoGenerator.generate(item);
        }).collect(Collectors.toList());


    }
}
