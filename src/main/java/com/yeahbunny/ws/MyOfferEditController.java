package com.yeahbunny.ws;

import com.yeahbunny.dao.*;
import com.yeahbunny.domain.*;
import com.yeahbunny.exception.NotOwnedByUserException;
import com.yeahbunny.exception.PhotoDoesntExistException;
import com.yeahbunny.service.GeocodingService;
import com.yeahbunny.service.OfferValuationService;
import com.yeahbunny.ws.dto.request.EditOfferRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
public class MyOfferEditController extends AbstractPersonOfferController{
    private static final Logger LOG = LoggerFactory.getLogger(MyOfferEditController.class);

    @Inject
    private DeletedPhotoRepository deletedPhotoRepository;

    @Inject
    private GeocodingService geocodingService;

    @Inject
    private OfferValuationService valuationService;


    @Transactional
    @RequestMapping(value = "/person/offer/edit", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> editOffer(@RequestBody @Valid EditOfferRequest editOfferRequest){
        LOG.debug("Editing offer");
        PersonEntity pe = super.personPrincipalProvider.getEntity();
        OfferEntity offerEntity = this.offerRepository.findOne(editOfferRequest.getId());

        super.isOfferNullOrNotOwnedByThisUser(offerEntity,pe);

        String countryCode = this.geocodingService.checkOfferAndGetCountryCode(
                editOfferRequest.getAddressStreet(),
                editOfferRequest.getAddressCity(),
                editOfferRequest.getAddressCountry(),
                editOfferRequest.getLatitude(),
                editOfferRequest.getLongitude());

        int offerPrice = this.valuationService.valuateEdit(editOfferRequest, countryCode).getSum();
        super.canAfford(pe,offerPrice);

        editOfferEntity(offerEntity,editOfferRequest);
        if(editOfferRequest.getExtendOffer()>0){
            if(offerEntity.getOfferActualToDate().isBefore(ZonedDateTime.now())){
                offerEntity.setOfferActualToDate(ZonedDateTime.now().plus(30, ChronoUnit.DAYS));
                offerEntity.setActive(true);
            }else{
                offerEntity.setOfferActualToDate(offerEntity.getOfferActualToDate().plus(30, ChronoUnit.DAYS));
            }
            super.pointsService.executePointsPayment(pe,offerPrice);
        }

        super.cleanUpTemporaryPhotos(personPrincipalProvider.getEntity().getId());
        return ResponseEntity.ok(offerEntity.getId().toString());
    }

    private OfferEntity editOfferEntity(OfferEntity offerEntity, EditOfferRequest editOfferRequest) {
        super.fillOfferEntity(editOfferRequest,offerEntity);
        super.fillPropertyEntity(editOfferRequest, offerEntity.getProperty());
        super.fillAddressEntity(editOfferRequest,offerEntity.getProperty().getAddress());
        super.fillPriceEntity(editOfferRequest,offerEntity.getPrice());

        if(editOfferRequest.getMainContact()){
            offerEntity.setContact(offerEntity.getPerson().getContact());
        }else{
            fillContactEntity(editOfferRequest,offerEntity.getContact());
        }

        List<PhotoEntity> newPhotoList = generateEditedPhotoList(editOfferRequest, offerEntity);

        this.cleanUpOldPhotos(offerEntity, newPhotoList);

        offerEntity.setPhotos(newPhotoList);

        super.cleanUpTemporaryPhotos(offerEntity.getPerson().getId());
        return offerEntity;
    }

    /**
     *  Jeżeli id zdjęcia jest na minusie, jest to zdjęcie nowo dodane czyli trzeba zabrać je z temporary photos (wartośc id to wartość absolutna), nastepnie usunać z temp
     *      jeżeli id zdjęcia jest większe od 0 to pobieramy photo z repo, sprawdzamy czy nalezy do tej oferty i wrzucamy do wyniku
     */
    private List<PhotoEntity> generateEditedPhotoList(EditOfferRequest editOfferRequest, OfferEntity offerEntity) {
        List<PhotoEntity> resultList = new ArrayList<>();
        long[] photosArray = editOfferRequest.getPhotos();

        for(int i =0; i< photosArray.length;i++){
            if(photosArray[i]<0){
                long tempPhotoId = Math.abs(photosArray[i]);
                TemporaryPhotoEntity temporaryPhotoEntity = this.temporaryPhotoRepository.findOne(tempPhotoId);
                if(temporaryPhotoEntity == null){
                    throw new PhotoDoesntExistException(Long.toString(tempPhotoId));
                }
                if(!temporaryPhotoEntity.getPerson().getId().equals(offerEntity.getPerson().getId())){
                    throw new SecurityException("Temp photo is not own by this user");
                }
                resultList.add(generatePhotoEntity(temporaryPhotoEntity,i,offerEntity));
                this.temporaryPhotoRepository.delete(temporaryPhotoEntity);
            }else{
                PhotoEntity photoEntity = this.photoRepository.findOne(photosArray[i]);
                if(photoEntity == null){
                    throw new PhotoDoesntExistException(Long.toString(photosArray[i]));
                }
                if(!photoEntity.getOffer().getId().equals(offerEntity.getId())){
                    throw new NotOwnedByUserException("Temp photo is not own by this offer");
                }
                photoEntity.setPriority(i);
                resultList.add(photoEntity);
            }
        }
        return resultList;
    }

    private void cleanUpOldPhotos(OfferEntity offerEntity, List<PhotoEntity> newPhotos) {
        offerEntity.getPhotos().removeAll(newPhotos);
        List<PhotoEntity> photosToRemove = offerEntity.getPhotos();
        offerEntity.setPhotos(Collections.emptyList());
        photosToRemove.forEach(this::removePhoto);
    }

    private void removePhoto(PhotoEntity photoEntity) {

        LOG.info("Removing photo {}", photoEntity.getId());

        DeletedPhotoEntity deletedPhotoEntity = this.generateDeletedPhotoEntity(photoEntity);
        this.deletedPhotoRepository.save(deletedPhotoEntity);

        this.photoRepository.delete(photoEntity);
        this.storageService.removePhoto(photoEntity);
    }

    private DeletedPhotoEntity generateDeletedPhotoEntity(PhotoEntity photoEntity) {
        DeletedPhotoEntity deletedPhotoEntity = new DeletedPhotoEntity();
        deletedPhotoEntity.setOffer(photoEntity.getOffer());
        deletedPhotoEntity.setStoreDate(photoEntity.getStoreDate());
        deletedPhotoEntity.setDeleteDate(ZonedDateTime.now());
        deletedPhotoEntity.setThumbUrl(photoEntity.getThumbUrl());
        deletedPhotoEntity.setUrl(photoEntity.getUrl());
        return deletedPhotoEntity;
    }
}
