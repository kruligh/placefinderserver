package com.yeahbunny.ws;


import com.yeahbunny.domain.*;
import com.yeahbunny.exception.BadArgumentsException;
import com.yeahbunny.exception.PhotoDoesntExistException;
import com.yeahbunny.service.GeocodingService;
import com.yeahbunny.service.NotifyUserService;
import com.yeahbunny.service.OfferValuationService;
import com.yeahbunny.service.PhotoConverterService;
import com.yeahbunny.ws.dto.request.AddOfferRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import javax.validation.Valid;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@RestController
public class MyOfferAddController extends AbstractPersonOfferController{
    private static final Logger LOG = LoggerFactory.getLogger(MyOfferAddController.class);

    @Inject
    private OfferValuationService valuationService;

    @Inject
    private GeocodingService geocodingService;

    @Inject
    private NotifyUserService notifyUserService;

    @Transactional
    @RequestMapping(value = "/person/offer/add", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> addOffer(@RequestBody @Valid AddOfferRequest addOfferRequest){
        LOG.debug("Adding new offer");

        PersonEntity pe = personPrincipalProvider.getEntity();

        if(!personPrincipalProvider.getEntity().isActivated()){
            return ResponseEntity.status(400).body("MAIL_NOT_ACTIVATED");
        }

        String countryCode = this.geocodingService.checkOfferAndGetCountryCode(
                addOfferRequest.getAddressStreet(),
                addOfferRequest.getAddressCity(),
                addOfferRequest.getAddressCountry(),
                addOfferRequest.getLatitude(),
                addOfferRequest.getLongitude());

        int offerPrice = this.valuationService.valuateAdd(addOfferRequest, countryCode).getSum();

        super.canAfford(pe,offerPrice);

        OfferEntity offerEntity = generateNewOfferEntity(addOfferRequest, pe);
        offerEntity.setOfferActualToDate(ZonedDateTime.now().plus(30, ChronoUnit.DAYS));
        offerEntity.setActive(true);

        super.offerRepository.save(offerEntity);
        super.pointsService.executePointsPayment(pe,offerPrice);
        super.cleanUpTemporaryPhotos(personPrincipalProvider.getEntity().getId());
        this.notifyUserService.notifyOfferActivated(offerEntity);
        return ResponseEntity.ok(offerEntity.getId().toString());
    }

    private OfferEntity generateNewOfferEntity(AddOfferRequest addOfferRequest, PersonEntity personEntity) {
        OfferEntity offerEntity = new OfferEntity();
        offerEntity.setPerson(personEntity);
        super.fillOfferEntity(addOfferRequest, offerEntity);

        offerEntity.setOfferActualToDate(null);
        offerEntity.setActive(false);

        ContactEntity contactEntity = generateContactEntity(addOfferRequest, personEntity);
        offerEntity.setContact(contactEntity);

        PropertyEntity propertyEntity = generatePropertyEntity(addOfferRequest);
        offerEntity.setProperty(propertyEntity);

        PriceEntity priceEntity = generatePriceEntity(addOfferRequest);
        offerEntity.setPrice(priceEntity);

        offerEntity.setPhotos(generatePhotoList(addOfferRequest.getPhotos(),offerEntity));

        return offerEntity;
    }

    private PropertyEntity generatePropertyEntity(AddOfferRequest addOfferRequest) {
        PropertyEntity propertyEntity = new PropertyEntity();
        propertyEntity.setAddress(generateAddressEntity(addOfferRequest));
        super.fillPropertyEntity(addOfferRequest, propertyEntity);

        return propertyEntity;
    }

    private PriceEntity generatePriceEntity(AddOfferRequest addOfferRequest) {
        PriceEntity priceEntity = new PriceEntity();
        super.fillPriceEntity(addOfferRequest, priceEntity);

        return priceEntity;
    }

    private AddressEntity generateAddressEntity(AddOfferRequest addOfferRequest) {

        AddressEntity addressEntity = new AddressEntity();
        super.fillAddressEntity(addOfferRequest, addressEntity);
        return addressEntity;
    }

    private ContactEntity generateContactEntity(AddOfferRequest addOfferRequest, PersonEntity personEntity){
        if(!addOfferRequest.isMainContact()) {
            return this.generateNewContactEntity(addOfferRequest);
        }else {
            return personEntity.getContact();
        }
    }

    private ContactEntity generateNewContactEntity(AddOfferRequest addOfferRequest) {

        if(addOfferRequest.getContactName() == null || addOfferRequest.getContactName().trim().isEmpty()
                || addOfferRequest.getContactEmail() == null || addOfferRequest.getContactEmail().trim().isEmpty()){
            throw new BadArgumentsException("If isMainContact == false contactName and contactEmail cannot be null and empty");
        }

        ContactEntity contactEntity = new ContactEntity();
        super.fillContactEntity(addOfferRequest,contactEntity);
        return contactEntity;
    }

    private List<PhotoEntity> generatePhotoList(long[] temporaryPhotosId, OfferEntity offerEntity)  {
        Long userId = offerEntity.getPerson().getId();

        List<PhotoEntity> result = new ArrayList<>();

        for(int i = 0; i<temporaryPhotosId.length; i++){
            TemporaryPhotoEntity temp = super.temporaryPhotoRepository.getOne(temporaryPhotosId[i]);
            if(temp == null || !temp.getPerson().getId().equals(userId)){
                throw new SecurityException("Temp photo is not own by this user");
            }
            PhotoEntity photoEntity = super.generatePhotoEntity(temp,i,offerEntity);

            super.temporaryPhotoRepository.delete(temp);
            result.add(photoEntity);
        }
        return result;
    }




}
