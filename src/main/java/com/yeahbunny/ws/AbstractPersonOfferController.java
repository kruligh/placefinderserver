package com.yeahbunny.ws;

import com.yeahbunny.dao.*;
import com.yeahbunny.domain.*;
import com.yeahbunny.domain.type.PriceType;
import com.yeahbunny.exception.CantAffordException;
import com.yeahbunny.exception.DoesNotExistException;
import com.yeahbunny.exception.NotOwnedByUserException;
import com.yeahbunny.service.PointsService;
import com.yeahbunny.service.StorageService;
import com.yeahbunny.ws.dto.request.OfferRequest;
import com.yeahbunny.ws.util.PersonPrincipalProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.time.ZonedDateTime;

abstract class AbstractPersonOfferController {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractPersonOfferController.class);

    @Inject
    protected PersonPrincipalProvider personPrincipalProvider;

    @Inject
    protected OfferRepository offerRepository;

    @Inject
    protected PersonRepository personRepository;

    @Inject
    protected StorageService storageService;

    @Inject
    protected TemporaryPhotoRepository temporaryPhotoRepository;

    @Inject
    protected PhotoRepository photoRepository;

    @Inject
    protected PointsService pointsService;

    @Inject
    private PointsAccountRepository pointsAccountRepository;

    protected PhotoEntity generatePhotoEntity(TemporaryPhotoEntity temp, int i, OfferEntity offerEntity) {
        PhotoEntity photoEntity = new PhotoEntity();
        fillPhotoEntity(photoEntity, temp,i,offerEntity);
        return photoEntity;
    }

    protected void fillOfferEntity(OfferRequest offerRequest, OfferEntity offerEntity) {
        offerEntity.setFakeLatitude(offerRequest.getLatitude()+90);
        offerEntity.setFakeLongitude(offerRequest.getLongitude()+180);
        offerEntity.setOfferType(offerRequest.getOfferType());
        offerEntity.setTitle(offerRequest.getTitle());
        offerEntity.setDescription(offerRequest.getDescription());
    }

    protected void fillPriceEntity(OfferRequest addOfferRequest, PriceEntity priceEntity) {
        priceEntity.setAmount(addOfferRequest.getPriceValue());
        priceEntity.setCurrency(addOfferRequest.getPriceCurrencyType());

        switch(addOfferRequest.getOfferType()){
            case HIRE:
                priceEntity.setPriceType(PriceType.MONTH);
                break;
            case VISIT:
                priceEntity.setPriceType(PriceType.DAY);
                break;
            case SELL:
                priceEntity.setPriceType(PriceType.ONCE);
                break;
        }
    }

    protected void fillPropertyEntity(OfferRequest offerRequest, PropertyEntity propertyEntity) {
        propertyEntity.setPropertyType(offerRequest.getPropertyType());
        propertyEntity.setMaxPerson(offerRequest.getPropertyMaxPerson());
        propertyEntity.setBuildingType(offerRequest.getPropertyBuildingType());
        propertyEntity.setMinPerson(offerRequest.getPropertyMinPerson());
        propertyEntity.setHeatingType(offerRequest.getPropertyHeatingType());
        propertyEntity.setArea(offerRequest.getPropertyArea());
        propertyEntity.setRoomCount(offerRequest.getPropertyRoomCount());
        propertyEntity.setFloor(offerRequest.getPropertyFloor());
        propertyEntity.setBuiltYear(offerRequest.getPropertyBuiltYear());
        propertyEntity.setFurnished(offerRequest.getPropertyIsFurnished());
        propertyEntity.setBalcony(offerRequest.getPropertyHasBalcony());
        propertyEntity.setLift(offerRequest.getPropertyHasLift());
        propertyEntity.setBasement(offerRequest.getPropertyHasBasement());
        propertyEntity.setParkingPlace(offerRequest.getPropertyHasParking());
        propertyEntity.setPets(offerRequest.getPropertyAllowPets());
        propertyEntity.setGarden(offerRequest.getPropertyHasGarden());
        propertyEntity.setClimatisation(offerRequest.getPropertyHasClima());
        propertyEntity.setSmoking(offerRequest.getPropertyAllowSmoking());
    }

    protected void fillAddressEntity(OfferRequest offerRequest, AddressEntity addressEntity) {
        addressEntity.setCity(offerRequest.getAddressCity());
        addressEntity.setCountry(offerRequest.getAddressCountry());
        addressEntity.setStreet(offerRequest.getAddressStreet());
    }

    protected void fillContactEntity(OfferRequest offerRequest, ContactEntity contactEntity) {
        contactEntity.setName(offerRequest.getContactName());
        contactEntity.setEmail(offerRequest.getContactEmail());
        contactEntity.setPhoneNumber(offerRequest.getContactPhoneNumber());
    }

    protected void fillPhotoEntity(PhotoEntity photoEntity, TemporaryPhotoEntity temp, int i, OfferEntity offerEntity) {
        photoEntity.setPriority(i);
        photoEntity.setUrl(temp.getUrl());
        photoEntity.setThumbUrl(temp.getThumbUrl());
        photoEntity.setStoreDate(ZonedDateTime.now());
        photoEntity.setOffer(offerEntity);
    }

    /***
     * Clean up photos if were uploaded but canceled
     */
    protected void cleanUpTemporaryPhotos(Long userId) {

        temporaryPhotoRepository.findAllByPersonId(userId).forEach((temporaryPhoto)->{
            if(storageService.removeTemporaryPhoto(temporaryPhoto)){
                temporaryPhotoRepository.delete(temporaryPhoto);
            }else {
                LOG.error("Cannot delete photo id {}", temporaryPhoto.getId());
            }

        });
    }

    protected void isOfferNullOrNotOwnedByThisUser(OfferEntity offerEntity, PersonEntity pe) {
        if(offerEntity==null){
            LOG.info("Offer does not exist");
            throw new DoesNotExistException();
        }

        if(!offerEntity.getPerson().getId().equals(pe.getId())){
            LOG.info("Offer {} is not owned by user {}", offerEntity.getId(), this.personPrincipalProvider.getEntity().getId());
            throw new NotOwnedByUserException("Offer is not owned by user");
        }
    }


    public void canAfford(PersonEntity pe, int offerPrice) {
        PointsAccountEntity accountEntity= this.pointsAccountRepository.findByPerson(pe);

        LOG.info("User points: {} price {}", accountEntity.getAmount(), offerPrice);
        if(accountEntity.getAmount()< offerPrice){
            throw new CantAffordException();
        }
    }
}
