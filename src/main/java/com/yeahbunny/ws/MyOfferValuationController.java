package com.yeahbunny.ws;

import com.yeahbunny.service.GeocodingService;
import com.yeahbunny.service.OfferValuationService;
import com.yeahbunny.ws.dto.request.AddOfferRequest;
import com.yeahbunny.ws.dto.request.EditOfferRequest;
import com.yeahbunny.ws.dto.response.OfferValuationResponseDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;

@RestController
@RequestMapping(value = "/person/offer")
public class MyOfferValuationController {

    @Inject
    private GeocodingService geocodingService;

    @Inject
    private OfferValuationService valuationService;

    private static final Logger LOG = LoggerFactory.getLogger(MyOfferValuationController.class);

    @RequestMapping(value = "/add-valuation", method = RequestMethod.POST)
    @ResponseBody
    public OfferValuationResponseDto getOfferAddValue(@RequestBody @Valid AddOfferRequest addOfferRequest){

        LOG.info("Get add offer value");

        String countryCode = this.geocodingService.checkOfferAndGetCountryCode(addOfferRequest.getAddressStreet(),
                addOfferRequest.getAddressCity(),
                addOfferRequest.getAddressCountry(),
                addOfferRequest.getLatitude(),
                addOfferRequest.getLongitude());

        return this.valuationService.valuateAdd(addOfferRequest,countryCode);
    }

    @RequestMapping(value = "/edit-valuation", method = RequestMethod.POST)
    @ResponseBody
    public OfferValuationResponseDto getOfferEditValue(@RequestBody @Valid EditOfferRequest editOfferRequest){

        LOG.info("Get edit offer value");

        String countryCode = this.geocodingService.checkOfferAndGetCountryCode(editOfferRequest.getAddressStreet(),
                editOfferRequest.getAddressCity(),
                editOfferRequest.getAddressCountry(),
                editOfferRequest.getLatitude(),
                editOfferRequest.getLongitude());

        return this.valuationService.valuateEdit(editOfferRequest,countryCode);
    }

}
