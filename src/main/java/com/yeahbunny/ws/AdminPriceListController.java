package com.yeahbunny.ws;

import com.yeahbunny.dao.PriceListRepository;
import com.yeahbunny.domain.PriceListEntity;
import com.yeahbunny.ws.dto.generator.PriceListItemDtoGenerator;
import com.yeahbunny.ws.dto.request.EditPriceListRequest;
import com.yeahbunny.ws.dto.response.PriceListItemResponseDto;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/admin/price_list")
public class AdminPriceListController {

    @Inject
    private PriceListRepository priceListRepository;

    @Inject
    private PriceListItemDtoGenerator priceListItemDtoGenerator;

    @RequestMapping(value = "/getCountriesCode", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getCountriesCodes(){
        List<String> result = this.priceListRepository.findAllCountryCodeDistinct();
        return result;
    }


    @RequestMapping(value="/get", method = RequestMethod.GET)
    @ResponseBody
    public List<PriceListItemResponseDto> getPriceList(@RequestParam(required = false) String country_code){
        List<PriceListEntity> entities = this.priceListRepository.findAllByCountryCode(country_code);
        return entities.stream().map(item->{
            return this.priceListItemDtoGenerator.generate(item);
        }).collect(Collectors.toList());
    }

    @Transactional
    @RequestMapping(value="/edit", method = RequestMethod.POST)
    @ResponseBody
    public List<PriceListItemResponseDto> editPriceList(@RequestBody @Valid EditPriceListRequest editPriceListRequest){

        PriceListEntity priceListEntity =  this.priceListRepository.findOne(editPriceListRequest.getId());
        priceListEntity.setPointsAmount(editPriceListRequest.getAmount());


        List<PriceListEntity> entities = this.priceListRepository.findAllByCountryCode(priceListEntity.getCountryCode());
        return entities.stream().map(item->{
            return this.priceListItemDtoGenerator.generate(item);
        }).collect(Collectors.toList());
    }

}
