package com.yeahbunny.ws;

import com.yeahbunny.dao.*;
import com.yeahbunny.domain.*;
import com.yeahbunny.domain.type.OfferActionType;
import com.yeahbunny.exception.BadArgumentsException;
import com.yeahbunny.service.NotifyAdminService;
import com.yeahbunny.ws.dto.generator.BanInfoDtoGenerator;
import com.yeahbunny.ws.dto.generator.MyOfferDtoGenerator;
import com.yeahbunny.ws.dto.generator.OfferHistoryDtoGenerator;
import com.yeahbunny.ws.dto.request.DeleteMyOfferRequest;
import com.yeahbunny.ws.dto.response.BanInfoResponseDto;
import com.yeahbunny.ws.dto.response.PageResponseDto;
import com.yeahbunny.ws.dto.response.OfferHistoryResponseDto;
import com.yeahbunny.ws.util.PageableHelper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.*;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.xml.ws.Response;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/person/offer")
public class MyOfferController extends AbstractPersonOfferController{

    private static final Logger LOG = LoggerFactory.getLogger(MyOfferController.class);

    @Inject
    ContactRepository contactRepository;

    @Inject
    MyOfferDtoGenerator offerDtoGeneratorService;

    @Inject
    BanInfoDtoGenerator banInfoDtoGenerator;

    @Inject
    private OfferHistoryRepository offerHistoryRepository;

    @Inject
    private NotifyAdminService notifyAdminService;

    @Inject
    private OfferHistoryDtoGenerator offerHistoryDtoGenerator;

    @Value("${app.back.host}")
    private String appHost;

    @ApiOperation(value = "Get my offers list page",response = PageResponseDto.class)
    @Transactional(readOnly = true)
    @RequestMapping(value = "/getMyOffers", method = RequestMethod.GET)
    @ResponseBody
    public PageResponseDto getMyOffers(@ApiParam(name = "page", required = false, defaultValue = "0", value = "page number")@RequestParam(required = false) Integer page,
                                       @ApiParam(name = "limit", required = false, defaultValue = "20", value = "offers per page") @RequestParam(required = false) Integer limit,
                                       @ApiParam(name = "sortBy", required = false, defaultValue = "0", value = "sort direction  1 - ASC 0 - DESC") @RequestParam(required = false) String sortBy,
                                       @ApiParam(name = "sortByValue", required = false, defaultValue = "", value = "propety to sort by ") @RequestParam(required = false) String sortByValue,
                                       HttpServletRequest request) throws UnknownHostException{

        Pageable pageRequest = PageableHelper.generatePageableMyOffers(page,limit,sortBy,sortByValue);

        Long personId = super.personPrincipalProvider.getEntity().getId();

        Page<OfferEntity> offerEntityPage = super.offerRepository.findAllByPersonIdAndIsDeletedFalse(personId,pageRequest);

        PageResponseDto resultPage = PageableHelper.generateResponseDto(offerDtoGeneratorService,offerEntityPage, appHost + request.getRequestURI(),page,limit, sortBy,sortByValue);

        return resultPage;
    }



    @Transactional
    @RequestMapping(value = "/deleteMyOffer", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> deleteMyOffer(@RequestBody @Valid DeleteMyOfferRequest deleteMyOfferRequest, HttpServletRequest request){
        LOG.info("Deleting offer {}", deleteMyOfferRequest.getOfferId());
        OfferEntity offerEntity = super.offerRepository.findOne(deleteMyOfferRequest.getOfferId());
        PersonEntity pe = personPrincipalProvider.getEntity();
        isOfferNullOrNotOwnedByThisUser(offerEntity,pe);

        OfferActionType actionType;
        if(offerEntity.isActive()){
            offerEntity.setActive(false);
            actionType = OfferActionType.DEACTIVATED_BY_USER;
            offerEntity.setOfferActualToDate(ZonedDateTime.now());
        }else{
            offerEntity.setDeleted(true);
            actionType = OfferActionType.DELETED_BY_USER;
        }


        OfferHistoryEntity historyEntity = new OfferHistoryEntity();
        historyEntity.setEventDate(ZonedDateTime.now());
        historyEntity.setAction(actionType);

        return ResponseEntity.ok("ok");
    }

    @RequestMapping(value = "/ban-info", method = RequestMethod.GET)
    public BanInfoResponseDto getBanInfo(@RequestParam(required = true) Long id, HttpServletResponse response){
        OfferEntity offerEntity = this.offerRepository.findOne(id);
        PersonEntity pe = super.personPrincipalProvider.getEntity();
        isOfferNullOrNotOwnedByThisUser(offerEntity,pe);

        List<OfferHistoryEntity> offerBannedHistory = this.offerHistoryRepository.findAllByOfferIdAndActions(id, OfferActionType.BANNED_BY_REPORT, OfferActionType.UNACCEPTED_BY_ADMIN);

        if(offerBannedHistory == null || offerBannedHistory.isEmpty()){
            LOG.info("Offer {} is not banned", offerEntity.getId());
            response.setStatus(404);
            return null;
        }

        return banInfoDtoGenerator.generate(offerBannedHistory.get(0));
    }

    @RequestMapping(value = "/history", method = RequestMethod.GET)
    public List<OfferHistoryResponseDto> getOfferHistory(@RequestParam(required = true) Long id, HttpServletResponse response){
        OfferEntity offerEntity = this.offerRepository.findOne(id);
        PersonEntity pe = super.personPrincipalProvider.getEntity();
        super.isOfferNullOrNotOwnedByThisUser(offerEntity,pe);
        List<OfferHistoryEntity> historyEntityList = this.offerHistoryRepository.findAllByOfferIdOrderByEventDateDesc(offerEntity.getId());
        List<OfferHistoryResponseDto> responseDtoList = new ArrayList<>();

        for(OfferHistoryEntity offerHistoryEntity: historyEntityList){
            responseDtoList.add(this.offerHistoryDtoGenerator.generate(offerHistoryEntity));
        }

        return responseDtoList;
    }


}
