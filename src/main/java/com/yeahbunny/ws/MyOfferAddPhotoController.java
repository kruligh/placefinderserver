package com.yeahbunny.ws;

import com.yeahbunny.dao.TemporaryPhotoRepository;
import com.yeahbunny.domain.TemporaryPhotoEntity;
import com.yeahbunny.exception.PhotoDoesntExistException;
import com.yeahbunny.service.PhotoConverterService;
import com.yeahbunny.service.StorageService;
import com.yeahbunny.ws.util.PersonPrincipalProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.time.ZonedDateTime;

@RestController
public class MyOfferAddPhotoController {

    private static final Logger LOG = LoggerFactory.getLogger(MyOfferAddPhotoController.class);

    private static float quality =0.5f;

    @Inject
    private PersonPrincipalProvider personPrincipalProvider;

    @Inject
    private StorageService storageService;

    @Inject
    private PhotoConverterService photoConverterService;

    @Inject
    private TemporaryPhotoRepository temporaryPhotoRepository;

    @Transactional
    @RequestMapping(value = "/person/offer/add/photo/upload", consumes = "multipart/form-data")
    public ResponseEntity<String> handleFileUpload(@RequestParam("files[]") MultipartFile file) {
        ZonedDateTime storeFileDateTime = ZonedDateTime.now();

        String photoPath = processPhoto(file,storeFileDateTime);

        String thumbPath  = processThumb(file,photoPath, storeFileDateTime);

        TemporaryPhotoEntity photoEntity = new TemporaryPhotoEntity();
        photoEntity.setUrl(photoPath);
        photoEntity.setThumbUrl(thumbPath);
        photoEntity.setPerson(personPrincipalProvider.getEntity());
        photoEntity.setStoreDate(storeFileDateTime);
        temporaryPhotoRepository.save(photoEntity);

        LOG.debug("Saved, temporary photos id {} ", photoEntity.getId().toString());
        return ResponseEntity.status(HttpStatus.OK).body(photoEntity.getId().toString());
    }

    private String processPhoto(MultipartFile file, ZonedDateTime storeFileDateTime) {
        try {
            String photoPath = storageService.preparePath(personPrincipalProvider.getEntity().getId(),storeFileDateTime);
            LOG.info("Saving photo {}", photoPath);
            BufferedImage resizedPhoto = photoConverterService.resizePhoto(file.getBytes());
            byte[] compressedPhoto = new byte[0];
            compressedPhoto = photoConverterService.compress1(resizedPhoto,quality);
            String photoUrl = storageService.store(photoPath, compressedPhoto);
            if(photoUrl == null){
                LOG.error("Save file failed, file {} ", photoPath);
                throw new PhotoDoesntExistException(photoPath);
            }

            return photoUrl;
        } catch (IOException e) {
            e.printStackTrace();
            throw new PhotoDoesntExistException("SAVE_PHOTO_ERROR");
        }
    }

    private String processThumb(MultipartFile file, String photoUrl, ZonedDateTime storeFileDateTime) {
        String thumbPath = storageService.prepareThumbPath(personPrincipalProvider.getEntity().getId(), storeFileDateTime);

        byte[] thumb = this.photoConverterService.createThumb(file);
        if(thumb.length==0){
            thumbError(photoUrl,thumbPath, "THUMB_CREATION_ERROR");
        }

        String thumbUrl = this.storageService.store(thumbPath, thumb);
        if(thumbUrl == null){
            thumbError(photoUrl,thumbPath, "STORE_ERROR");
        }

        return thumbPath;
    }

    private void thumbError(String photoUrl, String thumbPath, String msg) {
        if(this.storageService.removeFile(photoUrl)){
            LOG.info("Save file {} failed, file {} removed successfully", thumbPath, photoUrl);
        }else{
            LOG.error("Save file {} failed, file {} removed unsuccessfully", thumbPath, photoUrl);
        }
        throw new PhotoDoesntExistException(msg);
    }

}
