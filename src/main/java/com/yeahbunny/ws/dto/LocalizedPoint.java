package com.yeahbunny.ws.dto;

/**
 * Created by kroli on 01.06.2017.
 */
public interface LocalizedPoint {

    double getLatitude();
    double getLongitude();
}
