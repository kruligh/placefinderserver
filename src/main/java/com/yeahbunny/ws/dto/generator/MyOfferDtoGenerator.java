package com.yeahbunny.ws.dto.generator;


import com.yeahbunny.dao.OfferHistoryRepository;
import com.yeahbunny.domain.OfferEntity;
import com.yeahbunny.domain.OfferHistoryEntity;
import com.yeahbunny.domain.type.OfferActionType;
import com.yeahbunny.service.OfferHistoryService;
import com.yeahbunny.ws.dto.response.MyOfferResponseDto;
import com.yeahbunny.ws.dto.response.MyOfferStatus;
import com.yeahbunny.ws.dto.response.ResponseDto;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

@Component
public class MyOfferDtoGenerator implements DtoGenerator<OfferEntity>{

    @Inject
    OfferDtoGenerator offerDtoGenerator;

    @Inject
    OfferHistoryService historyService;

    @Override
    public MyOfferResponseDto generate(OfferEntity source) {
        MyOfferResponseDto responseDto = new MyOfferResponseDto();

        responseDto.setOffer(offerDtoGenerator.generate(source));

        responseDto.setMyOfferStatus(historyService.getOfferStatus(source));




        responseDto.setActualTo(source.getOfferActualToDate());
        return responseDto;
    }
}
