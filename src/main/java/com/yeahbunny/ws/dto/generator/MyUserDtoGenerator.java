package com.yeahbunny.ws.dto.generator;


import com.yeahbunny.dao.PointsAccountRepository;
import com.yeahbunny.domain.ContactEntity;
import com.yeahbunny.domain.PersonEntity;
import com.yeahbunny.ws.dto.response.ContactResponseDto;
import com.yeahbunny.ws.dto.response.MyUserResponseDto;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
public class MyUserDtoGenerator {

    @Inject
    private PointsAccountRepository pointsAccountRepository;

    public MyUserResponseDto generate(PersonEntity personEntity) {

        MyUserResponseDto myUserResponseDto = new MyUserResponseDto();
        myUserResponseDto.setId(personEntity.getId());
        myUserResponseDto.setEmail(personEntity.getEmail());
        myUserResponseDto.setContact(generateContactDto(personEntity.getContact()));
        myUserResponseDto.setName(personEntity.getName());
        myUserResponseDto.setActivated(personEntity.isActivated());
        myUserResponseDto.setPoints(getAccountBalance(personEntity));

        return myUserResponseDto;
    }

    private int getAccountBalance(PersonEntity personEntity) {
        return this.pointsAccountRepository.findByPersonId(personEntity.getId()).getAmount();
    }

    private ContactResponseDto generateContactDto(ContactEntity contactEntity) {
        return new DtoReflectionGenerator<ContactResponseDto>().generate(ContactResponseDto.class, contactEntity);
    }


}
