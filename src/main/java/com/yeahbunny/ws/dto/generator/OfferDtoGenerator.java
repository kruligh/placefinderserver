package com.yeahbunny.ws.dto.generator;

import com.yeahbunny.domain.AbstractEntity;
import com.yeahbunny.domain.OfferEntity;
import com.yeahbunny.domain.PhotoEntity;
import com.yeahbunny.ws.dto.response.OfferResponseDto;
import com.yeahbunny.ws.dto.response.PhotoResponseDto;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Component
public class OfferDtoGenerator implements DtoGenerator<OfferEntity> {

    @Inject
    private ContactDtoGenerator contactDtoGenerator;

    @Inject
    private PersonDtoGenerator personDtoGenerator;

    @Inject
    private PriceDtoGenerator priceDtoGenerator;

    @Inject
    private PropertyDtoGenerator propertyDtoGenerator;

    @Inject
    private PhotoDtoGenerator photoDtoGenerator;

    @Override
    public OfferResponseDto generate(OfferEntity offerEntity) {

        OfferResponseDto resultDto = new OfferResponseDto();

        resultDto.setOfferType(offerEntity.getOfferType());
        resultDto.setAddDate(offerEntity.getAddDate());
        resultDto.setTitle(offerEntity.getTitle());
        resultDto.setViews(offerEntity.getViews());
        resultDto.setDescription(offerEntity.getDescription());
        resultDto.setLatitude(offerEntity.getFakeLatitude()- 90);
        resultDto.setLongitude(offerEntity.getFakeLongitude() -180);
        resultDto.setId(offerEntity.getId());
        resultDto.setPromotionList(offerEntity.getPromotionList());
        resultDto.setPromotionMap(offerEntity.getPromotionMap());

        resultDto.setContact(this.contactDtoGenerator.generate(offerEntity.getContact()));
        resultDto.setPerson(this.personDtoGenerator.generate(offerEntity.getPerson()));
        resultDto.setPrice(this.priceDtoGenerator.generate(offerEntity.getPrice()));
        resultDto.setProperty(this.propertyDtoGenerator.generate(offerEntity.getProperty()));

        List<PhotoResponseDto> photoResponseDtoList = new ArrayList<>();
        for(PhotoEntity photoEntity : offerEntity.getPhotos()){
            photoResponseDtoList.add(this.photoDtoGenerator.generate(photoEntity));
        }
        resultDto.setPhotos(photoResponseDtoList);

        return resultDto;
    }

}
