package com.yeahbunny.ws.dto.generator;

import com.yeahbunny.domain.AbstractEntity;
import com.yeahbunny.domain.PersonEntity;
import com.yeahbunny.ws.dto.response.PersonResponseDto;
import org.springframework.stereotype.Component;

@Component
public class PersonDtoGenerator implements DtoGenerator<PersonEntity> {

    public PersonResponseDto generate(PersonEntity personEntity) {

        PersonResponseDto resultDto = new DtoReflectionGenerator<PersonResponseDto>().generate(PersonResponseDto.class,personEntity);
        return resultDto;
    }
}
