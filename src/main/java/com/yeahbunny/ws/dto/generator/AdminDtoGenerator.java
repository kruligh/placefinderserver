package com.yeahbunny.ws.dto.generator;

import com.yeahbunny.dao.OfferRepository;
import com.yeahbunny.dao.PersonRepository;
import com.yeahbunny.domain.AbstractEntity;
import com.yeahbunny.domain.AdminEntity;
import com.yeahbunny.ws.dto.response.AdminResponseDto;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
public class AdminDtoGenerator implements DtoGenerator<AdminEntity> {

    @Inject
    private OfferRepository offerRepository;

    @Inject
    private PersonRepository personRepository;

    @Override
    public AdminResponseDto generate(AdminEntity adminEntity) {

        AdminResponseDto resultDto = new AdminResponseDto();
        resultDto.setEmail(adminEntity.getEmail());
        resultDto.setName(adminEntity.getName());
        resultDto.setActiveOffersCount(getActiveOffersCount());
        resultDto.setUsersCount(getPersonCount());
        resultDto.setAllOffersCount(getAllOffersCount());
        return resultDto;
    }

    private long getAllOffersCount() {
        return this.offerRepository.count();
    }

    private long getPersonCount() {
        return this.personRepository.count();
    }

    private long getActiveOffersCount() {
        return this.offerRepository.countByActiveTrue();
    }

}
