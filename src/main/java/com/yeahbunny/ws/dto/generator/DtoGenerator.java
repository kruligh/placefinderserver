package com.yeahbunny.ws.dto.generator;

import com.yeahbunny.domain.AbstractEntity;
import com.yeahbunny.ws.dto.response.ResponseDto;

public interface DtoGenerator<T extends AbstractEntity> {

    ResponseDto generate(T source);
}
