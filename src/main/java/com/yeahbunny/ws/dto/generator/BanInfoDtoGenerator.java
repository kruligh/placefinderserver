package com.yeahbunny.ws.dto.generator;

import com.yeahbunny.domain.OfferHistoryEntity;
import com.yeahbunny.domain.type.OfferActionType;
import com.yeahbunny.ws.dto.response.BanInfoResponseDto;
import com.yeahbunny.ws.dto.response.ResponseDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BanInfoDtoGenerator implements DtoGenerator<OfferHistoryEntity> {

    @Value("${app.mail.from}")
    private String contactEmail;

    @Override
    public BanInfoResponseDto generate(OfferHistoryEntity source) {
        BanInfoResponseDto responseDto = new BanInfoResponseDto();
        responseDto.setReason(source.getReason());
        responseDto.setEventDate(source.getEventDate());
        responseDto.setContact(contactEmail);
        responseDto.setOfferId(source.getOffer().getId());
        return responseDto;
    }
}
