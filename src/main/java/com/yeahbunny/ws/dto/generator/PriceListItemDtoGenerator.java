package com.yeahbunny.ws.dto.generator;

import com.yeahbunny.domain.PriceListEntity;
import com.yeahbunny.ws.dto.response.PriceListItemResponseDto;
import com.yeahbunny.ws.dto.response.ResponseDto;
import org.springframework.stereotype.Component;

@Component
public class PriceListItemDtoGenerator implements DtoGenerator<PriceListEntity>{

    @Override
    public PriceListItemResponseDto generate(PriceListEntity source) {
        PriceListItemResponseDto responseDto = new PriceListItemResponseDto();
        responseDto.setId(source.getId());
        responseDto.setCountryCode(source.getCountryCode());
        responseDto.setProductType(source.getProductType());
        responseDto.setAmount(source.getPointsAmount());
        return responseDto;
    }
}
