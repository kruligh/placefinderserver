package com.yeahbunny.ws.dto.generator;

import com.yeahbunny.domain.ReportedOfferEntity;
import com.yeahbunny.ws.dto.response.ReportResponseDto;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
public class ReportDtoGenerator implements DtoGenerator<ReportedOfferEntity>{

    @Inject
    OfferDtoGenerator offerDtoGenerator;

    @Override
    public ReportResponseDto generate(ReportedOfferEntity source) {
        ReportResponseDto responseDto = new ReportResponseDto();

        responseDto.setId(source.getId());
        responseDto.setCause(source.getCauseDesc());
        responseDto.setReportDate(source.getReportDate());
        responseDto.setOffer(this.offerDtoGenerator.generate(source.getOffer()));

        return responseDto;
    }
}
