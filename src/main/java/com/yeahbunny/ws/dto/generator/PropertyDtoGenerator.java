package com.yeahbunny.ws.dto.generator;

import com.yeahbunny.domain.AbstractEntity;
import com.yeahbunny.domain.PropertyEntity;
import com.yeahbunny.ws.dto.response.PropertyResponseDto;
import org.springframework.stereotype.Component;

@Component
public class PropertyDtoGenerator implements DtoGenerator<PropertyEntity> {

    @Override
    public PropertyResponseDto generate(PropertyEntity propertyEntity) {

        PropertyResponseDto resultDto = new DtoReflectionGenerator<PropertyResponseDto>().generate(PropertyResponseDto.class,propertyEntity);
        return resultDto;
    }
}
