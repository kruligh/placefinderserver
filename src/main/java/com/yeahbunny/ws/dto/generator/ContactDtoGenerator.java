package com.yeahbunny.ws.dto.generator;

import com.yeahbunny.domain.AbstractEntity;
import com.yeahbunny.domain.ContactEntity;
import com.yeahbunny.ws.dto.response.ContactResponseDto;
import org.springframework.stereotype.Component;

@Component
public class ContactDtoGenerator implements DtoGenerator<ContactEntity> {

    public ContactResponseDto generate(ContactEntity contactEntity) {

        ContactResponseDto resultDto = new DtoReflectionGenerator<ContactResponseDto>().generate(ContactResponseDto.class,contactEntity);
        return resultDto;
    }
}
