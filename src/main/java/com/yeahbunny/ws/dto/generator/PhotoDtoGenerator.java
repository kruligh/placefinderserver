package com.yeahbunny.ws.dto.generator;

import com.yeahbunny.domain.PhotoEntity;
import com.yeahbunny.ws.dto.response.PhotoResponseDto;
import com.yeahbunny.ws.dto.response.ResponseDto;
import org.springframework.stereotype.Component;

@Component
public class PhotoDtoGenerator implements DtoGenerator<PhotoEntity> {

    @Override
    public PhotoResponseDto generate(PhotoEntity source) {
        PhotoResponseDto responseDto = new PhotoResponseDto();
        responseDto.setId(source.getId());
        return responseDto;
    }
}
