package com.yeahbunny.ws.dto.generator;

import com.yeahbunny.domain.OfferHistoryEntity;
import com.yeahbunny.ws.dto.response.OfferHistoryResponseDto;
import com.yeahbunny.ws.dto.response.ResponseDto;
import org.springframework.stereotype.Component;

@Component
public class OfferHistoryDtoGenerator implements DtoGenerator<OfferHistoryEntity>{

    @Override
    public OfferHistoryResponseDto generate(OfferHistoryEntity source) {
        OfferHistoryResponseDto responseDto = new OfferHistoryResponseDto();
        responseDto.setAction(source.getAction());
        responseDto.setEventDate(source.getEventDate());
        responseDto.setReason(source.getReason());
        return responseDto;
    }
}
