package com.yeahbunny.ws.dto.generator;

import com.yeahbunny.domain.AbstractEntity;
import com.yeahbunny.domain.PriceEntity;
import com.yeahbunny.ws.dto.response.PriceResponseDto;
import org.springframework.stereotype.Component;

@Component
public class PriceDtoGenerator implements DtoGenerator<PriceEntity> {

    @Override
    public PriceResponseDto generate(PriceEntity priceEntity) {

        PriceResponseDto resultDto = new DtoReflectionGenerator<PriceResponseDto>().generate(PriceResponseDto.class,priceEntity);
        return resultDto;
    }
}
