package com.yeahbunny.ws.dto.generator;


import com.yeahbunny.domain.PointsPacketEntity;
import com.yeahbunny.ws.dto.response.PointsPacketResponseDto;
import com.yeahbunny.ws.dto.response.ResponseDto;
import org.springframework.stereotype.Component;

@Component
public class PointsPacketDtoGenerator implements DtoGenerator<PointsPacketEntity>{

    @Override
    public PointsPacketResponseDto generate(PointsPacketEntity source) {
        DtoReflectionGenerator<PointsPacketResponseDto> generator = new DtoReflectionGenerator<>();

        return generator.generate(PointsPacketResponseDto.class,source);
    }
}
