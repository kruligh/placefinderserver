package com.yeahbunny.ws.dto.generator;

import com.yeahbunny.domain.AbstractEntity;
import com.yeahbunny.domain.ContactEntity;
import com.yeahbunny.domain.LazyOfferEntity;
import com.yeahbunny.domain.OfferEntity;
import com.yeahbunny.domain.type.OfferType;
import com.yeahbunny.domain.type.PropertyType;
import com.yeahbunny.ws.dto.response.ContactResponseDto;
import com.yeahbunny.ws.dto.response.MarkerResponseDto;
import org.springframework.stereotype.Component;

@Component
public class MarkerDtoGenerator implements DtoGenerator<LazyOfferEntity>{

    public MarkerResponseDto generate(LazyOfferEntity offerEntity) {

        MarkerResponseDto resultDto = new MarkerResponseDto();

        resultDto.setId(offerEntity.getId());
        resultDto.setOfferType(offerEntity.getOfferType());
        resultDto.setLatitude(offerEntity.getFakeLatitude() - 90);
        resultDto.setLongitude(offerEntity.getFakeLongitude() -180);
        resultDto.setPropertyType(offerEntity.getProperty().getPropertyType());

        return resultDto;

    }

}
