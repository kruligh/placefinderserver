package com.yeahbunny.ws.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PayuOrderNotificationRequest {

    PayuStandardOrderNoification order;


    public PayuStandardOrderNoification getOrder() {
        return order;
    }

    public void setOrder(PayuStandardOrderNoification order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return "PayuOrderNotificationRequest{" +
                "order=" + order +
                '}';
    }
}
