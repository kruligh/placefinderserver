package com.yeahbunny.ws.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yeahbunny.service.payu.PayuStandardOffer;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PayuStandardOrderNoification extends PayuStandardOffer{

    private String orderId;

    private String status;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "PayuStandardOrderNoification{" +
                "extId=" + getExtOrderId() +
                " status='" + status + '\'' +
                '}';
    }
}
