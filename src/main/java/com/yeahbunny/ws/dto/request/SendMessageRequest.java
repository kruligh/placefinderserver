package com.yeahbunny.ws.dto.request;


import com.yeahbunny.SizeConstraintsConfiguration;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class SendMessageRequest {

    @ApiModelProperty(required = true)
    @NotNull
    private Long offerId;

    @ApiModelProperty(required = true)
    @NotNull
    @NotBlank
    @Size(max = SizeConstraintsConfiguration.messageBody)
    private String content;

    @ApiModelProperty(required = true)
    @NotNull
    @NotBlank
    @Size(max = SizeConstraintsConfiguration.email)
    private String from;

    @ApiModelProperty(required = false)
    @Size(max = SizeConstraintsConfiguration.phoneNumber)
    private String fromPhoneNumber;

    public Long getOfferId() {
        return offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getFromPhoneNumber() {
        return fromPhoneNumber;
    }

    public void setFromPhoneNumber(String fromPhoneNumber) {
        this.fromPhoneNumber = fromPhoneNumber;
    }
}
