package com.yeahbunny.ws.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.yeahbunny.SizeConstraintsConfiguration;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class RegisterRequest {

    @NotNull
    @NotBlank
    private String email;

    @NotNull
    @NotBlank
    private String password;

    @NotNull
    @NotBlank
    @Size(max = SizeConstraintsConfiguration.contactName)
    private String name;

    @JsonProperty(value="phone", required = false)
    @Size(max = SizeConstraintsConfiguration.phoneNumber)
    private String phoneNumber;

    @NotNull
    private Boolean isAgency;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isAgency() {
        return isAgency;
    }

    public void setIsAgency(Boolean agency) {
        isAgency = agency;
    }
}
