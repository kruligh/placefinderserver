package com.yeahbunny.ws.dto.request;


import javax.validation.constraints.NotNull;

public class BuyPointsRequest {

    @NotNull
    private Long packetId;

    public Long getPacketId() {
        return packetId;
    }

    public void setPacketId(Long packetId) {
        this.packetId = packetId;
    }
}
