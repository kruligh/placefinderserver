package com.yeahbunny.ws.dto.request;


import javax.validation.constraints.NotNull;

public class RemovePhotoRequest {

    @NotNull
    private Long photoId;

    public Long getPhotoId() {
        return photoId;
    }

    public void setPhotoId(Long photoId) {
        this.photoId = photoId;
    }

}
