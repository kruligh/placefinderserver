package com.yeahbunny.ws.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class EditMyContactRequest {

    @NotNull
    @NotBlank
    @JsonProperty(value = "nazwa")
    @Size(max = 40)
    private String contactName;

    @NotNull
    @NotBlank
    @JsonProperty(value = "email")
    private String contactEmail;

    @JsonProperty(value = "telefon")
    @Size(max = 20)
    private String contactPhoneNumber;

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getContactPhoneNumber() {
        return contactPhoneNumber;
    }

    public void setContactPhoneNumber(String contactPhoneNumber) {
        this.contactPhoneNumber = contactPhoneNumber;
    }
}
