package com.yeahbunny.ws.dto.request;

import javax.validation.constraints.NotNull;

public class EditPriceListRequest {

    @NotNull
    private Long id;

    @NotNull
    private Integer amount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
