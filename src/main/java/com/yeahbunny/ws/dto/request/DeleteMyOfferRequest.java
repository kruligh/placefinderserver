package com.yeahbunny.ws.dto.request;


import javax.validation.constraints.NotNull;

public class DeleteMyOfferRequest {

    @NotNull
    Long offerId;

    public Long getOfferId() {
        return offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }
}
