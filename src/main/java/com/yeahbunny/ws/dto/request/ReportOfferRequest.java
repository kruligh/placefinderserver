package com.yeahbunny.ws.dto.request;

import com.yeahbunny.SizeConstraintsConfiguration;
import com.yeahbunny.domain.type.ReportCauseType;
import org.hibernate.validator.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ReportOfferRequest {
    @NotNull
    @NotBlank
    private String offerId;

   @NotNull
    private ReportCauseType cause;

    @Size(max = SizeConstraintsConfiguration.reportCause)
    private String causeDesc;

    public String getOfferId() {
        return offerId;
    }

    public ReportCauseType getCause() {
        return cause;
    }

    public void setCause(ReportCauseType cause) {
        this.cause = cause;
    }

    public String getCauseDesc() {
        return causeDesc;
    }

    public void setCauseDesc(String causeDesc) {
        this.causeDesc = causeDesc;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }
}
