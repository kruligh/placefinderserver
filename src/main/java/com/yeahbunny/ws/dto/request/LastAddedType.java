package com.yeahbunny.ws.dto.request;

public enum LastAddedType {
    DAY,THREE_DAYS, WEEK
}
