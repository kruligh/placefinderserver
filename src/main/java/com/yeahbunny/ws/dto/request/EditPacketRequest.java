package com.yeahbunny.ws.dto.request;


import org.springframework.data.domain.Example;

import javax.validation.constraints.NotNull;

public class EditPacketRequest {

    @NotNull
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private Integer pointsCount;

    @NotNull
    private Integer pricePln;

    public Long getId() {
        return id;
    }

    public int getPointsCount() {
        return pointsCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPricePln() {
        return pricePln;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setPointsCount(Integer pointsCount) {
        this.pointsCount = pointsCount;
    }

    public void setPricePln(Integer pricePln) {
        this.pricePln = pricePln;
    }
}
