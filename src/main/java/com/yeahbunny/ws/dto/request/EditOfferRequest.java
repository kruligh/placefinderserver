package com.yeahbunny.ws.dto.request;


import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

public class EditOfferRequest extends OfferRequest{

    @NotNull
    @JsonProperty(value = "id")
    private Long id;

    @NotNull
    @JsonProperty(value = "przedluzenie")
    private int extendOffer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getExtendOffer() {
        return extendOffer;
    }

    public void setExtendOffer(int extendOffer) {
        this.extendOffer = extendOffer;
    }
}
