package com.yeahbunny.ws.dto.request;


import javax.validation.constraints.NotNull;

public class AcceptOfferRequest {

    @NotNull
    private Long offerId;

    public Long getOfferId() {
        return offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }
}
