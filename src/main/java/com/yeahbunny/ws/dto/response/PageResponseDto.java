package com.yeahbunny.ws.dto.response;

import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

public class PageResponseDto<T> {
    private String next;
    private String prev;
    private long count;
    private Integer pages;
    private List<T> content;


    public PageResponseDto() {
        this.content = new ArrayList<T>();
    }

    public void add(T item) {
        this.content.add(item);
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getPrev() {
        return prev;
    }

    public void setPrev(String prev) {
        this.prev = prev;
    }

    public List<T> getContent() {
        return content;
    }

    public void setContent(List<T> content) {
        this.content = content;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public long getCount() {
        return count;
    }

    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }


}
