package com.yeahbunny.ws.dto.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.yeahbunny.ws.util.serialization.Java8DateTimeJsonSerializer;

import java.time.ZonedDateTime;

public class ReportResponseDto implements ResponseDto {

    private Long id;
    private OfferResponseDto offer;
    @JsonSerialize(using= Java8DateTimeJsonSerializer.class)
    private ZonedDateTime reportDate;
    private String cause;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OfferResponseDto getOffer() {
        return offer;
    }

    public void setOffer(OfferResponseDto offer) {
        this.offer = offer;
    }

    public ZonedDateTime getReportDate() {
        return reportDate;
    }

    public void setReportDate(ZonedDateTime reportDate) {
        this.reportDate = reportDate;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }
}
