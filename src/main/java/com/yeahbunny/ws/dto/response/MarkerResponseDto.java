package com.yeahbunny.ws.dto.response;

import com.yeahbunny.domain.type.OfferType;
import com.yeahbunny.domain.type.PropertyType;
import com.yeahbunny.ws.dto.LocalizedPoint;

public class MarkerResponseDto implements ResponseDto, LocalizedPoint {
    private Long id;
    private int offerType;
    private int propertyType;
    private double latitude;
    private double longitude;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getOfferType() {
        return offerType;
    }

    public void setOfferType(OfferType offerType) {
        this.offerType = offerType.ordinal();
    }

    public int getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(PropertyType propertyType) {
        this.propertyType = propertyType.getValue();
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }



}
