package com.yeahbunny.ws.dto.response;

public class PhotoResponseDto implements ResponseDto{
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}

