package com.yeahbunny.ws.dto.response;

import java.util.List;

public class OfferValuationResponseDto implements ResponseDto{
    private List<OfferValuationRow> rows;



    public void setRows(List<OfferValuationRow> rows) {
        this.rows = rows;
    }

    public List<OfferValuationRow> getRows() {
        return rows;
    }

    public int getSum() {
        int sum = 0;

        for(OfferValuationRow row: rows){
            sum += row.getPoints();
        }

       return sum;
    }
}
