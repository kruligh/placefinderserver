package com.yeahbunny.ws.dto.response;

public class PersonResponseDto implements ResponseDto {
    private boolean isAgency;

    public boolean getIsAgency() {
        return isAgency;
    }

    public void setIsAgency(boolean agency) {
        isAgency = agency;
    }
}
