package com.yeahbunny.ws.dto.response;

import com.yeahbunny.domain.AdminEntity;

public class AdminResponseDto implements ResponseDto{

    private String name;
    private String email;
    private long usersCount;
    private long activeOffersCount;
    private long allOffersCount;

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public long getUsersCount() {
        return usersCount;
    }

    public void setUsersCount(long usersCount) {
        this.usersCount = usersCount;
    }

    public long getActiveOffersCount() {
        return activeOffersCount;
    }

    public void setActiveOffersCount(long activeOffersCount) {
        this.activeOffersCount = activeOffersCount;
    }

    public long getAllOffersCount() {
        return allOffersCount;
    }

    public void setAllOffersCount(long allOffersCount) {
        this.allOffersCount = allOffersCount;
    }
}
