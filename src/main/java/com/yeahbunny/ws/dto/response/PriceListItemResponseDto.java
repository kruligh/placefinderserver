package com.yeahbunny.ws.dto.response;


import com.yeahbunny.domain.type.ProductType;

public class PriceListItemResponseDto implements ResponseDto{
    private Long id;
    private ProductType productType;
    private int amount;
    private String countryCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public String getProductType() {
        return productType.name();
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryCode() {
        return countryCode;
    }
}
