package com.yeahbunny.ws.dto.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.yeahbunny.ws.util.serialization.Java8DateTimeJsonSerializer;
import com.yeahbunny.ws.util.serialization.MyOfferResponseDtoSerializer;

import java.time.ZonedDateTime;

@JsonSerialize(using = MyOfferResponseDtoSerializer.class)
public class MyOfferResponseDto implements ResponseDto{

    private OfferResponseDto offer;
    private MyOfferStatus status;
    @JsonSerialize(using= Java8DateTimeJsonSerializer.class)
    private ZonedDateTime actualTo;

    public OfferResponseDto getOffer() {
        return offer;
    }

    public void setOffer(OfferResponseDto offer) {
        this.offer = offer;
    }

    public MyOfferStatus getMyOfferStatus() {
        return status;
    }

    public void setMyOfferStatus(MyOfferStatus status) {
        this.status = status;
    }

    public MyOfferStatus getStatus() {
        return status;
    }

    public void setStatus(MyOfferStatus status) {
        this.status = status;
    }

    public ZonedDateTime getActualTo() {
        return actualTo;
    }

    public void setActualTo(ZonedDateTime actualTo) {
        this.actualTo = actualTo;
    }
}
