package com.yeahbunny.ws.dto.response;


import javax.persistence.Column;

public class PointsPacketResponseDto implements ResponseDto{

    private Long id;

    private String name;
    private int pointsCount;
    private int pricePln;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPointsCount() {
        return pointsCount;
    }

    public void setPointsCount(int pointsCount) {
        this.pointsCount = pointsCount;
    }

    public int getPricePln() {
        return pricePln;
    }

    public void setPricePln(int pricePln) {
        this.pricePln = pricePln;
    }
}
