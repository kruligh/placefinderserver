package com.yeahbunny.ws.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.yeahbunny.domain.type.CurrencyType;

public class PriceResponseDto implements ResponseDto {

    @JsonProperty(value = "value")
    long amount;
    CurrencyType currency;

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public int getCurrency() {
        return currency.ordinal();
    }

    public void setCurrency(CurrencyType currency) {
        this.currency = currency;
    }

}
