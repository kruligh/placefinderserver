package com.yeahbunny.ws.dto.response;

public enum MyOfferStatus {
    ACTIVE, INACTIVE, BANNED
}
