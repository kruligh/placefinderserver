package com.yeahbunny.ws.dto.response;

public class PayuBuyResponseDto implements ResponseDto{

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
