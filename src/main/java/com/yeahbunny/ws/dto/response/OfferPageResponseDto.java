package com.yeahbunny.ws.dto.response;


import java.util.List;

public class OfferPageResponseDto {

    private int pages;
    private List<OfferResponseDto> content;

    public OfferPageResponseDto(int pages, List<OfferResponseDto> content) {
        this.pages = pages;
        this.content = content;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public List<OfferResponseDto> getContent() {
        return content;
    }

    public void setContent(List<OfferResponseDto> content) {
        this.content = content;
    }
}
