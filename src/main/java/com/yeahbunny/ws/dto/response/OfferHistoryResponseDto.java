package com.yeahbunny.ws.dto.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.yeahbunny.domain.type.OfferActionType;
import com.yeahbunny.ws.util.serialization.Java8DateTimeJsonSerializer;

import java.time.ZonedDateTime;

public class OfferHistoryResponseDto implements ResponseDto{

    private String reason;
    private OfferActionType action;
    @JsonSerialize(using= Java8DateTimeJsonSerializer.class)
    private ZonedDateTime eventDate;

    public int getAction() {
        return action.ordinal();
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }

    public void setAction(OfferActionType action) {
        this.action = action;
    }

    public void setEventDate(ZonedDateTime eventDate) {
        this.eventDate = eventDate;
    }

    public ZonedDateTime getEventDate() {
        return eventDate;
    }
}
