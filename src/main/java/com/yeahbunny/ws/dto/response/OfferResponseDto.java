package com.yeahbunny.ws.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.yeahbunny.domain.type.OfferType;
import com.yeahbunny.ws.dto.LocalizedPoint;
import com.yeahbunny.ws.util.serialization.Java8DateTimeJsonSerializer;

import java.time.ZonedDateTime;
import java.util.List;

public class OfferResponseDto implements ResponseDto, LocalizedPoint {
    private Long id;
    @JsonProperty(value = "latitude")
    private double latitude;
    @JsonProperty(value = "longitude")
    private double longitude;
    private OfferType offerType;
    @JsonSerialize(using= Java8DateTimeJsonSerializer.class)
    private ZonedDateTime addDate;
    private String title;
    private String description;
    private int views;
    private PriceResponseDto price;
    private PersonResponseDto person;
    private ContactResponseDto contact;
    private List<PhotoResponseDto> photos;
    private PropertyResponseDto property;
    private int promotionList;
    private int promotionMap;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getOfferType() {
        return offerType.ordinal();
    }

    public void setOfferType(OfferType offerType) {
        this.offerType = offerType;
    }
    
    public void setAddDate(ZonedDateTime startDate) {
        this.addDate = startDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PriceResponseDto getPrice() {
        return price;
    }

    public void setPrice(PriceResponseDto price) {
        this.price = price;
    }

    public PropertyResponseDto getProperty() {
        return property;
    }

    public void setProperty(PropertyResponseDto property) {
        this.property = property;
    }

    public ContactResponseDto getContact() {
        return contact;
    }

    public void setContact(ContactResponseDto contact) {
        this.contact = contact;
    }

    public PersonResponseDto getPerson() {
        return person;
    }

    public void setPerson(PersonResponseDto person) {
        this.person = person;
    }

    public ZonedDateTime getAddDate() {
        return addDate;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public List<PhotoResponseDto> getPhotos() {
        return photos;
    }

    public void setPhotos(List<PhotoResponseDto> photos) {
        this.photos = photos;
    }

    public int getPromotionList() {
        return promotionList;
    }

    public void setPromotionList(int promotionList) {
        this.promotionList = promotionList;
    }

    public int getPromotionMap() {
        return promotionMap;
    }

    public void setPromotionMap(int promotionMap) {
        this.promotionMap = promotionMap;
    }
}
