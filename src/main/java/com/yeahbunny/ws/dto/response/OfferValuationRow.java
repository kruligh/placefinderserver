package com.yeahbunny.ws.dto.response;


import com.yeahbunny.domain.type.ProductType;

public class OfferValuationRow {

    private ProductType productType;

    private int points;

    public int getProductType() {
        return productType.ordinal();
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
