package com.yeahbunny.ws.dto.response;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.yeahbunny.ws.util.serialization.Java8DateTimeJsonSerializer;

import java.time.ZonedDateTime;

public class BanInfoResponseDto implements ResponseDto{


    private String reason;
    @JsonSerialize(using= Java8DateTimeJsonSerializer.class)
    private ZonedDateTime eventDate;
    private String contact;
    private Long offerId;

    public Long getOfferId() {
        return offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }

    public void setEventDate(ZonedDateTime eventDate) {
        this.eventDate = eventDate;
    }

    public ZonedDateTime getEventDate() {
        return eventDate;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getContact() {
        return contact;
    }
}
