package com.yeahbunny.ws;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yeahbunny.dao.PointsPacketPaymentRepository;
import com.yeahbunny.dao.PointsPacketRepository;
import com.yeahbunny.domain.PointsPacketEntity;
import com.yeahbunny.domain.PointsPacketPaymentEntity;
import com.yeahbunny.domain.PersonEntity;
import com.yeahbunny.domain.type.CurrencyType;
import com.yeahbunny.exception.BadArgumentsException;
import com.yeahbunny.exception.DoesNotExistException;
import com.yeahbunny.exception.ForbiddenException;
import com.yeahbunny.service.PayuService;
import com.yeahbunny.service.PointsService;
import com.yeahbunny.service.TokenBuilderService;
import com.yeahbunny.ws.dto.generator.PointsPacketDtoGenerator;
import com.yeahbunny.ws.dto.request.BuyPointsRequest;
import com.yeahbunny.ws.dto.request.PayuOrderNotificationRequest;
import com.yeahbunny.ws.dto.response.PayuBuyResponseDto;
import com.yeahbunny.ws.dto.response.PointsPacketResponseDto;
import com.yeahbunny.ws.util.PersonPrincipalProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/person/points")
public class PersonPayuController {

    private static final Logger LOG = LoggerFactory.getLogger(PersonPayuController.class);

    @Value("${app.payment.payu.md5key}")
    private String payuMd5key;

    @Inject
    private PersonPrincipalProvider personPrincipalProvider;

    @Inject
    private PayuService payuService;

    @Inject
    private PointsPacketPaymentRepository pointsPacketPaymentRepository;

    @Inject
    private PointsPacketRepository pointsPacketRepository;

    @Inject
    private PointsService pointsService;

    private String OpenPayuSignatureHeaderName="OpenPayu-Signature";

    @Transactional
    @RequestMapping(value = "/payu/buy", method = RequestMethod.POST)
    public PayuBuyResponseDto buyPoints(@RequestBody @Valid BuyPointsRequest buyPointsRequest, HttpServletRequest request){
        LOG.error("PAYU BUY IS NOT REGISTERED YET");
        return null;
    /*    PersonEntity personEntity = this.personPrincipalProvider.getEntity();

        PointsPacketEntity packetEntity = this.pointsPacketRepository.findOne(buyPointsRequest.getPacketId());

        if(packetEntity == null){
            LOG.info("Packet {} does not exist", buyPointsRequest.getPacketId());
            throw new DoesNotExistException("Packet id " + buyPointsRequest.getPacketId() + " does not exists");
        }else{
            LOG.info("");
        }

        PointsPacketPaymentEntity pointsPacketPaymentEntity = generatePaymentEntity(personEntity, packetEntity, CurrencyType.PLN);

        String url = payuService.buyPoints(pointsPacketPaymentEntity, request.getRemoteAddr());

        PayuBuyResponseDto responseDto = new PayuBuyResponseDto();
        responseDto.setUrl(url);

        return responseDto;*/
    }

    private PointsPacketPaymentEntity generatePaymentEntity(PersonEntity personEntity, PointsPacketEntity packetEntity, CurrencyType currencyType) {
        PointsPacketPaymentEntity pointsPacketPaymentEntity = new PointsPacketPaymentEntity();
        pointsPacketPaymentEntity.setPointsPacket(packetEntity);
        pointsPacketPaymentEntity.setCurrency(currencyType);
        pointsPacketPaymentEntity.setPerson(personEntity);
        this.pointsPacketPaymentRepository.save(pointsPacketPaymentEntity);
        pointsPacketPaymentEntity.setToken(TokenBuilderService.paymentToken(pointsPacketPaymentEntity));
        return pointsPacketPaymentEntity;
    }

    @Transactional()
    @RequestMapping(value ="/payu/notify", method = RequestMethod.POST)
    public ResponseEntity<String> orderNotification(HttpServletRequest httpServletRequest){

        //Create json from request cause i will need raw json body to check payu signature
        String jsonBody = extractJson(httpServletRequest);
        //generate object from json
        PayuOrderNotificationRequest payuOrderNotificationRequest = generateObject(jsonBody);
        checkAuthenticationSignture(httpServletRequest, jsonBody);

        PointsPacketPaymentEntity pointsPacketPaymentEntity = this.pointsPacketPaymentRepository.findByTokenIgnoreCase(payuOrderNotificationRequest.getOrder().getExtOrderId());

        if(pointsPacketPaymentEntity == null){
            LOG.error("Points packet payment entity with token {} does not exist", payuOrderNotificationRequest.getOrder().getExtOrderId());
            this.payuService.reject(payuOrderNotificationRequest);
            return ResponseEntity.status(404).body("not exists");
        }

        if(pointsPacketPaymentEntity.isConsumed()){
            LOG.info("Points packet entity id {} is already consumed. Incoming status {}", pointsPacketPaymentEntity.getId(), payuOrderNotificationRequest.getOrder().getStatus());
            return ResponseEntity.ok("already consumed");
        }

        LOG.info("Payu Notify: extOrderId = {} status = {}", payuOrderNotificationRequest.getOrder().getExtOrderId(), payuOrderNotificationRequest.getOrder().getStatus());

        consumeNotification(payuOrderNotificationRequest, pointsPacketPaymentEntity);

        return ResponseEntity.ok("");
    }

    private void consumeNotification(PayuOrderNotificationRequest payuOrderNotificationRequest, PointsPacketPaymentEntity pointsPacketPaymentEntity) {
        switch (payuOrderNotificationRequest.getOrder().getStatus()){
            case "NEW":
            case "PENDING":
                break;
            case "WAITING_FOR_CONFIRMATION":
                //todo "WAITING_FOR_CONFIRMATION"
                break;
            case "COMPLETED":
                this.pointsService.executeSuccessPayment(pointsPacketPaymentEntity);

                break;
            case "CANCELED":
                //todo canceled, nothing?
                break;
            case "REJECTED":
                //todo "REJECTED"
                break;
            default:
                LOG.error("Unknown payment status: {}", payuOrderNotificationRequest.getOrder().getStatus());
                break;
        }
    }

    private void checkAuthenticationSignture(HttpServletRequest httpServletRequest, String jsonBody) {
        //extract signature params
        String signatureHeader = httpServletRequest.getHeader(this.OpenPayuSignatureHeaderName);
        if(signatureHeader == null || signatureHeader.isEmpty()){
            throw new ForbiddenException();
        }
        Map<String,String> signatureMap = generateSignatureMap(signatureHeader);

        //generate my local hash
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance(signatureMap.get("algorithm"));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            LOG.error("No such algorithm exception, algorithm value from request: {}",signatureMap.get("algorithm"));
            throw new BadArgumentsException("Bad algorithm");
        }
        String concatented = jsonBody + this.payuMd5key;
        md.update(concatented.getBytes());
        String myChecksum = DatatypeConverter.printHexBinary(md.digest());

        //compare local hash with signature value
        if(!signatureMap.get("signature").equalsIgnoreCase(myChecksum)){
            LOG.error("Bad signature local hash: {}, signature value: {}", myChecksum, signatureMap.get("signature"));
            throw new ForbiddenException();
        }else{
            LOG.info("Signature ok local hash: {}, signature value: {}", myChecksum, signatureMap.get("signature"));
        }
    }

    private PayuOrderNotificationRequest generateObject(String jsonBody) {
        try{
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(jsonBody, PayuOrderNotificationRequest.class);
        }catch (IOException e){
            LOG.error("Cannot create object from json body {}", jsonBody);
            e.printStackTrace();
            throw new BadArgumentsException("Cannot create object from json body");
        }
    }

    private String extractJson(HttpServletRequest httpServletRequest) {
        try{
            StringBuilder sb = new StringBuilder();
            httpServletRequest.getReader().lines().forEach(sb::append);
            return sb.toString();
        }catch (IOException e){
            LOG.error("Cannot extract json from request");
            throw new BadArgumentsException("Cannot extract json from request");
        }

    }

    /***
     * Convert payu signature key1=value1;k2=v2 to map key-value.
     * Example signature:
     * sender=checkout;signature=c33a38d89fb60f873c039fcec3a14743;algorithm=MD5;content=DOCUMENT
     * @param signatureHeader
     * @return map of key, value
     */
    private Map<String, String> generateSignatureMap(String signatureHeader) {
        String[] pairs = signatureHeader.split(";");
        Map<String,String> resultMap = new HashMap<>();
        for (String pair: pairs) {
            String[] temp = pair.split("=");
            if(temp.length!=2){
                LOG.error("Signature pair separated by ';' not contains exactly 1 '='. Signature {}, pair {}",signatureHeader,pair);
                throw new BadArgumentsException("Signature exception");
            }
            LOG.info("Payu signature key-value entry {} = {}",temp[0],temp[1]);
            resultMap.put(temp[0],temp[1]);
        }
        return resultMap;
    }
}
