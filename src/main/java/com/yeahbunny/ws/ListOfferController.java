package com.yeahbunny.ws;

import com.yeahbunny.dao.OfferRepository;
import com.yeahbunny.domain.OfferEntity;
import com.yeahbunny.ws.dto.generator.MarkerDtoGenerator;
import com.yeahbunny.ws.dto.generator.OfferDtoGenerator;
import com.yeahbunny.ws.dto.response.OfferResponseDto;
import com.yeahbunny.ws.dto.response.PageResponseDto;
import com.yeahbunny.ws.util.MarkerFilter;
import com.yeahbunny.ws.util.PageableHelper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * Klasa będzie przeniesiona do osobnego serwisu ktory bedzie korzystał z bazy grafowej do wyszukiwania ofert
 */
@RestController
@RequestMapping(value = "/offer")
public class ListOfferController {

    private static final Logger LOG = LoggerFactory.getLogger(ListOfferController.class);

    @Inject
    OfferRepository offerRepository;

    @Inject
    OfferDtoGenerator offerDtoGenerator;

    @Inject
    MarkerFilter markerFilter;

    @Inject
    MarkerDtoGenerator markerDtoGenerator;

    @Value("${app.back.host}")
    private String appHost;


    @Transactional(readOnly=true)
    @ApiOperation(value = "Getting sorted offer list",
            notes = "params page(required): page number indexed by 0, limit(optional, default 20): offers per page, " +
            "sortBy: 1 - ASC 0 - DESC(malejaco), sortByValue: popularity or price, if sortBy!=null => sortByValue != null")
    @RequestMapping(value = "/getList", method = RequestMethod.GET)
    private @ResponseBody
    PageResponseDto<OfferResponseDto> getOffersList(@ApiParam(name = "filter params", required = false, defaultValue = "", value = "other filter params") @RequestParam Map<String,String> params,
                                                    HttpServletRequest request){

        //jest to wszystko brzydkie ale nie refaktoruje
        // ponieważ jeżeli rpzeniesiemy sie rpzez grafy bvedzie to trzeba i tak napisać od nowa i zrobie to ladnie
        Integer page  = Integer.parseInt(params.remove("page"));
        Integer limit = Integer.parseInt(params.remove("limit"));
        String sortBy = params.remove("sortBy");
        String sortByValue = params.remove("sortByValue");
        Pageable pageRequest = PageableHelper.generatePageable(
                page,
                limit,
                sortBy,
                sortByValue);

        Specification spec = markerFilter.createSpecification(params);
        Page<OfferEntity> offerEntitiesPage = this.offerRepository.findAll(spec,pageRequest);

        //narazie sprawdzam cene tutaj bo nie wymyslilem jeszcze jak wlozyc ja do specyfikacji
        if(params.get("price")!=null){
            offerEntitiesPage.getContent().stream()
                    .filter(item ->{
                        return markerFilter.matchPrice(item.getPrice(),params.get("price"));
                    })
                    .collect(Collectors.toList());
        }

        return PageableHelper.generateResponseDto(offerDtoGenerator,offerEntitiesPage, appHost + request.getRequestURI(),page,limit,sortBy,sortByValue);
    }



}
