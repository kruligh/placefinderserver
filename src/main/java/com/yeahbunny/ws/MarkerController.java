package com.yeahbunny.ws;

import com.yeahbunny.dao.LazyOfferRepository;
import com.yeahbunny.dao.OfferRepository;
import com.yeahbunny.domain.*;
import com.yeahbunny.exception.BadArgumentsException;
import com.yeahbunny.ws.dto.response.MarkerResponseDto;
import com.yeahbunny.ws.dto.generator.MarkerDtoGenerator;
import com.yeahbunny.ws.util.AreaBoundary;
import com.yeahbunny.ws.util.MarkerFilter;
import com.yeahbunny.ws.util.MarkerPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.endsWith;
import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.startsWith;

/**
 * Klasa będzie przeniesiona do osobnego serwisu ktory bedzie korzystał z bazy grafowej do wyszukiwania ofert
 */
@RestController
@RequestMapping(value = "/marker")
public class MarkerController {
    private static final Logger LOG = LoggerFactory.getLogger(MarkerController.class);

    @Inject
    LazyOfferRepository offerRepository;

    @Inject
    MarkerFilter markerFilter;

    @Inject
    MarkerDtoGenerator markerDtoGenerator;

    @Transactional(readOnly=true)
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    private @ResponseBody List<MarkerResponseDto> getMarkers(@RequestParam Map<String,String> params){
        return getMarkersInRectangle(params);
    }

    private List<MarkerResponseDto> getMarkersInRectangle(Map<String, String> params) {
        Specification spec = markerFilter.createSpecification(params);
        List<LazyOfferEntity> offerEntities = this.offerRepository.findAll(spec);

        if(params.get("price")!=null){
            return  offerEntities.stream()
                    .filter(item ->{
                        return markerFilter.matchPrice(item.getPrice(),params.get("price"));
                    })
                    .map((offer)->{
                        return markerDtoGenerator.generate(offer);
                    })
                    .collect(Collectors.toList());
        }else{
            return  offerEntities.stream()
                    .map((offer)->{
                        return markerDtoGenerator.generate(offer);
                    })
                    .collect(Collectors.toList());
        }
    }

    @Transactional(readOnly=true)
    @RequestMapping(value = "/getCircle", method = RequestMethod.GET)
    private @ResponseBody List<MarkerResponseDto> getMarkersCircle(@RequestParam Map<String,String> params){

        double circleR = Double.parseDouble(params.remove("circleR"));
        double circleCenterLat =Double.parseDouble(params.remove("circleLat"));
        double circleCenterLng = Double.parseDouble(params.remove("circleLng"));

        List<MarkerResponseDto> markers = getMarkersInRectangle(params);
        markers = markerFilter.circle(markers,circleCenterLat,circleCenterLng, circleR);
        return markers;

    }

    @Transactional(readOnly=true)
    @RequestMapping(value = "/getNew", method = RequestMethod.GET)
    private @ResponseBody List<MarkerResponseDto> getNewMarkers(@RequestParam Map<String,String> params){
        long time1 = System.currentTimeMillis();

        Specification spec = markerFilter.createSpecificationNewMarkers(params);
        List<LazyOfferEntity> offerEntities = this.offerRepository.findAll(spec);
        LOG.info("Return {} new markers, time {} milis", offerEntities.size(), System.currentTimeMillis() - time1);
        return  offerEntities.stream()
                .map((offer)->{
                    return markerDtoGenerator.generate(offer);
                })
                .collect(Collectors.toList());
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<String> handleException(Exception ex){
        ex.printStackTrace();
        return ResponseEntity.status(400).body(ex.getMessage());
    }



}
