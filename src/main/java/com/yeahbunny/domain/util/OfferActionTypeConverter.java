package com.yeahbunny.domain.util;

import com.yeahbunny.domain.type.OfferActionType;

import javax.persistence.AttributeConverter;


public class OfferActionTypeConverter implements AttributeConverter<OfferActionType, Integer> {

    @Override
    public Integer convertToDatabaseColumn(OfferActionType propertyType) {
            if(propertyType == null){
                return null;
            }else{
                return propertyType.ordinal();
            }
    }

    @Override
    public OfferActionType convertToEntityAttribute(Integer integer) {
            if(integer == null){
                return null;
            }else{
                if(OfferActionType.values().length >= integer){
                    return null;
                }else{
                    return OfferActionType.values()[integer];
                }
            }
    }
}
