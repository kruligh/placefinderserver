package com.yeahbunny.domain;

import javax.persistence.*;

@Entity
@Table(name = "follow_offer",uniqueConstraints={@UniqueConstraint(columnNames = {"person_id" , "offer_id"})})

public class FollowOfferEntity extends AbstractEntity{

    @ManyToOne()
    @JoinColumn(name="person_id")
    private PersonEntity person;

    @ManyToOne
    @JoinColumn(name = "offer_id")
    private OfferEntity offer;


    public PersonEntity getPerson() {
        return person;
    }

    public void setPerson(PersonEntity person) {
        this.person = person;
    }

    public OfferEntity getOffer() {
        return offer;
    }

    public void setOffer(OfferEntity offer) {
        this.offer = offer;
    }
}
