package com.yeahbunny.domain;

import com.yeahbunny.domain.type.OfferActionType;
import com.yeahbunny.domain.util.Java8DateTimeJpaConverter;
import com.yeahbunny.domain.util.OfferActionTypeConverter;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Entity
@Table(name = "offer_history")
public class OfferHistoryEntity extends AbstractEntity{

    @ManyToOne
    @JoinColumn(name = "offer_id")
    private OfferEntity offer;

    @Column(name = "action_type")
    @Enumerated(EnumType.ORDINAL)
    private OfferActionType action;

    @Column(name = "event_date", nullable = false, columnDefinition = "timestamp")
    @Convert(converter = Java8DateTimeJpaConverter.class)
    private ZonedDateTime eventDate;

    @ManyToOne()
    @JoinColumn(name = "admin_id")
    private AdminEntity admin;

    @Column(name = "reason")
    private String reason;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "report_id")
    private ReportedOfferEntity report;

/*    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "offer_activation_payment_history_id")
    private OfferActivationPaymentHistoryEntity offerActivationPaymentHistoryEntity;*/

    public OfferEntity getOffer() {
        return offer;
    }

    public void setOffer(OfferEntity offer) {
        this.offer = offer;
    }

    public ZonedDateTime getEventDate() {
        return eventDate;
    }

    public void setEventDate(ZonedDateTime eventDate) {
        this.eventDate = eventDate;
    }

    public OfferActionType getAction() {
        return action;
    }

    public void setAction(OfferActionType action) {
        this.action = action;
    }

    public AdminEntity getAdmin() {
        return admin;
    }

    public void setAdmin(AdminEntity admin) {
        this.admin = admin;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public ReportedOfferEntity getReport() {
        return report;
    }

    public void setReport(ReportedOfferEntity report) {
        this.report = report;
    }
}
