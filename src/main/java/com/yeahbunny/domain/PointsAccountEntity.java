package com.yeahbunny.domain;

import javax.persistence.*;

@Entity
@Table(name = "person_points_account")
public class PointsAccountEntity extends AbstractEntity{

    @OneToOne()
    @JoinColumn(name="person_id", unique = true)
    private PersonEntity person;

    @Column(name = "points_amount")
    private int amount;

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public PersonEntity getPerson() {
        return person;
    }

    public void setPerson(PersonEntity person) {
        this.person = person;
    }

    public int addPoints(int i) {
        this.amount = this.amount +i;
        return this.amount;
    }
}
