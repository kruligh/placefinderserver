package com.yeahbunny.domain.type;

public enum CurrencyType {
    PLN,
    EUR,
    USD
}
