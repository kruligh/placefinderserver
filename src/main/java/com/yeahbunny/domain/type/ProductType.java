package com.yeahbunny.domain.type;

public enum ProductType {
    HIRE,
    SELL,
    VISIT,
    ADDITIONAL_PHOTO,
    ALIAS,
    EXTEND_HIRE,
    EXTEND_SELL,
    EXTEND_VISIT,


}
