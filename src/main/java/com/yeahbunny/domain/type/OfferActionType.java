package com.yeahbunny.domain.type;


public enum OfferActionType {
    ADDED,
    PAYMENT,
    ACCEPTED_BY_ADMIN,
    OUT_OF_DATE,//3
    UNACCEPTED_BY_ADMIN,
    BANNED_BY_REPORT,
     IGNORED_REPORT, //6
    DEACTIVATED_BY_USER,
    DELETED_BY_USER,
}
