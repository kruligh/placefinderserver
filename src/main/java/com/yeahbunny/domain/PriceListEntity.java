package com.yeahbunny.domain;

import com.yeahbunny.domain.type.ProductType;

import javax.persistence.*;

@Entity
@Table(name = "price_list")
public class PriceListEntity extends AbstractEntity{

    @Column(name = "country_short")
    private String countryCode;

    @Column(name = "product_type")
    @Enumerated(EnumType.ORDINAL)
    private ProductType productType;

    @Column(name = "points_amount")
    private int pointsAmount;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public int getPointsAmount() {
        return pointsAmount;
    }

    public void setPointsAmount(int pointsAmount) {
        this.pointsAmount = pointsAmount;
    }
}
