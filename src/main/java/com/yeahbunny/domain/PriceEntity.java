package com.yeahbunny.domain;

import com.yeahbunny.domain.type.CurrencyType;
import com.yeahbunny.domain.type.PriceType;

import javax.persistence.*;

@Entity
@Table(name = "price")
public class PriceEntity extends AbstractEntity{

    @Column(name = "amount")
    private long amount;

    @Column(name = "currency")
    @Enumerated(EnumType.ORDINAL)
    private CurrencyType currency;

    @Column(name = "type")
    @Enumerated(EnumType.ORDINAL)
    private PriceType priceType;

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public CurrencyType getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyType currency) {
        this.currency = currency;
    }

    public PriceType getPriceType() {
        return priceType;
    }

    public void setPriceType(PriceType priceType) {
        this.priceType = priceType;
    }
}
