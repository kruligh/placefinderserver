package com.yeahbunny.domain;

import org.hibernate.annotations.Cache;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "points_packet")
public class PointsPacketEntity extends AbstractEntity{

    @Column(name = "packet_name")
    private String name;

    @Column(name = "points_count")
    private int pointsCount;

    @Column(name = "price_pln")
    private int pricePln;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPointsCount() {
        return pointsCount;
    }

    public void setPointsCount(int pointsCount) {
        this.pointsCount = pointsCount;
    }

    public int getPricePln() {
        return pricePln;
    }

    public void setPricePln(int pricePln) {
        this.pricePln = pricePln;
    }
}
