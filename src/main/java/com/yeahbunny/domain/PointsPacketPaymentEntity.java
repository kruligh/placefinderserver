package com.yeahbunny.domain;

import com.yeahbunny.domain.type.CurrencyType;
import com.yeahbunny.domain.util.Java8DateTimeJpaConverter;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Entity
@Table(name = "points_packet_payment")
public class PointsPacketPaymentEntity extends AbstractEntity{

    @ManyToOne()
    @JoinColumn(name="person_id")
    private PersonEntity person;

    @Column(name = "token")
    private String token;

    @Column(name = "add_date", nullable = false, columnDefinition = "timestamp")
    @Convert(converter = Java8DateTimeJpaConverter.class)
    private ZonedDateTime addDate;

    @ManyToOne()
    @JoinColumn(name = "points_packet_id")
    private PointsPacketEntity pointsPacket;

    @Column(name = "currency")
    @Enumerated(EnumType.STRING)
    private CurrencyType currency;

    @Column(name = "consumed")
    private boolean consumed;

    public PersonEntity getPerson() {
        return person;
    }

    public void setPerson(PersonEntity person) {
        this.person = person;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public ZonedDateTime getAddDate() {
        return addDate;
    }

    public void setAddDate(ZonedDateTime addDate) {
        this.addDate = addDate;
    }

    public PointsPacketEntity getPointsPacket() {
        return pointsPacket;
    }

    public void setPointsPacket(PointsPacketEntity pointsPacket) {
        this.pointsPacket = pointsPacket;
    }

    public CurrencyType getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyType currency) {
        this.currency = currency;
    }

    public boolean isConsumed() {
        return consumed;
    }

    public void setConsumed(boolean consumed) {
        this.consumed = consumed;
    }
}
