package com.yeahbunny.dao;

import com.yeahbunny.domain.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.LockModeType;
import java.util.List;

public interface PersonRepository extends JpaRepository<PersonEntity, Long> {
    PersonEntity findByEmail(String email);

    List<PersonEntity> findByEmailContaining(String email);
}
