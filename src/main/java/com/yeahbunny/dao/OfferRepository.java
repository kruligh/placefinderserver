package com.yeahbunny.dao;

import com.yeahbunny.domain.OfferEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface OfferRepository extends JpaRepository<OfferEntity, Long>, JpaSpecificationExecutor {

    List<OfferEntity> findAllByActiveTrueAndFakeLatitudeBetweenAndFakeLongitudeBetween(double x1, double x2, double y1, double y2);

    OfferEntity findByAlias(String id);

    List<OfferEntity> findAllByConsumedByAdminFalseAndActiveTrue(Pageable pageRequest);

    List<OfferEntity> findAllByActiveTrue();

    List<OfferEntity> findAllByPersonIdAndIsDeletedFalse(Long personId);

    long countByActiveTrue();

    List<OfferEntity> findAllByActiveTrue(Specification spec);

    Page<OfferEntity> findAllByPersonIdAndIsDeletedFalse(Long personId, Pageable pageRequest);
}

