package com.yeahbunny.dao;

import com.yeahbunny.domain.FollowOfferEntity;
import com.yeahbunny.domain.OfferEntity;
import com.yeahbunny.domain.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FollowOfferRepository extends JpaRepository<FollowOfferEntity, Long> {
    FollowOfferEntity findByPersonAndOffer(PersonEntity person, OfferEntity offer);

    List<FollowOfferEntity> findAllByPerson(PersonEntity person);
}
