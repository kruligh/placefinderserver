package com.yeahbunny.dao;

import com.yeahbunny.domain.SendMailRequestEntity;
import org.springframework.data.jpa.repository.JpaRepository;


public interface SendMailRequestRepository extends JpaRepository<SendMailRequestEntity, Long> {
}
