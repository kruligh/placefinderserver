package com.yeahbunny.dao;


import com.yeahbunny.domain.OfferHistoryEntity;
import com.yeahbunny.domain.type.OfferActionType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface OfferHistoryRepository extends JpaRepository<OfferHistoryEntity, Long> {
    List<OfferHistoryEntity> findAllByOfferIdAndAction(Long id, OfferActionType action);

    @Query("SELECT history from OfferHistoryEntity history where history.offer.id = :offer_id and ( history.action = :action1 or history.action = :action2) order by history.eventDate desc")
    List<OfferHistoryEntity> findAllByOfferIdAndActions(@Param("offer_id")Long offerId, @Param("action1")OfferActionType action1, @Param("action2")OfferActionType action2);

    List<OfferHistoryEntity> findAllByOfferIdOrderByEventDateDesc(Long id);

    List<OfferHistoryEntity> findAllByOfferIdOrderByEventDateAsc(Long id);
}
