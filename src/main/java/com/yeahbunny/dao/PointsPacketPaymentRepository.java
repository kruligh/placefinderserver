package com.yeahbunny.dao;

import com.yeahbunny.domain.PointsPacketPaymentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PointsPacketPaymentRepository extends JpaRepository<PointsPacketPaymentEntity, Long>{
    PointsPacketPaymentEntity findByTokenIgnoreCase(String token);
}
