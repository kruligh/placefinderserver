package com.yeahbunny.dao;

import com.yeahbunny.domain.LazyOfferEntity;
import com.yeahbunny.domain.OfferEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface LazyOfferRepository extends JpaRepository<LazyOfferEntity, Long>, JpaSpecificationExecutor {

    }

