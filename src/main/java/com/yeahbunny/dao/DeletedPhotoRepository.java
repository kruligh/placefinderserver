package com.yeahbunny.dao;


import com.yeahbunny.domain.DeletedPhotoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeletedPhotoRepository extends JpaRepository<DeletedPhotoEntity, Long>{
}
