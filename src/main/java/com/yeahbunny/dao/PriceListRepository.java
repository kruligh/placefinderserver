package com.yeahbunny.dao;

import com.yeahbunny.domain.PriceListEntity;
import com.yeahbunny.domain.type.ProductType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PriceListRepository extends JpaRepository<PriceListEntity, Long> {
    PriceListEntity findByCountryCodeAndProductType(String countryCode, ProductType productType);

    PriceListEntity findByCountryCodeIsNullAndProductType(ProductType productType);

    @Query("select distinct en.countryCode from PriceListEntity en where en.countryCode is not null")
    List<String> findAllCountryCodeDistinct();

    List<PriceListEntity> findAllByCountryCode(String countryCode);
}
