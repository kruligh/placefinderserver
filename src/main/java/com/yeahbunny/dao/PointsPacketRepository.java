package com.yeahbunny.dao;

import com.yeahbunny.domain.PointsPacketEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PointsPacketRepository extends JpaRepository<PointsPacketEntity, Long>{
}
