package com.yeahbunny.dao;

import com.yeahbunny.domain.PersonEntity;
import com.yeahbunny.domain.PointsAccountEntity;
import com.yeahbunny.domain.PointsPacketEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;

import javax.persistence.LockModeType;

public interface PointsAccountRepository extends JpaRepository<com.yeahbunny.domain.PointsAccountEntity,Long> {

    /***
     * Lock for update
     * @param person
     * @return
     */
    @Lock(value = LockModeType.PESSIMISTIC_WRITE)
    PointsAccountEntity findByPerson(PersonEntity person);

    PointsAccountEntity findByPersonId(Long personId);
}
